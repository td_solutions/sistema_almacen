﻿/*====================================== VARIABLES =====================================*/
var oTable;
var $InsumoID = '';
var $AlmacenID = '';

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});


/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Inventario');
    eventos();
    cargar_insumos();
    cargar_almacen();
    //Crear_Tabla();    
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cns').click(function (e) {
        e.preventDefault();
        if (oTable != undefined) 
            oTable.destroy();
         
        Crear_Tabla();
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });
}

function cargar_insumos() {
    jQuery('#cmb-insumos').select2({
        ajax: {
            url: 'Insumos/GetInsumosList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $InsumoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}


function cargar_almacen() {
    jQuery('#cmb-almacen').select2({
        ajax: {
            url: 'Almacen/GetAlmacenList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $AlmacenID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}

/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    var optionSelected = $("input[type=radio][name=tipo_reporte]:checked").val();
    if (optionSelected == 'reorden')    
    {
        $('#Tbl_Registros').empty();
        oTable = $('#Tbl_Registros').DataTable({
            destroy: true,
            "ajax": {
                "type": "POST",
                "url": "RptInventario/Consulta_Reporte_Reorden",
                "data": function (d) {
                    d.Almacen_ID = $('#cmb-almacen :selected').val();
                    d.Insumo_ID = $('#cmb-insumos :selected').val();
                }
            },
            columns: [
                { title: "Codigo de barras", className: "text-center" },
                { title: "Descripcion", className: "text-center" },
                { title: "Unidad", className: "text-center" },
                { title: "Existencias", className: "text-center" },
                { title: "Sugeruido Compra", className: "text-center" },
                { title: "Precio Unitario", className: "text-center" },
                { title: "Importe", className: "text-center" },
            ],
            columnDefs: [
                { "width": "10%", "targets": 0 },
                { "width": "40%", "targets": 1 },
                { "width": "20%", "targets": 2 },
                { "width": "10%", "targets": 4 },
                { "width": "10%", "targets": 5 },
                { "width": "10%", "targets": 6 },
            ],
            rowCallback: function (row, data) {
                //if it is not the summation row the row should be selectable                      
            }
        });
    }
    else
    {
        $('#Tbl_Registros').empty();
        oTable = $('#Tbl_Registros').DataTable({
            destroy: true,
            "ajax": {
                "type": "POST",
                "url": "RptInventario/Consulta_Reporte",
                "data": function (d) {
                    d.Almacen_ID = $('#cmb-almacen :selected').val();
                    d.Insumo_ID = $('#cmb-insumos :selected').val();
                }
            },
            columns: [
                { title: "Codigo de barras", className: "text-center" },
                { title: "Descripcion", className: "text-center" },
                { title: "Unidad", className: "text-center" },
                { title: "Existencias", className: "text-center" }
            ],
            columnDefs: [
                { "width": "20%", "targets": 0 },
                { "width": "50%", "targets": 1 },
                { "width": "30%", "targets": 2 },
                {
                    render: function (data, type, row) {

                    },
                    //targets: 4
                }
            ],
            rowCallback: function (row, data) {
                //if it is not the summation row the row should be selectable                      
            }
        });
    }
};

function exportar_excel() {
    var optionSelected = $("input[type=radio][name=tipo_reporte]:checked").val();
    if ($('#txt-fecha_inicio').val() != '' && $('#txt-fecha_inicio').val() != undefined && $('#txt-fecha_inicio').val() != null)
        $('#txt-fecha_inicio').val($('#txt-fecha_inicio').val() + " 00:00:00");
    if ($('#txt-fecha_fin').val() != '' && $('#txt-fecha_fin').val() != undefined && $('#txt-fecha_fin').val() != null)
        $('#txt-fecha_fin').val($('#txt-fecha_fin').val() + " 23:59:59");
    if (optionSelected == 'por_linea')
        window.location = "RptInventario/Download_Reporte_Linea?Almacen_ID=" + $('#cmb-almacen :selected').val() + "&Almacen=" + $('#cmb-almacen :selected').text() + "&Insumo_ID=" + $('#cmb-insumos :selected').val();
    if (optionSelected == 'reorden')
        window.location = "RptInventario/Download_Reporte_Reorden?Almacen_ID=" + $('#cmb-almacen :selected').val() + "&Almacen=" + $('#cmb-almacen :selected').text() + "&Insumo_ID=" + $('#cmb-insumos :selected').val();
    if (optionSelected == 'fisico')
        window.location = "RptInventario/Download_Reporte_Fisico?Almacen_ID=" + $('#cmb-almacen :selected').val() + "&Almacen=" + $('#cmb-almacen :selected').text() + "&Insumo_ID=" + $('#cmb-insumos :selected').val();
    if (optionSelected == 'verificacion')
        window.location = "RptInventario/Download_Reporte_Verificacion?Almacen_ID=" + $('#cmb-almacen :selected').val() + "&Almacen=" + $('#cmb-almacen :selected').text() + "&Insumo_ID=" + $('#cmb-insumos :selected').val();
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if (($('#txt-fecha_inicio').val() == '' || $('#txt-fecha_inicio').val() == undefined || $('#txt-fecha_inicio').val() == null)
            && ($('#txt-fecha_fin').val() == '' || $('#txt-fecha_fin').val() == undefined || $('#txt-fecha_fin').val() == null)
            && ($('#txt-no_empleado').val() == '' || $('#txt-no_empleado').val() == undefined || $('#txt-no_empleado').val() == null)) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> SELECCIONE ALGUN FILTRO</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
