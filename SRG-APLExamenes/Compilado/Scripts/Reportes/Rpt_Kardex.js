﻿/*====================================== VARIABLES =====================================*/
var oTable;
var $InsumoID = '';

$.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Today",
    clear: "Clear",
    format: "dd/mm/yyyy",
    titleFormat: "dd/mm/yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
};
$("#txt-fecha-inicio").datepicker({
    format: 'dd/mm/yyyy',
    language: 'es',
    autoclose: true
});
$("#txt-fecha-fin").datepicker({
    format: 'dd/mm/yyyy',
    language: 'es',
    autoclose: true
});
$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});


/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Kardex');
    eventos();
    cargar_insumos();   
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cns').click(function (e) {
        e.preventDefault();
        Crear_Tabla();
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });

}

function cargar_insumos() {
    jQuery('#cmb-insumos').select2({
        ajax: {
            url: '../Insumos/GetInsumosList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $InsumoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}

/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": {
            "type": "POST",
            "url": "GetReporteKardex",
            "data": function (d) {
                d.Insumo_ID = $('#cmb-insumos :selected').val();
                d.Fecha_Inicio = $('#txt-fecha-inicio').val();
                d.Fecha_Fin = $('#txt-fecha-fin').val();
            }
        },
        columns: [
            { title: "Fecha", className: "text-center" },
            { title: "Num Movimiento", className: "text-center" },
            { title: "Movimiento", className: "text-center" },
            { title: "Tipo Movimiento", className: "text-center" },
            { title: "Unidades", className: "text-center" },
            { title: "Precio Venta", className: "text-center" },
            { title: "Precio Compra", className: "text-center" },
            { title: "Total", className: "text-right" }
        ],
        columnDefs: [
            { "width": "10%", "targets": 0 },
            { "width": "5%", "targets": 1 },
            { "width": "20%", "targets": 2 },
            { "width": "20%", "targets": 3 },
            { "width": "5%", "targets": 4 },
            { "width": "10%", "targets": 5 },
            { "width": "10%", "targets": 6 },
            { "width": "10%", "targets": 7 }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function exportar_excel() {
    Insumo_ID = $('#cmb-insumos :selected').val();
    Fecha_Inicio = $('#txt-fecha-inicio').val();
    Fecha_Fin = $('#txt-fecha-fin').val();
    window.location = "Download_Reporte_Kardex?Insumo_ID=" + Insumo_ID + "&Fecha_Inicio=" + Fecha_Inicio + "&Fecha_Fin=" + Fecha_Fin;
        //window.location = "Download_Reporte_Kardex";
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if (($('#txt-fecha_inicio').val() == '' || $('#txt-fecha_inicio').val() == undefined || $('#txt-fecha_inicio').val() == null)
            && ($('#txt-fecha_fin').val() == '' || $('#txt-fecha_fin').val() == undefined || $('#txt-fecha_fin').val() == null)
            && ($('#cmb-insumos :selected').val() == '' || $('#cmb-insumos :selected').val() == undefined || $('#cmb-insumos :selected').val() == null)) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> SELECCIONE FECHAS E INSUMO</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

function asignar_fecha() {
    var f = new Date();
    $("#txt-fecha-inicio").datepicker("setDate", f);
    $("#txt-fecha-fin").datepicker("setDate", f);
}