﻿/*====================================== VARIABLES =====================================*/
var $ParametroID = '';
var $EmpleadoID = '';
var $RanchoID = '';

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Parametros');
    eventos();
    cargar_parametros();
    cargar_ranchos();
    //cargar_empleados();    
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#cmb-mpl').on("select2:select", function (evt) {
        $EmpleadoID = evt.params.data.id;
    });
    $("#cmb-mpl").on("select2:unselecting instead", function (e) {
        $EmpleadoID = '';
    });
        
    $('#btn_guardar').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();
        } else {
            $('#btn_guardar').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
                placement: 'bottomLeft',
                showCloseBut: true,
                onDocumentClickClose: true,
                onDocumentClickClosePrevent: '',
                overflowContent: false,
                inline: true,
                asMenu: false,
                size: '',
                onOkBut: function (event, el) { },
                onCancelBut: function (event, el) { },
                onLoad: function (el) { },
                onClose: function (el) { }
            });
        }
    });

    /*********Combos**********/
    $('#cmb-rancho').on("select2:select", function (evt) {
        $RanchoID = evt.params.data.id;        
    });
    $("#cmb-rancho").on("select2:unselecting instead", function (e) {
        $RanchoID = '';
    });
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Obj_Capturado.Parametro_ID = $ParametroID;        
        Obj_Capturado.Proveedor_Servicio = $('#txt-prv-crr').val();
        Obj_Capturado.Puerto = $('#txt-prt').val();
        Obj_Capturado.SSL_Cifrado = $('#chk-ssl-cfr').prop('checked') ? "1" : "0"
        Obj_Capturado.Servidor_Correo = $('#txt-srv').val();
        if ($('#txt-cnt').val() == '' || $('#txt-cnt').val() == undefined || $('#txt-cnt').val() == null)
            Obj_Capturado.Contraseña = "-"
        else
            Obj_Capturado.Contraseña = $('#txt-cnt').val();
        Obj_Capturado.Rancho_ID = $('#cmb-rancho :selected').val();
        Obj_Capturado.Precio_Litro_Fumigacion = $('#txt-pre_lit_fum').val();
        if ($('#txt-comentarios').val() == '' || $('#txt-comentarios').val() == undefined || $('#txt-comentarios').val() == null)
            Obj_Capturado.Comentarios = "-";
        else
            Obj_Capturado.Comentarios = $('#txt-comentarios').val();
        Obj_Capturado.Captura = $ParametroID == "" ? 'I' : 'U';
        $.ajax({
            url: 'Parametros/OperationMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_Capturado),
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
    }
}
/// <summary>
/// FUNCION PARA CARGAR LOS REGISTROS
/// </summary>
function cargar_parametros() {
    try {
        $.ajax({
            url: 'Parametros/Consultar',
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {                
                if (Resultado.items.Data != undefined && Resultado.items.Data != null) {
                    row = JSON.parse(Resultado.items.Data);
                    if (row != null) {
                        if (row.length > 0) {
                            $ParametroID = row[0].Parametro_ID;
                            $ObraID = row[0].Obra_ID;
                            $('#txt-prv-crr').val(row[0].Proveedor_Servicio);
                            $('#txt-prt').val(row[0].Puerto);
                            if (row[0].SSL_Cifrado == 1) {
                                jQuery('#chk-ssl-cfr').prop("checked", true);
                            } else {
                                jQuery('#chk-ssl-cfr').prop("checked", false);
                            }
                            $('#txt-srv').val(row[0].Servidor_Correo);
                            if (row[0].Contraseña == "-")
                                $('#txt-cnt').val("");
                            else
                                $('#txt-cnt').val(row[0].Contraseña);
                            if (row[0].Rancho_ID != "") {
                                $("#cmb-rancho").select2({
                                    theme: "classic",
                                    data: [{ id: parseInt(row[0].Rancho_ID), text: row[0].Rancho }]
                                })
                                cargar_ranchos();
                            }
                            $('#txt-pre_lit_fum').val(row[0].Precio_Litro_Fumigacion);
                            if (row[0].Comentarios == "-")
                                $('#txt-comentarios').val("");
                            else
                                $('#txt-comentarios').val(row[0].Comentarios);                                                        
                        }
                    }
                }
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {

        if ($('#txt-prv-crr').val() == '' || $('#txt-prv-crr').val() == undefined || $('#txt-prv-crr').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> PROVEEDOR DE CORREO</strong>.</span><br />';
        }
        if ($('#txt-prt').val() == '' || $('#txt-prt').val() == undefined || $('#txt-prt').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> PUERTO</strong>.</span><br />';
        }
        if ($('#txt-srv').val() == '' || $('#txt-srv').val() == undefined || $('#txt-srv').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> SERVIDOR DE CORREO</strong>.</span><br />';
        }
        if ($('#cmb-rancho :selected').val() == '' || $('#cmb-rancho :selected').val() == undefined || $('#cmb-rancho :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> RANCHO</strong>.</span><br />';
        }
        if ($('#txt-pre_lit_fum').val() == '' || $('#txt-pre_lit_fum').val() == undefined || $('#txt-pre_lit_fum').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> PRECIO LITRO FUMIGACION</strong>.</span><br />';
        }
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
    } finally {
        return output;
    }
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

/// <summary>
/// FUNCION PARA CARGAR LOS REGISTROS
/// </summary>
function cargar_ranchos() {
    jQuery('#cmb-rancho').select2({
        ajax: {
            url: 'Ranchos/GetRanchosList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $RanchoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function formatRepoSelection(repo) {
    return repo.text;
}
