﻿/*====================================== VARIABLES =====================================*/
var $ProveedorID = '';
var oTable;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Estado Cuenta Proveedores');    
    eventos();
    cargar_proveedores();
    //Crear_Tabla();    
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cns').click(function (e) {
        e.preventDefault();
        $('#txt-total').val("0.00");
        $('#txt-abono').val("0.00");
        $('#txt-saldo').val("0.00");
        Crear_Tabla();
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });

    $('#btn-porpagar').click(function (e) {
        e.preventDefault();
        asignarClaseButon('btn-porpagar')
    });
    
    $('#btn-abonada').click(function (e) {
        e.preventDefault();
        asignarClaseButon('btn-abonada')
    });

    $('#btn-pagada').click(function (e) {
        e.preventDefault();
        asignarClaseButon('btn-pagada')
    });

    $('#btn-cancelada').click(function (e) {
        e.preventDefault();
        asignarClaseButon('btn-cancelada')
    });

    $("#txt-fecha_inicio").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_fin").datepicker({
        format: 'dd/mm/yyyy'
    });

    /*********Combos**********/
    $('#cmb-proveedor').on("select2:select", function (evt) {
        $ProveedorID = evt.params.data.id;
    });
    $("#cmb-proveedor").on("select2:unselecting instead", function (e) {
        $ProveedorID = '';
    });
}


/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    var Estatus = "";    
    if (document.getElementById('btn-porpagar').className == "btn btn_rojo") {
        Estatus += "'POR PAGAR'";
    }
    if (document.getElementById('btn-abonada').className == "btn btn_rojo") {
        if (Estatus != "")
            Estatus += ",";
        Estatus += "'ABONADA'";
    }
    if (document.getElementById('btn-pagada').className == "btn btn_rojo") {
        if (Estatus != "")
            Estatus += ",";
        Estatus += "'PAGADA'";
    }
    if (document.getElementById('btn-cancelada').className == "btn btn_rojo") {
        if (Estatus != "")
            Estatus += ",";
        Estatus += "'CANCELADA'";
    }

    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,        
        "ajax": {
            "type": "POST",
            "url": "GetReporteEstadoCuentaProveedores  ",
            "data": function (d) {
                d.NO_FACTURA = $('#txt-no_factura').val();
                d.PROVEEDOR_ID = $ProveedorID;
                d.ESTATUS = Estatus;
                d.Fecha_Inicio = $('#txt-fecha_inicio').val();                
                d.Fecha_Fin = $('#txt-fecha_fin').val();
            }
        },
        columns: [            
            { title: "No Factura", className: "text-center" },            
            { title: "Proveedor", className: "text-left" },
            { title: "Fecha", className: "text-center" },
            { title: "F Vencimiento", className: "text-center" },
            { title: "Total", className: "text-rigth" },
            { title: "Abono", className: "text-rigth" },
            { title: "Saldo", className: "text-rigth" },
            { title: "Estatus", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {

               },
               //targets: 4
           }
        ],
        rowCallback: function (row, data) {
            calcular_totales();
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function exportar_excel() {
    var Estatus = "";
    if (document.getElementById('btn-porpagar').className == "btn btn_rojo") {
        Estatus += "'POR PAGAR'";
    }
    if (document.getElementById('btn-abonada').className == "btn btn_rojo") {
        if (Estatus != "")
            Estatus += ",";
        Estatus += "'ABONADA'";
    }
    if (document.getElementById('btn-pagada').className == "btn btn_rojo") {
        if (Estatus != "")
            Estatus += ",";
        Estatus += "'PAGADA'";
    }
    if (document.getElementById('btn-cancelada').className == "btn btn_rojo") {
        if (Estatus != "")
            Estatus += ",";
        Estatus += "'CANCELADA'";
    }

    window.location = "Download_Reporte_Estado_Cuenta_Proveedores?NO_FACTURA=" + $('#txt-no_factura').val() + "&PROVEEDOR_ID=" + $ProveedorID + "&ESTATUS=" + Estatus + "&Fecha_Inicio=" + $('#txt-fecha_inicio').val() + "&Fecha_Fin=" + $('#txt-fecha_fin').val();
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

function cargar_proveedores() {
    jQuery('#cmb-proveedor').select2({
        ajax: {
            url: '../Proveedores/GetProveedoresList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $ProveedorID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function formatRepoSelection(repo) {
    return repo.text;
}

function asignarClaseButon(Buton_ID)
{
    var Control_ = document.getElementById(Buton_ID);
    if (Control_.className == "btn btn_rojo") {
        $('#' + Buton_ID + '').removeClass('btn btn_rojo');
        $('#' + Buton_ID + '').addClass('btn btn_verde');
    }
    else {
        $('#' + Buton_ID + '').removeClass('btn btn_verde');
        $('#' + Buton_ID + '').addClass('btn btn_rojo');
    }
}

function calcular_totales() {
    var tot_total = 0;
    var tot_abono = 0;
    var tot_saldo = 0;

    oTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        tot_total += parseFloat(data[4]);
        tot_abono += parseFloat(data[5]);
        tot_saldo += parseFloat(data[6]);
    });

    $('#txt-total').val(tot_total.toFixed(2));
    $('#txt-abono').val(tot_abono.toFixed(2));
    $('#txt-saldo').val(tot_saldo.toFixed(2));
}

