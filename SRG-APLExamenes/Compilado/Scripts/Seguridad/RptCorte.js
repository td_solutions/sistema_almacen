﻿/*====================================== VARIABLES =====================================*/
var oTable;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Destajo');
    eventos();    
    
    $('#Tbl_Registros').on('click', 'tr', function (event) {
        var data = oTable.row(this).data();
        Cargar_Informacion(data[0]);
    });
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cns').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            Crear_Tabla();
        } else{ 
            mostrar_mensaje("", output.Mensaje);
        }           
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });
    
    $('#btn-modal-close').click(function (e) {
        e.preventDefault();
        $('#myModal').hide();
    });

    $("#txt-fecha_inicio").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_fin").datepicker({
        format: 'dd/mm/yyyy'
    });
}


/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,        
        "ajax": {
            "type": "POST",
            "url": "GetReporteCorte  ",
            "data": function (d) {
                d.No_Tarjeta = $('#txt-no_empleado').val();
                if ($('#txt-fecha_inicio').val() != '' && $('#txt-fecha_inicio').val() != undefined && $('#txt-fecha_inicio').val() != null) {
                    d.Fecha_Inicio = $('#txt-fecha_inicio').val();
                    d.Fecha_Fin = $('#txt-fecha_fin').val();
                }
                //if ($('#txt-fecha_fin').val() != '' && $('#txt-fecha_fin').val() != undefined && $('#txt-fecha_fin').val() != null)
                //    d.Fecha_Fin = $('#txt-fecha_fin').val();
            }
        },
        columns: [
            { title: "ID", className: "text-center", visible: false },
            { title: "Fecha", className: "text-center" },                        
            { title: "Tabla", className: "text-center" },
            { title: "Tipo Destajo", className: "text-center" },
            { title: "Cantidad Empleados", className: "text-center" },
            { title: "Cantidad", className: "text-center" },            
        ],
        columnDefs: [
           {
               render: function (data, type, row) {

               },
               //targets: 4
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable 
            calcular_totales();
        }
    });
};

function exportar_excel() {    
    window.location = "Download_Reporte_Corte?No_Tarjeta=" + $('#txt-no_empleado').val() + "&Fecha_Inicio=" + $('#txt-fecha_inicio').val() + "&Fecha_Fin=" + $('#txt-fecha_inicio').val(); //$('#txt-fecha_fin').val();
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {        
        if (($('#txt-fecha_inicio').val() == '' || $('#txt-fecha_inicio').val() == undefined || $('#txt-fecha_inicio').val() == null))
            //&& ($('#txt-fecha_fin').val() == '' || $('#txt-fecha_fin').val() == undefined || $('#txt-fecha_fin').val() == null)
            //&& ($('#txt-no_empleado').val() == '' || $('#txt-no_empleado').val() == undefined || $('#txt-no_empleado').val() == null)) 
        {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> SELECCIONE ALGUN FILTRO</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

function calcular_totales() {
    var tot_empleados = 0;
    var tot_cantidad = 0;

    oTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        tot_empleados += parseFloat(data[4]);
        tot_cantidad += parseFloat(data[5]);
    });

    $('#txt-total_empleados').val(tot_empleados.toFixed(2));
    $('#txt-total_cantidad').val(tot_cantidad.toFixed(2));
}

/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    var res;
    try {
        $.ajax({
            url: '../Destajo/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                res = row[0].Fecha.split("T");
                $('#txt-fecha').val(res[0]);
                $('#txt-Tabla').val(row[0].Tabla);
                $('#txt-tipo_destajo').val(row[0].Tipo_Destajo);
                $('#txt-cantidad_empleados').val(row[0].Cantidad_Empleados);
                $('#txt-cantidad').val(row[0].Cantidad);
                
                $('#myModal').show();
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}



