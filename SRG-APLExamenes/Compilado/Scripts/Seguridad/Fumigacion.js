﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var $EmpleadoID = '';
var $SalidaID = '';
var oTable;
var oTableDet;
var oTableSal;
var $Lista_Partidas = [];

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Fumigacion');
    eventos();
    cargar_tabla();
    Obtener_Hectareas();
    cargar_empelados();
    cargar_salidas();
    $('#btn-new').click();
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $("#txt-fecha").datepicker({
        format: 'dd/mm/yyyy'
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');
        $('#btn-new').click();
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
        asignar_fecha();
        $('#txt-litros').focus();
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();               
        } else {
            $('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });
        }
    });

    $('#btn-add-emp').click(function (e) {
        e.preventDefault();
        var output = validar_datos_detalles();
        if (output.Estatus) {
            agregar_empleado();
        } else {           
            $('#btn-add-emp').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });
        }        
    });

    /*********Combos**********/
    $('#cmb-emp').on("select2:select", function (evt) {
        $EmpleadoID = evt.params.data.id;
        $('#txt-no_emp').val(parseInt($EmpleadoID));
        agregar_empleado();
    });
    $("#cmb-emp").on("select2:unselecting instead", function (e) {
        $EmpleadoID = '';
        $('#txt-no_emp').val('');
    });
       
    $('#cmb-sal').on("select2:select", function (evt) {
        $SalidaID = evt.params.data.id;
        $Lista_Partidas = [];
        cargar_salidas_detalles();
    });
    $("#cmb-sal").on("select2:unselecting instead", function (e) {
        $SalidaID = '';
        $Lista_Partidas = [];
        $("#Tbl_Registros_Salidas").empty();
    });

    /*********Cajas Texto**********/
    $("#txt-no_emp").blur(function () {
        $EmpleadoID = $("#txt-no_emp").val();
        cargar_datos_empleado($EmpleadoID);
    });

    $("#Tbl_Registros_Emp").on('click', 'button.del', function () {
        var $tr = $(this).closest('tr');

        // Le pedimos al DataTable que borre la fila
        oTableDet.row($tr).remove().draw(false);
    });
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    var Obj_Capturado_Det = new Object();
    var Array_Capturado_Det = new Array();
    var Obj_Capturado_Tab = new Object();
    var Array_Capturado_Tab = new Array();
    var tablas;
    var Contador = 0;

    try {
        Abrir_Ventana_Espera();

        Obj_Capturado.No_Fumigacion = $EventoID;
        Obj_Capturado.Fecha = $('#txt-fecha').val();
        Obj_Capturado.Litros = $('#txt-litros').val();
        Obj_Capturado.Pago_Por = $('#cmb-pago_por :selected').val();
        Obj_Capturado.Total_Empleados = oTableDet.data().count();
        Obj_Capturado.No_Salida = $('#cmb-sal :selected').val();
        Obj_Capturado.Estatus = "ACTIVO";
        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';

        oTableDet.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            Obj_Capturado_Det = new Object();            
            Obj_Capturado_Det.No_Empleado = data.No_Empleado;
            Obj_Capturado_Det.No_Tarjeta = data.No_Tarjeta;
            Obj_Capturado_Det.Nombre = data.Nombre;
            Obj_Capturado_Det.Cantidad = data.Cantidad;
            Obj_Capturado_Det.Captura = 'I';
            Array_Capturado_Det[Contador] = Obj_Capturado_Det;
            Contador++;
        });
        Contador = 0;

        tablas = $('#hf-tabla_id').val().split(",");
        for (var i = 0; i < tablas.length; i++) {
            if (tablas[i] != '' && tablas[i] != null && tablas[i] != 'undefined') {
                Obj_Capturado_Tab = new Object();
                Obj_Capturado_Tab.Tabla_ID = tablas[i];
                Obj_Capturado_Tab.No_Tuneles = $('#btn-Tabla_' + tablas[i]).attr("title");
                Obj_Capturado_Tab.Captura = 'I';
                Array_Capturado_Tab[i] = Obj_Capturado_Tab;
            }
        }

        $.ajax({
            url: 'Fumigacion/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: "{'Datos':'" + JSON.stringify(Obj_Capturado) + "', 'Detalles':'" + JSON.stringify(Array_Capturado_Det) + "', 'Tablas':'" + JSON.stringify(Array_Capturado_Tab) + "'}",
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                    $('#btn-new').click();
                    cargar_salidas();
                    Cerrar_Ventana_Espera();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                    Cerrar_Ventana_Espera();
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
        Cerrar_Ventana_Espera();
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Fumigacion/GetEvents",
        ordering: false,
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [            
            { title: "Fecha", className: "text-center" },
            { title: "Litros", className: "text-right" },
            { title: "Pago Por", className: "text-center" },
            { title: "Total Empleados", className: "text-center" },            
            { title: "BancoMateriales_ID", visible:false }            
        ],
        columnDefs: [
           {
               //render: function (data, type, row) {
               //    return '<button type="button" class="btn-primary" title="Modificar" onclick="Cargar_Informacion(' + "'" + row[6] + "'" + ')"><i class="fas fa-edit"></i></button>'
               //},
               //targets: 7
           }        
        ]
    });
}

function Obtener_Hectareas() {
    var Eve_ID = '';
    try {
        $.ajax({
            url: 'Tablas/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                Crear_Hectareas(row);
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

function Crear_Hectareas($data) {
    try {
        $('#Div_Hectareas').empty();

        for (var i = 0; i < $data.length; i++) {
            var $seccion = '<div class="col-lg-3">' +
                '<button id="btn-Tabla_' + $data[i].Tabla_ID + '" title="' + $data[i].No_Tuneles + '" value="' + $data[i].No_Tuneles + '" type="button" class="btn btn_verde" style="width:100%" onclick=Hectarea_Click(\'' + $data[i].Tabla_ID + '\');>' + $data[i].Nombre + '</button>' +
                '</div>';

            $('#Div_Hectareas').append($seccion);
            $seccion = '';
        }
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

//function Hectarea_Click(Hectarea_ID) {
//    $('#btn-Tabla_' + $('#hf-tabla_id').val()).removeClass('btn btn-danger btn_rojo');
//    $('#btn-Tabla_' + $('#hf-tabla_id').val()).addClass('btn btn_verde');
//    $('#btn-Tabla_' + Hectarea_ID).addClass('btn btn-danger btn_rojo');
//    $('#hf-tabla_id').val(Hectarea_ID);
//}

function Hectarea_Click(Hectarea_ID) {
    var Hectareas = $('#hf-tabla_id').val();
    var Control_ = document.getElementById('btn-Tabla_' + Hectarea_ID);

    if (Control_.className == "btn btn_rojo") {
        $('#btn-Tabla_' + Hectarea_ID).removeClass('btn btn_rojo');
        $('#btn-Tabla_' + Hectarea_ID).addClass('btn btn_verde');
        Hectareas = Hectareas.replace(Hectarea_ID + ",", '');
    }
    else {
        var No_Tuneles = prompt('Ingrese el No de Tuneles.', $('#btn-Tabla_' + Hectarea_ID).attr("title"));
        if (No_Tuneles) {
            var Tuneles_Orig = $('#btn-Tabla_' + Hectarea_ID).val();
            if (parseInt(No_Tuneles) <= parseInt(Tuneles_Orig))
                $('#btn-Tabla_' + Hectarea_ID).attr("title", No_Tuneles);
            else
                mostrar_mensaje('Informe Técnico', 'El No de Tuneles es mayor al permitido.');
        }
        $('#btn-Tabla_' + Hectarea_ID).removeClass('btn btn_verde');
        $('#btn-Tabla_' + Hectarea_ID).addClass('btn btn_rojo');        
        Hectareas = Hectareas + Hectarea_ID + ","
    }
    $('#hf-tabla_id').val(Hectareas);
}

function cargar_tabla_detalles() {
    oTableDet = $('#Tbl_Registros_Emp').DataTable({        
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,        
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [
            { title: "No Empleado", className: "text-center", data: "No_Empleado", visible: false },
            { title: "No Tarjeta", className: "text-center", data: "No_Tarjeta"},
            { title: "Nombre", className: "text-center", data: "Nombre" },
            { title: "Cantidad", className: "text-center", data: "Cantidad", visible: false },
            { title: "", data: "Boton" }
        ]        
    });
}

function cargar_salidas_detalles()
{
    $.ajax({
        url: 'Salidas/GetDetalles',
        data: "{'No_Salida':'" + $SalidaID + "'}",
        method: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        cache: false,
        success: function (Resultado) {

            for (x = 0; x < Resultado.aaData.length; x++) {
                var Obj_Partidas = new Object();
                Obj_Partidas.Cantidad = Resultado.aaData[x][0];
                Obj_Partidas.Unidad = Resultado.aaData[x][1];
                Obj_Partidas.Insumo = Resultado.aaData[x][2];
                Obj_Partidas.Insumo_Id = Resultado.aaData[x][3];
                $Lista_Partidas.push(Obj_Partidas);
            }
            recargarTablaPartidas();
        }
    });
}

function recargarTablaPartidas() {
    var dataSet = [];
    $Lista_Partidas.forEach(function (element) {
        var dato = [];
        dato.push(element.Cantidad);
        dato.push(element.Unidad);
        dato.push(element.Insumo);
        dato.push(element.Insumo_Id);
        dataSet.push(dato);
    });
    cargar_tabla_partidas(dataSet);
}

function cargar_tabla_partidas(data) {

    oTableSal = $('#Tbl_Registros_Salidas').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        data: data,
        lengthMenu: [5, 10, 50],
        columns: [
            { title: "Cantidad", className: "text-left" },
            { title: "Unidad", className: "text-center" },
            { title: "Insumo", className: "text-center" },
            { title: "Insumo_ID", visible: false }
        ],
        columnDefs: [            
        ]
    });
}

function agregar_empleado() {
    var no_empleado = '';
    var empleado = '';
    var repetido = 'NO';    

    if ($('#cmb-emp :selected').val() != '' && $('#cmb-emp :selected').val() != undefined && $('#cmb-emp :selected').val() != null) {
        no_empleado = $('#cmb-emp :selected').val();
        empleado = $('#cmb-emp :selected').text();
    }

    oTableDet.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        if (data.No_Tarjeta == $('#txt-no_emp').val())
            repetido = 'SI';
    });

    if (repetido == 'NO') {
        var persons = [
                        {
                            No_Empleado: no_empleado,
                            No_Tarjeta: $('#txt-no_emp').val(),
                            Nombre: empleado,
                            Cantidad: '0',
                            Boton: '<button type="button" class="btn-primary del" title="Eliminar"> <i class="fas fa-trash-alt"></i></button>'
                        }
        ];
        oTableDet.rows.add(persons).draw();
    }
    else {
        alert('Ya exsite el Empelado.');
    }

    $('#cmb-emp').val('').trigger("change");
    $('#txt-no_emp').val('');
    $('#txt-cantidad').val('');    
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-fecha').val() == '' || $('#txt-fecha').val() == undefined || $('#txt-fecha').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Fecha</strong>.</span><br />';
        }
        if ($('#txt-litros').val() == '' || $('#txt-litros').val() == undefined || $('#txt-litros').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Litros</strong>.</span><br />';
        }
        if ($('#cmb-pago_por :selected').val() == '' || $('#cmb-pago_por :selected').val() == undefined || $('#cmb-pago_por :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Pago Por</strong>.</span><br />';
        }
        if ($('#cmb-sal :selected').val() == '' || $('#cmb-sal :selected').val() == undefined || $('#cmb-sal :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> No Salida</strong>.</span><br />';
        }
        if ($('#hf-tabla_id').val() == '' || $('#hf-tabla_id').val() == undefined || $('#hf-tabla_id').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Tablas</strong>.</span><br />';
        }
        if (oTableDet.data().count() <= 0) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Empleados</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function validar_datos_detalles() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-no_emp').val() == '' || $('#txt-no_emp').val() == undefined || $('#txt-no_emp').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> No Empleado</strong>.</span><br />';
        }
        //if ($('#txt-cantidad').val() == '' || $('#txt-cantidad').val() == undefined || $('#txt-cantidad').val() == null) {
        //    output.Estatus = false;
        //    output.Mensaje += '<span class="fas fa-angle-right"><strong> Cantidad</strong>.</span><br />';
        //}
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg_Datos").css({ display: 'none' });
            cargar_tabla_detalles();
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg_Datos").css({ display: 'none' });
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg_Datos").css({ display: 'Block' });
            cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'Fumigacion/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;                          
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {
    //$('#btn-Tabla_' + $('#hf-tabla_id').val()).removeClass('btn btn-danger btn_rojo');
    //$('#btn-Tabla_' + $('#hf-tabla_id').val()).addClass('btn btn_verde');
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $("#Tbl_Registros_Emp").empty();
    $("#Tbl_Registros_Salidas").empty();
    $EventoID = '';
    $EmpleadoID = '';
    $SalidaID = '';
    $Lista_Partidas = [];
}

function cargar_datos_empleado(No_Empleado) {
    try {
        $.ajax({
            url: 'Empleados/GetEmployee',
            data: "{'No_Empleado':'" + No_Empleado + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EmpleadoID = parseInt(row[0].No_Empleado);
                $('#txt-no_emp').val(parseInt(row[0].No_Empleado));               
                $('#cmb-emp').html('');
                $("#cmb-emp").select2({
                    theme: "classic",
                    data: [{ id: parseInt(row[0].No_Empleado), text: row[0].Nombre }]
                })
                cargar_empelados();
                agregar_empleado();
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

/// <summary>
/// FUNCION PARA CARGAR LOS REGISTROS
/// </summary>
function cargar_empelados() {
    jQuery('#cmb-emp').select2({
        ajax: {
            url: 'Empleados/GetEmployeeList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $EmpleadoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function cargar_salidas() {
    jQuery('#cmb-sal').select2({
        ajax: {
            url: 'Salidas/GetSalidasList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    Tipo_Actividad: 'FUMIGACION',
                    page: params.page,
                    Area: $SalidaID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function formatRepoSelection(repo) {
    return repo.text;
}

function asignar_fecha() {
    var f = new Date();
    $("#txt-fecha").val(f.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());
}

function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}