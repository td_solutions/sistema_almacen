﻿/*====================================== VARIABLES =====================================*/
var oTable;
var oTableTabla;
var oTableDetalles;
var oTableSalidas;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Fumigacion');
    eventos();    
    //Crear_Tabla();    

    $('#Tbl_Registros').on('click', 'tr', function (event) {
        var data = oTable.row(this).data();
        Cargar_Informacion(data[0]);
    });
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cns').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            Crear_Tabla();
        } else{ 
            mostrar_mensaje("", output.Mensaje);
        }           
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });
    
    $('#btn-modal-close').click(function (e) {
        e.preventDefault();
        $('#myModal').hide();
    });

    $("#txt-fecha_inicio").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_fin").datepicker({
        format: 'dd/mm/yyyy'
    });
}


/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,        
        "ajax": {
            "type": "POST",
            "url": "GetReporteFumigacion  ",
            "data": function (d) {                
                if ($('#txt-fecha_inicio').val() != '' && $('#txt-fecha_inicio').val() != undefined && $('#txt-fecha_inicio').val() != null)
                    d.Fecha_Inicio = $('#txt-fecha_inicio').val();
                if ($('#txt-fecha_fin').val() != '' && $('#txt-fecha_fin').val() != undefined && $('#txt-fecha_fin').val() != null)
                    d.Fecha_Fin = $('#txt-fecha_fin').val();
            }
        },
        columns: [
            { title: "ID", className: "text-center", visible: false },
            { title: "Fecha", className: "text-center" },
            { title: "Avance", className: "text-center" },
            { title: "Pago Por", className: "text-center" },
            { title: "Total Empleados", className: "text-center" },
            { title: "Litros", className: "text-center" }
            //{ title: "Cantidad", className: "text-center" },
            //{ title: "Total", className: "text-center" },
            //{ title: "Pago", className: "text-center" },
            //{ title: "No Empleado", className: "text-center" },
            //{ title: "Empleado", className: "text-center" },
            //{ title: "Tabla", className: "text-center" },
            //{ title: "No Tuneles", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {

               },
               //targets: 4
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function exportar_excel() {    
    window.location = "Download_Reporte_Fumigacion?Fecha_Inicio=" + $('#txt-fecha_inicio').val() + "&Fecha_Fin=" + $('#txt-fecha_fin').val();
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {        
        if (($('#txt-fecha_inicio').val() == '' || $('#txt-fecha_inicio').val() == undefined || $('#txt-fecha_inicio').val() == null)
            && ($('#txt-fecha_fin').val() == '' || $('#txt-fecha_fin').val() == undefined || $('#txt-fecha_fin').val() == null)) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> SELECCIONE ALGUN FILTRO</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    var res;
    try {
        $.ajax({
            url: '../Fumigacion/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                res = row[0].Fecha.split("T");
                $('#txt-fecha').val(res[0]);
                $('#txt-no_salida').val(row[0].No_Salida);
                $('#txt-litros').val(row[0].Litros);
                $('#txt-pago_por').val(row[0].Pago_Por);
                $('#txt-total_empleados').val(row[0].Total_Empleados);

                Cargar_Tablas(Eve_ID);
                Cargar_Detalles(Eve_ID);
                Cargar_Salidas(row[0].No_Salida);

                $('#myModal').show();
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

function Cargar_Tablas(No_Fumigacion) {
    oTableTabla = $('#Tbl_Registros_Tablas').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        lengthMenu: [10, 25, 50, 75, 100],
        "ajax": {
            "type": "POST",
            "url": "../Fumigacion/GetFumigacionTablas  ",
            "data": function (d) {
                d.No_Fumigacion = No_Fumigacion;
            }
        },
        columns: [
            { title: "Tabla", className: "text-center" },
            { title: "No Tuneles", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
               },
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function Cargar_Detalles(No_Fumigacion) {
    oTableDetalles = $('#Tbl_Registros_Detalles').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        lengthMenu: [10, 25, 50, 75, 100],
        "ajax": {
            "type": "POST",
            "url": "../Fumigacion/GetFumigacionDetalles  ",
            "data": function (d) {
                d.No_Fumigacion = No_Fumigacion;
            }
        },
        columns: [
            { title: "No Tarjeta", className: "text-center" },
            { title: "Nombre", className: "text-center" }            
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
               },
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function Cargar_Salidas(No_Salida) {
    oTableSalidas = $('#Tbl_Registros_Salidas').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        lengthMenu: [10, 25, 50, 75, 100],
        "ajax": {
            "type": "POST",
            "url": "../Salidas/GetDetalles  ",
            "data": function (d) {
                d.No_Salida = No_Salida;
            }
        },
        columns: [
            { title: "Cantidad", className: "text-center" },
            { title: "Unidad", className: "text-center" },
            { title: "Insumo", className: "text-center" },
            { title: "Insumo_ID", className: "text-center", visible:false },
            { title: "Precio", className: "text-center" },
            { title: "Iva", className: "text-center" },
            { title: "Ieps", className: "text-center" },
            { title: "Total", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
               },
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

