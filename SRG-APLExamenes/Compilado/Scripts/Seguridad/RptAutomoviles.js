﻿/*====================================== VARIABLES =====================================*/
var oTable;
var oTableTabla;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Automoviles');
    eventos();    
    //Crear_Tabla();    

    $('#Tbl_Registros').on('click', 'tr', function (event) {
        var data = oTable.row(this).data();
        Cargar_Informacion(data[0]);
    });
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cns').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            Crear_Tabla();
        } else{ 
            mostrar_mensaje("", output.Mensaje);
        }           
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });
    
    $('#btn-modal-close').click(function (e) {
        e.preventDefault();
        $('#myModal').hide();
    });

    $("#txt-fecha_inicio").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_fin").datepicker({
        format: 'dd/mm/yyyy'
    });
}


/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,        
        "ajax": {
            "type": "POST",
            "url": "GetReporteAutomoviles  ",
            "data": function (d) {
                d.No_Tarjeta = $('#txt-no_empleado').val();
                if ($('#txt-fecha_inicio').val() != '' && $('#txt-fecha_inicio').val() != undefined && $('#txt-fecha_inicio').val() != null)
                    d.Fecha_Inicio = $('#txt-fecha_inicio').val();
                if ($('#txt-fecha_fin').val() != '' && $('#txt-fecha_fin').val() != undefined && $('#txt-fecha_fin').val() != null)
                    d.Fecha_Fin = $('#txt-fecha_fin').val();
            }
        },
        columns: [
            { title: "ID", className: "text-center" },
            { title: "Fecha", className: "text-center" },
            { title: "No Empleado", className: "text-center" },
            { title: "Empleado", className: "text-center" },            
            { title: "Unidad", className: "text-center" },
            { title: "Labor", className: "text-center" },
            { title: "Nivel Gasolina", className: "text-center" },
            { title: "Horas Uso", className: "text-center" },
            { title: "Litros", className: "text-center" },
            { title: "Precio Litro", className: "text-center" },
            { title: "Total", className: "text-center" },
            { title: "Tabla", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {

               },
               //targets: 4
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function exportar_excel() {
    window.location = "Download_Reporte_Automoviles?No_Tarjeta=" + $('#txt-no_empleado').val() + "&Fecha_Inicio=" + $('#txt-fecha_inicio').val() + "&Fecha_Fin=" + $('#txt-fecha_fin').val();
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {        
        if (($('#txt-fecha_inicio').val() == '' || $('#txt-fecha_inicio').val() == undefined || $('#txt-fecha_inicio').val() == null)
            && ($('#txt-fecha_fin').val() == '' || $('#txt-fecha_fin').val() == undefined || $('#txt-fecha_fin').val() == null)
            && ($('#txt-no_empleado').val() == '' || $('#txt-no_empleado').val() == undefined || $('#txt-no_empleado').val() == null)) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> SELECCIONE ALGUN FILTRO</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    var res;
    try {
        $.ajax({
            url: '../Automoviles/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                res = row[0].Fecha.split("T");
                $('#txt-fecha').val(res[0]);
                $('#txt-unidad').val(row[0].Unidad_Auto);
                $('#txt-no_tarjeta').val(row[0].No_Empleado);
                $('#txt-empleado').val(row[0].Empleado);
                $('#txt-nivel_gasolina').val(row[0].Nivel_Gasolina);
                $('#txt-horas').val(row[0].Horas);
                $('#txt-labor').val(row[0].Labor);
                $('#txt-litros').val(row[0].Litros);
                $('#txt-precio_litro').val(row[0].Precio_Litro);

                Cargar_Tablas(Eve_ID);

                $('#myModal').show();
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

function Cargar_Tablas(No_Automovil) {
    oTableTabla = $('#Tbl_Registros_Tablas').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        lengthMenu: [10, 25, 50, 75, 100],
        "ajax": {
            "type": "POST",
            "url": "../Automoviles/GetAutomovilesTablas  ",
            "data": function (d) {
                d.No_Automovil = No_Automovil;
            }
        },
        columns: [
            { title: "Tabla", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
               },
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};