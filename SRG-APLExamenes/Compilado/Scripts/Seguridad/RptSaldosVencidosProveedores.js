﻿/*====================================== VARIABLES =====================================*/
var $ProveedorID = '';
var oTable;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Saldos Vencidos Proveedores');    
    eventos();
    cargar_proveedores();
    //Crear_Tabla();    
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cns').click(function (e) {
        e.preventDefault();        
        $('#txt-saldo').val("0.00");
        Crear_Tabla();
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });

    $("#txt-fecha_inicio").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_fin").datepicker({
        format: 'dd/mm/yyyy'
    });

    /*********Combos**********/
    $('#cmb-proveedor').on("select2:select", function (evt) {
        $ProveedorID = evt.params.data.id;
    });
    $("#cmb-proveedor").on("select2:unselecting instead", function (e) {
        $ProveedorID = '';
    });
}


/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    var Estatus = "'POR PAGAR','ABONADA'";
    
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,        
        "ajax": {
            "type": "POST",
            "url": "GetReporteSaldosVencidosProveedores  ",
            "data": function (d) {
                d.NO_FACTURA = $('#txt-no_factura').val();
                d.PROVEEDOR_ID = $ProveedorID;
                d.ESTATUS = Estatus;                
                d.Fecha_Saldos_Fin = $('#txt-fecha_fin').val();
            }
        },
        columns: [            
            { title: "No Factura", className: "text-center" },            
            { title: "Proveedor", className: "text-left" },
            { title: "Fecha", className: "text-center" },
            { title: "F Vencimiento", className: "text-center" },            
            { title: "Saldo", className: "text-rigth" },
            { title: "Estatus", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {

               },
               //targets: 4
           }
        ],
        rowCallback: function (row, data) {
            calcular_totales();
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function exportar_excel() {
    var Estatus = "'POR PAGAR','ABONADA'";
    
    window.location = "Download_Reporte_Saldos_Vencidos_Proveedores?NO_FACTURA=" + $('#txt-no_factura').val() + "&PROVEEDOR_ID=" + $ProveedorID + "&ESTATUS=" + Estatus + "&Fecha_Saldos_Fin=" + $('#txt-fecha_fin').val();
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

function cargar_proveedores() {
    jQuery('#cmb-proveedor').select2({
        ajax: {
            url: '../Proveedores/GetProveedoresList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $ProveedorID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function formatRepoSelection(repo) {
    return repo.text;
}

function calcular_totales() {    
    var tot_saldo = 0;

    oTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();        
        tot_saldo += parseFloat(data[4]);
    });

    $('#txt-saldo').val(tot_saldo.toFixed(2));
}

