﻿/*====================================== VARIABLES =====================================*/
var oTable;
var oTableDetalles;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Notas de Salida');
    eventos();
    //Crear_Tabla(); 

    $('#Tbl_Registros').on('click', 'tr', function (event) {
        var data = oTable.row(this).data();
        Cargar_Informacion(data[0]);
    });
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cns').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            Crear_Tabla();
        } else {
            mostrar_mensaje("", output.Mensaje);
        }
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });

    $('#btn-modal-close').click(function (e) {
        e.preventDefault();
        $('#myModal').hide();
    });

    $("#txt-fecha_inicio").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_fin").datepicker({
        format: 'dd/mm/yyyy'
    });
}


/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": {
            "type": "POST",
            "url": "GetReporteNotasSalidas  ",
            "data": function (d) {
                if ($('#txt-fecha_inicio').val() != '' && $('#txt-fecha_inicio').val() != undefined && $('#txt-fecha_inicio').val() != null)
                    d.Fecha_Inicio = $('#txt-fecha_inicio').val();
                if ($('#txt-fecha_fin').val() != '' && $('#txt-fecha_fin').val() != undefined && $('#txt-fecha_fin').val() != null)
                    d.Fecha_Fin = $('#txt-fecha_fin').val();
            }
        },
        columns: [
            { title: "ID", className: "text-center", visible: false },
            { title: "Fecha", className: "text-center" },
            { title: "Agricultor", className: "text-center" },
            { title: "Clave", className: "text-center" },
            { title: "Producto", className: "text-center" },
            { title: "Peso", className: "text-center" },
            { title: "Cajas", className: "text-center" },
            { title: "Kilogramos", className: "text-center" },
            { title: "Precio", className: "text-center" },
            { title: "Total", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {

               },
               //targets: 4
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function exportar_excel() {
    window.location = "Download_Reporte_Notas_Salida?Fecha_Inicio=" + $('#txt-fecha_inicio').val() + "&Fecha_Fin=" + $('#txt-fecha_fin').val();
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if (($('#txt-fecha_inicio').val() == '' || $('#txt-fecha_inicio').val() == undefined || $('#txt-fecha_inicio').val() == null)
            && ($('#txt-fecha_fin').val() == '' || $('#txt-fecha_fin').val() == undefined || $('#txt-fecha_fin').val() == null)) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> SELECCIONE ALGUN FILTRO</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    var res;
    try {
        $.ajax({
            url: '../NotasSalida/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                res = row[0].Fecha.split("T");
                $('#txt-fecha').val(res[0]);
                $('#txt-agricultor').val(row[0].Agricultor);
                $('#txt-total_cajas').val(row[0].Total_Cajas);
                $('#txt-total_peso').val(row[0].Total_Peso);
                $('#txt-total_kilogramos').val(row[0].Total_Kilogramos);
                $('#txt-total').val(row[0].Total);

                Cargar_Detalles(Eve_ID);
                
                $('#myModal').show();
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

function Cargar_Detalles(No_Nota) {
    oTableDetalles = $('#Tbl_Registros_Detalles').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        lengthMenu: [10, 25, 50, 75, 100],
        "ajax": {
            "type": "POST",
            "url": "../NotasSalida/GetNotasSalidaDetalles  ",
            "data": function (d) {
                d.No_Nota = No_Nota;
            }
        },
        columns: [
            { title: "Clave", className: "text-center" },
            { title: "Producto", className: "text-center" },
            { title: "Peso", className: "text-center" },
            { title: "Cajas", className: "text-center" },
            { title: "Kilogramos", className: "text-center" },
            { title: "Precio", className: "text-center" },
            { title: "Total", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
               },
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

