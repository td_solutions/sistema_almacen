﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var $EmpleadoID = '';
var oTable;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Destajo');
    eventos();
    cargar_tabla();
    cargar_empelados();    
    Obtener_Tipo_Destajo();
    $('#btn-new').click();
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $("#txt-fecha").datepicker({
        format: 'dd/mm/yyyy'
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');
        $('#btn-new').click();
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
        asignar_fecha();
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();               
        } else {
            $('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });
        }
    });

    /*********Combos**********/
    $('#cmb-emp').on("select2:select", function (evt) {
        $EmpleadoID = evt.params.data.id;
        $('#txt-no_emp').val(parseInt($EmpleadoID));
        $('#txt-no_emp').prop('disabled', true);
    });
    $("#cmb-emp").on("select2:unselecting instead", function (e) {
        $EmpleadoID = '';
        $('#txt-no_emp').val('');
        $('#txt-no_emp').prop('disabled', false);
    });

    /*********Cajas Texto**********/
    $("#txt-no_emp").blur(function () {
        $EmpleadoID = $("#txt-no_emp").val();
        cargar_datos_empleado($EmpleadoID);
    });
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Abrir_Ventana_Espera();

        Obj_Capturado.No_Destajo = $EventoID;
        Obj_Capturado.Fecha = $('#txt-fecha').val();
        Obj_Capturado.No_Empleado = $('#cmb-emp :selected').val();        
        Obj_Capturado.Tipo_Destajo_ID = $('#hf-td_id').val();
        Obj_Capturado.No_Tarjeta = $('#txt-no_emp').val();
        Obj_Capturado.Cantidad = $('#txt-cajas').val();
        Obj_Capturado.Cantidad_Empleados = "1";
        Obj_Capturado.Estatus = "ACTIVO";
        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';
        $.ajax({
            url: 'Destajo/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_Capturado),
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                    $('#btn-new').click();
                    Cerrar_Ventana_Espera();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                    Cerrar_Ventana_Espera();
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
        Cerrar_Ventana_Espera();
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Destajo/GetEvents",
        ordering: false,
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [            
            { title: "Fecha", className: "text-center" },                        
            { title: "Tipo Destajo", className: "text-center" },
            { title: "Cajas", className: "text-right" },
            { title: "BancoMateriales_ID", visible:false }            
        ],
        columnDefs: [
           {
               //render: function (data, type, row) {
               //    return '<button type="button" class="btn-primary" title="Modificar" onclick="Cargar_Informacion(' + "'" + row[6] + "'" + ')"><i class="fas fa-edit"></i></button>'
               //},
               //targets: 7
           }        
        ]
    });
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-fecha').val() == '' || $('#txt-fecha').val() == undefined || $('#txt-fecha').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Fecha</strong>.</span><br />';
        }
        if ($('#txt-no_emp').val() == '' || $('#txt-no_emp').val() == undefined || $('#txt-no_emp').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> No Empleado</strong>.</span><br />';
        }        
        if ($('#hf-td_id').val() == '' || $('#hf-td_id').val() == undefined || $('#hf-td_id').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Tipo Destajo</strong>.</span><br />';
        }
        if ($('#txt-cajas').val() == '' || $('#txt-cajas').val() == undefined || $('#txt-cajas').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Cajas</strong>.</span><br />';
        }        
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });   
            $('#txt-no_emp').prop('disabled', false);
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });
            cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'Destajo/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;
                $EmpleadoID = row[0].No_Empleado;
                $TablaID = row[0].Tabla_ID;
                $TipoDestajoID = row[0].Tipo_Destajo_ID;

                $('#txt-fecha').val(row[0].Fecha);
                $('#txt-no_emp').val(row[0].No_Tarjeta);
                $('#txt-cajas').val(row[0].Cantidad);

                if (row[0].No_Empleado != "") {
                    $("#cmb-emp").select2({
                        theme: "classic",
                        data: [{ id: parseInt(row[0].No_Empleado), text: row[0].Empleado }]
                    })
                    cargar_empelados();
                }          
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {
    $('#btn-TD_' + $('#hf-td_id').val()).removeClass('btn btn-danger btn_rojo');
    $('#btn-TD_' + $('#hf-td_id').val()).addClass('btn btn_verde');
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $EventoID = '';
    $EmpleadoID = '';
    $TablaID = '';
    $TipoDestajoID = '';    
}

function cargar_datos_empleado(No_Empleado) {
    try {
        $.ajax({
            url: 'Empleados/GetEmployee',
            data: "{'No_Empleado':'" + No_Empleado + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EmpleadoID = parseInt(row[0].No_Empleado);
                $('#txt-no_emp').val(parseInt(row[0].No_Empleado));
                $('#txt-no_emp').prop('disabled', true);
                $('#cmb-emp').html('');
                $("#cmb-emp").select2({
                    theme: "classic",
                    data: [{ id: parseInt(row[0].No_Empleado), text: row[0].Nombre }]
                })
                cargar_empelados();
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

/// <summary>
/// FUNCION PARA CARGAR LOS REGISTROS
/// </summary>
function cargar_empelados() {
    jQuery('#cmb-emp').select2({
        ajax: {
            url: 'Empleados/GetEmployeeList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $EmpleadoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function Obtener_Tipo_Destajo() {
    var Eve_ID = '';
    try {
        $.ajax({
            url: 'TipoDestajo/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                Crear_Tipo_Destajo(row);
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

function Crear_Tipo_Destajo($data) {
    try {
        $('#Div_TD').empty();

        for (var i = 0; i < $data.length; i++) {
            var $seccion = '<div class="col-lg-3">' +
                '<button id="btn-TD_' + $data[i].Tipo_Destajo_ID + '" type="button" class="btn btn_verde" style="width:100%" onclick=Tipo_Destajo_Click(\'' + $data[i].Tipo_Destajo_ID + '\');>' + $data[i].Nombre + '</button>' +
                '</div>';

            $('#Div_TD').append($seccion);
            $seccion = '';
        }
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

function Tipo_Destajo_Click(Hectarea_ID) {
    $('#btn-TD_' + $('#hf-td_id').val()).removeClass('btn btn-danger btn_rojo');
    $('#btn-TD_' + $('#hf-td_id').val()).addClass('btn btn_verde');
    $('#btn-TD_' + Hectarea_ID).addClass('btn btn-danger btn_rojo');
    $('#hf-td_id').val(Hectarea_ID);
    $('#txt-cajas').focus();
}

function formatRepoSelection(repo) {
    return repo.text;
}

function asignar_fecha() {
    var f = new Date();
    $("#txt-fecha").val(f.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());
}

function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}