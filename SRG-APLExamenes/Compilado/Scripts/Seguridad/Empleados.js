﻿/*====================================== VARIABLES =====================================*/
var $Correo = '';
var $EmpleadoID = '';
var oTable;

var dataSet = [{ Menu: "Almacenes", Etiqueta: "Almacenes" },
               { Menu: "Empleados", Etiqueta: "Empleados" },               
               { Menu: "Insumos", Etiqueta: "Insumos" },
               { Menu: "Parámetros", Etiqueta: "Parametros" },
               { Menu: "Proveedores", Etiqueta: "Proveedores" },
               { Menu: "Tablas", Etiqueta: "Tablas" },
               { Menu: "Tipo Destajo", Etiqueta: "TipoDestajo" },
               { Menu: "Tipo Insumos", Etiqueta: "TipoInsumos" },
               { Menu: "Unidad", Etiqueta: "Unidad" },
               { Menu: "Unidad Insumo", Etiqueta: "UnidadInsumo" },
               { Menu: "Labores", Etiqueta: "Labores" },
               { Menu: "Actividades", Etiqueta: "Actividades" },
               { Menu: "Corte", Etiqueta: "DestajoCorte" },
               { Menu: "Destajo", Etiqueta: "Destajo" },
               { Menu: "Entradas", Etiqueta: "Entradas" },
               { Menu: "Facturas Proveedor", Etiqueta: "Facturas" },
               { Menu: "Pago Proveedores", Etiqueta: "PagoFacturas" },
               { Menu: "Cancelacion Pago Proveedores", Etiqueta: "CancelacionPagoProveedores" },
               { Menu: "Fumigacion", Etiqueta: "Fumigacion" },
               { Menu: "Notas Salida", Etiqueta: "NotasSalida" },
               { Menu: "Riego", Etiqueta: "Riego" },
               { Menu: "Salidas", Etiqueta: "Salidas" },
               { Menu: "Unidades", Etiqueta: "Automoviles" },
               { Menu: "Reporte Actividades", Etiqueta: "ReporteActividades" },
               { Menu: "Reporte Corte", Etiqueta: "ReporteCorte" },
               { Menu: "Reporte Destajo", Etiqueta: "ReporteDestajo" },
               { Menu: "Reporte Fumigacion", Etiqueta: "ReporteFumigacion" },
               { Menu: "Reporte Riego", Etiqueta: "ReporteRiego" },
               { Menu: "Reporte Automoviles", Etiqueta: "ReporteAutomoviles" },
               { Menu: "Reporte Inventario", Etiqueta: "ReporteInventario" },
               { Menu: "Reporte Entradas", Etiqueta: "ReporteEntradas" },
                { Menu: "Reporte Salidas", Etiqueta: "ReporteSalidas" },
               { Menu: "Reporte Estado Cuenta Proveedores", Etiqueta: "ReporteEstadoCuentaProveedores" },
               { Menu: "Reporte Saldos Vencidos Proveedores", Etiqueta: "ReporteSaldosVencidosProveedores" },
                { Menu: "Reporte Notas Salida", Etiqueta: "ReporteNotasSalida" },
                { Menu: "Reporte Kardex", Etiqueta: "ReporteKardex" }
               ];

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Empleados');
    eventos();
    cargar_tabla();
    cargar_tabla_accesos();
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-import').click(function (e) {
        e.preventDefault();
        ImportarDatos();
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('Cancel')
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();
        } else {
            $('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
                placement: 'bottomLeft',
                showCloseBut: true,
                onDocumentClickClose: true,
                onDocumentClickClosePrevent: '',
                overflowContent: false,
                inline: true,
                asMenu: false,
                size: '',
                onOkBut: function (event, el) { },
                onCancelBut: function (event, el) { },
                onLoad: function (el) { },
                onClose: function (el) { }
            });
        }
    });
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    var Obj_accesos = new Object();
    var Array_acceso = new Array();
    var Contador = 0;

    try {        
        Obj_Capturado.No_Empleado = $('#txt-no-mpl').val();
        Obj_Capturado.Nombre = $('#txt-nmb').val();
        Obj_Capturado.Email = $('#txt-crr-elc').val();
        Obj_Capturado.Usuario = $('#txt-usua').val();
        Obj_Capturado.Password = $('#txt-cnt').val();
        Obj_Capturado.Salario_Diario = $('#txt-salario').val();
        Obj_Capturado.Salario_Diario_Fumigacion = $('#txt-salario_Fumigacion').val();
        Obj_Capturado.Estatus = $('#cmb-stt :selected').val();
        Obj_Capturado.Captura = $('#txt-no-mpl').val() == "" ? 'I' : 'U';

        if ($('#chk_Almacenes').prop('checked') || $('#chk_Empleados').prop('checked') || $('#chk_Insumos').prop('checked') || $('#chk_Parametros').prop('checked') ||
            $('#chk_Proveedores').prop('checked') || $('#chk_Tablas').prop('checked') || $('#chk_TipoDestajo').prop('checked') || $('#chk_TipoInsumos').prop('checked') ||
            $('#chk_Unidad').prop('checked') || $('#chk_UnidadInsumo').prop('checked') || $('#chk_Labores').prop('checked')) {
            Obj_accesos = new Object();
            Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
            Obj_accesos.MenuID = "1";
            Obj_accesos.Nombre = "Catálogos";
            Array_acceso[Contador] = Obj_accesos;
            Contador++;
        }

        if ($('#chk_Actividades').prop('checked') || $('#chk_Destajo').prop('checked') || $('#chk_DestajoCorte').prop('checked') || $('#chk_Entradas').prop('checked') || $('#chk_Fumigacion').prop('checked') ||
            $('#chk_Riego').prop('checked') || $('#chk_Automoviles').prop('checked') || $('#chk_Salidas').prop('checked') || $('#chk_Facturas').prop('checked') ||
            $('#chk_PagoFacturas').prop('checked') || $('#chk_CancelacionPagoProveedores').prop('checked') || $('#chk_NotasSalida').prop('checked')) {
            Obj_accesos = new Object();
            Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
            Obj_accesos.MenuID = "2";
            Obj_accesos.Nombre = "Operacion";
            Array_acceso[Contador] = Obj_accesos;
            Contador++;
        }

        if ($('#chk_ReporteDestajo').prop('checked') || $('#chk_ReporteCorte').prop('checked') || $('#chk_ReporteFumigacion').prop('checked') || $('#chk_ReporteRiego').prop('checked') || $('#chk_ReporteAutomoviles').prop('checked') ||
            $('#chk_ReporteActividades').prop('checked') || $('#chk_ReporteInventario').prop('checked') || $('#chk_ReporteEstadoCuentaProveedores').prop('checked') ||
            $('#chk_ReporteSaldosVencidosProveedores').prop('checked') || $('#chk_ReporteNotasSalida').prop('checked')) {
            Obj_accesos = new Object();
            Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
            Obj_accesos.MenuID = "3";
            Obj_accesos.Nombre = "Reportes";
            Array_acceso[Contador] = Obj_accesos;
            Contador++;
        }

        $("input[name=Menu]").each(function (index) {
            if ($(this).is(':checked')) {
                Obj_accesos = new Object();

                if ($(this).val() == "Almacenes") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "4";
                    Obj_accesos.Nombre = "Almacenes";
                    Obj_accesos.Controlador = "Almacen";
                    Obj_accesos.Accion = "Almacen";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Empleados") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "5";
                    Obj_accesos.Nombre = "Empleados";
                    Obj_accesos.Controlador = "Empleados";
                    Obj_accesos.Accion = "Empleados";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }
                                
                if ($(this).val() == "Insumos") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "6";
                    Obj_accesos.Nombre = "Insumos";
                    Obj_accesos.Controlador = "Insumos";
                    Obj_accesos.Accion = "Insumos";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Parámetros") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "7";
                    Obj_accesos.Nombre = "Parámetros";
                    Obj_accesos.Controlador = "Parametros";
                    Obj_accesos.Accion = "Parametros";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Proveedores") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "8";
                    Obj_accesos.Nombre = "Proveedores";
                    Obj_accesos.Controlador = "Proveedores";
                    Obj_accesos.Accion = "Proveedores";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Tablas") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "9";
                    Obj_accesos.Nombre = "Tablas";
                    Obj_accesos.Controlador = "Tablas";
                    Obj_accesos.Accion = "Tablas";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Tipo Destajo") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "10";
                    Obj_accesos.Nombre = "Tipo Destajo";
                    Obj_accesos.Controlador = "TipoDestajo";
                    Obj_accesos.Accion = "TipoDestajo";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Tipo Insumos") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "11";
                    Obj_accesos.Nombre = "Tipo Insumos";
                    Obj_accesos.Controlador = "TipoInsumo";
                    Obj_accesos.Accion = "TipoInsumo";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Unidad") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "12";
                    Obj_accesos.Nombre = "Unidad";
                    Obj_accesos.Controlador = "Unidad";
                    Obj_accesos.Accion = "Unidad";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Unidad Insumo") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "13";
                    Obj_accesos.Nombre = "Unidad Insumo";
                    Obj_accesos.Controlador = "Unidades";
                    Obj_accesos.Accion = "Unidades";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Labores") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "14";
                    Obj_accesos.Nombre = "Labores";
                    Obj_accesos.Controlador = "Labores";
                    Obj_accesos.Accion = "Labores";
                    Obj_accesos.ParentID = "1";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Actividades") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "15";
                    Obj_accesos.Nombre = "Actividades";
                    Obj_accesos.Controlador = "Actividades";
                    Obj_accesos.Accion = "Actividades";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Corte") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "16";
                    Obj_accesos.Nombre = "Corte";
                    Obj_accesos.Controlador = "Destajo";
                    Obj_accesos.Accion = "DestajoCorte";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Destajo") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "17";
                    Obj_accesos.Nombre = "Destajo";
                    Obj_accesos.Controlador = "Destajo";
                    Obj_accesos.Accion = "Destajo";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }
                
                if ($(this).val() == "Entradas") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "18";
                    Obj_accesos.Nombre = "Entradas";
                    Obj_accesos.Controlador = "Entradas";
                    Obj_accesos.Accion = "Entradas";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Facturas Proveedor") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "19";
                    Obj_accesos.Nombre = "Facturas Proveedor";
                    Obj_accesos.Controlador = "Facturas";
                    Obj_accesos.Accion = "Facturas";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Pago Proveedores") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "20";
                    Obj_accesos.Nombre = "Pago Proveedores";
                    Obj_accesos.Controlador = "Facturas";
                    Obj_accesos.Accion = "PagoProveedores";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Cancelacion Pago Proveedores") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "21";
                    Obj_accesos.Nombre = "Cancelacion Pago Proveedores";
                    Obj_accesos.Controlador = "Facturas";
                    Obj_accesos.Accion = "CancelacionPagoProveedores";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Fumigacion") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "22";
                    Obj_accesos.Nombre = "Fumigacion";
                    Obj_accesos.Controlador = "Fumigacion";
                    Obj_accesos.Accion = "Fumigacion";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Notas Salida") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "23";
                    Obj_accesos.Nombre = "Notas Salida";
                    Obj_accesos.Controlador = "NotasSalida";
                    Obj_accesos.Accion = "NotasSalida";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Riego") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "24";
                    Obj_accesos.Nombre = "Riego";
                    Obj_accesos.Controlador = "Riego";
                    Obj_accesos.Accion = "Riego";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Salidas") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "25";
                    Obj_accesos.Nombre = "Salidas";
                    Obj_accesos.Controlador = "Salidas";
                    Obj_accesos.Accion = "Salidas";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Unidades") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "26";
                    Obj_accesos.Nombre = "Unidades";
                    Obj_accesos.Controlador = "Automoviles";
                    Obj_accesos.Accion = "Automoviles";
                    Obj_accesos.ParentID = "2";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Actividades") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "27";
                    Obj_accesos.Nombre = "Reporte Actividades";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptActividades";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Corte") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "28";
                    Obj_accesos.Nombre = "Reporte Corte";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptCorte";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Destajo") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "29";
                    Obj_accesos.Nombre = "Reporte Destajo";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptDestajo";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Fumigacion") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "30";
                    Obj_accesos.Nombre = "Reporte Fumigacion";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptFumigacion";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Riego") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "31";
                    Obj_accesos.Nombre = "Reporte Riego";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptRiego";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Automoviles") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "32";
                    Obj_accesos.Nombre = "Reporte Automoviles";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptAutomoviles";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Inventario") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "33";
                    Obj_accesos.Nombre = "Reporte Inventario";
                    Obj_accesos.Controlador = "RptInventario";
                    Obj_accesos.Accion = "RptInventario";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }                

                if ($(this).val() == "Reporte Estado Cuenta Proveedores") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "34";
                    Obj_accesos.Nombre = "Reporte Estado Cuenta Proveedores";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptEstadoCuentaProveedores";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Saldos Vencidos Proveedores") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "35";
                    Obj_accesos.Nombre = "Reporte Saldos Vencidos Proveedores";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptSaldosVencidosProveedores";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Entradas") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "36";
                    Obj_accesos.Nombre = "Reporte Entradas";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptEntradas";     
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Salidas") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "37";
                    Obj_accesos.Nombre = "Reporte Salidas";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptSalidas";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Notas Salida") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "38";
                    Obj_accesos.Nombre = "Reporte Notas Salida";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptNotasSalidas";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }

                if ($(this).val() == "Reporte Kardex") {
                    Obj_accesos.No_Empleado = $('#txt-no-mpl').val();
                    Obj_accesos.MenuID = "39";
                    Obj_accesos.Nombre = "Reporte Kardex Insumos";
                    Obj_accesos.Controlador = "Reportes";
                    Obj_accesos.Accion = "RptKardexInsumo";
                    Obj_accesos.ParentID = "3";
                    Array_acceso[Contador] = Obj_accesos;
                }
                Contador++;
            }
        });

        $.ajax({
            url: 'Empleados/OperationMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: "{'Datos':'" + JSON.stringify(Obj_Capturado) + "', 'Accesos':'" + JSON.stringify(Array_acceso) + "'}",
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    habilitar_controles();
                    limpiar_controles();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
    }
}
function ImportarDatos() {
    try {   
        $.ajax({
            url: 'Empleados/Importar',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            cache: false,
            beforeSend: function () {
                $.Toast.showToast({
                    "title": "LOADING...",
                    "icon": "loading"
                });
            },
            success: function (Resultado) {
                $.Toast.hideToast();
                if (Resultado.Estatus) {
                    limpiar_controles();
                    habilitar_controles();
                }
                else {
                    $('#btn-import').popModal({
                        html: "<h6> </h6> <hr /> " + Resultado.Mensaje + "<div class='popModal_footer'></div>",
                        placement: 'bottomLeft',
                        showCloseBut: true
                    });
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
    }
}
/// <summary>
/// FUNCION PARA CARGAR LOS ACCESOS DEL USUARIO SELECCIONADO
/// </summary>
function cargar_accesos() {
    var Obj_accesos = new Object();
    var registros = "{}";
    try {
        Obj_accesos.No_Empleado = $('#txt-no-mpl').val();;
        $.ajax({
            url: 'Empleados/GetAccessList',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_accesos),
            cache: false,
            success: function (Resultado) {
                if (Resultado.items.Data != undefined && Resultado.items.Data != null) {                    
                    registros = JSON.parse(Resultado.items.Data);
                    $.each(registros, function (i, item) {
                        $("input[name=Menu]").each(function (index) {
                            if ($(this).val() == item.Nombre) {
                                $(this).prop('checked', true);
                            }
                        });
                    });
                }
            }
        });
    } catch (e) {
        asignar_modal("Informe Técnico", e);
        jQuery('#modal_mensaje').modal({ backdrop: 'static', keyboard: false });
    }
}

function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Empleados/GetEmployee",
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [
            { title: "No Empleado", className: "text-center" },
            { title: "Nombre"},            
            { title: "Correo electrónico", visible: false },
            { title: "Contraseña", visible: false },            
            { title: "Estatus", className: "text-center" },
            { title: "Modificar", className: "text-center" },
            { title: "Salario", visible: false },
            { title: "Salario Fumigacion", visible: false },
            { title: "Usuario", visible: false }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
                   return '<button type="button" class="btn-primary" title="Modificar" onclick="Cargar_Informacion(' + "'" + row + "'" + ')"><i class="fas fa-edit"></i></button>'
               },
               targets: 5
           }
        ]
    });
}

/// <summary>
/// FUNCION QUE CREA UN PANEL CON LOS MENUS DE ACCESO
/// </summary>
function cargar_tabla_accesos() {
    var txt = "";
    jQuery.each(dataSet, function (index, value) {
        txt += "<div class='row'>";
        txt += "<div class='col-lg-3'>";
        txt += "<label style='font-size:15px; '>" + value.Menu + "</label>";
        txt += "</div>";
        txt += "<div class='col-lg-3'>";
        txt += "<div class='material-switch '>";
        txt += "<input type='checkbox' id='chk_" + value.Etiqueta + "' name='Menu' value='" + value.Menu + "'/>";
        txt += "<label for='chk_" + value.Etiqueta + "' class='badge-success'></label>";
        txt += "</div>";
        txt += "</div>";
        txt += "</div>";
    });
    $("#chk_access").append(txt);
}
/*====================================== GENERALES =====================================*/

/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
               
        if ($('#txt-nmb').val() == '' || $('#txt-nmb').val() == undefined || $('#txt-nmb').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong>NOMBRE</strong>.</span><br />';
        }
        if ($('#txt-salario').val() == '' || $('#txt-salario').val() == undefined || $('#txt-salario').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong>SALARIO DESTAJO</strong>.</span><br />';
        }
        if ($('#txt-salario_Fumigacion').val() == '' || $('#txt-salario_Fumigacion').val() == undefined || $('#txt-salario_Fumigacion').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong>SALARIO FUMIGACION</strong>.</span><br />';
        }
        if ($('#cmb-stt :selected').val() == '' || $('#cmb-stt :selected').val() == undefined || $('#cmb-stt :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong>ESTATUS</strong>.</span><br />';
        }
        if ($('#txt-crr-elc').val() != '' && $('#txt-cnt').val() != '') {
            //if (!$("input[name=Menu]").is(":checked")) {
            //    output.Estatus = false;
            //    output.Mensaje += '<span class="fas fa-angle-right"><strong>AL MENOS UN ACCESO</strong>.</span><hr />';
            //}
        }
        if ($("input[name=Menu]").is(":checked") && $('#txt-crr-elc').val() == '' && $('#txt-cnt').val() == '') {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> CORREO ELECTRONICO</strong>.</span><br />';
            output.Mensaje += '<span class="fas fa-angle-right"><strong> CONTRASEÑA</strong>.</span><br />';
        }
        //if ($('#txt-usua').val() == '' || $('#txt-usua').val() == null || $('#txt-usua').val() == undefined) {
        //    output.Estatus = false;
        //    output.Mensaje += '<span class="fas fa-angle-right"><strong> USUARIO</strong>.</span><br />';
        //}
        //if ($('#txt-crr-elc').val() == '' || $('#txt-cnt').val() == null || $('#txt-cnt').val() == undefined) {
        //    output.Estatus = false;
        //    output.Mensaje += '<span class="fas fa-angle-right"><strong> CONTRASEÑA</strong>.</span><br />';
        //}
        //if ($('#txt-cnt').val() != '' && $('#txt-crr-elc').val() == '') {
        //        output.Estatus = false;
        //        output.Mensaje += '<span class="fas fa-angle-right"><strong> CORREO ELECTRONICO</strong>.</span><br />';
        //}
        
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(row) {
    var res = row.split(",");
    $('#txt-no-mpl').val(res[0]);
    $('#txt-nmb').val(res[1]);    
    $('#txt-crr-elc').val(res[2]);
    $('#txt-cnt').val(res[3]);
    $('#cmb-stt').val(res[4]);
    $('#txt-salario').val(res[6]);
    $('#txt-salario_Fumigacion').val(res[7]);
    $('#txt-usua').val(res[8]);
    $Correo = res[2];
    cargar_accesos();
    habilitar_controles("Edit");
}


/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        case "Cancel":
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });
            cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val(''); });
    $("input[name=Menu]").each(function (index) {
        if ($(this).is(':checked')) {
            $(this).prop("checked", false)
        }
    });
}

/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}