﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var $ProveedorID = '';
var oTable;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Cancelacion Pago Proveedores');
    eventos();    
    cargar_proveedores();    
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $("#txt-fecha_inicio").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_fin").datepicker({
        format: 'dd/mm/yyyy'
    });

    $('#btn-cns').click(function (e) {
        e.preventDefault();
        cargar_tabla();
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');        
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();               
        } else {
            $('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });
        }
    });

    /*********Combos**********/
    $('#cmb-proveedor').on("select2:select", function (evt) {
        $ProveedorID = evt.params.data.id;        
    });
    $("#cmb-proveedor").on("select2:unselecting instead", function (e) {
        $ProveedorID = '';
    });
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Abrir_Ventana_Espera();

        Obj_Capturado.NO_MOVIMIENTO = $EventoID;
        Obj_Capturado.MOTIVO_CANCELACION = $('#txt-motivo').val();
        Obj_Capturado.ESTATUS = 'CANCELADO';

        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';
        $.ajax({
            url: 'EventMasterCancelarPagoProveedor',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_Capturado),
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                    Cerrar_Ventana_Espera();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                    Cerrar_Ventana_Espera();
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
        Cerrar_Ventana_Espera();
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": {
            "type": "POST",
            "url": "GetEventsMovimientosFacturas",
            "data": function (d) {
                d.NO_FACTURA_PROVEEDOR = $('#txt-no_factura').val();
                d.PROVEEDOR_ID = $ProveedorID;
                d.ESTATUS = "'ACTIVO'";
                d.Fecha_Inicio = $('#txt-fecha_inicio').val();
                d.Fecha_Fin = $('#txt-fecha_fin').val();
            }
        },
        ordering: false,
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [
            { title: "Fecha", className: "text-center" },
            { title: "No Factura", className: "text-center" },
            { title: "Proveedor", className: "text-center" },
            { title: "Concepto", className: "text-center" },
            { title: "Monto", className: "text-center" },            
            { title: "No Movimiento", visible:false }            
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
                   return '<button type="button" class="btn-primary" title="Cancelar" onclick="Cargar_Informacion(' + "'" + row[5] + "'" + ')"><i class="fas fa-edit"></i></button>'
               },
               targets: 6
           }        
        ]
    });
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-motivo').val() == '' || $('#txt-motivo').val() == undefined || $('#txt-motivo').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Motivo Cancelacion</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });            
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });
            cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'GetEventMovimiento',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;
                $ProveedorID = row[0].PROVEEDOR_ID;
                $('#txt-no_factura_proveedor').val(row[0].NO_FACTURA_PROVEEDOR);
                $('#txt-proveedor').val(row[0].PROVEEDOR);
                $('#txt-concepto').val(row[0].CONCEPTO);
                $('#txt-monto').val(row[0].CANTIDAD);
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {    
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $EventoID = '';
    $ProveedorID = '';    
}

/// <summary>
/// FUNCION PARA CARGAR LOS REGISTROS
/// </summary>
function cargar_proveedores() {
    jQuery('#cmb-proveedor').select2({
        ajax: {
            url: '../Proveedores/GetProveedoresList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $ProveedorID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function formatRepoSelection(repo) {
    return repo.text;
}

function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}