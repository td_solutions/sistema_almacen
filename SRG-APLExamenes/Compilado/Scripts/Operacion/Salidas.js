﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var oTable;
var $InsumoID = '';
var $AlmacenID = '';
var $ProveedorID = '';
var $Lista_Partidas = [];
var regex = '^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$';
var $total = 0;
var $ieps = 0;
var $iva = 0;
var $subtotal = 0;
 
$.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Today",
    clear: "Clear",
    format: "dd/mm/yyyy",
    titleFormat: "dd/mm/yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
};


$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Salidas');
    eventos();
    cargar_tabla();
    cargar_tabla_partidas();
    cargar_insumos();
    cargar_almacen();
    cargar_almacen_dest();
    //cargar_proveedor();
    //cargar_unidades();
    asignar_fecha();

    //$("#cmb-stt option[value='ACTIVO']").attr('selected', 'selected');
});
function cargar_insumos() {

        try {

            //  se consultara y cargara la información
            $('#cmb-insumos').select2({
                language: "es",
                theme: "classic",
                placeholder: 'SELECCIONE',
                allowClear: true,
                ajax: {
                    url: 'Insumos/ObtenerListaInsumos',
                    cache: true,
                    dataType: 'json',
                    type: "POST",
                    delay: 250,
                    params: {
                        contentType: 'application/json; charset=utf-8'
                    },
                    quietMillis: 100,
                    results: function (data) {
                        return { results: data };
                    },
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        return {
                            results: data.items
                        };
                    },
                }
            });


        } catch (e) {
            alert(e);
            //  se muestra el mensaje del error que se presento
            //_mostrar_mensaje('Informe técnico' + '[_load_cmb_responsable_plantas_nivel_organizacional_2]', e);
        }



    /*

    $('#cmb-insumos').select2({
        ajax: {
            url: 'Insumos/GetInsumosList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $InsumoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }*/
}

function cargar_unidades() {
    jQuery('#cmb-unidad').select2({
        ajax: {
            url: 'Unidades/GetUnidadesList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $UnidadID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
function cargar_almacen_dest() {
    jQuery('#cmb-almacen-destino').select2({
        ajax: {
            url: 'Almacen/GetAlmacenList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: Almacen_Destino_ID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
function cargar_almacen() {
    jQuery('#cmb-almacen-origen').select2({
        ajax: {
            url: 'Almacen/GetAlmacenList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $AlmacenID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
function cargar_proveedor() {
    jQuery('#cmb-proveedor').select2({
        ajax: {
            url: 'Proveedores/GetProveedoresList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $ProveedorID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {

    $("#txt-fecha").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });      
    $("#txt-realiza").change(function (e) {
        $("#hdn-no-empleado").val($(this).val());
        //DETALLES 
        $.ajax({
            url: 'Empleados/GetEmployee',
            data: "{'No_Empleado':'" + $(this).val() + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                if (row == null)
                    $("#txt-realiza").val('');
                else
                    $("#txt-realiza").val(row[0].Nombre);
            },
            error: function (res) {
                $("#txt-realiza").val('');
            }
        });
        //FIN AJAX DETALLES
    });
    $("#txt-fecha").change(function (e) {
        var dt_entrada;
        val = $("#txt-fecha").val();
        try {
            if (val.trim().match(regex)) {
                dt_entrada = new Date(val);
            }
            else {
                $("#txt-fecha").val('');
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Fecha incorrecta!',
                    footer: '<a href>Valide la fecha en el formato dd-mm-aaaa</a>'
                });
            }
        } catch (e) {
            $("#txt-fecha").val('')
            mostrar_mensaje("fecha no válida");
        }
    });   

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
        asignar_fecha();
    });
    $('.btn-agregar-partida').click(function (e) {
        e.preventDefault();

        var output = Validar_Partida();
        if (output.Estatus) {
            Agregar_Partidas();
        }
        else {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Algunos datos no se completaron!',
                footer: '<a href>' + output.Mensaje + '</a>'
            });
        }
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();
        } else {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Algunos datos no se completaron!',
                footer: '<a href>' + output.Mensaje + '</a>'
            });

            /*$('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });*/
        }
    });

    $("#cmb-tipo-salida").change(function (e) {
        if ($(this).val() == "TRASPASO") {
            $("#div_almacen_destino").css('visibility', 'visible');
        }
        else
            $("#div_almacen_destino").css('visibility', 'hidden');
    });
}
function Agregar_Partidas() {
    var repetido = false;
    var Obj_Partidas = new Object();

    var unidad_compra;
    var unidad_consumo;

    Obj_Partidas.Cantidad = $('#txt-cantidad').val();
    Obj_Partidas.Unidad = $('#cmb-unidad :selected').text();
    Obj_Partidas.Unidad_ID = $('#cmb-unidad :selected').val();
    Obj_Partidas.Insumo_Id = $('#cmb-insumos :selected').val();
    Obj_Partidas.Insumo = $('#cmb-insumos :selected').text();
    Obj_Partidas.Importe = parseFloat($('#txt-precio').val()) * parseFloat(Obj_Partidas.Cantidad);
    Obj_Partidas.Iva = 0;
    Obj_Partidas.Ieps = 0;
    if (parseFloat($('#hdn-iva').val()) > 0)
        Obj_Partidas.Iva = parseFloat(Obj_Partidas.Importe) * 0.16;
    if (parseFloat($('#hdn-ieps').val()) > 0)
        Obj_Partidas.Ieps = parseFloat(Obj_Partidas.Importe) * 0.07;
    Obj_Partidas.Total = parseFloat(Obj_Partidas.Importe) + parseFloat(Obj_Partidas.Iva) + parseFloat(Obj_Partidas.Ieps);

    //Obj_Partidas.IVA = $('#hdn-iva').val();
    //Obj_Partidas.IEPS = $('#hdn-ieps').val();
    //Obj_Partidas.Total = $('#hdn-total').val();

    unidad_consumo = $('#hdn-unidad-consumo').val();
    unidad_compra = $('#hdn-unidad-compra').val();
    /*if (unidad_compra === Obj_Partidas.Unidad) {
        if (parseFloat(Obj_Partidas.Cantidad) < 1) {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'No puede ser decimal cuando la unidad es de compra',
                footer: '<a href>Valide la fecha en el formato dd-mm-aaaa</a>'
            });
            return;
        }
    }*/

    $Lista_Partidas.forEach(function (element) {
        if (element.Insumo_Id === Obj_Partidas.Insumo_Id && element.Unidad === Obj_Partidas.Unidad) {
            repetido = true;
            Swal.fire({
                title: 'Ya se incluyó ' + element.Cantidad + " " + Obj_Partidas.Unidad + " de " + Obj_Partidas.Insumo,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    removeItemFromArr($Lista_Partidas, element);
                    element.Cantidad = parseFloat(element.Cantidad) + parseFloat(Obj_Partidas.Cantidad);
                    $Lista_Partidas.push(element);
                    recargarTablaPartidas();
                }
            });
        }
    });
    if (!repetido) {
        $Lista_Partidas.push(Obj_Partidas);
        recargarTablaPartidas();
    }
}
function sumarizarFacturas() {
    var table = $('#Tbl_Partidas').DataTable();
    var total_factura = 0;
    var data = table
        .rows()
        .data();
    $subtotal = 0;
    $total = 0;
    $iva = 0;
    $ieps = 0;
    for (x = 0; x < data.length; x++) {
        $subtotal +=  parseFloat(data[x][4]);
        $iva += (parseFloat(data[x][0]) * parseFloat(data[x][5]));
        $ieps += (parseFloat(data[x][0]) * parseFloat(data[x][6]));
        $total = $subtotal + $iva + $ieps;
    }
    $('#txt-total').val($total);
    $('#txt-sub-total').val($subtotal);
    $('#txt-iva').val($iva);
    $('#txt-ieps').val($ieps);
}
function recargarTablaPartidas() {
    var dataSet = [];
    $Lista_Partidas.forEach(function (element) {
        var dato = [];
        dato.push(element.Cantidad);
        dato.push(element.Unidad);
        dato.push(element.Insumo);
        dato.push(element.Insumo_Id);
        dato.push(element.Importe);
        dato.push(element.Iva);
        dato.push(element.Ieps);
        dato.push(element.Total);

        dataSet.push(dato);
    });
    cargar_tabla_partidas(dataSet);
    sumarizarFacturas();
}
function removeItemFromArr(arr, item) {
    var i = arr.indexOf(item);

    if (i !== -1) {
        arr.splice(i, 1);
    }
}
/*********Combos**********/
$('#cmb-insumos').on("select2:select", function (evt) {
    $InsumoID = evt.params.data.Insumo_Id;
    $unidadID = evt.params.data.Unidad_ID;
    $Unidad = evt.params.data.Unidad;
    $Stock = evt.params.data.Existencias;
    $Precio = evt.params.data.Costo;
    $iva_item = evt.params.data.Iva;
    $ieps_item = evt.params.data.Ieps;
    $total_item = evt.params.data.Total;
    $u_consumo = evt.params.data.Unidad_Consumo;
    $u_compra = evt.params.data.Unidad;
    $equivalencia = evt.params.data.Equivalencia;

    $unidadConsumoID = evt.params.data.Unidad_Consumo_ID;
    $UnidadConsumo = evt.params.data.Unidad_Consumo;
    $('#cmb-unidad option').remove();
    $("#cmb-unidad").append('<option value=' + $unidadID + '>' + $Unidad + '</option>');
    $("#cmb-unidad").append('<option value=' + $unidadConsumoID + '>' + $UnidadConsumo + '</option>');
    $("#txt-stock").val($Stock);
    $("#txt-precio").val($Precio);
    $('#hdn-iva').val($iva_item);
    $('#hdn-ieps').val($ieps_item);
    $('#hdn-total').val($total_item);

    $('#hdn-unidad-consumo').val($u_consumo);
    $('#hdn-unidad-compra').val($u_compra);

    $('#hdn-equivalencia').val($equivalencia);
    $('#hdn-stock').val($Stock);
    $('#hdn-precio').val($Precio);

    $(this).select2('focus');

/*=============================================
PETICION PARA CONSULTAR LAS UNIDADES DE CONSUMO Y DE COMPRA
=============================================*/


    /*var comboU = $("#cmb-unidad").select2();

    if (comboU != undefined)
        $("#cmb-unidad").select2('destroy'); 

    $("#cmb-unidad").select2({
        theme: "classic",
        data: [{ id: $unidadID, text: $Unidad }]
    });
    cargar_unidades();*/
});
$("#cmb-insumos").on("select2:unselecting instead", function (e) {
    $InsumoID = '';
});
$('#cmb-almacen').on("select2:select", function (evt) {
    $AlmacenID = evt.params.data.id;
});
$("#cmb-almacen").on("select2:unselecting instead", function (e) {
    $AlmacenID = '';
});
$('#cmb-proveedor').on("select2:select", function (evt) {
    $ProveedorID = evt.params.data.id;
});
$("#cmb-proveedor").on("select2:unselecting instead", function (e) {
    $ProveedorID = '';
});

$("#cmb-unidad").change(function (evt) {
    unidad_consumo = $('#hdn-unidad-consumo').val();
    unidad_compra = $('#hdn-unidad-compra').val();
    equivalencia = $('#hdn-equivalencia').val();
    existencia = $('#txt-stock').val();
    stock_original = $('#hdn-stock').val();
    precio_original = $('#hdn-precio').val();
    nueva_existencia = 0;
    nuevo_precio = 0;

    if ($("#cmb-unidad :selected").text() === unidad_consumo) {
        /*=============================================
        CONVERTIR UNIDADES
        =============================================*/
        if (isNaN(parseFloat(equivalencia)))
            return;
        nueva_existencia = parseFloat(existencia) * parseFloat(equivalencia);
        nuevo_precio = parseFloat(precio_original) / parseFloat(equivalencia);
        $('#txt-stock').val(nueva_existencia);
        $('#txt-precio').val(nuevo_precio.toFixed(2));
    }
    else {
        $('#txt-stock').val(stock_original);
        $('#txt-precio').val(precio_original);
    }


});


//$("#cmb-insumos").select2({}).focus(function () { $(this).select2('focus'); });

/*$("#cmb-insumos").select2({}).click(function (e) {
    $("cmb-insumos").select2('focus');
});*/
//$("#cmb-insumos").select2({}).focus(function () { $(this).select2('open'); });
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Obj_Capturado.No_Salida = $EventoID;
        Obj_Capturado.Almacen_Origen_ID = $('#cmb-almacen-origen :selected').val();
        Obj_Capturado.Almacen_Destino_ID = $('#cmb-almacen-destino :selected').val();
        Obj_Capturado.Tipo = $('#cmb-tipo-salida :selected').val();
        Obj_Capturado.Tipo_Actividad = $('#cmb-tipo-actividad :selected').val();
        Obj_Capturado.Costo_Total = $('#txt-costo-total').val();
        Obj_Capturado.Str_Fecha = $('#txt-fecha').val();
        //Obj_Capturado.Observaciones = $('#txt-observaciones').val();
        Obj_Capturado.Realiza = $('#txt-realiza').val();
        Obj_Capturado.Estatus = $('#cmb-stt :selected').val();        
        Obj_Capturado.Detalles = $Lista_Partidas;
        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';
        $.ajax({
            url: 'Salidas/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_Capturado),
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Salidas/Traer_Salida",
        ordering: false,
        lengthMenu: [10, 25, 10, 30],
        columns: [
            { title: "No Salida", className: "text-left" },
            { title: "Fecha", className: "text-left" },
            { title: "Tipo", className: "text-center" },
            { title: "Estatus", className: "text-center" }
        ],
        columnDefs: [
            {
                render: function (data, type, row) {
                    return '<button type="button" class="btn-primary" title="Modificar" onclick="Cargar_Informacion(' + "'" + row[0] + "'" + ')"><i class="fas fa-window-close"></i></button>'
                },
                targets: 4
            }
        ]
    });
}

function cargar_tabla_partidas(data) {

    oTable = $('#Tbl_Partidas').DataTable({
        destroy: true,
        ordering: false,
        data: data,
        lengthMenu: [5, 10, 20, 10, 10, 10, 10, 10 ],
        columns: [
            { title: "Cantidad", className: "text-left" },
            { title: "Unidad", className: "text-center" },
            { title: "Insumo", className: "text-center" },
            { title: "Insumo_ID", visible: false },
            { title: "Importe", className: "text-center" },
            { title: "Ieps", className: "text-center" },
            { title: "Iva", className: "text-center" },
            { title: "Total", className: "text-center" }

        ],
        columnDefs: [
            { "width": "5%", "targets": 0 },
            { "width": "10%", "targets": 1 },
            { "width": "20%", "targets": 2 },
            { "width": "10%", "targets": 3 },
            { "width": "10%", "targets": 4 },
            { "width": "10%", "targets": 5 },
            { "width": "10%", "targets": 6 },
            { "width": "10%", "targets": 7 },
            { "width": "10%", "targets": 8 },
            {
                render: function (data, type, row) {
                    return '<button type="button" class="btn-primary" title="Modificar" onclick="Quitar_Partida(' + "'" + row[0] + "','" + row[1] + "','" + row[2] + "','" + row[3] + "'" + ')"><i class="far fa-trash-alt"></i></button>';
                },
                targets: 8
            }
        ]
    });
}
function Quitar_Partida(cantidad, unidad, insumo, insumo_id) {
    var ediccion = $("#txt-no-salida").val();
    if (ediccion === '0') {
        $Lista_Partidas.forEach(function (element) {
            if (element.Insumo_Id == insumo_id && element.Unidad == unidad)
                removeItemFromArr($Lista_Partidas, element);
        });

        recargarTablaPartidas();
    }
}
/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-fecha').val() == '' || $('#txt-fecha').val() == undefined || $('#txt-fecha').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> FECHA SALIDA</strong>.</span><br />';
        }   
        if ($('#txt-realiza').val() == '' || $('#txt-realiza').val() == undefined || $('#txt-realiza').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> REALIZA</strong>.</span><br />';
        }   
        if ($('#cmb-stt :selected').val() == '' || $('#cmb-stt :selected').val() == undefined || $('#cmb-stt :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> ESTATUS</strong>.</span><br />';
        }
        if ($('#cmb-tipo-salida :selected').val() == '' || $('#cmb-tipo-salida :selected').val() == undefined || $('#cmb-tipo-salida :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> TIPO DE LA SALIDA</strong>.</span><br />';
        }
        if ($('#cmb-tipo-actividad :selected').val() == '' || $('#cmb-tipo-actividad :selected').val() == undefined || $('#cmb-tipo-actividad :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> TIPO DE LA ACTIVIDAD</strong>.</span><br />';
        }      
        if ($('#cmb-almacen-origen :selected').val() == '' || $('#cmb-almacen-origen :selected').val() == undefined || $('#cmb-almacen-origen :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> ALMACEN</strong>.</span><br />';
        }
        if ($Lista_Partidas.length <= 0) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> PARTIDAS</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
function Validar_Partida() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-cantidad').val() == '' || $('#txt-cantidad').val() == undefined || $('#txt-cantidad').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> CANTIDAD</strong>.</span><br />';
        }
        else {
            var cant = parseFloat($('#txt-cantidad').val());
            var exis = parseFloat($('#txt-stock').val());
            if (cant <= 0) {
                output.Estatus = false;
                output.Mensaje += '<span class="fas fa-angle-right"><strong> LA CANTIDAD DEBE SER MAYOR A 0</strong>.</span><br />';
            }
            else {
                if (exis < cant) {
                    output.Estatus = false;
                    output.Mensaje += '<span class="fas fa-angle-right"><strong> NO HAY SUFICIENTES EXISTENCIAS</strong>.</span><br />';
                }
            }
        }
        
        if ($('#cmb-insumos :selected').val() == '' || $('#cmb-insumos :selected').val() == undefined || $('#cmb-insumos :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> INSUMO</strong>.</span><br />';
        }
        if ($('#cmb-unidad :selected').val() == '' || $('#cmb-unidad :selected').val() == undefined || $('#cmb-unidad :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> UNIDAD</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            $("#cmb-stt").val("ACTIVO");
            $("#cmb-tipo-salida").val("NORMAL");

            $('input[type=text]').each(function () { $(this).prop("disabled", false); });
            $('input[tipo=numeros]').each(function () { $(this).prop("disabled", false); });
            $('select').each(function () { $(this).prop("disabled", false); });
            $('#cmb-stt').prop("disabled", false);
            $('.btn-agregar-partida').prop("disabled", false);

            $('#txt-no-salida').prop("disabled", true);           


            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });

            $('input[type=text]').each(function () { $(this).prop("disabled", true); });
            $('input[tipo=numeros]').each(function () { $(this).prop("disabled", true); });
            $('select').each(function () { $(this).prop("disabled", true); });
            $('#cmb-stt').prop("disabled", false);
            $('.btn-agregar-partida').prop("disabled", true);

            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });

            $('input[type=text]').each(function () { $(this).prop("disabled", true); });
            $('input[tipo=numeros]').each(function () { $(this).prop("disabled", true); });
            $('select').each(function () { $(this).prop("disabled", true); });
            $('#cmb-stt').prop("disabled", false);
            $('.btn-agregar-partida').prop("disabled", true);

            cargar_tabla();
            break;
    }
    $('#txt-ieps').prop("disabled", true);
    $('#txt-sub-total').prop("disabled", true);
    $('#txt-iva').prop("disabled", true);
    $('#txt-total').prop("disabled", true);
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'Salidas/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;
                $('#txt-no-salida').val(row[0].No_Salida);
                $('#cmb-stt').val(row[0].Estatus);
                $('#txt-costo-total').val(row[0].Costo_Total);
                $('#txt-realiza').val(row[0].Realiza);
                $('#cmb-tipo-salida').val(row[0].Tipo);
                $('#cmb-tipo-actividad').val(row[0].Tipo_Actividad);

                var f = new Date(row[0].Fecha);
                $("#txt-fecha").datepicker("setDate", f);

                $("#cmb-almacen-origen").select2({
                    theme: "classic",
                    data: [{ id: parseInt(row[0].Almacen_Origen_ID), text: row[0].Almacen_Origen }]
                });
                cargar_almacen();
                if (row[0].Tipo == "TRASPASO") {
                    $("#div_almacen_destino").css('visibility', 'visible');
                    $("#cmb-almacen-destino").select2({
                        theme: "classic",
                        data: [{ id: parseInt(row[0].Almacen_Destino_ID), text: row[0].Almacen_Destino }]
                    });
                    cargar_almacen_dest();
                }
                else
                    $("#div_almacen_destino").css('visibility', 'hidden');
                    
                /*var dt_entrada = new Date(row[0].fecha_entrada);
                $("#txt-fecha-entrada").datepicker("setDate", dt_entrada);
                var dt_factura = new Date(row[0].fecha_factura);
                $("#txt-fecha-factura").datepicker("setDate", dt_factura);
                $("#txt-total-factura").val(row[0].total_factura);
                var dt_recepcion = new Date(row[0].fecha_recepcion);
                $("#txt-fecha-recepcion").datepicker("setDate", dt_recepcion);
                var dt_pago = new Date(row[0].fecha_pago);
                $("#txt-fecha-pago").datepicker("setDate", dt_pago);*/

                //DETALLES
                $.ajax({
                    url: 'Salidas/GetDetalles',
                    data: "{'No_Salida':'" + row[0].No_Salida + "'}",
                    method: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    cache: false,
                    success: function (Resultado) {

                        for (x = 0; x < Resultado.aaData.length; x++) {
                            var Obj_Partidas = new Object();
                            Obj_Partidas.Cantidad = Resultado.aaData[x][0];
                            Obj_Partidas.Unidad = Resultado.aaData[x][1];
                            Obj_Partidas.Insumo = Resultado.aaData[x][2];
                            Obj_Partidas.Insumo_Id = Resultado.aaData[x][3];
                            Obj_Partidas.Importe = Resultado.aaData[x][4];
                            Obj_Partidas.Ieps = Resultado.aaData[x][5];
                            Obj_Partidas.Iva = Resultado.aaData[x][6];
                            Obj_Partidas.Total = Resultado.aaData[x][7];
                            $Lista_Partidas.push(Obj_Partidas);
                        }
                        recargarTablaPartidas();
                    }
                });
                //FIN AJAX DETALLES
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[tipo=numeros]').each(function () { $(this).val('0'); });
    $('#txt-cantidad').val('1');
    //$('#txt-observaciones').val('');
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $InsumoID = '';
    $AlmacenID = '';
    $ProveedorID = '';
    $EventoID = '';
    $Estatus = '';
    $Usuario = '';
    $Login = '';
    $Lista_Partidas = [];
}

function asignar_fecha() {
    var f = new Date();
    $("#txt-fecha").datepicker("setDate", f);
}
