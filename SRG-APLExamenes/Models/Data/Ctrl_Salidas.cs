﻿using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Salidas
    {
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        internal static DataTable Consulta(Mdl_Salidas Dato)
        {
            decimal costo = 0;
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.No_Salida.ToString() != "0")
                Query += " AND No_Salida = @prmId";
            if (Dato.Almacen_Origen_ID.ToString() != "0")
                Query += " AND Almacen_Origen_ID = @prmAlmO";
            if (Dato.Almacen_Destino_ID.ToString() != "0")
                Query += " AND Almacen_Destino_ID = @prmAlmD";
            //if (!String.IsNullOrEmpty(Dato.Estatus))
                Query += " AND sal.Estatus = 'ACTIVO'";
            if (!String.IsNullOrEmpty(Dato.Observaciones))
                Query += " AND Observaciones = @prmObservaciones";
            if (!String.IsNullOrEmpty(Dato.Realiza))
                Query += " AND Realiza = @prmRealiza";
            if (!String.IsNullOrEmpty(Dato.Tipo_Actividad))
                Query += " AND Tipo_Actividad = @prmTipoActividad";
            if (!String.IsNullOrEmpty(Dato.No_Fumigacion))
                Query += " AND No_Fumigacion = @prmNo_Fumigacion";
            if (!String.IsNullOrEmpty(Dato.No_Riego))
                Query += " AND No_Riego = @prmNo_Riego";
            if (!String.IsNullOrEmpty(Dato.Costo_Total.ToString()))
            {
                Decimal.TryParse(Dato.Costo_Total.ToString(), out costo);
                if (costo>0)
                Query += " AND Costo_Total = @prmCosto_Total";
            }
            if (Dato.Fecha != DateTime.MinValue)
                Query += " AND Fecha = @prmFecha";

            if (!String.IsNullOrEmpty(Dato.Salio))
                Query += " AND ISNULL(Salio,'NO') = '" + Dato.Salio + "'";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                //using (SqlCommand cmd = new SqlCommand("SELECT sal.*,alm.Nombre as [Almacen_Origen], isnull(alm2.Nombre,'') as [Almacen_Destino], sad.Cantidad,ins.Nombre AS Insumo, sad.Total " +
                using (SqlCommand cmd = new SqlCommand("SELECT sal.*,alm.Nombre as [Almacen_Origen], isnull(alm2.Nombre,'') as [Almacen_Destino] " +
                    " FROM Ope_Salidas sal " +
                    " join Cat_Almacen alm ON alm.Almacen_ID = sal.Almacen_Origen_ID " +
                    //" join Ope_Salidas_Detalles sad ON sal.No_Salida = sad.No_Salida " +
                    //" join Cat_Insumos ins ON sad.Insumo_ID = ins.Insumo_ID " +
                    " left join Cat_Almacen alm2 on alm2.Almacen_ID = sal.Almacen_Destino_ID WHERE 1=1 " + Query + " ORDER BY sal.Fecha DESC", con))
                {
                    if (Dato.No_Salida.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.No_Salida.ToString());
                    if (!String.IsNullOrEmpty(Dato.Almacen_Origen_ID.ToString()))
                        cmd.Parameters.AddWithValue("@prmAlmO", "" + Dato.Almacen_Origen_ID.ToString() + "");
                    if (!String.IsNullOrEmpty(Dato.Almacen_Destino_ID.ToString()))
                        cmd.Parameters.AddWithValue("@prmAlmD", "" + Dato.Almacen_Destino_ID.ToString() + "");
                    if (!String.IsNullOrEmpty(Dato.Tipo_Actividad))
                        cmd.Parameters.AddWithValue("@prmTipoActividad", "" + Dato.Tipo_Actividad + "");
                    if (!String.IsNullOrEmpty(Dato.No_Fumigacion))
                        cmd.Parameters.AddWithValue("@prmNo_Fumigacion", "" + Dato.No_Fumigacion + "");
                    if (!String.IsNullOrEmpty(Dato.No_Riego))
                        cmd.Parameters.AddWithValue("@prmNo_Riego", "" + Dato.No_Riego + "");
                    //if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@prmEstatus", "ACTIVO");
                    if (Dato.Fecha != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFecha", "" + Dato.Fecha.ToString("MM/dd/yyyy") + "");
                    if (costo > 0)
                        cmd.Parameters.AddWithValue("@prmCosto_Total", "" + costo.ToString() + "");
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        internal static DataTable Consulta_Detalles(Mdl_Salidas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.No_Salida.ToString() != "0")
                Query += " AND No_Salida = @prmId";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT sd.No_Salida, sd.Cantidad,i.Nombre as [Insumo],u.Nombre as [Unidad], i.Costo AS Precio ,i.Insumo_ID, sd.Iva, sd.Ieps, sd.Total FROM Ope_Salidas_Detalles sd, Cat_Insumos i, Cat_Unidades u WHERE i.Insumo_ID = sd.Insumo_ID AND u.Unidad_ID = sd.Unidad_ID " + Query + " ORDER BY sd.No_Salida_Detalle ASC", con))
                {
                    if (Dato.No_Salida.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.No_Salida.ToString());

                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        private bool MaterManagementSalida(Mdl_Salidas Elemento, MODO_DE_CAPTURA Captura)
        {
            string strConexion = Database.BD;
            SqlConnection ObjConexion;
            TransactionScope ts = new TransactionScope();

            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                using (ts)
                {
                    using (SqlConnection Conexion = new SqlConnection(strConexion))
                    {
                        Conexion.Open();
                        Query = "set language español";
                        using (SqlCommand Obj_Comando = Conexion.CreateCommand())
                        {
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.ExecuteNonQuery();
                        }
                        ts.Complete();
                    }
                }



                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                ts.Dispose();
                //respuesta = ex.Message;
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        internal static DataTable ConsultaReporte(Mdl_Salidas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.No_Salida.ToString() != "0")
                Query += " AND sal.No_Salida = @prmId";
            //if (Dato.Proveedor_ID.ToString() != "0")
            //    Query += " AND ent.Proveedor_ID = @prmProvID";
            if (Dato.Insumo_ID.ToString() != "0")
                Query += " AND osal.Insumo_ID = @insID";
            if (Dato.Almacen_Origen_ID.ToString() != "0")
                Query += " AND sal.Almacen_Origen_ID = @prmAlmID";
            if (!String.IsNullOrEmpty(Dato.Estatus))
                Query += " AND Estatus = @prmEstatus";
            if (Dato.Fecha_Inicio != DateTime.MinValue)
                Query += " AND Fecha >= @prmFechaIni";
            if (Dato.Fecha_Fin != DateTime.MinValue)
                Query += " AND Fecha <= @prmFechaFin";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT sal.No_Salida, sal.Fecha, sal.Estatus, alm.Nombre as Almacen, sal.Tipo, sum(osal.Iva) as IVA, sum(osal.Ieps) as IEPS, sum(osal.Total) as Costo_Total  FROM Ope_Salidas sal JOIN Cat_Almacen alm ON sal.almacen_origen_id = alm.Almacen_ID JOIN Ope_Salidas_Detalles osal ON sal.No_Salida = osal.No_Salida JOIN Cat_Insumos ins ON osal.Insumo_ID = ins.Insumo_ID WHERE 1=1 " + Query + " group by sal.No_Salida, sal.Fecha, sal.Estatus, alm.Nombre, sal.Tipo ORDER BY sal.Fecha DESC", con))
                {
                    if (Dato.No_Salida.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.No_Salida.ToString());
                    //if (!String.IsNullOrEmpty(Dato.Proveedor_ID.ToString()))
                    //    cmd.Parameters.AddWithValue("@prmProvID", "" + Dato.Proveedor_ID.ToString() + "");
                    if (!String.IsNullOrEmpty(Dato.Almacen_Origen_ID.ToString()))
                        cmd.Parameters.AddWithValue("@prmAlmID", "" + Dato.Almacen_Origen_ID.ToString() + "");
                    if (Dato.Fecha_Inicio != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaIni", "" + Dato.Fecha_Inicio.ToString("dd/MM/yyyy") + "");
                    if (Dato.Fecha_Fin != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaFin", "" + Dato.Fecha_Fin.ToString("dd/MM/yyyy") + "");
                    if (Dato.Insumo_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@insID", "" + Dato.Insumo_ID.ToString() + "");
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        internal static DataTable ConsultaReporteDetalles(Mdl_Salidas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.No_Salida.ToString() != "0")
                Query += " AND sal.No_Salida = @prmId";     
            if (Dato.Insumo_ID.ToString() != "0")
                Query += " AND osal.Insumo_ID = @insID";
            if (Dato.Almacen_Origen_ID.ToString() != "0")
                Query += " AND sal.Almacen_Origen_ID = @prmAlmID";
            if (!String.IsNullOrEmpty(Dato.Estatus))
                Query += " AND Estatus = @prmEstatus";
            if (Dato.Fecha_Inicio != DateTime.MinValue)
                Query += " AND Fecha >= @prmFechaIni";
            if (Dato.Fecha_Fin != DateTime.MinValue)
                Query += " AND Fecha <= @prmFechaFin";
            if (DateTime.Parse(Dato.Str_Fecha_Inicio) != DateTime.MinValue)
                Query += " AND Fecha >= @prmFechaIni";
            if ( DateTime.Parse(Dato.Str_Fecha_Fin) != DateTime.MinValue)
                Query += " AND Fecha <= @prmFechaFin";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT sal.No_Salida, sal.Fecha, sal.Estatus, alm.Nombre as Almacen, sal.Tipo,sal.Costo_Total as Costo_Total, " + 
                        "osal.cantidad as Cantidad, osal.Insumo_ID as Codigo_barras, ins.Nombre AS Insumo, uni.Nombre as Unidades, " +
                        "osal.Precio, osal.Importe, osal.Ieps as IEPS_Prod, osal.Iva as IVA_Prod, osal.Total as Gran_Total " +
                        "FROM Ope_Salidas sal " +
                        "JOIN Cat_Almacen alm ON sal.almacen_origen_id = alm.Almacen_ID " +
                        "JOIN Ope_Salidas_Detalles osal ON sal.No_Salida = osal.No_Salida " +
                        "JOIN Cat_Insumos ins ON osal.Insumo_ID = ins.Insumo_ID " +
                        "JOIN Cat_Unidades uni ON osal.Unidad_ID = uni.Unidad_ID " +
                        "WHERE 1=1 " + Query + " " +
                        "GROUP BY sal.No_Salida, sal.Fecha, sal.Estatus, alm.Nombre, sal.Tipo, osal.cantidad, ins.Codigo_barras, ins.Nombre, uni.Nombre, sal.Costo_Total, osal.Insumo_ID, " +
                        "osal.Precio, osal.Importe, osal.Ieps, osal.Iva, osal.Total " + 
                        "ORDER BY sal.Fecha ASC", con))
                {
                    if (Dato.No_Salida.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.No_Salida.ToString());
                    if (!String.IsNullOrEmpty(Dato.Almacen_Origen_ID.ToString()))
                        cmd.Parameters.AddWithValue("@prmAlmID", "" + Dato.Almacen_Origen_ID.ToString() + "");
                    if (Dato.Fecha_Inicio != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaIni", "" + Dato.Fecha_Inicio.ToString("dd/MM/yyyy") + "");
                    if (Dato.Fecha_Fin != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaFin", "" + Dato.Fecha_Fin.ToString("dd/MM/yyyy") + "");
                    if (DateTime.Parse(Dato.Str_Fecha_Inicio) != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaIni", "" + DateTime.Parse(Dato.Str_Fecha_Inicio).ToString("dd/MM/yyyy") + "");
                    if (DateTime.Parse(Dato.Str_Fecha_Fin) != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaFin", "" + DateTime.Parse(Dato.Str_Fecha_Fin).ToString("dd/MM/yyyy") + "");
                    if (Dato.Insumo_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@insID", "" + Dato.Insumo_ID.ToString() + "");
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        internal static Boolean AltaSalida(TablaDB Elemento, MODO_DE_CAPTURA Captura, List<Mdl_Salidas_Detalles> Detalles)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            SqlConnection Obj_Conexion = null;
            SqlCommand Obj_Comando = null;
            TransactionScope ts = new TransactionScope();
            Mdl_Salidas objSalida;
            DataTable Dt_Resultado_Unidades = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            decimal cantidad = 1;
            try
            {
               
                objSalida = (Mdl_Salidas)Elemento;
                //Asignar conexion
                using (ts)
                {
                    Obj_Conexion = new SqlConnection(Database.BD);                    
                    Obj_Conexion.Open();
                    Obj_Comando = Obj_Conexion.CreateCommand();

                    Query = "set language español";
                    Obj_Comando.CommandText = Query;
                    Obj_Comando.CommandType = CommandType.Text;
                    Obj_Comando.ExecuteNonQuery();

                    Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                    Query += " ; SELECT SCOPE_IDENTITY()";
                    
                    Obj_Comando.CommandText = Query;
                    Obj_Comando.CommandType = CommandType.Text;
                    //Ejecutar consulta
                    object objId = Obj_Comando.ExecuteScalar();
                    int id = Int32.Parse(objId.ToString());

                    foreach (Mdl_Salidas_Detalles bean in Detalles)
                    {
                        Dt_Resultado_Unidades.Clear();
                        cantidad = bean.Cantidad;
                        Query = "SELECT Unidad_ID as [unidad_id],isnull(Unidad_Consumo_ID,Unidad_ID) as [unidad_consumo_id],isnull(Equivalencia,1) as [Equivalencia] FROM Cat_Insumos WHERE Insumo_ID = @InId";
                        Obj_Comando.CommandText = Query;
                        Obj_Comando.Parameters.Add("@InId", SqlDbType.Int).Value = bean.Insumo_ID;
                        sqlDataAdapter.SelectCommand = Obj_Comando;
                        sqlDataAdapter.Fill(Dt_Resultado_Unidades);
                        decimal eq = 0;
                        if (Dt_Resultado_Unidades.Rows.Count > 0)
                        {
                        /*=============================================
                        SI LA UNIDAD SELECCIONADA ES LA UNIAD DE CONUSMO EN EL CATALOGO CONVERTIR CANTIDAD PARA DESCONTAR DEL STOCK
                        =============================================*/
                            if (bean.Unidad_ID.ToString() == Dt_Resultado_Unidades.Rows[0]["unidad_consumo_id"].ToString())
                            {
                                eq = decimal.Parse(Dt_Resultado_Unidades.Rows[0]["Equivalencia"].ToString());
                                cantidad = bean.Cantidad / eq;
                            }
                        }

                        Query = "INSERT INTO Ope_Salidas_Detalles (No_Salida,Insumo_ID,Cantidad,Precio,Importe,Ieps,Iva,Total,Unidad_ID,Fecha_Creo) VALUES (@salida,@InId,@Cntd,@pre,@imp,@ieps,@iva,@tot,@uni,getdate())";
                        Obj_Comando.CommandText = Query;
                        Obj_Comando.Parameters.Add("@salida", SqlDbType.Int).Value = id;
                        Obj_Comando.Parameters.Add("@Cntd", SqlDbType.Decimal).Value = bean.Cantidad;
                        Obj_Comando.Parameters.Add("@pre", SqlDbType.Decimal).Value = bean.Importe / bean.Cantidad;
                        Obj_Comando.Parameters.Add("@imp", SqlDbType.Decimal).Value = bean.Importe;
                        Obj_Comando.Parameters.Add("@uni", SqlDbType.Int).Value = bean.Unidad_ID;
                        Obj_Comando.Parameters.Add("@ieps", SqlDbType.Decimal).Value = bean.Ieps;
                        Obj_Comando.Parameters.Add("@iva", SqlDbType.Decimal).Value = bean.Iva;
                        Obj_Comando.Parameters.Add("@tot", SqlDbType.Decimal).Value = bean.Total;
                        Obj_Comando.Parameters.Add("@CantidadConvertida", SqlDbType.Decimal).Value = cantidad;
                        Obj_Comando.ExecuteNonQuery();

                        if (objSalida.Tipo != "TRASPASO")
                        {
                            /*=============================================
                            Se descuenta existencia del productto
                            =============================================*/
                            Query = "UPDATE Cat_Insumos SET Existencias = Existencias - @CantidadConvertida WHERE Insumo_ID = @InId";
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.ExecuteNonQuery();

                            /*=============================================
                            se quita el producto del alamcen
                            =============================================*/
                            Query = "UPDATE Ope_Almacen_Insumos SET Existencias = Existencias - @CantidadConvertida WHERE Insumo_ID = @InId AND Almacen_ID = @almID";
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.Parameters.Add("@almID", SqlDbType.Int).Value = objSalida.Almacen_Origen_ID;
                            Obj_Comando.ExecuteNonQuery();
                        }
                        
                        /*=============================================
                        si es traspaso se agrega al almacen destino
                        =============================================*/
                        if (objSalida.Tipo == "TRASPASO" && objSalida.Almacen_Destino_ID > 0)
                        {
                            /*=============================================
                            Se descuenta existencia del productto
                            =============================================*/
                            Query = "UPDATE Cat_Insumos SET Existencias = Existencias - @CantidadConvertida WHERE Insumo_ID = @InId";
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.ExecuteNonQuery();

                            /*=============================================
                            se quita el producto del alamcen
                            =============================================*/
                            Query = "UPDATE Ope_Almacen_Insumos SET Existencias = Existencias - @CantidadConvertida WHERE Insumo_ID = @InId AND Almacen_ID = @almID";
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.Parameters.Add("@almID", SqlDbType.Int).Value = objSalida.Almacen_Origen_ID;
                            Obj_Comando.ExecuteNonQuery();

                            Query = "UPDATE Ope_Almacen_Insumos SET Existencias = Existencias + @CantidadConvertida WHERE Insumo_ID = @InId AND Almacen_ID = @almIDD";
                            Obj_Comando.Parameters.Add("@almIDD", SqlDbType.Int).Value = objSalida.Almacen_Destino_ID;
                            Obj_Comando.CommandText = Query;
                            int x = Obj_Comando.ExecuteNonQuery();
                            /*=============================================
                            Si x es cero es porque no hay registros de ese 
                            producto en ese almacen y se inserta relacion insumo-almacen
                            =============================================*/
                            if (x <= 0)
                            {
                                Query = "INSERT INTO Ope_Almacen_Insumos VALUES (@almIDD,@InId,@CantidadConvertida)";
                                Obj_Comando.CommandText = Query;
                                Obj_Comando.ExecuteNonQuery();
                            }
                        }

                        Obj_Comando.Parameters.Clear();
                    }
                    ts.Complete();
                    Transaccion = true;
                    //Cerrar conexion
                    Obj_Conexion.Close();
                }
            }
            catch (Exception Ex)
            {
                ts.Dispose();
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }

        internal static Boolean UpdateSalidas(TablaDB Elemento, MODO_DE_CAPTURA Captura, List<Mdl_Salidas_Detalles> Detalles)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            SqlConnection Obj_Conexion = null;
            SqlCommand Obj_Comando = null;
            TransactionScope ts = new TransactionScope();
            Mdl_Salidas objSalidas;
            //Cantidad salida convertida unidad de consumo-compra
            Double Dbl_Cantidad_Convertida = 0;
            int Unidad_Consumo_Id;
            int Unidad_Seleccionada_Id;
            int Unidad_Compra_Id;
            Double Equivalencia;
            Double Cantidad;
            DataTable Dt_Resultado_Unidades = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            
            try
            {
                objSalidas = (Mdl_Salidas)Elemento;
                //Asignar conexion
                using (ts)
                {
                    Obj_Conexion = new SqlConnection(Database.BD);
                    Obj_Conexion.Open();
                    Obj_Comando = Obj_Conexion.CreateCommand();

                    Query = "set language español";
                    Obj_Comando.CommandText = Query;
                    Obj_Comando.CommandType = CommandType.Text;
                    Obj_Comando.ExecuteNonQuery();

                    Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";

                    Obj_Comando.CommandText = Query;
                    Obj_Comando.CommandType = CommandType.Text;
                    Obj_Comando.ExecuteNonQuery();

                    #region CONTROL DE EXISTENCIAS

                    /*==============================================
                         SE RECORRE LOS ELEMENTOS DETALLES 
                         PARA REALIZAR LA CONVERSION DE 
                         UNIDAD DE CONSUMO
                    ================================================*/
                    foreach (Mdl_Salidas_Detalles bean in Detalles)
                    {
                        Dt_Resultado_Unidades.Clear();
                        Query = "SELECT isnull(fd.Cantidad,0) cantidad,isnull(i.Unidad_ID,0) unidad_id,isnull(i.Unidad_Consumo_ID,i.Unidad_ID) unidad_consumo_id,isnull(i.Equivalencia,1) equivalencia" +
                            ",fd.unidad_ID as [unidad_seleccionada]" +
                        " FROM Ope_Salidas_Detalles fd" +
                        " JOIN Cat_Insumos i ON fd.Insumo_ID = i.Insumo_ID" +
                        " WHERE fd.Insumo_ID = @insumoID" +
                        " AND fd.No_Salida = @NoSalida";
                        Obj_Comando.CommandText = Query;
                        Obj_Comando.Parameters.Add("@insumoID", SqlDbType.Int).Value = bean.Insumo_ID;
                        Obj_Comando.Parameters.Add("@NoSalida", SqlDbType.Int).Value = objSalidas.No_Salida;
                        sqlDataAdapter.SelectCommand = Obj_Comando;
                        sqlDataAdapter.Fill(Dt_Resultado_Unidades);

                        Unidad_Compra_Id = Int32.Parse(Dt_Resultado_Unidades.Rows[0]["unidad_id"].ToString());
                        Unidad_Consumo_Id = Int32.Parse(Dt_Resultado_Unidades.Rows[0]["unidad_consumo_id"].ToString());
                        Unidad_Seleccionada_Id = Int32.Parse(Dt_Resultado_Unidades.Rows[0]["unidad_seleccionada"].ToString());
                        Cantidad = Double.Parse(Dt_Resultado_Unidades.Rows[0]["cantidad"].ToString());
                        Equivalencia = Double.Parse(Dt_Resultado_Unidades.Rows[0]["equivalencia"].ToString());
                        Dbl_Cantidad_Convertida = Cantidad;
                        if (Unidad_Consumo_Id != Unidad_Compra_Id)
                        {
                            if (Unidad_Consumo_Id == Unidad_Seleccionada_Id)
                            {
                                Dbl_Cantidad_Convertida = Cantidad / Equivalencia;
                            }
                        }

                        /*=============================================
                        SI NO ES TRASPASO SE DESCUENTA LA CANTIDAD YA CONVERTIDA EN CATALOGO DE INSUMOS
                        =============================================*/
                        if (objSalidas.Tipo != "TRASPASO")
                        {
                            Query = "UPDATE i SET i.Existencias = (i.Existencias + " + Dbl_Cantidad_Convertida + ")" +
                            " FROM Cat_Insumos i WHERE i.Insumo_ID = @insumoID";                        
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.ExecuteNonQuery();

                            /*=============================================
                            CONTROL DE EXISTENCIAS
                            SE DESCUENTA CANTIDAD CONVERTIDA EN EL ALMACEN QUE CORRESPONDE
=============================================*/
                            Query = "UPDATE Ope_Almacen_Insumos SET Existencias = (Existencias + " + Dbl_Cantidad_Convertida + ")" +
                                " WHERE Insumo_ID = @insumoID AND Almacen_ID = @almIDD";
                            Obj_Comando.CommandText = Query;
                            //Obj_Comando.Parameters.Add("@NoS", SqlDbType.Int).Value = objSalidas.No_Salida;
                            Obj_Comando.Parameters.Add("@almIDD", SqlDbType.Int).Value = objSalidas.Almacen_Destino_ID;
                            Obj_Comando.Parameters.Add("@almID", SqlDbType.Int).Value = objSalidas.Almacen_Origen_ID;
                            Obj_Comando.ExecuteNonQuery();
                        }
                        /*=============================================
                        SI ES TRASPASO
                        =============================================*/
                        if (objSalidas.Tipo == "TRASPASO")
                        {
                            Query = "UPDATE i SET i.Existencias = (i.Existencias + " + Dbl_Cantidad_Convertida + ")" +
                            " FROM Cat_Insumos i WHERE i.Insumo_ID = @insumoID";
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.ExecuteNonQuery();

                            Query = "UPDATE Ope_Almacen_Insumos SET Existencias = (Existencias - " + Dbl_Cantidad_Convertida + ")" +
                             " WHERE Insumo_ID = @insumoID AND Almacen_ID = @almIDD";
                            Obj_Comando.Parameters.Add("@almIDD", SqlDbType.Int).Value = objSalidas.Almacen_Destino_ID;
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.ExecuteNonQuery();

                            Query = "UPDATE Ope_Almacen_Insumos SET Existencias = (Existencias + " + Dbl_Cantidad_Convertida + ")" +
                            " WHERE Insumo_ID = @insumoID AND Almacen_ID = @almID";
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.Parameters.Add("@almID", SqlDbType.Int).Value = objSalidas.Almacen_Origen_ID;
                            Obj_Comando.ExecuteNonQuery();
                        }


                        Obj_Comando.Parameters.Clear();
                    }
                    
                    #endregion
                     
                    ts.Complete();
                    Transaccion = true;
                    //Cerrar conexion
                    Obj_Conexion.Close();

                }
            }
            catch (Exception Ex)
            {
                ts.Dispose();
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
    }
}