﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;


namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Empleados
    {

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: 
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura, List<Mdl_Accesos> Accesos)
        {
            SqlConnection Obj_Conexion = new SqlConnection(Database.BD);
            SqlCommand Obj_Comando = new SqlCommand();
            SqlTransaction Obj_Transaccion = null;
            Boolean Transaccion = false;
            String Temporal = String.Empty;
            Mdl_Accesos Obj_Temp = new Mdl_Accesos();
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";

                Temporal = QuerySQL.ObtenValorDeParametro(Elemento.ObtenParametros(Captura), "No_Empleado").ToString();
                for (int i=0; i < Accesos.Count; i++)
                {
                    Accesos[i].No_Empleado = Temporal;
                }
                List<TablaDB> Lista_Accesos = Accesos.ToList<TablaDB>();
                Accesos = new List<Mdl_Accesos>();
                Obj_Temp.No_Empleado = Temporal;
                Accesos.Add(Obj_Temp);
                List<TablaDB> Lista_Accesos_Temp = Accesos.ToList<TablaDB>();
                Query += QuerySQL.GeneraCMDExecDetalles(
                    new DetallesBD(Lista_Accesos_Temp, MODO_DE_CAPTURA.CAPTURA_ELIMINAR),
                    new DetallesBD(Lista_Accesos, MODO_DE_CAPTURA.CAPTURA_ALTA));

                Obj_Conexion.Open();
                Obj_Transaccion = Obj_Conexion.BeginTransaction();
                Obj_Comando.Transaction = Obj_Transaccion;
                Obj_Comando.Connection = Obj_Conexion;
                Obj_Comando.CommandText = Query;
                Obj_Comando.ExecuteNonQuery();
                Obj_Transaccion.Commit();
                Obj_Conexion.Close();
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                Obj_Transaccion.Rollback();
                throw new Exception(Ex.Message);
            }
            finally
            {
                if (!Transaccion)
                {
                    Obj_Conexion.Close();
                }
            }
            return Transaccion;
        }
        
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consulta_Usuario
        ///DESCRIPCIÓN: CONSULTAR  LOS EMPLEADOS DEL PORTAL
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable Consultemployee(Mdl_Empleados Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (!String.IsNullOrEmpty(Dato.Usuario))
                Query += " WHERE CONVERT(VARCHAR (MAX), Usuario)  = @employeeUsuario";
            if (!String.IsNullOrEmpty(Dato.Password))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND CONVERT(VARCHAR (MAX), Password) = @employeePassword";
                else
                    Query += " WHERE CONVERT(VARCHAR (MAX), Password) = @employeePassword";
            }            
            if (!String.IsNullOrEmpty(Dato.Estatus))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Estatus = @Status";
                else
                    Query += " WHERE Estatus = @Status";
            }
            if (!String.IsNullOrEmpty(Dato.No_Empleado))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND No_Empleado = @NoEmp";
                else
                    Query += " WHERE No_Empleado = @NoEmp";
            }            
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Cat_Empleados " + Query + " ORDER BY No_Empleado", con))
                {
                    if (!String.IsNullOrEmpty(Dato.Usuario))
                        cmd.Parameters.AddWithValue("@employeeUsuario", Seguridad.Encriptar(Dato.Usuario.Trim()));
                    if (!String.IsNullOrEmpty(Dato.Password))
                        cmd.Parameters.AddWithValue("@employeePassword", Seguridad.Encriptar(Dato.Password.Trim()));                    
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@Status", Dato.Estatus);
                    if (!String.IsNullOrEmpty(Dato.No_Empleado))
                        cmd.Parameters.AddWithValue("@NoEmp", Dato.No_Empleado);                    
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: ConsultMaximoEmpleado
        ///DESCRIPCIÓN: CONSULTAR  LOS EMPLEADOS DEL PORTAL
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable ConsultMaximoEmpleado(Mdl_Empleados Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;            
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(No_Empleado), 0) + 1 AS No_Empleado FROM Cat_Empleados", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}