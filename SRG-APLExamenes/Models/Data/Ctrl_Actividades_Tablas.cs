﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Actividades_Tablas
    {
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: MasterManagement
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Checkoperation
        ///DESCRIPCIÓN: CONSULTAR  OPERACIONES
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable Consultevent(Mdl_Actividades_Tablas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (!String.IsNullOrEmpty(Dato.No_Actividad))
                Query += " AND Ope_Actividades_Tablas.No_Actividad = @Eve";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT Ope_Actividades_Tablas.*, Cat_Tablas.Nombre AS Tabla FROM Ope_Actividades_Tablas, Cat_Tablas WHERE Ope_Actividades_Tablas.Tabla_ID = Cat_Tablas.Tabla_ID" + Query + " ORDER BY No_Actividad DESC", con))
                {
                    if (!String.IsNullOrEmpty(Dato.No_Actividad))
                        cmd.Parameters.AddWithValue("@Eve", Dato.No_Actividad);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}