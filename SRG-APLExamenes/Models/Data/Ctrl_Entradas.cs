﻿using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Entradas
    {
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        internal static DataTable Consulta(Mdl_Entradas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.No_Entrada.ToString() != "0")
                Query += " AND No_Entrada = @prmId";
            if (Dato.Proveedor_ID.ToString() != "0")
                Query += " AND Proveedor_ID = @prmProvID";
            if (Dato.Almacen_ID.ToString() != "0")
                Query += " AND Almacen_ID = @prmAlmID";
            if (!String.IsNullOrEmpty(Dato.Estatus))
                Query += " AND Estatus = @prmEstatus";
            if (Dato.Fecha_Recepcion != DateTime.MinValue)
                Query += " AND Fecha_Recepcion = @prmFechaRecep";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                //using (SqlCommand cmd = new SqlCommand("SELECT ent.*,p.Nombre as [proveedor],alm.Nombre as [Almacen], ins.codigo_barras AS Codigo_Barras, ins.Nombre, " +
                //            " ins.Descripcion, ins.Existencias, uni.Nombre AS Unidad, ins.Costo, enp.total AS Total_Costo, ins.Tipo_Insumo_ID, tpi.Nombre AS Tipo_Insumo " +
                //            " FROM Ope_Entradas ent " +
                //            " JOIN Cat_Proveedores p ON ent.proveedor_id = p.Proveedor_ID " +
                //            " JOIN Cat_Almacen alm ON ent.almacen_id = alm.Almacen_ID " +
                //            " JOIN Ope_Entrada_Partidas enp ON ent.no_entrada = enp.No_Entrada " +
                //            " JOIN Cat_Insumos ins ON enp.Insumo_ID = ins.Insumo_ID " +
                //            " JOIN Cat_Tipo_Insumo tpi ON ins.Tipo_Insumo_ID = tpi.Tipo_Insumo_ID " +
                //            " JOIN Cat_Unidades uni ON ins.Unidad_Consumo_ID = uni.Unidad_ID " +
                //            " WHERE 1 = 1 " + Query + " ORDER BY ent.Fecha_Entrada DESC", con) )
                using (SqlCommand cmd = new SqlCommand("SELECT ent.*,p.Nombre as [proveedor],alm.Nombre as [Almacen] " +
                            " FROM Ope_Entradas ent " +
                            " JOIN Cat_Proveedores p ON ent.proveedor_id = p.Proveedor_ID " +
                            " JOIN Cat_Almacen alm ON ent.almacen_id = alm.Almacen_ID " +
                            
                            " WHERE 1 = 1 " + Query + " ORDER BY ent.Fecha_Entrada DESC", con))
                {
                    if (Dato.No_Entrada.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.No_Entrada.ToString());
                    if (!String.IsNullOrEmpty(Dato.Proveedor_ID.ToString()))
                        cmd.Parameters.AddWithValue("@prmProvID", "" + Dato.Proveedor_ID.ToString() + "");
                    if (!String.IsNullOrEmpty(Dato.Almacen_ID.ToString()))
                        cmd.Parameters.AddWithValue("@prmAlmID", "" + Dato.Almacen_ID.ToString() + "");
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@prmEstatus", "" + Dato.Estatus.Trim() + "");
                    if (Dato.Fecha_Recepcion!=DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaRecep", "" + Dato.Fecha_Recepcion.ToString("MM/dd/yyyy") + "");
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        internal static DataTable ConsultaReporte(Mdl_Entradas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.No_Entrada.ToString() != "0")
                Query += " AND ent.No_Entrada = @prmId";
            if (Dato.Proveedor_ID.ToString() != "0")
                Query += " AND ent.Proveedor_ID = @prmProvID";
            if (Dato.Insumo_ID.ToString() != "0")
                Query += " AND oep.Insumo_ID = @insID";
            if (Dato.Almacen_ID.ToString() != "0")
                Query += " AND ent.Almacen_ID = @prmAlmID";
            if (!String.IsNullOrEmpty(Dato.Estatus))
                Query += " AND Estatus = @prmEstatus";
            if (Dato.Fecha_Inicio != DateTime.MinValue)
                Query += " AND Fecha_Entrada >= @prmFechaIni";
            if (Dato.Fecha_Fin != DateTime.MinValue)
                Query += " AND Fecha_Entrada <= @prmFechaFin";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ent.*,p.Nombre as [proveedor],alm.Nombre as [Almacen] " + 
                    "FROM Ope_Entradas ent " +
                    "JOIN Cat_Proveedores p ON p.Proveedor_ID = ent.proveedor_id " +
                    "JOIN Cat_Almacen alm ON ent.almacen_id = alm.Almacen_ID " +
                    //"JOIN Ope_Entrada_Partidas oep ON ent.no_entrada = oep.No_Entrada " +
                    "WHERE 1 = 1" + Query + " " +
                    "ORDER BY ent.Fecha_Entrada DESC", con))
                {
                    if (Dato.No_Entrada.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.No_Entrada.ToString());
                    if (!String.IsNullOrEmpty(Dato.Proveedor_ID.ToString()))
                        cmd.Parameters.AddWithValue("@prmProvID", "" + Dato.Proveedor_ID.ToString() + "");
                    if (!String.IsNullOrEmpty(Dato.Almacen_ID.ToString()))
                        cmd.Parameters.AddWithValue("@prmAlmID", "" + Dato.Almacen_ID.ToString() + "");
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@prmEstatus", "" + Dato.Estatus.Trim() + "");
                    if (Dato.Fecha_Inicio != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaIni", "" + Dato.Fecha_Inicio.ToString("dd/MM/yyyy") + "");
                    if (Dato.Fecha_Fin != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaFin", "" + Dato.Fecha_Fin.ToString("dd/MM/yyyy") + "");
                    if (Dato.Insumo_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@insID", "" + Dato.Insumo_ID.ToString() + "");
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        internal static DataTable ConsultaDetalleReporte(Mdl_Entradas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.No_Entrada.ToString() != "0")
                Query += " AND ent.No_Entrada = @prmId";
            if (Dato.Proveedor_ID.ToString() != "0")
                Query += " AND ent.Proveedor_ID = @prmProvID";
            if (Dato.Insumo_ID.ToString() != "0")
                Query += " AND oep.Insumo_ID = @insID";
            if (Dato.Almacen_ID.ToString() != "0")
                Query += " AND ent.Almacen_ID = @prmAlmID";
            if (!String.IsNullOrEmpty(Dato.Estatus))
                Query += " AND Estatus = @prmEstatus";
            if (Dato.Fecha_Inicio != DateTime.MinValue)
                Query += " AND Fecha_Entrada >= @prmFechaIni";
            if (Dato.Fecha_Fin != DateTime.MinValue)
                Query += " AND Fecha_Entrada <= @prmFechaFin";
            if (DateTime.Parse(Dato.Str_Fecha_Inicio) != DateTime.MinValue)
                Query += " AND Fecha_Entrada >= @prmFechaIni";
            if (DateTime.Parse(Dato.Str_Fecha_Fin) != DateTime.MinValue)
                Query += " AND Fecha_Entrada <= @prmFechaFin";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ent.no_entrada, ent.fecha_entrada, p.Proveedor_ID, p.Nombre AS Proveedor, alm.Almacen_ID, alm.Nombre AS Almacen, " +
                                        " ent.tipo_entrada, ent.subtotal, ent.iva AS IVA, ent.ieps AS IEPS, ent.total AS Total, oep.Cantidad, ins.Insumo_ID as Codigo_barras, " +
                                        " ins.Nombre, oep.Precio as Costo, uni.Nombre as Unidades, oep.subtotal as importe, oep.iva as IVA_Prod, oep.ieps as IEPS_Prod, oep.total as gran_total " +
                                        "FROM Ope_Entradas ent " +
                                        "JOIN Cat_Proveedores p ON p.Proveedor_ID = ent.Proveedor_ID " +
                                        "JOIN Cat_Almacen alm ON alm.Almacen_ID = ent.almacen_id " +
                                        "JOIN Ope_Entrada_Partidas oep ON ent.no_entrada = oep.No_Entrada " +
                                        "JOIN Cat_Insumos ins ON oep.Insumo_ID = ins.Insumo_ID " +
                                        "LEFT JOIN Cat_Unidades uni ON oep.Unidad_ID = uni.Unidad_ID " +
                                        "WHERE 1 = 1" + Query + " " +
                                        "ORDER BY ent.Fecha_Entrada ASC", con))
                {
                    if (Dato.No_Entrada.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.No_Entrada.ToString());
                    if (!String.IsNullOrEmpty(Dato.Proveedor_ID.ToString()))
                        cmd.Parameters.AddWithValue("@prmProvID", "" + Dato.Proveedor_ID.ToString() + "");
                    if (!String.IsNullOrEmpty(Dato.Almacen_ID.ToString()))
                        cmd.Parameters.AddWithValue("@prmAlmID", "" + Dato.Almacen_ID.ToString() + "");
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@prmEstatus", "" + Dato.Estatus.Trim() + "");
                    if (Dato.Fecha_Inicio != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaIni", "" + Dato.Fecha_Inicio.ToString("dd/MM/yyyy") + "");
                    if (Dato.Fecha_Fin != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaFin", "" + Dato.Fecha_Fin.ToString("dd/MM/yyyy") + "");
                    if (DateTime.Parse(Dato.Str_Fecha_Inicio) != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaIni", "" + DateTime.Parse(Dato.Str_Fecha_Inicio).ToString("dd/MM/yyyy") + "");
                    if (DateTime.Parse(Dato.Str_Fecha_Fin) != DateTime.MinValue)
                        cmd.Parameters.AddWithValue("@prmFechaFin", "" + DateTime.Parse(Dato.Str_Fecha_Fin).ToString("dd/MM/yyyy") + "");
                    if (Dato.Insumo_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@insID", "" + Dato.Insumo_ID.ToString() + "");
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        internal static DataTable Consulta_Detalles(Mdl_Entradas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.No_Entrada.ToString() != "0")
                Query += " AND No_Entrada = @prmId";            

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT e.Cantidad,i.Nombre as [Insumo],u.Nombre as [Unidad], e.Precio,e.Insumo_ID," +
                    " e.subtotal as [Importe],e.iva as [Iva],e.ieps as [Ieps],e.total as [Total] " +
                    "FROM Ope_Entrada_Partidas e, Cat_Insumos i, Cat_Unidades u WHERE i.Insumo_ID = e.Insumo_ID AND u.Unidad_ID = i.Unidad_ID " + Query + " ORDER BY No_Partida ASC", con))
                {
                    if (Dato.No_Entrada.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.No_Entrada.ToString());
                    
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        internal static DataTable Obtener_Reimpresion(Mdl_Entradas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.No_Entrada.ToString() != "0")
                Query += " AND No_Entrada = @prmId";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT e.Cantidad,i.Nombre as [Insumo],u.Nombre as [Unidad], e.Precio,e.Insumo_ID FROM Ope_Entrada_Partidas e, Cat_Insumos i, Cat_Unidades u WHERE i.Insumo_ID = e.Insumo_ID AND u.Unidad_ID = i.Unidad_ID " + Query + " ORDER BY No_Partida ASC", con))
                {
                    if (Dato.No_Entrada.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.No_Entrada.ToString());

                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        private bool MaterManagementEntrada(Mdl_Entradas Elemento, MODO_DE_CAPTURA Captura)
        {
            string strConexion = Database.BD;
            SqlConnection ObjConexion;
            TransactionScope ts = new TransactionScope();

            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                using (ts)
                {
                    using (SqlConnection Conexion = new SqlConnection(strConexion))
                    {
                        Conexion.Open();
                        Query = "set language español";
                        using (SqlCommand Obj_Comando = Conexion.CreateCommand())
                        {
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.ExecuteNonQuery();
                        }
                        ts.Complete();
                    }
                }



                        Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                ts.Dispose();
                //respuesta = ex.Message;
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        internal static int AltaEntrada(TablaDB Elemento, MODO_DE_CAPTURA Captura, List<Mdl_Entradas_Detalles> Detalles)
        {
            int respuesta = 0;
            Boolean Transaccion = false;
            String Query = String.Empty;
            SqlConnection Obj_Conexion = null;
            SqlCommand Obj_Comando = null;
            TransactionScope ts = new TransactionScope();
            Mdl_Entradas objEntrada;
            try
            {
                objEntrada = (Mdl_Entradas)Elemento;
                //Asignar conexion
                using (ts)
                {
                    Obj_Conexion = new SqlConnection(Database.BD);
                    Obj_Conexion.Open();
                    Obj_Comando = Obj_Conexion.CreateCommand();
                    Query = "set language español";
                    Obj_Comando.CommandText = Query;
                    Obj_Comando.CommandType = CommandType.Text;
                    Obj_Comando.ExecuteNonQuery();


                    Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                    Query += " ; SELECT SCOPE_IDENTITY()";
                    Obj_Comando.CommandText = Query;
                    Obj_Comando.CommandType = CommandType.Text;
                    //Ejecutar consulta
                    object objId = Obj_Comando.ExecuteScalar();
                    int id = Int32.Parse(objId.ToString());
                    respuesta = id;
                    foreach (Mdl_Entradas_Detalles bean in Detalles)
                    {
                        Query = "INSERT INTO Ope_Entrada_Partidas (No_Entrada,Insumo_ID,Cantidad,Precio,unidad_id,subtotal,iva,ieps,total) VALUES (@entrada,@InId,@Cntd,@pre,@uni,@imp,@iva,@ieps,@tot)";
                        Obj_Comando.CommandText = Query;
                        Obj_Comando.Parameters.Add("@entrada", SqlDbType.Int).Value = id;
                        Obj_Comando.Parameters.Add("@InId", SqlDbType.Int).Value = bean.Insumo_ID;
                        Obj_Comando.Parameters.Add("@Cntd", SqlDbType.Decimal).Value = decimal.Parse(bean.Cantidad);
                        Obj_Comando.Parameters.Add("@pre", SqlDbType.Decimal).Value = bean.Precio;
                        Obj_Comando.Parameters.Add("@uni", SqlDbType.Int).Value = bean.Unidad_ID;
                        Obj_Comando.Parameters.Add("@imp", SqlDbType.Decimal).Value = bean.Importe;
                        Obj_Comando.Parameters.Add("@iva", SqlDbType.Decimal).Value = bean.Iva;
                        Obj_Comando.Parameters.Add("@ieps", SqlDbType.Decimal).Value = bean.Ieps;
                        Obj_Comando.Parameters.Add("@tot", SqlDbType.Decimal).Value = bean.Total;

                        Obj_Comando.ExecuteNonQuery();
                        /*=============================================
                        ACTUALIZAR PRECIO ANTERIOR
                        =============================================*/
                        Query = "UPDATE Cat_Insumos SET Precio_Anterior = Costo WHERE Insumo_ID = @InId";
                        Obj_Comando.CommandText = Query;
                        Obj_Comando.ExecuteNonQuery();

                        /*=============================================
                        ACTUALIZAR EXISTENCIAS Y PRECIO
                        =============================================*/
                        Query = "UPDATE Cat_Insumos SET Existencias = Existencias + @Cntd, Costo=@pre WHERE Insumo_ID = @InId";
                        Obj_Comando.CommandText = Query;
                        Obj_Comando.ExecuteNonQuery();
                        /*=============================================
                        se actualiza existencia de insumo en almacen
                        =============================================*/
                        Query = "UPDATE Ope_Almacen_Insumos SET Existencias = Existencias + @Cntd WHERE Insumo_ID = @InId AND Almacen_ID = @almID";
                        Obj_Comando.CommandText = Query;
                        Obj_Comando.Parameters.Add("@almID", SqlDbType.Int).Value = objEntrada.Almacen_ID;
                        int x = Obj_Comando.ExecuteNonQuery();
                        /*=============================================
                        Si x es cero es porque no hay registros de ese 
                        producto en ese almacen y se inserta relacion insumo-almacen
                        =============================================*/
                        if (x <= 0)
                        {
                            Query = "INSERT INTO Ope_Almacen_Insumos VALUES (@almID,@InId,@Cntd)";
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.ExecuteNonQuery();
                        }

                        Obj_Comando.Parameters.Clear();
                    }
                    /*=============================================
                       AFECTACION FACTURAS
                    =============================================*/
                    if (objEntrada.Tipo_Entrada != "OTROS")
                    {
                        Query = "INSERT INTO OPE_FACTURAS_PROVEEDOR (FACTURA_ID,NO_FACTURA,PROVEEDOR_ID,CONCEPTO" +
                        ",TIPO,FECHA,FECHA_RECEPCION,FECHA_VENCIMIENTO,MONEDA,IMPORTE,IVA,DESCUENTO" +
                        ",TOTAL,ABONO,SALDO,PAGADA,CANCELADA,FECHA_PAGO,FORMA_PAGO,TIPO_CAMBIO" +
                        ",ESTATUS,USUARIO_CREO,FECHA_CREO" +
                        ") VALUES ((SELECT isnull(max(factura_id),0)+1 from OPE_FACTURAS_PROVEEDOR),@no_fa,@pro,@con,@tip,@fec,@fecRe,@fecVen,@mon,@imp,@iv,@desc,@tot,@ab,@sal,@pag,@can,@fecPa,@forPa,@tiCa,@est,@usuCre,getdate())";

                        Obj_Comando.Parameters.Add("@no_fa", SqlDbType.VarChar).Value = objEntrada.No_Factura;
                        Obj_Comando.Parameters.Add("@pro", SqlDbType.Int).Value = objEntrada.Proveedor_ID;
                        Obj_Comando.Parameters.Add("@con", SqlDbType.VarChar).Value = "Factura generada por entrada no." + id.ToString();
                        Obj_Comando.Parameters.Add("@tip", SqlDbType.VarChar).Value = objEntrada.Tipo_Factura;
                        Obj_Comando.Parameters.Add("@fec", SqlDbType.DateTime).Value = objEntrada.Fecha_Factura;
                        Obj_Comando.Parameters.Add("@fecRe", SqlDbType.DateTime).Value = objEntrada.Fecha_Recepcion;
                        Obj_Comando.Parameters.Add("@fecVen", SqlDbType.DateTime).Value = objEntrada.Fecha_Pago;
                        Obj_Comando.Parameters.Add("@mon", SqlDbType.VarChar).Value = "MXN";
                        Obj_Comando.Parameters.Add("@imp", SqlDbType.Decimal).Value = objEntrada.Total_Factura/1.16;
                        Obj_Comando.Parameters.Add("@iv", SqlDbType.Decimal).Value = objEntrada.Total_Factura * (0.16);
                        Obj_Comando.Parameters.Add("@desc", SqlDbType.Decimal).Value = "0";
                        Obj_Comando.Parameters.Add("@tot", SqlDbType.Decimal).Value = objEntrada.Total_Factura;
                        Obj_Comando.Parameters.Add("@ab", SqlDbType.Decimal).Value = "0";
                        Obj_Comando.Parameters.Add("@sal", SqlDbType.Decimal).Value = "0";
                        Obj_Comando.Parameters.Add("@pag", SqlDbType.VarChar).Value = "NO";
                        Obj_Comando.Parameters.Add("@can", SqlDbType.VarChar).Value = "NO";
                        Obj_Comando.Parameters.Add("@fecPa", SqlDbType.DateTime).Value = objEntrada.Fecha_Pago;
                        Obj_Comando.Parameters.Add("@forPa", SqlDbType.VarChar).Value = "Efectivo";
                        Obj_Comando.Parameters.Add("@tiCa", SqlDbType.Decimal).Value = "0";
                        Obj_Comando.Parameters.Add("@est", SqlDbType.VarChar).Value = "ACTIVA";
                        Obj_Comando.Parameters.Add("@usuCre", SqlDbType.VarChar).Value = objEntrada.Usuario_Registro;

                        Obj_Comando.CommandText = Query;
                        Obj_Comando.ExecuteNonQuery();
                    }


                    ts.Complete();
                    Transaccion = true;
                    //Cerrar conexion
                    Obj_Conexion.Close();

                }
            }
            catch (Exception Ex)
            {
                ts.Dispose();
                Transaccion = false;
                respuesta = 0;
                throw new Exception(Ex.Message);
            }

            return respuesta;
        }

        internal static Boolean UpdateEntrada(TablaDB Elemento, MODO_DE_CAPTURA Captura, List<Mdl_Entradas_Detalles> Detalles)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            SqlConnection Obj_Conexion = null;
            SqlCommand Obj_Comando = null;
            TransactionScope ts = new TransactionScope();

            try
            {
                //Asignar conexion
                using (ts)
                {
                    Mdl_Entradas objEntrada = (Mdl_Entradas)Elemento;
                    Obj_Conexion = new SqlConnection(Database.BD);
                    Obj_Conexion.Open();
                    Obj_Comando = Obj_Conexion.CreateCommand();
                    Query = "set language español";
                    Obj_Comando.CommandText = Query;
                    Obj_Comando.CommandType = CommandType.Text;
                    Obj_Comando.ExecuteNonQuery();

                    Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                    Obj_Comando.CommandText = Query;
                    Obj_Comando.CommandType = CommandType.Text;
                    //Ejecutar consulta
                    Obj_Comando.ExecuteNonQuery();
                    /*=============================================
                        control de existencias
                        regreso la cantidad de la entrada a los insumos
                    =============================================*/
                    Query = "UPDATE i SET i.Existencias = (i.Existencias - ep.Cantidad)" +
                        " FROM Cat_Insumos i" +
                        " INNER JOIN Ope_Entrada_Partidas ep" +
                        " ON i.Insumo_ID = ep.Insumo_ID" +
                        " WHERE ep.No_Entrada = @NoE";
                    Obj_Comando.CommandText = Query;
                    Obj_Comando.Parameters.Add("@NoE", SqlDbType.Int).Value = objEntrada.No_Entrada;
                    Obj_Comando.ExecuteNonQuery();
                    Query = "UPDATE i SET i.Existencias = (i.Existencias - ep.Cantidad)" +
                        " FROM Ope_Almacen_Insumos i" +
                        " INNER JOIN Ope_Entrada_Partidas ep" +
                        " ON i.Insumo_ID = ep.Insumo_ID AND i.Almacen_ID = @almID" +
                        " WHERE ep.No_Entrada = @NoE";
                    Obj_Comando.CommandText = Query;
                    Obj_Comando.Parameters.Add("@almID", SqlDbType.Int).Value = objEntrada.Almacen_ID;
                    Obj_Comando.ExecuteNonQuery();
                    /*=============================================
                        luego borro los detalles de la entrada
                    =============================================*/
                    Query = "DELETE Ope_Entrada_Partidas WHERE No_Entrada = @NoE";
                    Obj_Comando.CommandText = Query;
                    Obj_Comando.ExecuteNonQuery();
                    Obj_Comando.Parameters.Clear();
                    /*=============================================
                        despues se insertan de nuevo como en el alta
                    =============================================*/
                    if(objEntrada.Estatus!="INACTIVA" && objEntrada.Estatus != "INACTIVO")
                    {
                        foreach (Mdl_Entradas_Detalles bean in Detalles)
                        {
                            Query = "INSERT INTO Ope_Entrada_Partidas (No_Entrada,Insumo_ID,Cantidad,Precio,Fecha_Creo) VALUES (@entrada,@InId,@Cntd,@pre,getdate())";
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.Parameters.Add("@entrada", SqlDbType.Int).Value = objEntrada.No_Entrada;
                            Obj_Comando.Parameters.Add("@InId", SqlDbType.Int).Value = bean.Insumo_ID;
                            Obj_Comando.Parameters.Add("@Cntd", SqlDbType.Decimal).Value = decimal.Parse(bean.Cantidad);
                            Obj_Comando.Parameters.Add("@pre", SqlDbType.Decimal).Value = bean.Precio;
                            Obj_Comando.ExecuteNonQuery();
                            /*=============================================
                            control de existencias
                            se actualiza cat_insumos
                            =============================================*/
                            Query = "UPDATE Cat_Insumos SET Existencias = Existencias + @Cntd WHERE Insumo_ID = @InId";
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.ExecuteNonQuery();
                            /*=============================================
                            se actualiza existencia de insumo en almacen
                            =============================================*/
                            Query = "UPDATE Ope_Almacen_Insumos SET Existencias = Existencias + @Cntd WHERE Insumo_ID = @InId AND Almacen_ID = @almID";
                            Obj_Comando.CommandText = Query;
                            Obj_Comando.Parameters.Add("@almID", SqlDbType.Int).Value = objEntrada.Almacen_ID;
                            int x = Obj_Comando.ExecuteNonQuery();
                            /*=============================================
                            Si x es cero es porque no hay registros de ese 
                            producto en ese almacen y se inserta relacion insumo-almacen
                            =============================================*/
                            if (x <= 0)
                            {
                                Query = "INSERT INTO Ope_Almacen_Insumos VALUES (@almID,@InId,@Cntd)";
                                Obj_Comando.CommandText = Query;
                                Obj_Comando.ExecuteNonQuery();
                            }

                            Obj_Comando.Parameters.Clear();
                        }
                    }
                    else
                    {
                        Query = "UPDATE OPE_FACTURAS_PROVEEDOR set ESTATUS = 'CANCELADA',CANCELADA='SI',FECHA_MODIFICO=GETDATE(),USUARIO_MODIFICO='"+objEntrada.Usuario_Registro+"' WHERE CONCEPTO = 'Factura generada por entrada no." + objEntrada.No_Entrada.ToString() + "'";
                        Obj_Comando.CommandText = Query;
                        Obj_Comando.ExecuteNonQuery();
                        
                    }
                    
                    ts.Complete();
                    Transaccion = true;
                    //Cerrar conexion
                    Obj_Conexion.Close();

                }
            }
            catch (Exception Ex)
            {
                ts.Dispose();
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
    }
}