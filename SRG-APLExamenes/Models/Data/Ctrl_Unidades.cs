﻿using SharpContent.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;
using System.Data;
using System.Data.SqlClient;
namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Unidades
    {
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        internal static DataTable Consulta(Mdl_Unidades Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.Unidad_ID.ToString() != "0")
                Query += " WHERE Unidad_ID = @prmId";
            if (!String.IsNullOrEmpty(Dato.Nombre))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Nombre LIKE @prmNombre";
                else
                    Query += " WHERE Nombre LIKE @prmNombre";
            }
            if (!String.IsNullOrEmpty(Dato.Estatus))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Estatus = @prmEstatus";
                else
                    Query += " WHERE Estatus = @prmEstatus";
            }

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Cat_Unidades" + Query + " ORDER BY Nombre ASC", con))
                {
                    if (Dato.Unidad_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.Unidad_ID.ToString());
                    if (!String.IsNullOrEmpty(Dato.Nombre))
                        cmd.Parameters.AddWithValue("@prmNombre", "%" + Dato.Nombre.Trim() + "%");
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@prmEstatus", "" + Dato.Estatus.Trim() + "");
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}