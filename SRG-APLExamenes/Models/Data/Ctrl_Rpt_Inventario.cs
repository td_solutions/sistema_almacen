﻿using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Rpt_Inventario
    {
        internal static DataTable Consulta_Reorden(Mdl_Rpt_Inventario Dato)
        {
            decimal costo = 0;
            DataTable Dt_Registro = new DataTable();
            string mySQL = String.Empty;
            String Query = String.Empty;


            if (Dato.Almacen_ID.ToString() != "0")
            {
                if (Dato.Almacen_ID.ToString() != "0")
                    Query += " AND ai.Almacen_ID = @prmAlm";
                if (Dato.Insumo_ID.ToString() != "0")
                    Query += " AND ai.Insumo_ID = @prmIns";

                mySQL = "SELECT i.Nombre,i.Descripcion,isnull(i.Codigo_Barras,i.Insumo_ID) AS [Codigo_Barras],ai.Existencias, u.Nombre as [Unidad],i.Ubicacion, i.Minimo, i.Maximo, i.Maximo - ai.Existencias as [Sugerido], i.Costo, (i.Maximo - ai.Existencias) * i.costo as Total_Costo " +
                    "FROM Ope_Almacen_Insumos ai JOIN Cat_Insumos i " +
                    " ON ai.Insumo_ID = i.Insumo_ID " +
                    " JOIN Cat_Unidades u ON i.Unidad_ID = u.Unidad_ID" +
                    " WHERE i.Estatus != 'INACTIVO' AND ai.Existencias <= i.Punto_Reorden " + Query + " ORDER BY i.Nombre";

            }

            else
            {
                if (Dato.Insumo_ID.ToString() != "0")
                    Query += " AND i.Insumo_ID = @prmIns";

                mySQL = "SELECT i.Nombre,i.Descripcion,isnull(i.Codigo_Barras,i.Insumo_ID) AS [Codigo_Barras],i.Existencias, u.Nombre as [Unidad], i.Ubicacion, i.Minimo, i.Maximo, i.Maximo - i.Existencias as [Sugerido], i.Costo, (i.Maximo - i.Existencias) * i.costo as Total_Costo  " +
                       "FROM Cat_Insumos i " +
                       " JOIN Cat_Unidades u ON i.Unidad_ID = u.Unidad_ID" +
                       " JOIN Cat_Tipo_Insumo ti ON i.Tipo_Insumo_ID = ti.Tipo_Insumo_ID" +
                       " WHERE i.Estatus != 'INACTIVO' AND i.Existencias <= i.Punto_Reorden " + Query + " ORDER BY ti.Nombre,i.Nombre";
            }


            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(mySQL, con))
                {
                    if (Dato.Insumo_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmIns", Dato.Insumo_ID.ToString());
                    if (Dato.Almacen_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmAlm", Dato.Almacen_ID.ToString());

                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        internal static DataTable Consulta(Mdl_Rpt_Inventario Dato)
        {
            decimal costo = 0;
            DataTable Dt_Registro = new DataTable();
            string mySQL = String.Empty;
            String Query = String.Empty;
            

            if (Dato.Almacen_ID.ToString() != "0")
            {
                if (Dato.Almacen_ID.ToString() != "0")
                    Query += " AND ai.Almacen_ID = @prmAlm";
                if (Dato.Insumo_ID.ToString() != "0")
                    Query += " AND ai.Insumo_ID = @prmIns";

                mySQL = "SELECT i.Nombre,i.Descripcion,isnull(i.Codigo_Barras,i.Insumo_ID) AS [Codigo_Barras],ai.Existencias, u.Nombre as [Unidad], i.Total as Costo, (( i.Maximo - ISNULL( ai.Existencias, 0 ) ) * i.Total) as [Total_Costo],i.Tipo_Insumo_ID,ti.Nombre as [Tipo_Insumo], i.Maximo, ( i.Maximo - ISNULL( ai.Existencias, 0 ) ) as [Sugerido] " +
                    "FROM Ope_Almacen_Insumos ai JOIN Cat_Insumos i " +
                    " ON ai.Insumo_ID = i.Insumo_ID " +
                    " JOIN Cat_Unidades u ON i.Unidad_ID = u.Unidad_ID" +
                    " JOIN Cat_Tipo_Insumo ti ON i.Tipo_Insumo_ID = ti.Tipo_Insumo_ID" +
                    " WHERE i.Estatus != 'INACTIVO' " + Query + " ORDER BY ti.Nombre, i.Nombre";

            }

            else
            {
                if (Dato.Insumo_ID.ToString() != "0")
                    Query += " AND i.Insumo_ID = @prmIns";

                mySQL = "SELECT i.Nombre,i.Descripcion,isnull(i.Codigo_Barras,i.Insumo_ID) AS [Codigo_Barras],i.Existencias, u.Nombre as [Unidad], i.Total as Costo, (( i.Maximo - ISNULL( i.Existencias, 0 ) ) * i.Total) as [Total_Costo] ,i.Tipo_Insumo_ID,ti.Nombre as [Tipo_Insumo], ( i.Maximo - i.Existencias) as [Sugerido] " +
                       "FROM Cat_Insumos i " +
                       " JOIN Cat_Unidades u ON i.Unidad_ID = u.Unidad_ID" +
                       " JOIN Cat_Tipo_Insumo ti ON i.Tipo_Insumo_ID = ti.Tipo_Insumo_ID" +
                       " WHERE i.Estatus != 'INACTIVO' " + Query + " ORDER BY ti.Nombre,i.Nombre";
            }
                

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(mySQL, con))
                {
                    if (Dato.Insumo_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmIns", Dato.Insumo_ID.ToString());
                    if (Dato.Almacen_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmAlm", Dato.Almacen_ID.ToString());

                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        internal static DataTable Consulta_Detalles(Mdl_Rpt_Inventario Dato)
        {
            DataTable Dt_Registro = new DataTable();
            //String Query = String.Empty;
            //if (Dato.No_Salida.ToString() != "0")
            //    Query += " AND No_Salida = @prmId";

            //using (SqlConnection con = new SqlConnection(Database.BD))
            //{
            //    con.Open();
            //    using (SqlCommand cmd = new SqlCommand("SELECT sd.Cantidad,i.Nombre as [Insumo],u.Nombre as [Unidad], sd.Precio,e.Insumo_ID FROM Ope_Salidas_Detalles sd, Cat_Insumos i, Cat_Unidades u WHERE i.Insumo_ID = e.Insumo_ID AND u.Unidad_ID = i.Unidad_ID " + Query + " ORDER BY No_Partida ASC", con))
            //    {
            //        if (Dato.No_Salida.ToString() != "0")
            //            cmd.Parameters.AddWithValue("@prmId", Dato.No_Salida.ToString());

            //        using (SqlDataReader dataReader = cmd.ExecuteReader())
            //        {
            //            Dt_Registro.Load(dataReader);
            //        }
            //    }
            //}
            return Dt_Registro;
        }
    }
}