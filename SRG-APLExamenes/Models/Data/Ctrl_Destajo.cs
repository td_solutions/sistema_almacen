﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Destajo
    {
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: MasterManagement
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Checkoperation
        ///DESCRIPCIÓN: CONSULTAR  OPERACIONES
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable Consultevent(Mdl_Destajo Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;

            Query = " SELECT Ope_Destajo.*, ISNULL(Cat_Empleados.Nombre, '') AS Empleado, ISNULL(Cat_Tablas.Nombre, '') AS Tabla, Cat_Tipo_Destajo.Nombre AS Tipo_Destajo" 
                  + " FROM Ope_Destajo LEFT JOIN Cat_Empleados"
                  + " ON Ope_Destajo.No_Empleado = Cat_Empleados.No_Empleado"
                  + " LEFT JOIN Cat_Tablas ON Ope_Destajo.Tabla_ID = Cat_Tablas.Tabla_ID, Cat_Tipo_Destajo"
                  + " WHERE Ope_Destajo.Tipo_Destajo_ID = Cat_Tipo_Destajo.Tipo_Destajo_ID";

            if (!String.IsNullOrEmpty(Dato.No_Destajo))
                Query += " AND Ope_Destajo.No_Destajo = @Eve";
            if (!String.IsNullOrEmpty(Dato.No_Tarjeta))
                Query += " AND Ope_Destajo.No_Tarjeta = @No_Tarjeta";
            if (!String.IsNullOrEmpty(Dato.Tipo_Destajo_ID))
                Query += " AND Ope_Destajo.Tipo_Destajo_ID = @Tipo_Destajo";
            if (!String.IsNullOrEmpty(Dato.Estatus))
                Query += " AND Ope_Destajo.Estatus = @Estatus";
            if (Dato.Cantidad_Empleados > 1)
                Query += " AND ISNULL(Cantidad_Empleados, 0) > 1";
            if (Dato.Cantidad_Empleados == 1)
                Query += " AND ISNULL(Cantidad_Empleados, 0) = 1";
            if (!String.IsNullOrEmpty(Dato.Tabla_ID))
                Query += " AND Ope_Destajo.No_Nota IS NULL";
            if (!String.IsNullOrEmpty(Dato.No_Nota))
                Query += " AND Ope_Destajo.No_Nota = @No_Nota";
            if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Ope_Destajo.Fecha >= '" + Dato.Fecha_Inicio + " 00:00:00'";
                else
                    Query += " WHERE Ope_Destajo.Fecha >= '" + Dato.Fecha_Inicio + " 00:00:00'";
            }
            if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Ope_Destajo.Fecha <= '" + Dato.Fecha_Fin + " 23:59:59'";
                else
                    Query += " WHERE Ope_Destajo.Fecha <= '" + Dato.Fecha_Fin + " 23:59:59'";
            }
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(Query + " ORDER BY Ope_Destajo.No_Destajo DESC", con))
                {
                    if (!String.IsNullOrEmpty(Dato.No_Destajo))
                        cmd.Parameters.AddWithValue("@Eve", Dato.No_Destajo);
                    if (!String.IsNullOrEmpty(Dato.No_Tarjeta))
                        cmd.Parameters.AddWithValue("@No_Tarjeta", Dato.No_Tarjeta);
                    if (!String.IsNullOrEmpty(Dato.Tipo_Destajo_ID))
                        cmd.Parameters.AddWithValue("@Tipo_Destajo", Dato.Tipo_Destajo_ID);
                    if (!String.IsNullOrEmpty(Dato.No_Nota))
                        cmd.Parameters.AddWithValue("@No_Nota", Dato.No_Nota);
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@Estatus", Dato.Estatus);
                    //if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
                    //    cmd.Parameters.AddWithValue("@fechaInicio", Dato.Fecha_Inicio);
                    //if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
                    //    cmd.Parameters.AddWithValue("@fechaFin", Dato.Fecha_Fin);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: ConsultAgrupadoTDCorte
        ///DESCRIPCIÓN: CONSULTAR  OPERACIONES
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable ConsultAgrupadoTDCorte(Mdl_Destajo Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;

            Query = " SELECT Cat_Tipo_Destajo.Tipo_Destajo_ID, Cat_Tipo_Destajo.Nombre AS Tipo_Destajo, SUM(Cantidad_Empleados) AS Cantidad_Empleados, SUM(Cantidad) AS Cantidad, SUM(Total_Pago) AS Total_Pago"
                  + " FROM Ope_Destajo, Cat_Tipo_Destajo"
                  + " WHERE Ope_Destajo.Tipo_Destajo_ID = Cat_Tipo_Destajo.Tipo_Destajo_ID"                  
                  + " AND ISNULL(Cantidad_Empleados, 0) > 0";

            if (!String.IsNullOrEmpty(Dato.No_Nota))
                Query += " AND Ope_Destajo.No_Nota = @No_Nota";

            if (!String.IsNullOrEmpty(Dato.Tabla_ID))
                Query += " AND Ope_Destajo.No_Nota IS NULL";

            if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Ope_Destajo.Fecha >= '" + Dato.Fecha_Inicio + " 00:00:00'";
                else
                    Query += " WHERE Ope_Destajo.Fecha >= '" + Dato.Fecha_Inicio + " 00:00:00'";
            }
            if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Ope_Destajo.Fecha <= '" + Dato.Fecha_Fin + " 23:59:59'";
                else
                    Query += " WHERE Ope_Destajo.Fecha <= '" + Dato.Fecha_Fin + " 23:59:59'";
            }
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(Query + " GROUP BY Cat_Tipo_Destajo.Tipo_Destajo_ID, Cat_Tipo_Destajo.Nombre", con))
                {
                    if (!String.IsNullOrEmpty(Dato.No_Nota))
                        cmd.Parameters.AddWithValue("@No_Nota", Dato.No_Nota);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: ConsultMaximoDestajo
        ///DESCRIPCIÓN: CONSULTAR  LOS EMPLEADOS DEL PORTAL
        ///PARAMETROS:  
        ///CREO:       David Herrera Rincon
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable ConsultMaximoDestajo(Mdl_Destajo Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(No_Destajo), 0) + 1 AS No_Destajo FROM Ope_Destajo", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}