﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Movimientos_Pagos
    {///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: MasterManagement
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Checkoperation
        ///DESCRIPCIÓN: CONSULTAR  OPERACIONES
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable Consultevent(Mdl_Movimientos_Pagos Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;

            Query = " SELECT OPE_MOVIMIENTOS_PAGOS.*, Cat_Proveedores.Nombre AS Proveedor"
                  + " FROM OPE_MOVIMIENTOS_PAGOS, Cat_Proveedores"
                  + " WHERE OPE_MOVIMIENTOS_PAGOS.PROVEEDOR_ID = Cat_Proveedores.Proveedor_ID";

            if (!String.IsNullOrEmpty(Dato.NO_MOVIMIENTO))
                Query += " AND OPE_MOVIMIENTOS_PAGOS.NO_MOVIMIENTO = @Eve";
            if (!String.IsNullOrEmpty(Dato.NO_FACTURA_PROVEEDOR))
                Query += " AND OPE_MOVIMIENTOS_PAGOS.NO_FACTURA_PROVEEDOR = @No_Factura";
            if (!String.IsNullOrEmpty(Dato.PROVEEDOR_ID))
                Query += " AND OPE_MOVIMIENTOS_PAGOS.PROVEEDOR_ID = @Proveedor_ID";
            if (!String.IsNullOrEmpty(Dato.ESTATUS))
                Query += " AND OPE_MOVIMIENTOS_PAGOS.Estatus IN (" + Dato.ESTATUS + ")";
            if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
                Query += " AND OPE_MOVIMIENTOS_PAGOS.Fecha >= '" + Dato.Fecha_Inicio + " 00:00:00'";
            if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
                Query += " AND OPE_MOVIMIENTOS_PAGOS.Fecha <= '" + Dato.Fecha_Fin + " 23:59:59'";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(Query + " ORDER BY OPE_MOVIMIENTOS_PAGOS.FECHA DESC", con))
                {
                    if (!String.IsNullOrEmpty(Dato.NO_MOVIMIENTO))
                        cmd.Parameters.AddWithValue("@Eve", Dato.NO_MOVIMIENTO);
                    if (!String.IsNullOrEmpty(Dato.NO_FACTURA_PROVEEDOR))
                        cmd.Parameters.AddWithValue("@No_Factura", Dato.NO_FACTURA_PROVEEDOR);
                    if (!String.IsNullOrEmpty(Dato.PROVEEDOR_ID))
                        cmd.Parameters.AddWithValue("@Proveedor_ID", Dato.PROVEEDOR_ID);
                    //if (!String.IsNullOrEmpty(Dato.Estatus))
                    //    cmd.Parameters.AddWithValue("@Estatus", Dato.Estatus);
                    //if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
                    //    cmd.Parameters.AddWithValue("@fechaInicio", Dato.Fecha_Inicio);
                    //if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
                    //    cmd.Parameters.AddWithValue("@fechaFin", Dato.Fecha_Fin);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: ConsultMaximoDestajo
        ///DESCRIPCIÓN: CONSULTAR  LOS EMPLEADOS DEL PORTAL
        ///PARAMETROS:  
        ///CREO:       David Herrera Rincon
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable ConsultMaximoMovimiento(Mdl_Movimientos_Pagos Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(NO_MOVIMIENTO), 0) + 1 AS NO_MOVIMIENTO FROM OPE_MOVIMIENTOS_PAGOS", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}