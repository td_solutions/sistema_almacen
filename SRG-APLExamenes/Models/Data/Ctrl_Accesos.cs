﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Models.Assistant;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Accesos
    {
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Consultaccess
        ///DESCRIPCIÓN: CONSULTAR  PARAMETROS
        ///PARAMETROS:  
        ///CREO:       FRANCISCO JAVIER BECERRA TOLEDO
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable Consultaccess(Mdl_Accesos Dato)
        {
            DataTable Dt_Registro = new DataTable();
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Apl_Accesos WHERE No_Empleado = @No_Empleado Order By Menu_ID", con))
                {
                    cmd.Parameters.AddWithValue("@No_Empleado", Dato.No_Empleado);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
                con.Close();
            }
            return Dt_Registro;
        }
    }
}