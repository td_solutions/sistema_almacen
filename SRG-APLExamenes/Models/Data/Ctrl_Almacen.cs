﻿using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Almacen
    {
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        internal static DataTable Consulta(Mdl_Almacen Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.Almacen_ID.ToString() != "0")
                Query += " AND alm.Almacen_ID = @prmId";
            if (!String.IsNullOrEmpty(Dato.Nombre))
                Query += " AND alm.Nombre LIKE @prmNombre";
            if (!String.IsNullOrEmpty(Dato.Clave))
                Query += " AND alm.Clave LIKE @prmClave";
            if (!String.IsNullOrEmpty(Dato.Estatus))
                Query += " AND alm.Estatus = @prmEstatus";
            if (Dato.Rancho_ID.ToString() != "0")
                Query += " AND alm.Rancho_ID = @prmRancho";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT r.Nombre as [Rancho] ,alm.* FROM Cat_Almacen alm,Cat_Ranchos r WHERE r.Rancho_ID = alm.Rancho_ID " + Query + " ORDER BY Nombre ASC", con))
                {
                    if (Dato.Almacen_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.Almacen_ID.ToString());
                    if (!String.IsNullOrEmpty(Dato.Nombre))
                        cmd.Parameters.AddWithValue("@prmNombre", "%" + Dato.Nombre.Trim() + "%");
                    if (!String.IsNullOrEmpty(Dato.Clave))
                        cmd.Parameters.AddWithValue("@prmClave", "" + Dato.Clave.Trim() + "");
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@prmEstatus", "" + Dato.Estatus.Trim() + "");
                    if (Dato.Rancho_ID > 0)
                        cmd.Parameters.AddWithValue("@prmRancho", "" + Dato.Rancho_ID.ToString() + "");
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}