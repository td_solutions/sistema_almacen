﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Labores
    {
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: MasterManagement
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Checkoperation
        ///DESCRIPCIÓN: CONSULTAR  OPERACIONES
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable Consultevent(Mdl_Labores Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (!String.IsNullOrEmpty(Dato.Labor_ID))
                Query += " WHERE Labor_ID = @Eve";
            if (!String.IsNullOrEmpty(Dato.Nombre))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Nombre LIKE @eventCliente";
                else
                    Query += " WHERE Nombre LIKE @eventCliente";
            }
            if (!String.IsNullOrEmpty(Dato.Estatus))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Estatus = @eventEstatus";
                else
                    Query += " WHERE Estatus = @eventEstatus";
            }

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Cat_Labores" + Query + " ORDER BY Nombre ASC", con))
                {
                    if (!String.IsNullOrEmpty(Dato.Labor_ID))
                        cmd.Parameters.AddWithValue("@Eve", Dato.Labor_ID);
                    if (!String.IsNullOrEmpty(Dato.Nombre))
                        cmd.Parameters.AddWithValue("@eventCliente", "%" + Dato.Nombre.Trim() + "%");
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@eventEstatus", "" + Dato.Estatus.Trim() + "");
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: ConsultMaximoTipoDestajo
        ///DESCRIPCIÓN: CONSULTAR  LOS EMPLEADOS DEL PORTAL
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable ConsultMaximoLabores(Mdl_Labores Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(Labor_ID), 0) + 1 AS Labor_ID FROM Cat_Labores", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}