﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Facturas
    {
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: MasterManagement
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Checkoperation
        ///DESCRIPCIÓN: CONSULTAR  OPERACIONES
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable Consultevent(Mdl_Facturas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;

            Query = " SELECT OPE_FACTURAS_PROVEEDOR.*, Cat_Proveedores.Nombre AS Proveedor, ISNULL((SELECT top 1 no_entrada FROM Ope_Entradas WHERE no_factura = OPE_FACTURAS_PROVEEDOR.NO_FACTURA AND proveedor_id = OPE_FACTURAS_PROVEEDOR.PROVEEDOR_ID AND tipo_factura = TIPO), '') AS No_Entrada"
                  + " FROM OPE_FACTURAS_PROVEEDOR, Cat_Proveedores"
                  + " WHERE OPE_FACTURAS_PROVEEDOR.PROVEEDOR_ID = Cat_Proveedores.Proveedor_ID";                  

            if (!String.IsNullOrEmpty(Dato.FACTURA_ID))
                Query += " AND OPE_FACTURAS_PROVEEDOR.FACTURA_ID = @Eve";
            if (!String.IsNullOrEmpty(Dato.NO_FACTURA))
                Query += " AND OPE_FACTURAS_PROVEEDOR.NO_FACTURA = @No_Factura";
            if (!String.IsNullOrEmpty(Dato.PROVEEDOR_ID))
                Query += " AND OPE_FACTURAS_PROVEEDOR.PROVEEDOR_ID = @Proveedor_ID";
            if (!String.IsNullOrEmpty(Dato.ESTATUS))
                Query += " AND OPE_FACTURAS_PROVEEDOR.Estatus IN (" + Dato.ESTATUS + ")";
            if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
                Query += " AND OPE_FACTURAS_PROVEEDOR.Fecha >= @fechaInicio";                
            if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
                Query += " AND OPE_FACTURAS_PROVEEDOR.Fecha <= @fechaFin";
            if (!String.IsNullOrEmpty(Dato.Fecha_Saldos_Inicio))
                Query += " AND OPE_FACTURAS_PROVEEDOR.FECHA_VENCIMIENTO >= @fechaSalInicio";
            if (!String.IsNullOrEmpty(Dato.Fecha_Saldos_Fin))
                Query += " AND OPE_FACTURAS_PROVEEDOR.FECHA_VENCIMIENTO <= @fechaSalFin";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(Query + " ORDER BY OPE_FACTURAS_PROVEEDOR.FECHA DESC", con))
                {
                    if (!String.IsNullOrEmpty(Dato.FACTURA_ID))
                        cmd.Parameters.AddWithValue("@Eve", Dato.FACTURA_ID);
                    if (!String.IsNullOrEmpty(Dato.NO_FACTURA))
                        cmd.Parameters.AddWithValue("@No_Factura", Dato.NO_FACTURA);
                    if (!String.IsNullOrEmpty(Dato.PROVEEDOR_ID))
                        cmd.Parameters.AddWithValue("@Proveedor_ID", Dato.PROVEEDOR_ID);
                    //if (!String.IsNullOrEmpty(Dato.Estatus))
                    //    cmd.Parameters.AddWithValue("@Estatus", Dato.Estatus);
                    if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
                        cmd.Parameters.AddWithValue("@fechaInicio", Dato.Fecha_Inicio);
                    if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
                        cmd.Parameters.AddWithValue("@fechaFin", Dato.Fecha_Fin);
                    if (!String.IsNullOrEmpty(Dato.Fecha_Saldos_Inicio))
                        cmd.Parameters.AddWithValue("@fechaSalInicio", Dato.Fecha_Saldos_Inicio);
                    if (!String.IsNullOrEmpty(Dato.Fecha_Saldos_Fin))
                        cmd.Parameters.AddWithValue("@fechaSalFin", Dato.Fecha_Saldos_Fin);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: ConsultMaximoDestajo
        ///DESCRIPCIÓN: CONSULTAR  LOS EMPLEADOS DEL PORTAL
        ///PARAMETROS:  
        ///CREO:       David Herrera Rincon
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable ConsultMaximoFactura(Mdl_Facturas Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(FACTURA_ID), 0) + 1 AS FACTURA_ID FROM OPE_FACTURAS_PROVEEDOR", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}