﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Actividades
    {
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: MasterManagement
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Checkoperation
        ///DESCRIPCIÓN: CONSULTAR  OPERACIONES
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable Consultevent(Mdl_Actividades Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (!String.IsNullOrEmpty(Dato.No_Actividad))
                Query += " WHERE No_Actividad = @Eve";
            if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Fecha >= '" + Dato.Fecha_Inicio + " 00:00:00'";
                else
                    Query += " WHERE Fecha >= '" + Dato.Fecha_Inicio + " 00:00:00'";
            }
            if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Fecha <= '" + Dato.Fecha_Fin + " 23:59:59'";
                else
                    Query += " WHERE Fecha <= '" + Dato.Fecha_Fin + " 23:59:59'";
            }
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT *, (SELECT SUM(No_Tuneles) FROM Ope_Actividades_Tablas WHERE No_Actividad = Ope_Actividades.No_Actividad) / (SELECT COUNT(No_Tarjeta) FROM Ope_Actividades_Detalles WHERE No_Actividad = Ope_Actividades.No_Actividad) AS Avance FROM Ope_Actividades" + Query + " ORDER BY No_Actividad DESC", con))
                {
                    if (!String.IsNullOrEmpty(Dato.No_Actividad))
                        cmd.Parameters.AddWithValue("@Eve", Dato.No_Actividad);
                    //if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
                    //    cmd.Parameters.AddWithValue("@fechaInicio", Dato.Fecha_Inicio);
                    //if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
                    //    cmd.Parameters.AddWithValue("@fechaFin", Dato.Fecha_Fin);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: ConsultMaximoDestajo
        ///DESCRIPCIÓN: CONSULTAR  LOS EMPLEADOS DEL PORTAL
        ///PARAMETROS:  
        ///CREO:       David Herrera Rincon
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable ConsultMaximoActividad(Mdl_Actividades Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(No_Actividad), 0) + 1 AS No_Actividad FROM Ope_Actividades", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}