﻿using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Rpt_Kardex
    {
        internal static DataTable Consulta(Mld_Rpt_Kardex Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT i.Insumo_ID, i.Codigo_barras, i.Nombre, e.no_entrada AS NumMovimiento, e.fecha_entrada AS Fecha, " +
                                    "'ENTRADA' AS Movimiento, e.tipo_entrada AS TipoMovimiento, op.Cantidad AS Unidades, op.Cantidad auxUnidad, " +
                                    "0 AS PrecioVenta, (op.Precio + op.iva + op.ieps) AS CostoCompra, (op.Cantidad * (op.Precio + op.iva + op.ieps))  AS Total " +
                                    "FROM Cat_Insumos i " +
                                    "JOIN Ope_Entrada_Partidas op ON i.Insumo_ID = op.Insumo_ID " +
                                    "JOIN Ope_Entradas e ON op.No_Entrada = e.no_entrada " +
                                    "WHERE e.fecha_entrada BETWEEN '" + Dato.Fecha_Inicio.ToString("yyyyMMdd") + "' AND '" + Dato.Fecha_Fin.ToString("yyyyMMdd") + "' " +
                                    "AND i.Insumo_ID = " + Dato.Insumo_ID + " " +
                                    "AND e.estatus = 'ACTIVO' " +
                                    "UNION ALL " +
                                    "SELECT i.Insumo_ID, i.Codigo_barras, i.Nombre, s.No_Salida as NumMovimiento, " +
                                    "s.fecha AS Fecha, 'SALIDA' AS Movimiento, s.Tipo AS TipoMovimiento, os.Cantidad AS Unidades, (os.Cantidad * -1) AS auxUnidad, "+
                                    "(os.Precio + os.iva + os.ieps) AS PrecioVenta, 0 AS CostoCompra, (os.Cantidad * (os.Precio + os.iva + os.ieps))  AS Total " +
                                    "FROM Cat_Insumos i " +
                                    "JOIN Ope_Salidas_Detalles os ON i.Insumo_ID = os.Insumo_ID "+
                                    "JOIN Ope_Salidas s ON os.No_Salida = s.No_Salida " +
                                    "WHERE s.Fecha BETWEEN '" + Dato.Fecha_Inicio.ToString("yyyyMMdd") + "' AND '" + Dato.Fecha_Fin.ToString("yyyyMMdd") + "' " +
                                    "AND i.Insumo_ID = " + Dato.Insumo_ID + " " +
                                    "AND s.estatus = 'ACTIVO' " +
                                    "ORDER BY Fecha DESC", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        internal static DataTable ConsultarReporteDetalles(Mld_Rpt_Kardex Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT i.Insumo_ID, i.Codigo_barras, i.Nombre, e.no_entrada AS NumMovimiento, e.fecha_entrada AS Fecha, " +
                                    "'ENTRADA' AS Movimiento, e.tipo_entrada AS TipoMovimiento, op.Cantidad AS Unidades, op.Cantidad auxUnidad, " +
                                    "0 AS PrecioVenta, (op.Precio + op.iva + op.ieps) AS CostoCompra, (op.Cantidad * (op.Precio + op.iva + op.ieps))  AS Total " +
                                    "FROM Cat_Insumos i " +
                                    "JOIN Ope_Entrada_Partidas op ON i.Insumo_ID = op.Insumo_ID " +
                                    "JOIN Ope_Entradas e ON op.No_Entrada = e.no_entrada " +
                                    "WHERE e.fecha_entrada BETWEEN '" + Dato.Fecha_Inicio.ToString("yyyyMMdd") + "' AND '" + Dato.Fecha_Fin.ToString("yyyyMMdd") + "' " +
                                    "AND i.Insumo_ID = " + Dato.Insumo_ID + " " +
                                    "AND e.estatus = 'ACTIVO' " +
                                    "UNION ALL " +
                                    "SELECT i.Insumo_ID, i.Codigo_barras, i.Nombre, s.No_Salida as NumMovimiento, " +
                                    "s.fecha AS Fecha, 'SALIDA' AS Movimiento, s.Tipo AS TipoMovimiento, os.Cantidad AS Unidades, (os.Cantidad * -1) AS auxUnidad, " +
                                    "(os.Precio + os.iva + os.ieps) AS PrecioVenta, 0 AS CostoCompra, (os.Cantidad * (os.Precio + os.iva + os.ieps))  AS Total " +
                                    "FROM Cat_Insumos i " +
                                    "JOIN Ope_Salidas_Detalles os ON i.Insumo_ID = os.Insumo_ID " +
                                    "JOIN Ope_Salidas s ON os.No_Salida = s.No_Salida " +
                                    "WHERE s.Fecha BETWEEN '" + Dato.Fecha_Inicio.ToString("yyyyMMdd") + "' AND '" + Dato.Fecha_Fin.ToString("yyyyMMdd") + "' " +
                                    "AND i.Insumo_ID = " + Dato.Insumo_ID + " " +
                                    "AND s.estatus = 'ACTIVO' " +
                                    "ORDER BY Fecha ASC", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}