﻿using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Insumos
    {
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        internal static DataTable Consulta(Mdl_Insumos Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (Dato.Insumo_ID.ToString() != "0")
                Query += " AND i.Insumo_ID = @prmId";
            //if (Dato.Almacen_ID.ToString() != "0")
            //    Query += " AND i.Almacen = @almId";
            if (!String.IsNullOrEmpty(Dato.Nombre))
                    Query += " AND i.Nombre LIKE @prmNombre";
            if (!String.IsNullOrEmpty(Dato.Estatus))
                    Query += " AND i.Estatus = @prmEstatus";
            if (Dato.Tipo_Insumo_ID.ToString() != "0")
                Query += " AND i.Tipo_Insumo_ID = @prmTipoIns";

            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ti.Nombre as [Tipo_Insumo], u.Nombre as [Unidad], uc.Nombre as [Unidad_Consumo], p.Nombre as [Proveedor], i.* FROM Cat_Insumos i " +
                    " LEFT OUTER JOIN Cat_Tipo_Insumo ti ON i.Tipo_Insumo_ID = ti.Tipo_Insumo_ID " +
                    " LEFT OUTER JOIN Cat_Unidades u ON i.Unidad_ID = u.Unidad_ID " +
                    " LEFT OUTER JOIN Cat_Unidades uc ON i.Unidad_Consumo_ID = uc.Unidad_ID " +
                    " LEFT OUTER JOIN Cat_Proveedores p ON i.Proveedor_ID = p.Proveedor_ID WHERE 1=1 " +
                    "" + Query + " ORDER BY Nombre ASC", con))
                {
                    if (Dato.Insumo_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@prmId", Dato.Insumo_ID.ToString());
                    /*if (Dato.Almacen_ID.ToString() != "0")
                        cmd.Parameters.AddWithValue("@almId", Dato.Almacen_ID.ToString());*/
                    if (!String.IsNullOrEmpty(Dato.Nombre))
                        cmd.Parameters.AddWithValue("@prmNombre", "%" + Dato.Nombre.Trim() + "%");
                    if (!String.IsNullOrEmpty(Dato.Estatus))
                        cmd.Parameters.AddWithValue("@prmEstatus", "" + Dato.Estatus.Trim() + "");
                    if (Dato.Tipo_Insumo_ID>0)
                        cmd.Parameters.AddWithValue("@prmTipoIns", "" + Dato.Tipo_Insumo_ID.ToString() + "");
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}