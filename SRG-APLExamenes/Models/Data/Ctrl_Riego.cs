﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Riego
    {
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: MasterManagement
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Checkoperation
        ///DESCRIPCIÓN: CONSULTAR  OPERACIONES
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable Consultevent(Mdl_Riego Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            if (!String.IsNullOrEmpty(Dato.No_Riego))
                Query += " WHERE Ope_Riego.No_Riego = @Eve";
            if (!String.IsNullOrEmpty(Dato.Fecha))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Ope_Riego.Fecha = @fecha";
                else
                    Query += " WHERE Ope_Riego.Fecha = @fecha";
            }
            if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Ope_Riego.Fecha >= '" + Dato.Fecha_Inicio + " 00:00:00'";
                else
                    Query += " WHERE Ope_Riego.Fecha >= '" + Dato.Fecha_Inicio + " 00:00:00'";
            }
            if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
            {
                if (Query.Contains("WHERE"))
                    Query += " AND Ope_Riego.Fecha <= '" + Dato.Fecha_Fin + " 23:59:59'";
                else
                    Query += " WHERE Ope_Riego.Fecha <= '" + Dato.Fecha_Fin + " 23:59:59'";
            }
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Ope_Riego" + Query + " ORDER BY Ope_Riego.No_Riego DESC", con))
                {
                    if (!String.IsNullOrEmpty(Dato.No_Riego))
                        cmd.Parameters.AddWithValue("@Eve", Dato.No_Riego);
                    if (!String.IsNullOrEmpty(Dato.Fecha))
                        cmd.Parameters.AddWithValue("@fecha", Dato.Fecha);
                    //if (!String.IsNullOrEmpty(Dato.Fecha_Inicio))
                    //    cmd.Parameters.AddWithValue("@fechaInicio", Dato.Fecha_Inicio);
                    //if (!String.IsNullOrEmpty(Dato.Fecha_Fin))
                    //    cmd.Parameters.AddWithValue("@fechaFin", Dato.Fecha_Fin);
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: ConsultAcidoRiego
        ///DESCRIPCIÓN: CONSULTAR  LOS EMPLEADOS DEL PORTAL
        ///PARAMETROS:  
        ///CREO:       David Herrera Rincon
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable ConsultAcidoRiego(Mdl_Riego Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT TOP 1 * FROM Ope_Riego ORDER BY No_Riego DESC", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: ConsultMaximoDestajo
        ///DESCRIPCIÓN: CONSULTAR  LOS EMPLEADOS DEL PORTAL
        ///PARAMETROS:  
        ///CREO:       David Herrera Rincon
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static DataTable ConsultMaximoRiego(Mdl_Riego Dato)
        {
            DataTable Dt_Registro = new DataTable();
            String Query = String.Empty;
            using (SqlConnection con = new SqlConnection(Database.BD))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(No_Riego), 0) + 1 AS No_Riego FROM Ope_Riego", con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        Dt_Registro.Load(dataReader);
                    }
                }
            }
            return Dt_Registro;
        }
    }
}