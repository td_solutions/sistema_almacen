﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using SharpContent.ApplicationBlocks.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;
using System.Transactions;

namespace SRG_APLExamenes.Models.Data
{
    public class Ctrl_Operaciones
    {
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: MasterManagement
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       FRANCISCO JAVIER BECERRA TOLEDO
        ///FECHA_CREO:  
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        internal static Boolean MasterManagement(TablaDB Elemento, MODO_DE_CAPTURA Captura)
        {
            Boolean Transaccion = false;
            String Query = String.Empty;
            try
            {
                Query = QuerySQL.GeneraCMDExecGeneral(Elemento, Captura) + "\n";
                SqlHelper.ExecuteNonQuery(Database.BD, CommandType.Text, Query);
                Transaccion = true;
            }
            catch (Exception Ex)
            {
                Transaccion = false;
                throw new Exception(Ex.Message);
            }

            return Transaccion;
        }

        

        public string obtenerRutaParaGuardar(String sFileName)
        {
            string respuesta = "";

            if (!Path.IsPathRooted(sFileName))
                sFileName = Path.Combine(Path.GetTempPath(), sFileName);
            if (System.IO.File.Exists(sFileName))
            {
                String sDateTime = DateTime.Now.ToString("yyyyMMdd\\_HHmmss");
                String s = Path.GetFileNameWithoutExtension(sFileName) + "_" + sDateTime + Path.GetExtension(sFileName);
                sFileName = Path.Combine(Path.GetDirectoryName(sFileName), s);
            }
            else
                Directory.CreateDirectory(Path.GetDirectoryName(sFileName));

            respuesta = sFileName;
            return respuesta;
        }
    }
}