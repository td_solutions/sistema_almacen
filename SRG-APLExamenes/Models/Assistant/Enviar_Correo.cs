﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.VisualBasic;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;

namespace SRG_APLExamenes.Models.Assistant
{
    public class Enviar_Correo
    {
        #region (VARIABLES)
        //datos del mail
        private String Envia;
        private String Recibe;
        private String Subject;
        private String Texto;
        //datos de configuracion
        private String Servidor;
        private String Password;
        private String Puerto;
        private String SSL;
        private String Adjunto;        
        #endregion

        #region (VARIABLES PUBLICAS)
        public String P_Adjunto
        {
            get { return Adjunto; }
            set { Adjunto = value; }
        }
        public string P_Envia
        {
            get { return Envia; }
            set { Envia = value; }
        }
        public string P_Recibe
        {
            get { return Recibe; }
            set { Recibe = value; }
        }
        public string P_Subject
        {
            get { return Subject; }
            set { Subject = value; }
        }
        public string P_Texto
        {
            get { return Texto; }
            set { Texto = value; }
        }
        public string P_Servidor
        {
            get { return Servidor; }
            set { Servidor = value; }
        }
        public string P_Password
        {
            get { return Password; }
            set { Password = value; }
        }

        public string P_Puerto
        {
            get { return Puerto; }
            set { Puerto = value; }
        }        
        #endregion

        #region (METODOS)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Enviar_Correo_Generico
        ///DESCRIPCIÓN: Realiza el envio de correos
        ///PROPIEDADES: 
        ///METODOS    : 
        ///CREO:        José Antonio López Hernández
        ///FECHA_CREO: 11/Septiembre/2015 13:40
        ///MODIFICO:
        ///FECHA_MODIFICO: 
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public String Enviar_Correo_Generico(Enviar_Correo Datos, String Imagen)
        {
            Mdl_Parametros ControladorParametros = new Mdl_Parametros();            
            DataTable Dt_Servidor = ControladorParametros.Consult();
            string[] correo = Recibe.Split(';');
            try
            {
                String Mi_SQL = "";
                String Servidor_Correo = "";

                if (Dt_Servidor.Rows.Count > 0)
                {
                    Servidor_Correo = Dt_Servidor.Rows[0]["Proveedor_Servicio"].ToString();
                    Envia = Dt_Servidor.Rows[0]["Servidor_Correo"].ToString();
                    Password = Dt_Servidor.Rows[0]["Contraseña"].ToString();
                    Puerto = Dt_Servidor.Rows[0]["Puerto"].ToString();
                    SSL = Dt_Servidor.Rows[0]["SSL_Cifrado"].ToString();                    
                }
                Dt_Servidor.Dispose();

                //Si existe parametro de servidor continua con el proceso
                if (!String.IsNullOrEmpty(Servidor_Correo))
                {
                    SmtpClient serverobj = new SmtpClient();
                    serverobj.DeliveryMethod = SmtpDeliveryMethod.Network;
                    if (!String.IsNullOrEmpty(Password) && (Password != "-"))
                        serverobj.Credentials = new NetworkCredential(Envia, Password);
                    else
                    {
                        serverobj.Credentials = new NetworkCredential(Envia, "");
                        serverobj.UseDefaultCredentials = true;
                    }
                    serverobj.Host = Servidor_Correo;
                    serverobj.Port = int.Parse(Puerto);
                    
                    if (SSL == "1")
                        serverobj.EnableSsl = true;
                    else
                        serverobj.EnableSsl = false;                    

                    MailMessage message = new MailMessage();
                    correo = Recibe.Split(';');
                    message.To.Clear();
                    if (correo.Length > 1)
                    {
                        for (int i = 0; i < correo.Length; i++)
                        {
                            if (!String.IsNullOrEmpty(correo[i].ToString()))
                                message.To.Add(correo[i].ToString());
                        }
                    }
                    else
                        message.To.Add(Recibe);
                    message.From = new MailAddress(Envia, "Monitoreo Eventos", System.Text.Encoding.UTF8);
                    message.Priority = MailPriority.High;
                    message.Subject = Subject;
                    message.Body = Texto;
                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    message.IsBodyHtml = true;
                    serverobj.Send(message);
                    message = null;
                    return "ok";
                }
                else
                {
                    return "No existe servidor de correo configurado en los parametros.";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }//fin de enviar correo generico
        #endregion
    }
}