﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SRG_APLExamenes.Models.Assistant
{
    public class Select2Model
    {
        public String id            { get; set; }
        public String text       { get; set; }
        public String Data2      { get; set; }
        public String Data3 { get; set; }
        public String Data4 { get; set; }
        public String Data5 { get; set; }
        public String Data6 { get; set; }
        public String Data7 { get; set; }
    }
        
public class Select2Insumos
    {
        public String id { get; set; }
        public String text { get; set; }
        public int Insumo_ID { get; set; }
        public int Tipo_Insumo_ID { get; set; }
        public int Unidad_ID { get; set; }
        public String Unidad { get; set; }
        public int Unidad_Consumo_ID { get; set; }
        public String Unidad_Consumo { get; set; }
        public int Proveedor_ID { get; set; }
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
        public String Estatus { get; set; }
        public String Ubicacion { get; set; }
        public decimal Existencias { get; set; }
        public decimal Equivalencia { get; set; }
        public decimal Costo { get; set; }
        public decimal Ieps { get; set; }
        public decimal Iva { get; set; }
        public decimal Total { get; set; }
        public int Maximo { get; set; }
        public int Minimo { get; set; }
        public int Punto_Reorden { get; set; }
    }

    public class Respuesta
    {
        public String Mensaje    { get; set; }
        public Boolean Estatus   { get; set; }
        public String Data       { get; set; }
        public String Data2      { get; set; }
        public String Data3      { get; set; }
        public String Data4      { get; set; }
        public String Ruta       { get; set; }
        public string Url_PDF { get; set; }
    }


    public class AreaID
    {
        public String Area_ID { get; set; }
    }


    public class Login
    {
        [Required(ErrorMessage = "Se requiere un usuario")]
        //[RegularExpression(".+\\@.+\\..+",
        //ErrorMessage = "Se requiere un usuario valido")]
        public String Usuario { get; set; }

        [Required(ErrorMessage = "Se requiere una Contraseña")]
        public String Password { get; set; }
    }

    public static class Seguridad
    {
        /// Encripta una cadena
        public static String Encriptar(this String _cadenaAencriptar)
        {
            String result = String.Empty;
            Byte[] encryted = System.Text.Encoding.Unicode.GetBytes(_cadenaAencriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }

        /// Esta función desencripta la cadena que le envíamos en el parámentro de entrada.
        public static String DesEncriptar(this String _cadenaAdesencriptar)
        {
            String result = String.Empty;
            Byte[] decryted = Convert.FromBase64String(_cadenaAdesencriptar);
            //result = System.Text.Encoding.Unicode.GetString(decryted, 0, decryted.ToArray().Length);
            result = System.Text.Encoding.Unicode.GetString(decryted);
            return result;
        }
    }

    public class SessionSRG
    {
        public static String Nombre
        {
            set { HttpContext.Current.Session["Nombre"] = value; }
            get
            {
                if (HttpContext.Current.Session["Nombre"] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session["Nombre"].ToString();
            }
        }

        public static String No_Empleado
        {
            set { HttpContext.Current.Session["NoEmpleado"] = value; }
            get
            {
                if (HttpContext.Current.Session["NoEmpleado"] == null)
                    return String.Empty;
                else
                    return HttpContext.Current.Session["NoEmpleado"].ToString();
            }
        }
    }
}