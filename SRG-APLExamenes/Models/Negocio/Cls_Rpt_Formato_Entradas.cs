﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace SRG_APLExamenes.Models.Negocio
{
    public class Cls_Rpt_Formato_Entradas
    {

        public static string Obtener_Pdf(Mdl_Entradas dt_fac)
        {
            iTextSharp.text.Document Documento = null;
            string ruta = String.Empty;
            String ruta_carpeta_PDF = "../Formatos/Entradas/";
            String ruta_web_carpeta_PDF = "/Formatos/Entradas/";
            String Ruta_Imagen = "../Content/Image/logo_empresa.jpg";
            Boolean crear = true;
            String Ruta_Pdf = ruta_carpeta_PDF + "Entrada" + dt_fac.No_Entrada + ".pdf";
            String Ruta_Fisica_Pdf = ruta_web_carpeta_PDF + "Entrada" + dt_fac.No_Entrada + ".pdf";
            try
            {

                if (!Directory.Exists(HttpContext.Current.Server.MapPath(ruta_carpeta_PDF)))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ruta_carpeta_PDF));

                ruta = Ruta_Pdf;

                //if (!File.Exists(HttpContext.Current.Server.MapPath(ruta)))
                //    crear = true;

                //if (dt_fac.Estatus.Trim().ToLower() == "cancelada")
                //    crear = true;

                if (crear)
                {
                    Documento = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10, 10, 10, 10);
                    PdfWriter oWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(Documento, new FileStream(HttpContext.Current.Server.MapPath(ruta), FileMode.Create));
                    Documento.Open();
                    Exportar_Datos_PDF(Documento, dt_fac, oWriter, Ruta_Imagen);
                    Documento.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            String path = HttpContext.Current.Server.MapPath(@"~" + Ruta_Fisica_Pdf);

            return path;
        }

        //obtener pdf nota de credito

        public static void Exportar_Datos_PDF(iTextSharp.text.Document Documento, Mdl_Entradas dt_fac, PdfWriter writer, String P_Ruta_Imagen)
        {
            string domicilio = "Carretera Libramiento Norte Parque industrial Apolo";
            DataTable dt_fac_det = new DataTable();
            decimal total_factura_sumatoria = 0;
            try
            {
                iTextSharp.text.FontFactory.RegisterDirectory(@"C:\Windows\Fonts");
                //Creamos el objeto de tipo tabla para almacenar el resultado de la búsqueda. 
                iTextSharp.text.pdf.PdfPTable Rpt_Tabla = new iTextSharp.text.pdf.PdfPTable(8);
                //Obtenemos y establecemos el formato de las columnas de la tabla.
                //float[] Ancho_Cabeceras = new[] { 100f, 200f, 100, 140f, 120f, 120f, 120f, 120f };
                //Creamos y definimos algunas propiedades que tendrá la fuente que se aplicara a las celdas de la tabla de resultados.
                iTextSharp.text.Font Fuente_Tabla_Contenido = iTextSharp.text.FontFactory.GetFont("Century Gothic", 7, iTextSharp.text.Font.NORMAL, BaseColor.GRAY);
                iTextSharp.text.Font Fuente_Etiquetas = iTextSharp.text.FontFactory.GetFont("Century Gothic", 10, iTextSharp.text.Font.BOLD, new BaseColor(7, 87, 127));
                iTextSharp.text.Font Fuente_Titulo_Reporte = iTextSharp.text.FontFactory.GetFont("Century Gothic", 12, iTextSharp.text.Font.BOLD, new BaseColor(7, 87, 127));
                iTextSharp.text.Font Fuente_Etiquetas_ = iTextSharp.text.FontFactory.GetFont("Century Gothic", 9, iTextSharp.text.Font.BOLD, new BaseColor(51, 51, 51));
                iTextSharp.text.Font Fuente_Etiquetas1 = iTextSharp.text.FontFactory.GetFont("Century Gothic", 9, iTextSharp.text.Font.NORMAL, new BaseColor(51, 51, 51));
                iTextSharp.text.Font Fuente_Valor_Etq = iTextSharp.text.FontFactory.GetFont("Century Gothic", 7, iTextSharp.text.Font.NORMAL, new BaseColor(5, 34, 73));
                iTextSharp.text.Font Fuente_Valor_Entrada = iTextSharp.text.FontFactory.GetFont("Century Gothic", 7, iTextSharp.text.Font.BOLD, new BaseColor(255, 255, 255));
                iTextSharp.text.Font Fuente_Valor_Etq_Bold = iTextSharp.text.FontFactory.GetFont("Century Gothic", 7, iTextSharp.text.Font.BOLD, new BaseColor(5, 34, 73));
                iTextSharp.text.Font Fuente_Etiquetas2 = iTextSharp.text.FontFactory.GetFont("Century Gothic", 8, iTextSharp.text.Font.NORMAL, new BaseColor(51, 51, 51));
                iTextSharp.text.Font Fuente_Etiquetas2_ = iTextSharp.text.FontFactory.GetFont("Century Gothic", 7, iTextSharp.text.Font.NORMAL, new BaseColor(51, 51, 51));

               
                //creamos la imagen
                iTextSharp.text.Image Logo = null;
                if (File.Exists(HttpContext.Current.Server.MapPath(P_Ruta_Imagen)))
                {
                    Logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(P_Ruta_Imagen));
                    Logo.ScaleAbsolute(110f, 45f);
                 
                }

                iTextSharp.text.pdf.PdfPTable tbl_datos_emp1 = new iTextSharp.text.pdf.PdfPTable(3);
                tbl_datos_emp1.SetWidthPercentage(new float[] { 100,200,100 }, PageSize.A4);
                tbl_datos_emp1.WidthPercentage = 95;
                tbl_datos_emp1.DefaultCell.Border = iTextSharp.text.pdf.PdfPCell.NO_BORDER;
                tbl_datos_emp1.DefaultCell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                tbl_datos_emp1.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;

                PdfPCell cellLogo = new PdfPCell(Logo);
                cellLogo.Border = iTextSharp.text.pdf.PdfPCell.NO_BORDER;
                cellLogo.VerticalAlignment = iTextSharp.text.Element.ALIGN_TOP;
                cellLogo.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;

                tbl_datos_emp1.AddCell(cellLogo);
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("Congeladora el Niño", Fuente_Etiquetas));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));

                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("R.F.C.: TORJ870107EB5", Fuente_Etiquetas_));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));

                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase(domicilio, Fuente_Etiquetas_));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));

                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("Parque Industrial Apolo", Fuente_Etiquetas_));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));

                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("Irapuato Gto.", Fuente_Etiquetas_));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));

                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("\n", Fuente_Etiquetas));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("\n", Fuente_Etiquetas));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("\n", Fuente_Etiquetas));

                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("Formato de Entrada", Fuente_Titulo_Reporte));
                tbl_datos_emp1.AddCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));

                //tbl_datos_emp.AddCell(tbl_datos_emp1);

                #region (Detalles)


                iTextSharp.text.pdf.PdfPTable tbl_det_header2 = new iTextSharp.text.pdf.PdfPTable(8);
                tbl_det_header2.SetWidthPercentage(new float[] { 110, 110, 110, 200, 110, 180, 150, 350 }, PageSize.A4);
                tbl_det_header2.WidthPercentage = 98;
                tbl_det_header2.DefaultCell.BorderWidth = 0.5f;
                tbl_det_header2.DefaultCell.BorderColor = new BaseColor(205, 203, 203);
                tbl_det_header2.DefaultCell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                tbl_det_header2.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;
                PdfPCell noEntradaCell = new PdfPCell(new iTextSharp.text.Phrase(dt_fac.No_Entrada.ToString(), Fuente_Valor_Entrada));
                noEntradaCell.BackgroundColor = new BaseColor(51, 51, 51);
                noEntradaCell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                tbl_det_header2.AddCell(new iTextSharp.text.Phrase("No. Entrada", Fuente_Valor_Etq_Bold));
                tbl_det_header2.AddCell(noEntradaCell);
                tbl_det_header2.AddCell(new iTextSharp.text.Phrase("Fecha Entrada", Fuente_Valor_Etq_Bold));
                tbl_det_header2.AddCell(new iTextSharp.text.Phrase(dt_fac.Fecha_Registro.ToString(), Fuente_Valor_Etq));
                tbl_det_header2.AddCell(new iTextSharp.text.Phrase("Tipo", Fuente_Valor_Etq_Bold));
                tbl_det_header2.AddCell(new iTextSharp.text.Phrase(dt_fac.Tipo_Entrada.ToString(), Fuente_Valor_Etq));
                tbl_det_header2.AddCell(new iTextSharp.text.Phrase("Proveedor", Fuente_Valor_Etq_Bold));
                tbl_det_header2.AddCell(new iTextSharp.text.Phrase(dt_fac.Proveedor.ToString(), Fuente_Valor_Etq));                
                tbl_det_header2.AddCell(new iTextSharp.text.Phrase("Tipo Factura", Fuente_Valor_Etq_Bold));

                if(dt_fac.Tipo_Entrada == "OTROS")
                {
                    tbl_det_header2.AddCell(new iTextSharp.text.Phrase("Sin factura", Fuente_Valor_Etq));
                    tbl_det_header2.AddCell(new iTextSharp.text.Phrase("", Fuente_Valor_Etq_Bold));
                    tbl_det_header2.AddCell(new iTextSharp.text.Phrase("", Fuente_Valor_Etq));
                    tbl_det_header2.AddCell(new iTextSharp.text.Phrase("", Fuente_Valor_Etq_Bold));
                    tbl_det_header2.AddCell(new iTextSharp.text.Phrase("", Fuente_Valor_Etq));

                }
                else
                {
                    tbl_det_header2.AddCell(new iTextSharp.text.Phrase(dt_fac.Tipo_Factura.ToString(), Fuente_Valor_Etq));
                    tbl_det_header2.AddCell(new iTextSharp.text.Phrase("Fecha recepcion", Fuente_Valor_Etq_Bold));
                    tbl_det_header2.AddCell(new iTextSharp.text.Phrase(dt_fac.Fecha_Recepcion.ToString(), Fuente_Valor_Etq));
                    tbl_det_header2.AddCell(new iTextSharp.text.Phrase("Fecha pago", Fuente_Valor_Etq_Bold));
                    tbl_det_header2.AddCell(new iTextSharp.text.Phrase(dt_fac.Fecha_Pago.ToString(), Fuente_Valor_Etq));
                }

                tbl_det_header2.AddCell(new iTextSharp.text.Phrase("Almacen", Fuente_Valor_Etq_Bold));
                tbl_det_header2.AddCell(new iTextSharp.text.Phrase(dt_fac.Almacen.ToString(), Fuente_Valor_Etq));


                iTextSharp.text.pdf.PdfPTable tbl_datos_emp3 = new iTextSharp.text.pdf.PdfPTable(1);
                tbl_datos_emp3.SetWidthPercentage(new float[] { 100 }, PageSize.A4);
                tbl_datos_emp3.WidthPercentage = 98;
                tbl_datos_emp3.DefaultCell.Border = iTextSharp.text.pdf.PdfPCell.NO_BORDER;
                tbl_datos_emp3.DefaultCell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                tbl_datos_emp3.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;

                tbl_datos_emp3.AddCell(new iTextSharp.text.Phrase("Detalles", Fuente_Etiquetas));

                iTextSharp.text.pdf.PdfPTable tbl_datos_emp4 = new iTextSharp.text.pdf.PdfPTable(4);
                tbl_datos_emp4.SetWidthPercentage(new float[] { 50, 70, 100, 50 }, PageSize.A4);
                tbl_datos_emp4.WidthPercentage = 98;
                tbl_datos_emp4.DefaultCell.BorderWidth = 0.5f;
                tbl_datos_emp4.DefaultCell.BorderColor = new BaseColor(205, 203, 203);
                tbl_datos_emp4.DefaultCell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                tbl_datos_emp4.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;


                tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase("Cantidad", Fuente_Etiquetas_));
                tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase("Unidad", Fuente_Etiquetas_));
                tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase("Insumo", Fuente_Etiquetas_));
                tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase("Precio", Fuente_Etiquetas_));


                //iTextSharp.text.Phrase _frase = null;
                //iTextSharp.text.pdf.PdfPCell _celda = null;

                foreach (Mdl_Entradas_Detalles bean in dt_fac.Detalles)
                {
                    tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase(String.Format("{0:n}", bean.Cantidad), Fuente_Etiquetas1));
                    tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase(bean.Unidad, Fuente_Etiquetas1));
                    tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase( bean.Insumo, Fuente_Etiquetas1));
                    tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase(string.Format("{0:n}", bean.Precio), Fuente_Etiquetas1));
                    total_factura_sumatoria += bean.Precio;
                }


                iTextSharp.text.pdf.PdfPTable tbl_datos_emp5 = new iTextSharp.text.pdf.PdfPTable(4);
                tbl_datos_emp4.SetWidthPercentage(new float[] { 50, 70, 100, 50 }, PageSize.A4);
                tbl_datos_emp4.WidthPercentage = 98;
                tbl_datos_emp4.DefaultCell.BorderWidth = 0.5f;
                tbl_datos_emp4.DefaultCell.BorderColor = new BaseColor(205, 203, 203);
                tbl_datos_emp4.DefaultCell.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE;
                tbl_datos_emp4.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT;

                PdfPCell celda_vacia = new PdfPCell(new iTextSharp.text.Phrase("", Fuente_Etiquetas));
                celda_vacia.Border = iTextSharp.text.pdf.PdfPCell.NO_BORDER;
                tbl_datos_emp4.AddCell(celda_vacia);
                tbl_datos_emp4.AddCell(celda_vacia);
                tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase("Total", Fuente_Etiquetas_));
                tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase(String.Format("{0:n}", total_factura_sumatoria), Fuente_Etiquetas_));

                tbl_datos_emp4.AddCell(celda_vacia);
                tbl_datos_emp4.AddCell(celda_vacia);
                if (dt_fac.Tipo_Entrada == "OTROS")
                {
                    tbl_datos_emp4.AddCell(celda_vacia);
                    tbl_datos_emp4.AddCell(celda_vacia);
                }
                else
                {
                    tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase("Total Factura", Fuente_Etiquetas_));
                    tbl_datos_emp4.AddCell(new iTextSharp.text.Phrase(String.Format("{0:n}", dt_fac.Total_Factura), Fuente_Etiquetas_));
                }
                    
                #endregion              

                // Se agrega el PDFTable al documento.
                Documento.Add(tbl_datos_emp1);
                Documento.Add(new iTextSharp.text.Paragraph("\n"));                
                Documento.Add(tbl_det_header2);
                Documento.Add(new iTextSharp.text.Paragraph("\n"));
                Documento.Add(tbl_datos_emp3);
                Documento.Add(tbl_datos_emp4);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


    }
}