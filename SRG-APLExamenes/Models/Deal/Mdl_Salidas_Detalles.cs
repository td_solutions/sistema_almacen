﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Salidas_Detalles
    {
        public int No_Salida_Detalle { get; set; }
        public int No_Salida { get; set; }
        public string Insumo { get; set; }
        public decimal Cantidad { get; set; }
        public string Unidad { get; set; }
        public int Unidad_ID { get; set; }
        public int Insumo_ID { get; set; }
        public decimal Precio { get; set; }
        public decimal Importe { get; set; }
        public decimal Ieps { get; set; }
        public decimal Iva { get; set; }
        public decimal Total { get; set; }
    }
}