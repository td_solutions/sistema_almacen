﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Fumigacion : TablaDB
    {
        public String No_Fumigacion { get; set; }        
        public String Fecha { get; set; }
        public String Litros { get; set; }
        public String Pago_Por { get; set; }
        public Decimal Total_Empleados { get; set; }
        public String Estatus { get; set; }   

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public String Fecha_Inicio { get; set; }
        public String Fecha_Fin { get; set; }

        public Mdl_Fumigacion() { }

        public Mdl_Fumigacion(String No_Fumigacion, String Fecha = "", String Litros = "", String Pago_Por = "", Decimal Total_Empleados = 0, String Estatus = "")
        {
            this.No_Fumigacion = No_Fumigacion;
            this.Fecha = Fecha;
            this.Litros = Litros;
            this.Pago_Por = Pago_Por;
            this.Total_Empleados = Total_Empleados;
            this.Estatus = Estatus;
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Fumigacion.Consultevent(this); }
        public DataTable ConsultMaximoFumigacion() { return Ctrl_Fumigacion.ConsultMaximoFumigacion(this); }        

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Fumigacion"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Fumigacion"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("No_Fumigacion", No_Fumigacion.Trim()));
            parametrosBD.Add(new ParametroBD("Fecha", Fecha.Trim()));
            parametrosBD.Add(new ParametroBD("Litros", Litros.Trim()));
            parametrosBD.Add(new ParametroBD("Pago_Por", Pago_Por.Trim()));
            parametrosBD.Add(new ParametroBD("Total_Empleados", Total_Empleados.ToString()));
            parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));  
            return parametrosBD;
        }
    }
}