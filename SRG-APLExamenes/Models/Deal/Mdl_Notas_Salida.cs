﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;


namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Notas_Salida: TablaDB
    {
        public String No_Nota { get; set; }        
        public String Fecha { get; set; }
        public String Agricultor { get; set; }
        public String Total_Cajas { get; set; }
        public String Total_Peso { get; set; }
        public String Total_Kilogramos { get; set; }   
        public String Total { get; set; }  
        public String Estatus { get; set; }   

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public String Fecha_Inicio { get; set; }
        public String Fecha_Fin { get; set; }

        public Mdl_Notas_Salida() { }

        public Mdl_Notas_Salida(String No_Nota, String Fecha = "", String Agricultor = "", String Total_Cajas = "", String Total_Peso = "", String Total_Kilogramos = "", String Total = "", String Estatus = "")
        {
            this.No_Nota = No_Nota;
            this.Fecha = Fecha;
            this.Agricultor = Agricultor;
            this.Total_Cajas = Total_Cajas;
            this.Total_Peso = Total_Peso;
            this.Total_Kilogramos = Total_Kilogramos;
            this.Total = Total;
            this.Estatus = Estatus;
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Notas_Salida.Consultevent(this); }
        public DataTable ConsultMaximoNotaSalida() { return Ctrl_Notas_Salida.ConsultMaximoNotaSalida(this); }        

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Notas_Salida"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Nota"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("No_Nota", No_Nota.Trim()));
            parametrosBD.Add(new ParametroBD("Fecha", Fecha.Trim()));
            parametrosBD.Add(new ParametroBD("Agricultor", Agricultor.Trim()));
            parametrosBD.Add(new ParametroBD("Total_Cajas", Total_Cajas.Trim()));
            parametrosBD.Add(new ParametroBD("Total_Peso", Total_Peso.ToString()));
            parametrosBD.Add(new ParametroBD("Total_Kilogramos", Total_Kilogramos.Trim()));            
            parametrosBD.Add(new ParametroBD("Total", Total.ToString()));
            parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));            
            parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));  
            return parametrosBD;
        }
    }
}