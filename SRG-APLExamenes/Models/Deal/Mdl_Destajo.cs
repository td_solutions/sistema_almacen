﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Destajo : TablaDB
    {
        public String No_Destajo { get; set; }        
        public String Fecha { get; set; }
        public String No_Empleado { get; set; }
        public String Tabla_ID { get; set; }
        public String Tipo_Destajo_ID { get; set; }
        public String No_Tarjeta { get; set; }
        public Decimal Cantidad { get; set; }
        public String Estatus { get; set; }
        public Decimal Pago { get; set; }
        public Decimal Total_Pago { get; set; }
        public Decimal Cantidad_Empleados { get; set; }
        public String No_Nota { get; set; }        

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public String Fecha_Inicio { get; set; }
        public String Fecha_Fin { get; set; }

        public Mdl_Destajo() { }

        public Mdl_Destajo(String No_Destajo, String Fecha = "", String No_Empleado = "", String Tabla_ID = "", String Tipo_Destajo_ID = "", String No_Tarjeta = "", Decimal Cantidad = 0, String Estatus = "", Decimal Pago = 0, Decimal Total_Pago = 0, Decimal Cantidad_Empleados = 0, String No_Nota = "")
        {
            this.No_Destajo = No_Destajo;
            this.Fecha = Fecha;
            this.No_Empleado = No_Empleado;
            this.Tabla_ID = Tabla_ID;
            this.Tipo_Destajo_ID = Tipo_Destajo_ID;
            this.No_Tarjeta = No_Tarjeta;
            this.Cantidad = Cantidad;
            this.Estatus = Estatus;
            this.Pago = Pago;
            this.Total_Pago = Total_Pago;
            this.Cantidad_Empleados = Cantidad_Empleados;
            this.No_Nota = No_Nota;
            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = Fecha_Registro;            
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Destajo.Consultevent(this); }
        public DataTable ConsultAgrupadoTDCorte() { return Ctrl_Destajo.ConsultAgrupadoTDCorte(this); }
        public DataTable ConsultMaximoDestajo() { return Ctrl_Destajo.ConsultMaximoDestajo(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Destajo"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Destajo"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("No_Destajo", No_Destajo));
            if (!String.IsNullOrEmpty(Fecha))
                parametrosBD.Add(new ParametroBD("Fecha", Fecha.Trim()));
            if (!String.IsNullOrEmpty(No_Empleado))
                parametrosBD.Add(new ParametroBD("No_Empleado", No_Empleado.Trim()));
            if (!String.IsNullOrEmpty(Tabla_ID))
                parametrosBD.Add(new ParametroBD("Tabla_ID", Tabla_ID.Trim()));
            if (!String.IsNullOrEmpty(Tipo_Destajo_ID))
                parametrosBD.Add(new ParametroBD("Tipo_Destajo_ID", Tipo_Destajo_ID.Trim()));
            if (!String.IsNullOrEmpty(No_Tarjeta))
                parametrosBD.Add(new ParametroBD("No_Tarjeta", No_Tarjeta.Trim()));
            if (!String.IsNullOrEmpty(Cantidad.ToString()))
            {
                if (Cantidad != 0)
                    parametrosBD.Add(new ParametroBD("Cantidad", Cantidad.ToString()));
            }
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (!String.IsNullOrEmpty(Pago.ToString()))
            {
                if(Pago != 0)
                    parametrosBD.Add(new ParametroBD("Pago", Pago.ToString()));
            }
            if (!String.IsNullOrEmpty(Total_Pago.ToString()))
            {
                if(Total_Pago != 0)
                    parametrosBD.Add(new ParametroBD("Total_Pago", Total_Pago.ToString()));
            }
            if (!String.IsNullOrEmpty(Cantidad_Empleados.ToString()))
            {
                if(Cantidad_Empleados != 0)
                    parametrosBD.Add(new ParametroBD("Cantidad_Empleados", Cantidad_Empleados.ToString()));
            }
            if (!String.IsNullOrEmpty(No_Nota))
                parametrosBD.Add(new ParametroBD("No_Nota", No_Nota.Trim()));
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            }
            else
            {
                if (!String.IsNullOrEmpty(Usuario_Registro))
                    parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                if (!String.IsNullOrEmpty(Fecha_Registro))
                    parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro));
            }
            return parametrosBD;
        }
    }
}