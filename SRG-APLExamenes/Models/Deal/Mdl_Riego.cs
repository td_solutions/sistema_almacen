﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Riego : TablaDB
    {
        public String No_Riego { get; set; }        
        public String Fecha { get; set; }               
        public Decimal Horas { get; set; }
        public Decimal Total_Empleados { get; set; }
        public String Estatus { get; set; }
        public String Tanque { get; set; }
        public String Num_Tanque { get; set; }
        public String Acido { get; set; }
        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public String Fecha_Inicio { get; set; }
        public String Fecha_Fin { get; set; }

        public Mdl_Riego() { }

        public Mdl_Riego(String No_Riego, String Fecha = "", Decimal Horas = 0, Decimal Total_Empleados = 0, String Estatus = "", String Tanque = "", String Num_Tanque = "", String Acido = "")
        {
            this.No_Riego = No_Riego;
            this.Fecha = Fecha;            
            this.Horas = Horas;
            this.Total_Empleados = Total_Empleados;       
            this.Estatus = Estatus;
            this.Tanque = Tanque;
            this.Num_Tanque = Num_Tanque;            
            this.Acido = Acido;
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Riego.Consultevent(this); }
        public DataTable ConsultMaximoRiego() { return Ctrl_Riego.ConsultMaximoRiego(this); }
        public DataTable ConsultAcidoRiego() { return Ctrl_Riego.ConsultAcidoRiego(this); }   

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Riego"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Riego"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("No_Riego", No_Riego.Trim()));
            parametrosBD.Add(new ParametroBD("Fecha", Fecha.Trim()));            
            parametrosBD.Add(new ParametroBD("Horas", Horas.ToString().Trim()));
            parametrosBD.Add(new ParametroBD("Total_Empleados", Total_Empleados.ToString().Trim()));
            parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (!String.IsNullOrEmpty(Tanque))
                parametrosBD.Add(new ParametroBD("Tanque", Tanque.Trim()));
            if (!String.IsNullOrEmpty(Num_Tanque))
                parametrosBD.Add(new ParametroBD("Num_Tanque", Num_Tanque.Trim()));            
            if (!String.IsNullOrEmpty(Acido))
                parametrosBD.Add(new ParametroBD("Acido", Acido.Trim()));
            parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));  
            return parametrosBD;
        }
    }
}