﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;


namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Empleados : TablaDB
    {        
        public String No_Empleado { get; set; }
        public String Nombre { get; set; }        
        public String Email { get; set; }
        public String Usuario { get; set; }
        public String Password { get; set; }
        public String Salario_Diario { get; set; }
        public String Salario_Diario_Fumigacion { get; set; }
        public String Estatus { get; set; }        

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public Mdl_Empleados() { }

        public Mdl_Empleados(String No_Empleado = "", String Nombre = "", String Email = "", String Usuario = "",
            String Password = "", String Salario_Diario = "", String Salario_Diario_Fumigacion = "", String Estatus = "", String Usuario_Registro = "", String Fecha_Registro = "")
        {            
            this.No_Empleado = No_Empleado;
            this.Nombre = Nombre;            
            this.Email = Email;
            this.Usuario = Usuario;
            this.Password = Password;
            this.Salario_Diario = Salario_Diario;
            this.Salario_Diario_Fumigacion = Salario_Diario_Fumigacion;
            this.Estatus = Estatus;            
            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = Fecha_Registro;
        }

        public Boolean MasterManagement(List<Mdl_Accesos> Accesos, MODO_DE_CAPTURA captura) { return Ctrl_Empleados.MasterManagement(this, captura, Accesos); }        
        public DataTable Consult() { return Ctrl_Empleados.Consultemployee(this); }
        public DataTable ConsultMaximoEmpleado() { return Ctrl_Empleados.ConsultMaximoEmpleado(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Cat_Empleados"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Empleado"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("No_Empleado", No_Empleado.Trim()));
            if (!String.IsNullOrEmpty(Nombre))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            if (!String.IsNullOrEmpty(Email))
                parametrosBD.Add(new ParametroBD("Email", Seguridad.Encriptar(Email.Trim())));
            if (!String.IsNullOrEmpty(Usuario))
                parametrosBD.Add(new ParametroBD("Usuario", Seguridad.Encriptar(Usuario.Trim())));
            if (!String.IsNullOrEmpty(Password))
                parametrosBD.Add(new ParametroBD("Password", Seguridad.Encriptar(Password.Trim())));
            if (!String.IsNullOrEmpty(Salario_Diario))
                parametrosBD.Add(new ParametroBD("Salario_Diario", Salario_Diario.Trim()));
            else
                parametrosBD.Add(new ParametroBD("Salario_Diario", "0.00"));
            if (!String.IsNullOrEmpty(Salario_Diario_Fumigacion))
                parametrosBD.Add(new ParametroBD("Salario_Diario_Fumigacion", Salario_Diario_Fumigacion.Trim()));
            else
                parametrosBD.Add(new ParametroBD("Salario_Diario_Fumigacion", "0.00"));
            parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));

            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {                
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            }
            else
            {                
                parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro.Trim()));
            }

            //Retornamos la lista.
            return parametrosBD;
        }
    }
}