﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Facturas : TablaDB
    {
        public String FACTURA_ID { get; set; }        
        public String NO_FACTURA { get; set; }
        public String PROVEEDOR_ID { get; set; }
        public String CONCEPTO { get; set; }
        public String TIPO { get; set; }
        public String FECHA { get; set; }
        public String FECHA_RECEPCION { get; set; }
        public String FECHA_VENCIMIENTO { get; set; }
        public String MONEDA { get; set; }
        public String IMPORTE { get; set; }
        public String IEPS { get; set; }
        public String IVA { get; set; }
        public String DESCUENTO { get; set; }
        public String TOTAL { get; set; }
        public String ABONO { get; set; }
        public String SALDO { get; set; }
        public String PAGADA { get; set; }
        public String CANCELADA { get; set; }
        public String FECHA_PAGO { get; set; }
        public String FORMA_PAGO { get; set; }
        public String COMENTARIOS { get; set; }
        public String TIPO_CAMBIO { get; set; }
        public String ESTATUS { get; set; }

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public String Fecha_Inicio { get; set; }
        public String Fecha_Fin { get; set; }
        public String Fecha_Saldos_Inicio { get; set; }
        public String Fecha_Saldos_Fin { get; set; }

        public Mdl_Facturas() { }

        public Mdl_Facturas(String FACTURA_ID, String NO_FACTURA = "", String PROVEEDOR_ID = "", String CONCEPTO = "", String TIPO = "", String FECHA = "",
                            String FECHA_RECEPCION = "", String FECHA_VENCIMIENTO = "", String MONEDA = "", String IMPORTE = "", String IEPS = "", String IVA = "", String DESCUENTO = "",
                            String TOTAL = "", String ABONO = "", String SALDO = "", String PAGADA = "", String CANCELADA = "", String FECHA_PAGO = "", String FORMA_PAGO = "",
                            String COMENTARIOS = "", String TIPO_CAMBIO = "", String ESTATUS = "")
        {
            this.FACTURA_ID = FACTURA_ID;
            this.NO_FACTURA = NO_FACTURA;
            this.PROVEEDOR_ID = PROVEEDOR_ID;
            this.CONCEPTO = CONCEPTO;
            this.TIPO = TIPO;
            this.FECHA = FECHA;
            this.FECHA_RECEPCION = FECHA_RECEPCION;
            this.FECHA_VENCIMIENTO = FECHA_VENCIMIENTO;
            this.MONEDA = MONEDA;
            this.IMPORTE = IMPORTE;
            this.IEPS = IEPS;
            this.IVA = IVA;
            this.DESCUENTO = DESCUENTO;
            this.TOTAL = TOTAL;
            this.ABONO = ABONO;
            this.SALDO = SALDO;
            this.PAGADA = PAGADA;
            this.CANCELADA = CANCELADA;
            this.FECHA_PAGO = FECHA_PAGO;
            this.FORMA_PAGO = FORMA_PAGO;
            this.COMENTARIOS = COMENTARIOS;
            this.TIPO_CAMBIO = TIPO_CAMBIO;
            this.ESTATUS = ESTATUS;
            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = Fecha_Registro;            
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Facturas.Consultevent(this); }
        public DataTable ConsultMaximoFactura() { return Ctrl_Facturas.ConsultMaximoFactura(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "OPE_FACTURAS_PROVEEDOR"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "FACTURA_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("FACTURA_ID", FACTURA_ID));
            if (!String.IsNullOrEmpty(NO_FACTURA))
                parametrosBD.Add(new ParametroBD("NO_FACTURA", NO_FACTURA.Trim()));
            if (!String.IsNullOrEmpty(PROVEEDOR_ID))
                parametrosBD.Add(new ParametroBD("PROVEEDOR_ID", PROVEEDOR_ID.Trim()));
            if (!String.IsNullOrEmpty(CONCEPTO))
                parametrosBD.Add(new ParametroBD("CONCEPTO", CONCEPTO.Trim()));
            if (!String.IsNullOrEmpty(TIPO))
                parametrosBD.Add(new ParametroBD("TIPO", TIPO.Trim()));
            if (!String.IsNullOrEmpty(FECHA))
                parametrosBD.Add(new ParametroBD("FECHA", FECHA.Trim()));
            if (!String.IsNullOrEmpty(FECHA_RECEPCION))
                parametrosBD.Add(new ParametroBD("FECHA_RECEPCION", FECHA_RECEPCION.ToString()));
            if (!String.IsNullOrEmpty(FECHA_VENCIMIENTO))
                parametrosBD.Add(new ParametroBD("FECHA_VENCIMIENTO", FECHA_VENCIMIENTO.Trim()));
            if (!String.IsNullOrEmpty(MONEDA))
                parametrosBD.Add(new ParametroBD("MONEDA", MONEDA.ToString()));
            if (!String.IsNullOrEmpty(IMPORTE))
                parametrosBD.Add(new ParametroBD("IMPORTE", IMPORTE.ToString()));
            if (!String.IsNullOrEmpty(IEPS))
                parametrosBD.Add(new ParametroBD("IEPS", IEPS.Trim()));
            if (!String.IsNullOrEmpty(IVA))
                parametrosBD.Add(new ParametroBD("IVA", IVA.Trim()));
            if (!String.IsNullOrEmpty(DESCUENTO))
                parametrosBD.Add(new ParametroBD("DESCUENTO", DESCUENTO.Trim()));
            if (!String.IsNullOrEmpty(TOTAL))
                parametrosBD.Add(new ParametroBD("TOTAL", TOTAL.Trim()));
            if (!String.IsNullOrEmpty(ABONO))
                parametrosBD.Add(new ParametroBD("ABONO", ABONO.Trim()));
            if (!String.IsNullOrEmpty(SALDO))
                parametrosBD.Add(new ParametroBD("SALDO", SALDO.Trim()));
            if (!String.IsNullOrEmpty(PAGADA))
                parametrosBD.Add(new ParametroBD("PAGADA", PAGADA.ToString()));
            if (!String.IsNullOrEmpty(CANCELADA))
                parametrosBD.Add(new ParametroBD("CANCELADA", CANCELADA.Trim()));
            if (!String.IsNullOrEmpty(FECHA_PAGO))
                parametrosBD.Add(new ParametroBD("FECHA_PAGO", FECHA_PAGO.ToString()));
            if (!String.IsNullOrEmpty(FORMA_PAGO))
                parametrosBD.Add(new ParametroBD("FORMA_PAGO", FORMA_PAGO.ToString()));
            if (!String.IsNullOrEmpty(COMENTARIOS))
                parametrosBD.Add(new ParametroBD("COMENTARIOS", COMENTARIOS.ToString()));
            if (!String.IsNullOrEmpty(TIPO_CAMBIO))
                parametrosBD.Add(new ParametroBD("TIPO_CAMBIO", TIPO_CAMBIO.ToString()));
            if (!String.IsNullOrEmpty(ESTATUS))
                parametrosBD.Add(new ParametroBD("ESTATUS", ESTATUS.ToString()));

            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            }
            else
            {
                if (!String.IsNullOrEmpty(Usuario_Registro))
                    parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                if (!String.IsNullOrEmpty(Fecha_Registro))
                    parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro));
            }
            return parametrosBD;
        }
    }
}