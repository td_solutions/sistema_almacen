﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Riego_Detalles : TablaDB
    {
        public String No_Riego { get; set; }        
        public String No_Empleado { get; set; }
        public String No_Tarjeta { get; set; }
        public String Nombre { get; set; }
        public Decimal Pago { get; set; }
               
        public String Captura { get; set; }

        public Mdl_Riego_Detalles() { }

        public Mdl_Riego_Detalles(String No_Riego, String No_Empleado = "", String No_Tarjeta = "", String Nombre = "", Decimal Pago = 0)
        {
            this.No_Riego = No_Riego;
            this.No_Empleado = No_Empleado;
            this.No_Tarjeta = No_Tarjeta;
            this.Nombre = Nombre;
            this.Pago = Pago;
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Riego_Detalles.Consultevent(this); }        

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Riego_Detalles"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Riego"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("No_Riego", No_Riego.Trim()));
            if (!String.IsNullOrEmpty(No_Empleado.Trim()))
                parametrosBD.Add(new ParametroBD("No_Empleado", No_Empleado.Trim()));
            parametrosBD.Add(new ParametroBD("No_Tarjeta", No_Tarjeta.Trim()));
            if (!String.IsNullOrEmpty(Nombre.Trim()))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            parametrosBD.Add(new ParametroBD("Pago", Pago.ToString()));            
            return parametrosBD;
        }
    }
}