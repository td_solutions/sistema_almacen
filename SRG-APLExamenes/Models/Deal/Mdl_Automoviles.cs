﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Automoviles : TablaDB
    {
        public String No_Automovil { get; set; }        
        public String Fecha { get; set; }
        public String Unidad { get; set; }
        public String No_Empleado { get; set; }
        public String No_Tarjeta { get; set; }
        public String Tabla_ID { get; set; }
        public String Labor { get; set; }
        public String Nivel_Gasolina { get; set; }
        public Decimal Horas_Uso { get; set; }
        public String Carga_Gasolina { get; set; }
        public Decimal Litros { get; set; }
        public Decimal Precio_Litro { get; set; }
        public String Estatus { get; set; }

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public String Fecha_Inicio { get; set; }
        public String Fecha_Fin { get; set; }

        public Mdl_Automoviles() { }

        public Mdl_Automoviles(String No_Automovil, String Fecha = "", String Unidad = "", String No_Empleado = "", String No_Tarjeta = "", String Tabla_ID = "", String Labor = "", String Nivel_Gasolina = "", Decimal Hora_Uso = 0, String Carga_Gasolina = "", Decimal Litros = 0, Decimal Precio_Litro = 0, String Estatus = "")
        {
            this.No_Automovil = No_Automovil;
            this.Fecha = Fecha;
            this.Unidad = Unidad;
            this.No_Empleado = No_Empleado;
            this.No_Tarjeta = No_Tarjeta;
            this.Tabla_ID = Tabla_ID;
            this.Labor = Labor;
            this.Nivel_Gasolina = Nivel_Gasolina;
            this.Horas_Uso = Horas_Uso;
            this.Carga_Gasolina = Carga_Gasolina;
            this.Litros = Litros;
            this.Precio_Litro = Precio_Litro;
            this.Estatus = Estatus;       
            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = Fecha_Registro;            
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Automoviles.Consultevent(this); }
        public DataTable ConsultMaximoAutomovil() { return Ctrl_Automoviles.ConsultMaximoAutomovil(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Automoviles"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Automovil"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("No_Automovil", No_Automovil));
            if (!String.IsNullOrEmpty(Fecha))
                parametrosBD.Add(new ParametroBD("Fecha", Fecha.Trim()));
            if (!String.IsNullOrEmpty(Unidad))
                parametrosBD.Add(new ParametroBD("Unidad", Unidad.Trim()));
            if (!String.IsNullOrEmpty(No_Empleado))
                parametrosBD.Add(new ParametroBD("No_Empleado", No_Empleado.Trim()));
            if (!String.IsNullOrEmpty(No_Tarjeta))
                parametrosBD.Add(new ParametroBD("No_Tarjeta", No_Tarjeta.Trim()));
            if (!String.IsNullOrEmpty(Tabla_ID))
                parametrosBD.Add(new ParametroBD("Tabla_ID", Tabla_ID.Trim()));
            if (!String.IsNullOrEmpty(Labor))
                parametrosBD.Add(new ParametroBD("Labor", Labor.Trim()));
            if (!String.IsNullOrEmpty(Nivel_Gasolina))
                parametrosBD.Add(new ParametroBD("Nivel_Gasolina", Nivel_Gasolina.Trim()));
            if (!String.IsNullOrEmpty(Horas_Uso.ToString()))
                parametrosBD.Add(new ParametroBD("Horas_Uso", Horas_Uso.ToString()));
            if (!String.IsNullOrEmpty(Carga_Gasolina))
                parametrosBD.Add(new ParametroBD("Carga_Gasolina", Carga_Gasolina.Trim()));
            if (!String.IsNullOrEmpty(Litros.ToString()))
                parametrosBD.Add(new ParametroBD("Litros", Litros.ToString()));
            if (!String.IsNullOrEmpty(Precio_Litro.ToString()))
                parametrosBD.Add(new ParametroBD("Precio_Litro", Precio_Litro.ToString()));
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            }
            else
            {
                if (!String.IsNullOrEmpty(Usuario_Registro))
                    parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                if (!String.IsNullOrEmpty(Fecha_Registro))
                    parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro));
            }
            return parametrosBD;
        }            
    }
}
