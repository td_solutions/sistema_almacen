﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;
namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Notas_Salida_Detalles : TablaDB
    {
        public String No_Nota { get; set; }
        public String Clave { get; set; }
        public String Producto { get; set; }
        public String Cajas { get; set; }
        public String Peso { get; set; }
        public String Kilogramos { get; set; }        
        public String Precio { get; set; }
        public String Total { get; set; } 

        public String Captura { get; set; }

        public Mdl_Notas_Salida_Detalles() { }

        public Mdl_Notas_Salida_Detalles(String No_Nota, String Clave = "", String Producto = "", String Cajas = "", String Peso = "", String Kilogramos = "", String Precio = "", String Total = "")
        {
            this.No_Nota = No_Nota;
            this.Clave = Clave;
            this.Producto = Producto;
            this.Cajas = Cajas;
            this.Peso = Peso;
            this.Kilogramos = Kilogramos;
            this.Precio = Precio;
            this.Total = Total;         
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Notas_Salida_Detalles.Consultevent(this); }        

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Notas_Salida_Detalles"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Nota"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("No_Nota", No_Nota.Trim()));
            parametrosBD.Add(new ParametroBD("Clave", Clave.Trim()));
            parametrosBD.Add(new ParametroBD("Producto", Producto.ToString()));            
            parametrosBD.Add(new ParametroBD("Cajas", Cajas.Trim()));
            parametrosBD.Add(new ParametroBD("Peso", Peso.Trim()));
            parametrosBD.Add(new ParametroBD("Kilogramos", Kilogramos.ToString()));            
            parametrosBD.Add(new ParametroBD("Precio", Precio.ToString()));
            parametrosBD.Add(new ParametroBD("Total", Total.ToString()));        
            return parametrosBD;
        }
    }
}