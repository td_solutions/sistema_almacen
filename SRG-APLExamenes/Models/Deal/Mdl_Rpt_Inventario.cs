﻿using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Rpt_Inventario : TablaDB
    {
        public string Fecha { get; set; }
        public int Almacen_ID { get; set; }
        public string Almacen { get; set; }
        public string Codigo_Barras { get; set; }
        public int Insumo_ID { get; set; }
        public string Descripcion { get; set; }
        public int Existencias { get; set; }
        public DataTable dt_existencias { get; set; }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Rpt_Inventario.Consulta(this); }
        public DataTable Consulta_Reorden() { return Ctrl_Rpt_Inventario.Consulta_Reorden(this); }
        public DataTable ConsultDet() { return Ctrl_Rpt_Inventario.Consulta_Detalles(this); }

        //public Boolean Alta_Entrada(MODO_DE_CAPTURA captura) { return this.MaterManagementEntrada(this, captura); }



        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Entradas"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Entrada"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("Almacen_ID", Almacen_ID.ToString()));
            return parametrosBD;
        }
    }
}
