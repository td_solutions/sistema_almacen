﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Actividades : TablaDB
    {
        public String No_Actividad { get; set; }
        public String Fecha { get; set; }
        public String Labor { get; set; }
        public String Tabla_ID { get; set; }
        public String No_Tuneles { get; set; }
        public String No_Empleado { get; set; }
        public String No_Tarjeta { get; set; }
        public String Monto { get; set; }
        public String Estatus { get; set; }   

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public String Fecha_Inicio { get; set; }
        public String Fecha_Fin { get; set; }

        public Mdl_Actividades() { }

        public Mdl_Actividades(String No_Actividad, String Fecha = "", String Labor = "", String Tabla_ID = "", String No_Tuneles = "", String No_Empleado = "", String No_Tarjeta = "", String Monto = "", String Estatus = "")
        {
            this.No_Actividad = No_Actividad;
            this.Fecha = Fecha;
            this.Labor = Labor;
            this.Tabla_ID = Tabla_ID;
            this.No_Tuneles = No_Tuneles;
            this.No_Empleado = No_Empleado;
            this.No_Tarjeta = No_Tarjeta;
            this.Monto = Monto;
            this.Estatus = Estatus;
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Actividades.Consultevent(this); }
        public DataTable ConsultMaximoActividad() { return Ctrl_Actividades.ConsultMaximoActividad(this); }        

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Actividades"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Actividad"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("No_Actividad", No_Actividad.Trim()));
            parametrosBD.Add(new ParametroBD("Fecha", Fecha.Trim()));
            parametrosBD.Add(new ParametroBD("Labor", Labor.Trim()));
            //parametrosBD.Add(new ParametroBD("Tabla_ID", Tabla_ID.Trim()));
            //if (!String.IsNullOrEmpty(No_Tuneles))
            //    parametrosBD.Add(new ParametroBD("No_Tuneles", No_Tuneles.Trim()));
            //if (!String.IsNullOrEmpty(No_Empleado.Trim()))
            //    parametrosBD.Add(new ParametroBD("No_Empleado", No_Empleado.Trim()));
            //parametrosBD.Add(new ParametroBD("No_Tarjeta", No_Tarjeta.ToString()));
            //if (!String.IsNullOrEmpty(Monto.ToString().Trim()))
            //    parametrosBD.Add(new ParametroBD("Monto", Monto.ToString()));
            //else
            //    parametrosBD.Add(new ParametroBD("Monto", "0"));
            parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
            parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));  
            return parametrosBD;
        }
    }
}