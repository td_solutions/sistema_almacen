﻿using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Salidas : TablaDB
    {
        public int No_Salida { get; set; }
        public int Insumo_ID { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime Fecha_Inicio { get; set; }
        public DateTime Fecha_Fin { get; set; }
        public string Str_Fecha_Inicio { get; set; }
        public string Str_Fecha_Fin { get; set; }
        public string Str_Fecha { get; set; }
        public int Almacen_Origen_ID { get; set; }
        public int Almacen_Destino_ID { get; set; }
        public string Realiza { get; set; }
        public string Observaciones { get; set; }
        public string Tipo { get; set; }
        public string Tipo_Actividad { get; set; }
        public string Estatus { get; set; }
        public decimal Costo_Total { get; set; }
        public string Salio { get; set; }
        public string No_Fumigacion { get; set; }
        public string No_Riego { get; set; }
        public List<Mdl_Salidas_Detalles> Detalles { get; set; }
        public String Usuario_Registro { get; set; }
        public DateTime Fecha_Registro { get; set; }
        public String Captura { get; set; }
        public Mdl_Salidas() { }
        public Mdl_Salidas(int No_Salida, int Almacen_Origen_ID, string Realiza, string Tipo, string Estatus, int Almacen_Destino_ID = 0, decimal Costo_Total = 0, string Fecha = "", string Salio = "", string No_Fumigacion = "", string No_Riego = "")
        {
            this.No_Salida = No_Salida;
            this.Almacen_Origen_ID = Almacen_Origen_ID;
            this.Almacen_Destino_ID = Almacen_Destino_ID;
            this.Tipo = Tipo;
            this.Estatus = Estatus;
            this.Costo_Total = Costo_Total;
            this.Salio = Salio;
            this.No_Fumigacion = No_Fumigacion;
            this.No_Riego = No_Riego;

            try
            {
                this.Fecha = DateTime.Parse(Fecha);
            }
            catch
            {
                this.Fecha = DateTime.MinValue;
            }
            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = DateTime.Now;
        }
        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public Boolean AltaSalida(MODO_DE_CAPTURA captura) { return Ctrl_Salidas.AltaSalida(this, captura, this.Detalles); }
        internal DataTable ConsultarReporte() { return Ctrl_Salidas.ConsultaReporte(this); }
        internal DataTable ConsultarReporteDetalles() { return Ctrl_Salidas.ConsultaReporteDetalles(this); }
        public Boolean UpdateSalida(MODO_DE_CAPTURA captura) { return Ctrl_Salidas.UpdateSalidas(this, captura, this.Detalles); }
        public DataTable Consult() { return Ctrl_Salidas.Consulta(this); }
        public DataTable ConsultDet() { return Ctrl_Salidas.Consulta_Detalles(this); }

        //public Boolean Alta_Entrada(MODO_DE_CAPTURA captura) { return this.MaterManagementEntrada(this, captura); }



        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Salidas"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Salida"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("No_Salida", No_Salida.ToString()));
            if (Almacen_Origen_ID != 0)
                parametrosBD.Add(new ParametroBD("Almacen_Origen_ID", Almacen_Origen_ID.ToString()));
            if (!String.IsNullOrEmpty(Realiza))
                parametrosBD.Add(new ParametroBD("Realiza", Realiza.ToString()));
            if (Fecha != DateTime.MinValue)
                parametrosBD.Add(new ParametroBD("Fecha", Fecha.ToString("dd/MM/yyyy")));
            if (Almacen_Destino_ID != 0)
                parametrosBD.Add(new ParametroBD("Almacen_Destino_ID", Almacen_Destino_ID.ToString()));
            if (!String.IsNullOrEmpty(Tipo))
                parametrosBD.Add(new ParametroBD("Tipo", Tipo.Trim()));
            if (!String.IsNullOrEmpty(Tipo_Actividad))
                parametrosBD.Add(new ParametroBD("Tipo_Actividad", Tipo_Actividad.Trim()));
            if (!String.IsNullOrEmpty(Observaciones))
                parametrosBD.Add(new ParametroBD("Observaciones", Observaciones.Trim()));
            if (!String.IsNullOrEmpty(Costo_Total.ToString()))
                parametrosBD.Add(new ParametroBD("Costo_Total", Costo_Total.ToString()));            
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (!String.IsNullOrEmpty(Salio))
                parametrosBD.Add(new ParametroBD("Salio", Salio.Trim()));
            if (!String.IsNullOrEmpty(No_Fumigacion))
                parametrosBD.Add(new ParametroBD("No_Fumigacion", No_Fumigacion.Trim()));
            if (!String.IsNullOrEmpty(No_Riego))
                parametrosBD.Add(new ParametroBD("No_Riego", No_Riego.Trim()));

            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA) || Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA_IDENTITY))
            {
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.ToString("dd/MM/yyyy")));
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            }
            else
            {
                if (!String.IsNullOrEmpty(Usuario_Registro))
                    parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                if (Fecha_Registro != DateTime.MinValue)
                    parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro.ToString("dd/MM/yyyy")));
            }
            return parametrosBD;
        }

    }
}
