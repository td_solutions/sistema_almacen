﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Tablas : TablaDB
    {
        public String Tabla_ID { get; set; }
        public String Clave { get; set; }
        public String Nombre { get; set; }
        public String Medida_HAS { get; set; }        
        public String Estatus { get; set; }
        public String No_Tuneles { get; set; }
        public String Rancho_ID { get; set; }

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public Mdl_Tablas() { }

        public Mdl_Tablas(String Tabla_ID, String Clave = "", String Nombre = "", String Medida_HAS = "", String Estatus = "", String No_Tuneles = "", String Rancho_ID = "")
        {
            this.Tabla_ID = Tabla_ID;
            this.Clave = Clave;
            this.Nombre = Nombre;
            this.Medida_HAS = Medida_HAS;            
            this.Estatus = Estatus;
            this.No_Tuneles = No_Tuneles;
            this.Rancho_ID = Rancho_ID;           
            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = Fecha_Registro;            
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Tablas.Consultevent(this); }
        public DataTable ConsultMaximoTablas() { return Ctrl_Tablas.ConsultMaximoTablas(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Cat_Tablas"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Tabla_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Tabla_ID", Tabla_ID));
            if (!String.IsNullOrEmpty(Clave))
                parametrosBD.Add(new ParametroBD("Clave", Clave.Trim()));
            if (!String.IsNullOrEmpty(Nombre))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            if (!String.IsNullOrEmpty(Medida_HAS))
                parametrosBD.Add(new ParametroBD("Medida_HAS", Medida_HAS.Trim()));            
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (!String.IsNullOrEmpty(No_Tuneles))
                parametrosBD.Add(new ParametroBD("No_Tuneles", No_Tuneles.Trim()));
            if (!String.IsNullOrEmpty(Rancho_ID))
                parametrosBD.Add(new ParametroBD("Rancho_ID", Rancho_ID.Trim()));
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            }
            else
            {                
                if (!String.IsNullOrEmpty(Usuario_Registro))
                    parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                if (!String.IsNullOrEmpty(Fecha_Registro))
                    parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro));
            }
            return parametrosBD;
        }
    }
}