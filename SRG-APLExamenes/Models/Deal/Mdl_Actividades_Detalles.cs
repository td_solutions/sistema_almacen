﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Actividades_Detalles : TablaDB
    {
        public String No_Actividad { get; set; }        
        public String No_Empleado { get; set; }
        public String No_Tarjeta { get; set; }
        public String Nombre { get; set; }        
        public Decimal Monto { get; set; } 
               
        public String Captura { get; set; }

        public Mdl_Actividades_Detalles() { }

        public Mdl_Actividades_Detalles(String No_Actividad, String No_Empleado = "", String No_Tarjeta = "", String Nombre = "", Decimal Monto = 0)
        {
            this.No_Actividad = No_Actividad;
            this.No_Empleado = No_Empleado;
            this.No_Tarjeta = No_Tarjeta;
            this.Nombre = Nombre;
            this.Monto = Monto;                     
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Actividades_Detalles.Consultevent(this); }        

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Actividades_Detalles"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Actividad"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("No_Actividad", No_Actividad.Trim()));
            if (!String.IsNullOrEmpty(No_Empleado.Trim()))
                parametrosBD.Add(new ParametroBD("No_Empleado", No_Empleado.Trim()));
            parametrosBD.Add(new ParametroBD("No_Tarjeta", No_Tarjeta.Trim()));
            if (!String.IsNullOrEmpty(Nombre.Trim()))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            parametrosBD.Add(new ParametroBD("Monto", Monto.ToString()));            
            return parametrosBD;
        }
    }
}