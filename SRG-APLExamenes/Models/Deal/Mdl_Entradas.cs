﻿using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Entradas : TablaDB
    {
        public int No_Entrada { get; set; }
        public int Proveedor_ID { get; set; }
        public int Almacen_ID { get; set; }
        public string Tipo_Factura { get; set; }
        public string No_Factura { get; set; }
        public DateTime Fecha_Factura { get; set; }
        public DateTime Fecha_Recepcion { get; set; }
        public DateTime Fecha_Pago { get; set; }
        public DateTime Fecha_Registro { get; set; }
        public string Str_Fecha_Factura { get; set; }
        public string Str_Fecha_Recepcion { get; set; }
        public string Str_Fecha_Pago { get; set; }
        public string Str_Fecha_Registro { get; set; }
        public double Total_Factura { get; set; }
        public double Subtotal { get; set; }
        public double Iva { get; set; }
        public double Ieps { get; set; }
        public double Total { get; set; }
        public List<Mdl_Entradas_Detalles> Detalles { get; set; }
        public String Captura { get; set; }
        public string Estatus { get; set; }
        public string Tipo_Entrada { get; set; }
        public string Observaciones { get; set; }
        public String Usuario_Registro { get; set; }
        public string Proveedor { get; set; }
        public string Almacen { get; set; }

        public int Insumo_ID { get; set; }
        public string Insumo { get; set; }
        public string Str_Fecha_Inicio { get; set; }
        public string Str_Fecha_Fin { get; set; }
        public DateTime Fecha_Inicio { get; set; }
        public DateTime Fecha_Fin { get; set; }

        public Mdl_Entradas() { }
        public Mdl_Entradas(int No_Entrada, int Proveedor_ID, int Almacen_ID, string Tipo_Factura = "", string No_Factura = "", string Fecha_Factura = "", string Fecha_Recepcion = "", string Fecha_Pago = "", string Fecha_Registro="", double Total_Factura = 0)
        {
            this.No_Entrada = No_Entrada;
            this.Proveedor_ID = Proveedor_ID;
            this.Almacen_ID = Almacen_ID;
            this.Tipo_Factura = Tipo_Factura;
            this.No_Factura = No_Factura;
            try
            {
                this.Fecha_Factura = DateTime.Parse(Fecha_Factura);
            }
            catch
            {
                this.Fecha_Factura = DateTime.MinValue;
            }
            try
            {
                this.Fecha_Recepcion = DateTime.Parse(Fecha_Recepcion);
            }
            catch
            {
                this.Fecha_Recepcion = DateTime.MinValue;
            }
            try
            {
                this.Fecha_Pago = DateTime.Parse(Fecha_Pago);
            }
            catch
            {
                this.Fecha_Pago = DateTime.MinValue;
            }
            try
            {
                this.Fecha_Registro = DateTime.Parse(Fecha_Registro);
            }
            catch
            {
                this.Fecha_Registro = DateTime.MinValue;
            }

            this.Total_Factura = Total_Factura;
            this.Estatus = Estatus;
            this.Usuario_Registro = Usuario_Registro;
            //this.Fecha_Registro = DateTime.Now;
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 
        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public Int32 AltaEntrada(MODO_DE_CAPTURA captura) { return Ctrl_Entradas.AltaEntrada(this, captura,this.Detalles); }
        public Boolean UpdateEntrada(MODO_DE_CAPTURA captura) { return Ctrl_Entradas.UpdateEntrada(this, captura, this.Detalles); }
        public DataTable Consult() { return Ctrl_Entradas.Consulta(this); }
        internal DataTable ConsultarReporte() { return Ctrl_Entradas.ConsultaReporte(this); }
        internal DataTable ConsultaDetalleReporte() { return Ctrl_Entradas.ConsultaDetalleReporte(this); }
        public DataTable ConsultDet() { return Ctrl_Entradas.Consulta_Detalles(this); }
        public DataTable Obtener_Reimpresion() { return Ctrl_Entradas.Obtener_Reimpresion(this); }

        //public Boolean Alta_Entrada(MODO_DE_CAPTURA captura) { return this.MaterManagementEntrada(this, captura); }



        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Entradas"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Entrada"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("No_Entrada", No_Entrada.ToString()));
            parametrosBD.Add(new ParametroBD("Almacen_ID", Almacen_ID.ToString()));
            parametrosBD.Add(new ParametroBD("Proveedor_ID", Proveedor_ID.ToString()));
            if (!String.IsNullOrEmpty(Tipo_Factura))
                parametrosBD.Add(new ParametroBD("Tipo_Factura", Tipo_Factura.Trim()));
            if (!String.IsNullOrEmpty(Tipo_Entrada))
                parametrosBD.Add(new ParametroBD("Tipo_Entrada", Tipo_Entrada.Trim()));
            if (!String.IsNullOrEmpty(Observaciones))
                parametrosBD.Add(new ParametroBD("Observaciones", Observaciones.Trim()));
            if (!String.IsNullOrEmpty(No_Factura))
                parametrosBD.Add(new ParametroBD("No_Factura", No_Factura.Trim()));
            if (Fecha_Registro != DateTime.MinValue)
                parametrosBD.Add(new ParametroBD("Fecha_Entrada", Fecha_Registro.ToString("dd/MM/yyyy")));
            if (Fecha_Factura != DateTime.MinValue)
                parametrosBD.Add(new ParametroBD("Fecha_Factura", Fecha_Factura.ToString("dd/MM/yyyy")));
            if (Fecha_Recepcion != DateTime.MinValue)
                parametrosBD.Add(new ParametroBD("Fecha_Recepcion", Fecha_Recepcion.ToString("dd/MM/yyyy")));
            if (Fecha_Pago != DateTime.MinValue)
                parametrosBD.Add(new ParametroBD("Fecha_Pago", Fecha_Pago.ToString("dd/MM/yyyy")));
            if (!String.IsNullOrEmpty(Total_Factura.ToString()))
                parametrosBD.Add(new ParametroBD("Total_Factura", Total_Factura.ToString().Trim()));
            if (!String.IsNullOrEmpty(Subtotal.ToString()))
                parametrosBD.Add(new ParametroBD("Subtotal", Subtotal.ToString().Trim()));
            if (!String.IsNullOrEmpty(Iva.ToString()))
                parametrosBD.Add(new ParametroBD("Iva", Iva.ToString().Trim()));
            if (!String.IsNullOrEmpty(Ieps.ToString()))
                parametrosBD.Add(new ParametroBD("Ieps", Ieps.ToString().Trim()));
            if (!String.IsNullOrEmpty(Total.ToString()))
                parametrosBD.Add(new ParametroBD("Total", Total.ToString().Trim()));
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));

            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA) || Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA_IDENTITY))
            {
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.ToString("dd/MM/yyyy")));
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            }
            else
            {
                if (!String.IsNullOrEmpty(Usuario_Registro))
                    parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                if (Fecha_Registro != DateTime.MinValue)
                    parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro.ToString("dd/MM/yyyy")));
            }
            return parametrosBD;
        }

        public static implicit operator Mdl_Entradas(JsonResult v)
        {
            throw new NotImplementedException();
        }
    }
}