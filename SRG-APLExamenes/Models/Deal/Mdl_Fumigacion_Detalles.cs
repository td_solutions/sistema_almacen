﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Fumigacion_Detalles : TablaDB
    {
        public String No_Fumigacion { get; set; }        
        public String No_Empleado { get; set; }
        public String No_Tarjeta { get; set; }
        public String Nombre { get; set; }
        public Decimal Cantidad { get; set; }
        public Decimal Pago { get; set; } 
               
        public String Captura { get; set; }

        public Mdl_Fumigacion_Detalles() { }

        public Mdl_Fumigacion_Detalles(String No_Fumigacion, String No_Empleado = "", String No_Tarjeta = "", String Nombre = "", Decimal Cantidad = 0, Decimal Pago = 0)
        {
            this.No_Fumigacion = No_Fumigacion;
            this.No_Empleado = No_Empleado;
            this.No_Tarjeta = No_Tarjeta;
            this.Nombre = Nombre;
            this.Cantidad = Cantidad;
            this.Pago = Pago;                     
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Fumigacion_Detalles.Consultevent(this); }        

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Fumigacion_Detalles"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Fumigacion"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("No_Fumigacion", No_Fumigacion.Trim()));
            if (!String.IsNullOrEmpty(No_Empleado.Trim()))
                parametrosBD.Add(new ParametroBD("No_Empleado", No_Empleado.Trim()));
            parametrosBD.Add(new ParametroBD("No_Tarjeta", No_Tarjeta.Trim()));
            if (!String.IsNullOrEmpty(Nombre.Trim()))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            parametrosBD.Add(new ParametroBD("Cantidad", Cantidad.ToString()));
            parametrosBD.Add(new ParametroBD("Pago", Pago.ToString()));            
            return parametrosBD;
        }
    }
}