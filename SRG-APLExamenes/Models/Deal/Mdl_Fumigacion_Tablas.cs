﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Fumigacion_Tablas : TablaDB
    {
        public String No_fumigacion { get; set; }        
        public String Tabla_ID { get; set; }
        public Decimal No_Tuneles { get; set; }
        
        public String Captura { get; set; }

        public Mdl_Fumigacion_Tablas() { }

        public Mdl_Fumigacion_Tablas(String No_fumigacion, String Tabla_ID = "", Decimal No_Tuneles = 0)
        {
            this.No_fumigacion = No_fumigacion;
            this.Tabla_ID = Tabla_ID;
            this.No_Tuneles = No_Tuneles;                     
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Fumigacion_Tablas.Consultevent(this); }        

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Fumigacion_Tablas"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_fumigacion"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("No_fumigacion", No_fumigacion.Trim()));            
            parametrosBD.Add(new ParametroBD("Tabla_ID", Tabla_ID.Trim()));
            parametrosBD.Add(new ParametroBD("No_Tuneles", No_Tuneles.ToString().Trim()));
            return parametrosBD;
        }
    }
}