﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Entradas_Detalles
    {
        public int No_Entrada_Detalle { get; set; }
        public int No_Entrada { get; set; }
        public string Insumo { get; set; }
        public string Cantidad { get; set; }
        public string Unidad { get; set; }
        public string Unidad_ID { get; set; }
        public int Insumo_ID { get; set; }
        public decimal Precio { get; set; }
        public decimal Importe { get; set; }
        public decimal Total { get; set; }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }

    }
}