﻿using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Proveedores : TablaDB
    {
        public int Proveedor_ID { get; set; }
        public int Dias_Credito { get; set; }
        public string Nombre { get; set; }
        public string Razon_Social { get; set; }
        public string Estatus { get; set; }
        public string Rfc { get; set; }

        public string Direccion { get; set; }
        public string Colonia { get; set; }
        public string Cp { get; set; }
        public string Estado { get; set; }
        public string Telefono { get; set; }
        public string Contacto { get; set; }
        public double Descuento { get; set; }
        public string Email { get; set; }
        public double Limite_Credito { get; set; }
        public string Observaciones { get; set; }
        public string Ciudad { get; set; }

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public Mdl_Proveedores() { }
        public Mdl_Proveedores(int Proveedor_ID, int Dias_Credito = 0, string Nombre="", string Razon_Social = "", string Rfc = "" ,string Direccion = "", string Colonia = "", string Cp= "", string Estado= "", string Telefono = "", string Contacto = "", double Descuento = 0, string Email = "", double Limite_Credito = 0, string Observaciones = "", string Ciudad = "")
        {
            this.Nombre = Nombre;
            this.Dias_Credito = Dias_Credito;
            this.Razon_Social = Razon_Social;
            this.Rfc = Rfc;
            this.Direccion = Direccion;
            this.Colonia = Colonia;
            this.Cp = Cp;
            this.Estado = Estado;
            this.Telefono = Telefono;
            this.Contacto = Contacto;
            this.Descuento = Descuento;
            this.Email = Email;
            this.Limite_Credito = Limite_Credito;
            this.Observaciones = Observaciones;
            this.Ciudad = Ciudad;

            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = DateTime.Now.ToString();
        }
        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Proveedores.Consulta(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Cat_Proveedores"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Proveedor_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Proveedor_ID", Proveedor_ID.ToString()));
            if (!String.IsNullOrEmpty(Nombre))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            if (!String.IsNullOrEmpty(Dias_Credito.ToString()))
                parametrosBD.Add(new ParametroBD("Dias_Credito", Dias_Credito.ToString()));
            if (!String.IsNullOrEmpty(Razon_Social))
                parametrosBD.Add(new ParametroBD("Razon_Social", Razon_Social.Trim()));
            if (!String.IsNullOrEmpty(Rfc))
                parametrosBD.Add(new ParametroBD("Rfc", Rfc.Trim()));
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));

            if (!String.IsNullOrEmpty(Direccion))
                parametrosBD.Add(new ParametroBD("Direccion", Direccion.Trim()));
            if (!String.IsNullOrEmpty(Colonia))
                parametrosBD.Add(new ParametroBD("Colonia", Colonia.Trim()));
            if (!String.IsNullOrEmpty(Cp))
                parametrosBD.Add(new ParametroBD("Cp", Cp.Trim()));
            if (!String.IsNullOrEmpty(Ciudad))
                parametrosBD.Add(new ParametroBD("Ciudad", Ciudad.Trim()));
            if (!String.IsNullOrEmpty(Estado))
                parametrosBD.Add(new ParametroBD("Estado", Estado.Trim()));            
            if (!String.IsNullOrEmpty(Telefono))
                parametrosBD.Add(new ParametroBD("Telefono", Telefono.Trim()));
            if (!String.IsNullOrEmpty(Contacto))
                parametrosBD.Add(new ParametroBD("Contacto", Contacto.Trim()));
            if (!String.IsNullOrEmpty(Descuento.ToString()))
                parametrosBD.Add(new ParametroBD("Descuento", Descuento.ToString()));
            if (!String.IsNullOrEmpty(Email))
                parametrosBD.Add(new ParametroBD("Email", Email.Trim()));
            if (!String.IsNullOrEmpty(Limite_Credito.ToString()))
                parametrosBD.Add(new ParametroBD("Limite_Credito", Limite_Credito.ToString()));
            if (!String.IsNullOrEmpty(Observaciones))
                parametrosBD.Add(new ParametroBD("Observaciones", Observaciones.Trim()));

            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA) || Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA_IDENTITY))
            {
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            }
            else
            {
                if (!String.IsNullOrEmpty(Usuario_Registro))
                    parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                if (!String.IsNullOrEmpty(Fecha_Registro))
                    parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro));
            }
            return parametrosBD;
        }

    }
}