﻿using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mld_Rpt_Kardex 
    {
        public DateTime Fecha_Inicio { get; set; }
        public DateTime Fecha_Fin { get; set; }
        public int Insumo_ID { get; set; }

        public Mld_Rpt_Kardex() { }
        public Mld_Rpt_Kardex(DateTime fecha_inicio, DateTime fecha_fin, int insuimo_id = 0 )
        {
            this.Fecha_Inicio = fecha_inicio;
            this.Fecha_Fin = fecha_fin;
            this.Insumo_ID = insuimo_id;
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 
        public DataTable Consult() { return Ctrl_Rpt_Kardex.Consulta(this); }
        public DataTable ConsultarReporteDetalles() { return Ctrl_Rpt_Kardex.ConsultarReporteDetalles(this); }
    }
}