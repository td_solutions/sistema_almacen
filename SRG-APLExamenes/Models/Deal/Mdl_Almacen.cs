﻿using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Almacen : TablaDB
    {
        public int Almacen_ID { get; set; }
        public string Nombre { get; set; }
        public string Clave { get; set; }
        public string Descripcion { get; set; }
        public string Estatus { get; set; }
        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }
        public int Rancho_ID { get; set; }

        public Mdl_Almacen() { }
        public Mdl_Almacen(int Almacen_ID, int Rancho_ID, String Nombre = "", String Clave = "", String Descripcion = "", String Estatus = "")
        {
            this.Almacen_ID = Almacen_ID;
            this.Rancho_ID = Rancho_ID;
            this.Nombre = Nombre;
            this.Clave = Clave;
            this.Descripcion = Descripcion;
            this.Estatus = Estatus;
            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = DateTime.Now.ToString();
        }
        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Almacen.Consulta(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Cat_Almacen"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Almacen_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Almacen_ID", Almacen_ID.ToString()));
            parametrosBD.Add(new ParametroBD("Rancho_ID", Rancho_ID.ToString()));
            if (!String.IsNullOrEmpty(Nombre))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            if (!String.IsNullOrEmpty(Clave))
                parametrosBD.Add(new ParametroBD("Clave", Clave.Trim()));
            if (!String.IsNullOrEmpty(Descripcion))
                parametrosBD.Add(new ParametroBD("Descripcion", Descripcion.Trim()));
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            }
            else
            {
                if (!String.IsNullOrEmpty(Usuario_Registro))
                    parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                if (!String.IsNullOrEmpty(Fecha_Registro))
                    parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro));
            }
            return parametrosBD;
        }


    }
}