﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Movimientos_Pagos : TablaDB
    {
        public String NO_MOVIMIENTO { get; set; }
        public String PROVEEDOR_ID { get; set; }
        public String TIPO { get; set; }
        public String ESTATUS { get; set; }
        public String FECHA { get; set; }
        public String REFERENCIA { get; set; }
        public String NO_FACTURA_PROVEEDOR { get; set; }
        public String FORMA_PAGO { get; set; }
        public String CANTIDAD { get; set; }
        public String MONEDA { get; set; }
        public String TIPO_CAMBIO { get; set; }
        public String CONCEPTO { get; set; }
        public String SALDO { get; set; }
        public String BENEFICIARIO { get; set; }
        public String FECHA_CANCELACION { get; set; }
        public String MOTIVO_CANCELACION { get; set; }

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public String Fecha_Inicio { get; set; }
        public String Fecha_Fin { get; set; }

        public Mdl_Movimientos_Pagos() { }

        public Mdl_Movimientos_Pagos(String NO_MOVIMIENTO, String PROVEEDOR_ID = "", String TIPO = "", String ESTATUS = "", String FECHA = "", String REFERENCIA = "",
                            String NO_FACTURA_PROVEEDOR = "", String FORMA_PAGO = "", String CANTIDAD = "", String MONEDA = "", String TIPO_CAMBIO = "", String CONCEPTO = "",
                            String SALDO = "", String BENEFICIARIO = "", String FECHA_CANCELACION = "", String MOTIVO_CANCELACION = "")
        {
            this.NO_MOVIMIENTO = NO_MOVIMIENTO;
            this.PROVEEDOR_ID = PROVEEDOR_ID;
            this.TIPO = TIPO;
            this.ESTATUS = ESTATUS;
            this.FECHA = FECHA;
            this.REFERENCIA = REFERENCIA;
            this.NO_FACTURA_PROVEEDOR = NO_FACTURA_PROVEEDOR;
            this.FORMA_PAGO = FORMA_PAGO;
            this.CANTIDAD = CANTIDAD;
            this.MONEDA = MONEDA;
            this.TIPO_CAMBIO = TIPO_CAMBIO;
            this.CONCEPTO = CONCEPTO;
            this.SALDO = SALDO;
            this.BENEFICIARIO = BENEFICIARIO;
            this.FECHA_CANCELACION = FECHA_CANCELACION;
            this.MOTIVO_CANCELACION = MOTIVO_CANCELACION;            
            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = Fecha_Registro;            
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Movimientos_Pagos.Consultevent(this); }
        public DataTable ConsultMaximoMovimiento() { return Ctrl_Movimientos_Pagos.ConsultMaximoMovimiento(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "OPE_MOVIMIENTOS_PAGOS"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "NO_MOVIMIENTO"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("NO_MOVIMIENTO", NO_MOVIMIENTO));
            if (!String.IsNullOrEmpty(PROVEEDOR_ID))
                parametrosBD.Add(new ParametroBD("PROVEEDOR_ID", PROVEEDOR_ID.Trim()));
            if (!String.IsNullOrEmpty(TIPO))
                parametrosBD.Add(new ParametroBD("TIPO", TIPO.Trim()));
            if (!String.IsNullOrEmpty(ESTATUS))
                parametrosBD.Add(new ParametroBD("ESTATUS", ESTATUS.Trim()));
            if (!String.IsNullOrEmpty(FECHA))
                parametrosBD.Add(new ParametroBD("FECHA", FECHA.Trim()));
            if (!String.IsNullOrEmpty(REFERENCIA))
                parametrosBD.Add(new ParametroBD("REFERENCIA", REFERENCIA.Trim()));
            if (!String.IsNullOrEmpty(NO_FACTURA_PROVEEDOR))
                parametrosBD.Add(new ParametroBD("NO_FACTURA_PROVEEDOR", NO_FACTURA_PROVEEDOR.ToString()));
            if (!String.IsNullOrEmpty(FORMA_PAGO))
                parametrosBD.Add(new ParametroBD("FORMA_PAGO", FORMA_PAGO.Trim()));
            if (!String.IsNullOrEmpty(CANTIDAD))
                parametrosBD.Add(new ParametroBD("CANTIDAD", CANTIDAD.ToString()));
            if (!String.IsNullOrEmpty(MONEDA))
                parametrosBD.Add(new ParametroBD("MONEDA", MONEDA.ToString()));
            if (!String.IsNullOrEmpty(TIPO_CAMBIO))
                parametrosBD.Add(new ParametroBD("TIPO_CAMBIO", TIPO_CAMBIO.Trim()));
            if (!String.IsNullOrEmpty(CONCEPTO))
                parametrosBD.Add(new ParametroBD("CONCEPTO", CONCEPTO.Trim()));
            if (!String.IsNullOrEmpty(SALDO))
                parametrosBD.Add(new ParametroBD("SALDO", SALDO.Trim()));
            if (!String.IsNullOrEmpty(BENEFICIARIO))
                parametrosBD.Add(new ParametroBD("BENEFICIARIO", BENEFICIARIO.Trim()));
            if (!String.IsNullOrEmpty(FECHA_CANCELACION))
                parametrosBD.Add(new ParametroBD("FECHA_CANCELACION", FECHA_CANCELACION.Trim()));
            if (!String.IsNullOrEmpty(MOTIVO_CANCELACION))
                parametrosBD.Add(new ParametroBD("MOTIVO_CANCELACION", MOTIVO_CANCELACION.ToString()));

            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            }
            else
            {
                if (!String.IsNullOrEmpty(Usuario_Registro))
                    parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                if (!String.IsNullOrEmpty(Fecha_Registro))
                    parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro));
            }
            return parametrosBD;
        }
    }
}