﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Automoviles_Tablas : TablaDB
    {
        public String No_Automovil { get; set; }        
        public String Tabla_ID { get; set; }        
        
        public String Captura { get; set; }

        public Mdl_Automoviles_Tablas() { }

        public Mdl_Automoviles_Tablas(String No_Automovil, String Tabla_ID = "")
        {
            this.No_Automovil = No_Automovil;
            this.Tabla_ID = Tabla_ID;                                 
        }

        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Automoviles_Tablas.Consultevent(this); }        

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Ope_Automoviles_Tablas"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "No_Automovil"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();
            parametrosBD.Add(new ParametroBD("No_Automovil", No_Automovil.Trim()));            
            parametrosBD.Add(new ParametroBD("Tabla_ID", Tabla_ID.Trim()));
            return parametrosBD;
        }
    }
}