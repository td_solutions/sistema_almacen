﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Rpt_Entradas
    {
        public string Fecha { get; set; }
        public int Almacen_ID { get; set; }
        public string Almacen { get; set; }
        public string Codigo_Barras { get; set; }
        public int Insumo_ID { get; set; }
        public string Descripcion { get; set; }
        public int Existencias { get; set; }
        public DataTable dt_partidas { get; set; }
    }
}