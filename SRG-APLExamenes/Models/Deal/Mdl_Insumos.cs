﻿using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Insumos : TablaDB
    {
        public int Insumo_ID { get; set; }
        public int Tipo_Insumo_ID { get; set; }
        public int Unidad_ID { get; set; }
        public int Unidad_Consumo_ID { get; set; }
        public int Proveedor_ID { get; set; }
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
        public String Estatus { get; set; }
        public String Ubicacion { get; set; }
        public double Existencias { get; set; }
        public double Costo { get; set; }
        public double Equivalencia { get; set; }
        public double Ieps { get; set; }
        public double Iva { get; set; }
        public double Total { get; set; }
        public int Maximo { get; set; }
        public int Minimo { get; set; }
        public int Punto_Reorden { get; set; }

        //public int Almacen_ID { get; set; }

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }

        public String Captura { get; set; }
        public Mdl_Insumos() { }
        public Mdl_Insumos(int Insumo_ID, int Tipo_Insumo_ID, int Unidad_ID, int Proveedor_ID, String Nombre = "", String Descripcion = "", String Ubicacion = "", String Estatus = "", int Maximo = 0, int Minimo = 0, int Punto_Reorden = 0, double costo = 0, double Existencias = 0)
        {
            this.Insumo_ID = Insumo_ID;
            this.Tipo_Insumo_ID = Tipo_Insumo_ID;
            this.Unidad_ID = Unidad_ID;
            this.Proveedor_ID = Proveedor_ID;
            this.Nombre = Nombre;
            this.Descripcion = Descripcion;
            this.Ubicacion = Ubicacion;
            this.Estatus = Estatus;
            this.Punto_Reorden = Punto_Reorden;
            this.Maximo = Maximo;
            this.Minimo = Minimo;
            this.Costo = costo;
            //this.Almacen_ID = Almacen_ID;
            this.Existencias = Existencias;
            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = DateTime.Now.ToString();
        }
        //********************************// IMPLEMENTACIONES DE METODOS  //********************************// 

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Operaciones.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Insumos.Consulta(this); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Cat_Insumos"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Insumo_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();

            parametrosBD.Add(new ParametroBD("Insumo_ID", Insumo_ID.ToString()));
            parametrosBD.Add(new ParametroBD("Tipo_Insumo_ID", Tipo_Insumo_ID.ToString()));
            parametrosBD.Add(new ParametroBD("Unidad_ID", Unidad_ID.ToString()));
            parametrosBD.Add(new ParametroBD("Unidad_Consumo_ID", Unidad_Consumo_ID.ToString()));
            parametrosBD.Add(new ParametroBD("Equivalencia", Equivalencia.ToString()));
            parametrosBD.Add(new ParametroBD("Proveedor_ID", Proveedor_ID.ToString()));
            //parametrosBD.Add(new ParametroBD("Almacen_ID", Almacen_ID.ToString()));
            if (!String.IsNullOrEmpty(Nombre))
                parametrosBD.Add(new ParametroBD("Nombre", Nombre.Trim()));
            if (!String.IsNullOrEmpty(Descripcion))
                parametrosBD.Add(new ParametroBD("Descripcion", Descripcion.Trim()));
            if (!String.IsNullOrEmpty(Ubicacion))
                parametrosBD.Add(new ParametroBD("Ubicacion", Ubicacion.Trim()));
            if (!String.IsNullOrEmpty(Estatus))
                parametrosBD.Add(new ParametroBD("Estatus", Estatus.Trim()));
            if (Punto_Reorden>0)
                parametrosBD.Add(new ParametroBD("Punto_Reorden", Punto_Reorden.ToString()));
            if (Maximo > 0)
                parametrosBD.Add(new ParametroBD("Maximo", Maximo.ToString()));
            if (Minimo> 0)
                parametrosBD.Add(new ParametroBD("Minimo", Minimo.ToString()));
            if (Costo > 0)
                parametrosBD.Add(new ParametroBD("Costo", Costo.ToString()));
            parametrosBD.Add(new ParametroBD("IVA", Iva.ToString()));
            parametrosBD.Add(new ParametroBD("IEPS", Ieps.ToString()));
            parametrosBD.Add(new ParametroBD("Total", Total.ToString()));
            parametrosBD.Add(new ParametroBD("Existencias", Existencias.ToString()));
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
            {
                parametrosBD.Add(new ParametroBD("Fecha_Creo", Fecha_Registro.Trim()));
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            }
            else
            {
                if (!String.IsNullOrEmpty(Usuario_Registro))
                    parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                if (!String.IsNullOrEmpty(Fecha_Registro))
                    parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro));
            }
            return parametrosBD;
        }


    }
}