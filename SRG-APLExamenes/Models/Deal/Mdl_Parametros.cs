﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Data;

namespace SRG_APLExamenes.Models.Deal
{
    public class Mdl_Parametros :TablaDB
    {
        public String Parametro_ID { get; set; }        
        public String Proveedor_Servicio { get; set; }
        public String Puerto { get; set; }
        public String SSL_Cifrado { get; set; }
        public String Servidor_Correo { get; set; }
        public String Contraseña { get; set; }        
        public String Comentarios { get; set; }
        public String Rancho_ID { get; set; }
        public String Precio_Litro_Fumigacion { get; set; }

        public String Usuario_Registro { get; set; }
        public String Fecha_Registro { get; set; }
        public String Captura { get; set; }

        public Mdl_Parametros() { }

        public Mdl_Parametros(String Parametro_ID = "", String Proveedor_Servicio = "", String Puerto = "",
            String SSL_Cifrado = "", String Servidor_Correo = "", String Contraseña = "", String Usuario_Registro = "",
            String Fecha_Registro = "", String Comentarios = "", String Rancho_ID = "", String Precio_Litro_Fumigacion = "")
        {
            this.Parametro_ID = Parametro_ID;            
            this.Proveedor_Servicio = Proveedor_Servicio;
            this.Puerto = Puerto;
            this.SSL_Cifrado = SSL_Cifrado;
            this.Servidor_Correo = Servidor_Correo;
            this.Contraseña = Contraseña;            
            this.Usuario_Registro = Usuario_Registro;
            this.Fecha_Registro = Fecha_Registro;
            this.Comentarios = Comentarios;
            this.Rancho_ID = Rancho_ID;
            this.Precio_Litro_Fumigacion = Precio_Litro_Fumigacion;
        }

        public Boolean MasterManagement(MODO_DE_CAPTURA captura) { return Ctrl_Parametros.MasterManagement(this, captura); }
        public DataTable Consult() { return Ctrl_Parametros.Consultparameters(); }

        //********************************// IMPLEMENTACIONES DE LA INTERFAZ  //********************************// 

        /// <summary>
        /// Obtiene el nombre de la tabla.
        /// </summary>
        public String Tabla { get { return "Apl_Parametros"; } }
        /// <summary>
        /// Obtiene el id de la tabla.
        /// </summary>
        public String ID { get { return "Parametro_ID"; } }
        /// <summary>
        /// Obtiene los parametros de operacion en la BD.
        /// </summary>
        /// <returns>Lista de Parámetros para operaciones en la BD.</returns>
        public List<ParametroBD> ObtenParametros(MODO_DE_CAPTURA Captura)
        {
            //Creamos los parametros de BD.
            List<ParametroBD> parametrosBD = new List<ParametroBD>();            
            parametrosBD.Add(new ParametroBD("Proveedor_Servicio", Proveedor_Servicio.Trim()));
            parametrosBD.Add(new ParametroBD("Puerto", Puerto));
            parametrosBD.Add(new ParametroBD("SSL_Cifrado", SSL_Cifrado));
            parametrosBD.Add(new ParametroBD("Servidor_Correo", Servidor_Correo.Trim()));
            if (!String.IsNullOrEmpty(Contraseña.Trim()))
                parametrosBD.Add(new ParametroBD("Contraseña", Contraseña.Trim()));            
            parametrosBD.Add(new ParametroBD("Comentarios", Comentarios));
            parametrosBD.Add(new ParametroBD("Rancho_ID", Rancho_ID));
            parametrosBD.Add(new ParametroBD("Precio_Litro_Fumigacion", Precio_Litro_Fumigacion));            
            if (Captura.Equals(MODO_DE_CAPTURA.CAPTURA_ALTA))
                parametrosBD.Add(new ParametroBD("Usuario_Creo", Usuario_Registro.Trim()));
            else
            {
                parametrosBD.Add(new ParametroBD("Parametro_ID", Parametro_ID));
                parametrosBD.Add(new ParametroBD("Usuario_Modifico", Usuario_Registro));
                parametrosBD.Add(new ParametroBD("Fecha_Modifico", Fecha_Registro));
            }
            //Retornamos la lista.
            return parametrosBD;
        }
    }
}