﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var $EmpleadoID = '';
var $Unidad = '';
var $Labor = '';
var oTable;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Automoviles');
    eventos();
    cargar_tabla();
    cargar_empelados();
    cargar_unidad();
    cargar_labores();
    Obtener_Hectareas();
    $('#btn-new').click();
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $("#txt-fecha").datepicker({
        format: 'dd/mm/yyyy'
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');
        $('#btn-new').click();
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
        asignar_fecha();
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();               
        } else {
            $('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });
        }
    });

    $("#chk-carga_gasolina").click(function () {
        if ($(this).is(':checked')) {
            $("#Div_Litros").css({ display: 'Block' });
            $("#Div_Precio_Lit").css({ display: 'Block' });
        }
        else {
            $("#Div_Litros").css({ display: 'none' });
            $("#Div_Precio_Lit").css({ display: 'none' });
        }
    });

    /*********Combos**********/
    $('#cmb-emp').on("select2:select", function (evt) {
        $EmpleadoID = evt.params.data.id;
        $('#txt-no_emp').val(parseInt($EmpleadoID));
        $('#txt-no_emp').prop('disabled', true);
    });
    $("#cmb-emp").on("select2:unselecting instead", function (e) {
        $EmpleadoID = '';
        $('#txt-no_emp').val('');
        $('#txt-no_emp').prop('disabled', false);
    });

    $('#cmb-unidad').on("select2:select", function (evt) {
        $Unidad = evt.params.data.id;
    });
    $("#cmb-unidad").on("select2:unselecting instead", function (e) {
        $Unidad = '';
    });

    $('#cmb-labor').on("select2:select", function (evt) {
        $Labor = evt.params.data.id;
    });
    $("#cmb-labor").on("select2:unselecting instead", function (e) {
        $Labor = '';
    });

    /*********Cajas Texto**********/
    $("#txt-no_emp").blur(function () {
        $EmpleadoID = $("#txt-no_emp").val();
        cargar_datos_empleado($EmpleadoID);
    });
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    var Obj_Capturado_Tab = new Object();
    var Array_Capturado_Tab = new Array();
    var tablas;

    try {
        Abrir_Ventana_Espera();

        Obj_Capturado.No_Automovil = $EventoID;
        Obj_Capturado.Fecha = $('#txt-fecha').val();
        Obj_Capturado.No_Empleado = $('#cmb-emp :selected').val();
        Obj_Capturado.No_Tarjeta = $('#txt-no_emp').val();
        Obj_Capturado.Unidad = $('#cmb-unidad :selected').val();
        Obj_Capturado.Labor = $('#cmb-labor :selected').text();
        Obj_Capturado.Nivel_Gasolina = $('#cmb-gasolina :selected').val();
        Obj_Capturado.Horas_Uso = $('#txt-hrs').val();
        Obj_Capturado.Carga_Gasolina = $('#chk-carga_gasolina').prop('checked') ? "SI" : "NO"
        if ($('#txt-litros').val() == '' || $('#txt-litros').val() == undefined || $('#txt-litros').val() == null)
            Obj_Capturado.Litros = '0';
        else
            Obj_Capturado.Litros = $('#txt-litros').val();
        if ($('#txt-precio').val() == '' || $('#txt-precio').val() == undefined || $('#txt-precio').val() == null)        
            Obj_Capturado.Precio_Litro = '0';
        else
            Obj_Capturado.Precio_Litro = $('#txt-precio').val();
        Obj_Capturado.Estatus = "ACTIVO";
        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';

        tablas = $('#hf-tabla_id').val().split(",");
        for (var i = 0; i < tablas.length; i++) {
            if (tablas[i] != '' && tablas[i] != null && tablas[i] != 'undefined') {
                Obj_Capturado_Tab = new Object();
                Obj_Capturado_Tab.Tabla_ID = tablas[i];
                Obj_Capturado_Tab.Captura = 'I';
                Array_Capturado_Tab[i] = Obj_Capturado_Tab;
            }
        }

        $.ajax({
            url: 'Automoviles/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: "{'Datos':'" + JSON.stringify(Obj_Capturado) + "', 'Tablas':'" + JSON.stringify(Array_Capturado_Tab) + "'}",
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                    $('#btn-new').click();
                    Cerrar_Ventana_Espera();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                    Cerrar_Ventana_Espera();
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
        Cerrar_Ventana_Espera();
    }
}

/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Automoviles/GetEvents",
        ordering: false,
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [
            { title: "Fecha", className: "text-center" },
            { title: "Unidad", className: "text-center" },
            { title: "Empleado", className: "text-center" },                        
            { title: "Nivel Gasolina", className: "text-center" },
            { title: "Horas Uso", className: "text-center" },
            { title: "Labor", className: "text-center" },
            { title: "BancoMateriales_ID", visible:false }            
        ],
        columnDefs: [
           {
               //render: function (data, type, row) {
               //    return '<button type="button" class="btn-primary" title="Modificar" onclick="Cargar_Informacion(' + "'" + row[6] + "'" + ')"><i class="fas fa-edit"></i></button>'
               //},
               //targets: 7
           }        
        ]
    });
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-fecha').val() == '' || $('#txt-fecha').val() == undefined || $('#txt-fecha').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Fecha</strong>.</span><br />';
        }
        if ($('#cmb-unidad :selected').val() == '' || $('#cmb-unidad :selected').val() == undefined || $('#cmb-unidad :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Empleado</strong>.</span><br />';
        }
        if ($('#txt-no_emp').val() == '' || $('#txt-no_emp').val() == undefined || $('#txt-no_emp').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> No Empleado</strong>.</span><br />';
        }
        if ($('#cmb-gasolina :selected').val() == '' || $('#cmb-gasolina :selected').val() == undefined || $('#cmb-gasolina :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Nivel Gasolina</strong>.</span><br />';
        }
        if ($('#txt-hrs').val() == '' || $('#txt-hrs').val() == undefined || $('#txt-hrs').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Horas Uso</strong>.</span><br />';
        }
        if ($('#chk-carga_gasolina').prop('checked'))
        {
            if ($('#txt-litros').val() == '' || $('#txt-litros').val() == undefined || $('#txt-litros').val() == null) {
                output.Estatus = false;
                output.Mensaje += '<span class="fas fa-angle-right"><strong> Litros</strong>.</span><br />';
            }
            if ($('#txt-precio').val() == '' || $('#txt-precio').val() == undefined || $('#txt-precio').val() == null) {
                output.Estatus = false;
                output.Mensaje += '<span class="fas fa-angle-right"><strong> Precio Litro</strong>.</span><br />';
            }
        }
        if ($('#cmb-labor :selected').val() == '' || $('#cmb-labor :selected').val() == undefined || $('#cmb-labor :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Labor</strong>.</span><br />';
        }
        if ($('#hf-tabla_id').val() == '' || $('#hf-tabla_id').val() == undefined || $('#hf-tabla_id').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Tabla</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });            
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });
            cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'Automoviles/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;                
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {
    $('#btn-Tabla_' + $('#hf-tabla_id').val()).removeClass('btn btn-danger btn_rojo');
    $('#btn-Tabla_' + $('#hf-tabla_id').val()).addClass('btn btn_verde');
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $EventoID = '';
    $EmpleadoID = '';
    $Unidad = '';
}

function cargar_datos_empleado(No_Empleado) {
    try {
        $.ajax({
            url: 'Empleados/GetEmployee',
            data: "{'No_Empleado':'" + No_Empleado + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EmpleadoID = parseInt(row[0].No_Empleado);
                $('#txt-no_emp').val(parseInt(row[0].No_Empleado));
                $('#txt-no_emp').prop('disabled', true);
                $('#cmb-emp').html('');
                $("#cmb-emp").select2({
                    theme: "classic",
                    data: [{ id: parseInt(row[0].No_Empleado), text: row[0].Nombre }]
                })
                cargar_empelados();
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

/// <summary>
/// FUNCION PARA CARGAR LOS REGISTROS
/// </summary>
function cargar_empelados() {
    jQuery('#cmb-emp').select2({
        ajax: {
            url: 'Empleados/GetEmployeeList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $EmpleadoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function cargar_unidad() {
    jQuery('#cmb-unidad').select2({
        ajax: {
            url: 'Unidad/GetUnidadList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $Unidad
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function cargar_labores() {
    jQuery('#cmb-labor').select2({
        ajax: {
            url: 'Labores/GetLaboresList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $Labor
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function Obtener_Hectareas() {
    var Eve_ID = '';    
    try {
        $.ajax({
            url: 'Tablas/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                Crear_Hectareas(row);
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

function Crear_Hectareas($data) {
    try {
        $('#Div_Hectareas').empty();
                
        for (var i = 0; i < $data.length; i++) {
            var $seccion = '<div class="col-lg-3">' +                
                '<button id="btn-Tabla_' + $data[i].Tabla_ID + '" type="button" class="btn btn_verde" style="width:100%" onclick=Hectarea_Click(\'' + $data[i].Tabla_ID + '\');>' + $data[i].Nombre + '</button>' +
                '</div>';

            $('#Div_Hectareas').append($seccion);
            $seccion = '';
        }        
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

function Hectarea_Click(Hectarea_ID) {
    $('#btn-Tabla_' + $('#hf-tabla_id').val()).removeClass('btn btn-danger btn_rojo');
    $('#btn-Tabla_' + $('#hf-tabla_id').val()).addClass('btn btn_verde');
    $('#btn-Tabla_' + Hectarea_ID).addClass('btn btn-danger btn_rojo');
    $('#hf-tabla_id').val(Hectarea_ID);
}

//function Hectarea_Click(Hectarea_ID) {
//    var Hectareas = $('#hf-tabla_id').val();
//    var Control_ = document.getElementById('btn-Tabla_' + Hectarea_ID);

//    if (Control_.className == "btn btn_rojo") {
//        $('#btn-Tabla_' + Hectarea_ID).removeClass('btn btn_rojo');
//        $('#btn-Tabla_' + Hectarea_ID).addClass('btn btn_verde');
//        Hectareas = Hectareas.replace(Hectarea_ID + ",", '');
//    }
//    else {
//        $('#btn-Tabla_' + Hectarea_ID).removeClass('btn btn_verde');
//        $('#btn-Tabla_' + Hectarea_ID).addClass('btn btn_rojo');
//        Hectareas = Hectareas + Hectarea_ID + ","
//    }
//    $('#hf-tabla_id').val(Hectareas);
//}

function formatRepoSelection(repo) {
    return repo.text;
}

function asignar_fecha() {
    var f = new Date();
    $("#txt-fecha").val(f.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());
}

function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}