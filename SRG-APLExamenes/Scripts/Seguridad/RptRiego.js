﻿/*====================================== VARIABLES =====================================*/
var oTable;
var oTableTabla;
var oTableDetalles;
var oTableSalidas;
var oTableSalidasDetalles;
var $Lista_Partidas = [];

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Riego');
    eventos();    
    asignar_fecha();
    Crear_Tabla();

    $('#Tbl_Registros').on('click', 'tr', function (event) {
        var data = oTable.row(this).data();
        Cargar_Informacion(data[0]);
    });
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-regresar').click(function (e) {
        e.preventDefault();
        habilitar_controles('consulta');
    });

    $('#btn-cns').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            Crear_Tabla();
        } else{ 
            mostrar_mensaje("", output.Mensaje);
        }           
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });
        
    $("#txt-fecha_inicio").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_fin").datepicker({
        format: 'dd/mm/yyyy'
    });
}


/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        order: [[1, 'desc']],
        "ajax": {
            "type": "POST",
            "url": "GetReporteRiego  ",
            "data": function (d) {                
                if ($('#txt-fecha_inicio').val() != '' && $('#txt-fecha_inicio').val() != undefined && $('#txt-fecha_inicio').val() != null)
                    d.Fecha_Inicio = $('#txt-fecha_inicio').val();
                if ($('#txt-fecha_fin').val() != '' && $('#txt-fecha_fin').val() != undefined && $('#txt-fecha_fin').val() != null)
                    d.Fecha_Fin = $('#txt-fecha_fin').val();
            }
        },
        columns: [            
            { title: "ID", className: "text-center", visible: false },                          
            { title: "Fecha", className: "text-center" },
            { title: "Horas", className: "text-center" },
            { title: "Acido", className: "text-center" },
            { title: "Total Empleados", className: "text-center" },
            { title: "Hrs/Empleados", className: "text-center" }
            //{ title: "Pago", className: "text-center" },
            //{ title: "No Empelado", className: "text-center" },
            //{ title: "Empleado", className: "text-center" },
            //{ title: "Tabla", className: "text-center" }            
        ],
        columnDefs: [
           {
               targets: 1,
               render: $.fn.dataTable.render.moment('DD/MM/YYYY', 'DD-MMM-YYYY')
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function exportar_excel() {
    window.location = "Download_Reporte_Riego?Fecha_Inicio=" + $('#txt-fecha_inicio').val() + "&Fecha_Fin=" + $('#txt-fecha_fin').val();
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {        
        if (($('#txt-fecha_inicio').val() == '' || $('#txt-fecha_inicio').val() == undefined || $('#txt-fecha_inicio').val() == null)
            && ($('#txt-fecha_fin').val() == '' || $('#txt-fecha_fin').val() == undefined || $('#txt-fecha_fin').val() == null)) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> SELECCIONE ALGUN FILTRO</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    var res;
    try {
        $.ajax({
            url: '../Riego/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                res = row[0].Fecha.split("T");
                $('#txt-fecha').val(res[0]);
                $('#txt-no_salida').val(row[0].No_Salida);
                $('#txt-horas').val(row[0].Horas);
                $('#txt-acido').val(row[0].Acido);
                $('#txt-total_empleados').val(row[0].Total_Empleados);

                $("#Tbl_Registros_Salidas_Detalles").empty();
                $Lista_Partidas = [];

                Cargar_Tablas(Eve_ID);
                Cargar_Detalles(Eve_ID);
                Cargar_Salidas(Eve_ID);

                habilitar_controles('detalles');
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

function Cargar_Tablas(No_Riego) {
    oTableTabla = $('#Tbl_Registros_Tablas').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        lengthMenu: [10, 25, 50, 75, 100],
        "ajax": {
            "type": "POST",
            "url": "../Riego/GetRiegoTablas  ",
            "data": function (d) {
                d.No_Riego = No_Riego;
            }
        },
        columns: [
            { title: "Tabla", className: "text-center" }            
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
               },
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function Cargar_Detalles(No_Riego) {
    oTableDetalles = $('#Tbl_Registros_Detalles').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        lengthMenu: [10, 25, 50, 75, 100],
        "ajax": {
            "type": "POST",
            "url": "../Riego/GetRiegoDetalles  ",
            "data": function (d) {
                d.No_Riego = No_Riego;
            }
        },
        columns: [
            { title: "No Tarjeta", className: "text-center" },
            { title: "Nombre", className: "text-center" },
            { title: "Pago", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
               },
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function Cargar_Salidas(No_Riego) {
    oTableSalidas = $('#Tbl_Registros_Salidas').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        lengthMenu: [10, 25, 50, 75, 100],
        "ajax": {
            "type": "POST",
            "url": "../Salidas/GetSalidas  ",
            "data": function (d) {
                d.Tipo = "RIEGO";
                d.No_Fumi_Riego = No_Riego;
            }
        },
        columns: [
            { title: "No Salida", className: "text-center" },
            { title: "Fecha", className: "text-center" },
            { title: "Tipo", className: "text-center" },
            { title: "Estatus", className: "text-center" },
            { title: "Costo", className: "text-right" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
               },
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable    
            cargar_salidas_detalles();
        }
    });
};

function cargar_salidas_detalles() {
    oTableSalidas.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        var no_salida = data[0];
        $.ajax({
            url: '../Salidas/GetDetalles',
            data: "{'No_Salida':'" + data[0] + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {

                for (x = 0; x < Resultado.aaData.length; x++) {
                    var Obj_Partidas = new Object();
                    Obj_Partidas.Cantidad = Resultado.aaData[x][0];
                    Obj_Partidas.Unidad = Resultado.aaData[x][1];
                    Obj_Partidas.Insumo = Resultado.aaData[x][2];
                    Obj_Partidas.Insumo_Id = Resultado.aaData[x][3];
                    Obj_Partidas.Precio = Resultado.aaData[x][4];
                    Obj_Partidas.Iva = Resultado.aaData[x][5];
                    Obj_Partidas.Ieps = Resultado.aaData[x][6];
                    Obj_Partidas.Total = Resultado.aaData[x][7];
                    $Lista_Partidas.push(Obj_Partidas);
                }
                recargarTablaPartidas();
            }
        });
    });
}

function recargarTablaPartidas() {
    var dataSet = [];
    $Lista_Partidas.forEach(function (element) {
        var dato = [];
        dato.push(element.Cantidad);
        dato.push(element.Unidad);
        dato.push(element.Insumo);
        dato.push(element.Insumo_Id);
        dato.push(element.Precio);
        dato.push(element.Iva);
        dato.push(element.Ieps);
        dato.push(element.Total);
        dataSet.push(dato);
    });
    cargar_tabla_partidas(dataSet);
}

function cargar_tabla_partidas(data) {

    oTableSalidasDetalles = $('#Tbl_Registros_Salidas_Detalles').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        data: data,
        lengthMenu: [5, 10, 50],
        columns: [
            { title: "Cantidad", className: "text-center" },
            { title: "Unidad", className: "text-center" },
            { title: "Insumo", className: "text-center" },
            { title: "Insumo_ID", className: "text-center", visible: false },
            { title: "Precio", className: "text-center" },
            { title: "Iva", className: "text-center" },
            { title: "Ieps", className: "text-center" },
            { title: "Total", className: "text-center" }
        ],
        columnDefs: [
        ]
    });
}

/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "consulta":
            $('#btn-cns').css({ display: 'Block' });
            $('#btn-xcl').css({ display: 'Block' });
            $('#btn-regresar').css({ display: 'none' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'Block' });
            $("#detalles").css({ display: 'none' });

            break;
        case "detalles":
            $('#btn-cns').css({ display: 'none' });
            $('#btn-xcl').css({ display: 'none' });
            $('#btn-regresar').css({ display: 'Block' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'none' });
            $("#detalles").css({ display: 'Block' });
            break;
        default:
            break;
    }
}

function asignar_fecha() {
    var f = new Date();
    var primerDia = new Date(f.getFullYear(), f.getMonth(), 1);
    var ultimoDia = new Date(f.getFullYear(), f.getMonth() + 1, 0);

    $("#txt-fecha_inicio").val(primerDia.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());
    $("#txt-fecha_fin").val(ultimoDia.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());
}