﻿/*====================================== VARIABLES =====================================*/
var oTable;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Destajo');
    eventos();    
    asignar_fecha();
    Crear_Tabla();

    $('#Tbl_Registros').on('click', 'tr', function (event) {
        var data = oTable.row(this).data();
        Cargar_Informacion(data[0]);
    });
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-regresar').click(function (e) {
        e.preventDefault();
        habilitar_controles('consulta');
    });

    $('#btn-cns').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            Crear_Tabla();
        } else{ 
            mostrar_mensaje("", output.Mensaje);
        }           
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });
    
    $("#txt-fecha_inicio").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_fin").datepicker({
        format: 'dd/mm/yyyy'
    });
}


/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        order: [[1, 'desc']],
        "ajax": {
            "type": "POST",
            "url": "GetReporteDestajo  ",
            "data": function (d) {
                d.No_Tarjeta = $('#txt-no_empleado').val();
                if ($('#txt-fecha_inicio').val() != '' && $('#txt-fecha_inicio').val() != undefined && $('#txt-fecha_inicio').val() != null)
                    d.Fecha_Inicio = $('#txt-fecha_inicio').val();
                if ($('#txt-fecha_fin').val() != '' && $('#txt-fecha_fin').val() != undefined && $('#txt-fecha_fin').val() != null)
                    d.Fecha_Fin = $('#txt-fecha_fin').val();
            }
        },
        columns: [
            { title: "ID", className: "text-center", visible: false },
            { title: "Fecha", className: "text-center" },            
            { title: "No Empleado", className: "text-center" },
            { title: "Empleado", className: "text-center" },            
            { title: "Tipo Destajo", className: "text-center" },            
            { title: "Cantidad", className: "text-center" },
            { title: "Pago", className: "text-center" },
            { title: "Total Pago", className: "text-center" },
        ],
        columnDefs: [
           {
               targets: 1,
               render: $.fn.dataTable.render.moment('DD/MM/YYYY', 'DD-MMM-YYYY')
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function exportar_excel() {    
    window.location = "Download_Reporte_Destajo?No_Tarjeta=" + $('#txt-no_empleado').val() + "&Fecha_Inicio=" + $('#txt-fecha_inicio').val() + "&Fecha_Fin=" + $('#txt-fecha_fin').val();
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {        
        if (($('#txt-fecha_inicio').val() == '' || $('#txt-fecha_inicio').val() == undefined || $('#txt-fecha_inicio').val() == null)
            && ($('#txt-fecha_fin').val() == '' || $('#txt-fecha_fin').val() == undefined || $('#txt-fecha_fin').val() == null)
            && ($('#txt-no_empleado').val() == '' || $('#txt-no_empleado').val() == undefined || $('#txt-no_empleado').val() == null)) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> SELECCIONE ALGUN FILTRO</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    var res;
    try {
        $.ajax({
            url: '../Destajo/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                res = row[0].Fecha.split("T");
                $('#txt-fecha').val(res[0]);
                $('#txt-no_tarjeta').val(row[0].No_Tarjeta);
                $('#txt-empleado').val(row[0].Empleado);
                $('#txt-tipo_destajo').val(row[0].Tipo_Destajo);
                $('#txt-cantidad').val(row[0].Cantidad);
                $('#txt-pago').val(row[0].Pago);
                $('#txt-total_pago').val(row[0].Total_Pago);

                habilitar_controles('detalles');
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}

/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "consulta":
            $('#btn-cns').css({ display: 'Block' });
            $('#btn-xcl').css({ display: 'Block' });
            $('#btn-regresar').css({ display: 'none' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'Block' });
            $("#detalles").css({ display: 'none' });

            break;
        case "detalles":
            $('#btn-cns').css({ display: 'none' });
            $('#btn-xcl').css({ display: 'none' });
            $('#btn-regresar').css({ display: 'Block' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'none' });
            $("#detalles").css({ display: 'Block' });
            break;
        default:
            break;
    }
}

function asignar_fecha() {
    var f = new Date();
    var primerDia = new Date(f.getFullYear(), f.getMonth(), 1);
    var ultimoDia = new Date(f.getFullYear(), f.getMonth() + 1, 0);

    $("#txt-fecha_inicio").val(primerDia.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());
    $("#txt-fecha_fin").val(ultimoDia.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());
}
