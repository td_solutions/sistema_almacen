﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var $EmpleadoID = '';
var $SalidaID = '';
var oTable;
var oTableDet;
var oTableSal;
var oTableDes;
var $Lista_Partidas = [];

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});

/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Notas Salida');
    eventos();    
    $('#btn-new').click();

    $('#Tbl_Registros_Destajo').on('click', 'tr', function (event) {
        var data = oTableDes.row(this).data();
        if ($(this)[0].cells[0].childNodes[0].children[0].checked == true) {
            $(this)[0].cells[0].childNodes[0].children[0].value = 'SI';
            data[0] = "SI";
            $('#txt-producto').val(data[2]);
            $('#txt-cajas').val(data[4]);
            oTableDes.rows(this).data(data);
        }
        else {
            $(this)[0].cells[0].childNodes[0].children[0].value = 'NO';
            data[0] = "NO";
            $('#txt-producto').val('');
            $('#txt-cajas').val('');
            oTableDes.rows(this).data(data);
        }
    });
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $("#txt-fecha").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_corte").datepicker({
        format: 'dd/mm/yyyy'
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');
        $('#btn-new').click();
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
        asignar_fecha();
        $('#txt-litros').focus();
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();               
        } else {
            $('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });
        }
    });

    $('#btn-add-emp').click(function (e) {
        e.preventDefault();
        var output = validar_datos_detalles();
        if (output.Estatus) {
            agregar_producto();
        } else {           
            $('#btn-add-emp').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });
        }        
    });
    
    $('#btn-add-corte').click(function (e) {
        e.preventDefault();
        if ($('#txt-fecha_corte').val() == '' || $('#txt-fecha_corte').val() == undefined || $('#txt-fecha_corte').val() == null) {
            alert('Seleccione la Fecha Corte');
        } else {
            Cargar_Destajo_Corte($('#txt-fecha_corte').val());
        }
    });

    $("#Tbl_Registros_Emp").on('click', 'button.del', function () {
        var $tr = $(this).closest('tr');

        // Le pedimos al DataTable que borre la fila
        oTableDet.row($tr).remove().draw(false);
    });

    /*********Cajas Texto**********/
    $("#txt-cajas").blur(function () {
        calcular_kilogramos();
    });
    $("#txt-peso").blur(function () {
        calcular_kilogramos();
    });
    $("#txt-precio").blur(function () {
        calcular_kilogramos();
    });
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    var Obj_Capturado_Det = new Object();
    var Array_Capturado_Det = new Array();
    var Obj_Capturado_Tab = new Object();
    var Array_Capturado_Tab = new Array();
    var tablas;
    var Contador = 0;

    try {
        Abrir_Ventana_Espera();

        Obj_Capturado.No_Nota = $EventoID;
        Obj_Capturado.Fecha = $('#txt-fecha').val();
        Obj_Capturado.Agricultor = $('#txt-agricultor').val();
        Obj_Capturado.Total_Cajas = $('#txt-tot_cajas').val();
        Obj_Capturado.Total_Peso = $('#txt-tot_peso').val();
        Obj_Capturado.Total_Kilogramos = $('#txt-tot_kg').val();
        Obj_Capturado.Total = $('#txt-total').val();
        Obj_Capturado.Estatus = "ACTIVO";
        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';

        oTableDet.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            Obj_Capturado_Det = new Object();            
            Obj_Capturado_Det.Clave = data.Clave;
            Obj_Capturado_Det.Producto = data.Producto;
            Obj_Capturado_Det.Cajas = data.Cajas;
            Obj_Capturado_Det.Peso = data.Peso;
            Obj_Capturado_Det.Kilogramos = data.Kilogramos;
            Obj_Capturado_Det.Precio = data.Precio;
            Obj_Capturado_Det.Total = data.Total;
            Obj_Capturado_Det.Captura = 'I';
            Array_Capturado_Det[Contador] = Obj_Capturado_Det;
            Contador++;
        });
        Contador = 0;

        oTableDes.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            if (data[0] == "SI") {
                Obj_Capturado_Tab = new Object();
                Obj_Capturado_Tab.Fecha = $('#txt-fecha_corte').val();
                Obj_Capturado_Tab.Tipo_Destajo_ID = data[1];
                Obj_Capturado_Tab.Captura = 'U';
                Array_Capturado_Tab[Contador] = Obj_Capturado_Tab;
                Contador++;
            }
        });
        Contador = 0;

        $.ajax({
            url: 'NotasSalida/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: "{'Datos':'" + JSON.stringify(Obj_Capturado) + "', 'Detalles':'" + JSON.stringify(Array_Capturado_Det) + "', 'Destajo':'" + JSON.stringify(Array_Capturado_Tab) + "'}",
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                    $('#btn-new').click();                    
                    Cerrar_Ventana_Espera();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                    Cerrar_Ventana_Espera();
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
        Cerrar_Ventana_Espera();
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "NotasSalida/GetEvents",
        ordering: false,
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [            
            { title: "Fecha", className: "text-center" },
            { title: "Agricultor", className: "text-right" },
            { title: "Total Cajas", className: "text-center" },
            { title: "Total", className: "text-center" },            
            { title: "No_Nota", visible: false }
        ],
        columnDefs: [
           {
               //render: function (data, type, row) {
               //    return '<button type="button" class="btn-primary" title="Modificar" onclick="Cargar_Informacion(' + "'" + row[6] + "'" + ')"><i class="fas fa-edit"></i></button>'
               //},
               //targets: 7
           }        
        ]
    });
}

function cargar_tabla_detalles() {
    oTableDet = $('#Tbl_Registros_Productos').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,        
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [
            { title: "Clave", className: "text-center", data: "Clave" },
            { title: "Producto", className: "text-left", data: "Producto" },
            { title: "Peso", className: "text-right", data: "Peso" },
            { title: "Cajas", className: "text-center", data: "Cajas" },            
            { title: "Kilogramos", className: "text-right", data: "Kilogramos" },            
            { title: "Precio", className: "text-right", data: "Precio" },
            { title: "Total", className: "text-right", data: "Total" },
            { title: "", data: "Boton" }
        ]        
    });
}

function agregar_producto() {
    var repetido = 'NO';
        
    oTableDet.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        if (data.Producto == $('#txt-producto').val())
            repetido = 'SI';
    });

    if (repetido == 'NO') {
        var persons = [
                        {
                            Clave: $('#txt-clave').val(),
                            Producto: $('#txt-producto').val(),
                            Peso: $('#txt-peso').val(),
                            Cajas: $('#txt-cajas').val(),
                            Kilogramos: $('#txt-kg').val(),
                            Precio: $('#txt-precio').val(),
                            Total: $('#txt-tot').val(),
                            Boton: '<button type="button" class="btn-primary del" title="Eliminar"> <i class="fas fa-trash-alt"></i></button>'
                        }
        ];
        oTableDet.rows.add(persons).draw();
    }
    else {
        alert('Ya exsite el Empelado.');
    }
    calcular_totales();    
    $('#txt-clave').val('');
    $('#txt-producto').val('');
    $('#txt-cajas').val('');
    $('#txt-peso').val('');
    $('#txt-kg').val('');
    $('#txt-precio').val('');
    $('#txt-tot').val('');
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-fecha').val() == '' || $('#txt-fecha').val() == undefined || $('#txt-fecha').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Fecha</strong>.</span><br />';
        }
        if ($('#txt-agricultor').val() == '' || $('#txt-agricultor').val() == undefined || $('#txt-agricultor').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Agricultor</strong>.</span><br />';
        }        
        if (oTableDet.data().count() <= 0) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Productos</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function validar_datos_detalles() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-clave').val() == '' || $('#txt-clave').val() == undefined || $('#txt-clave').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Clave</strong>.</span><br />';
        }
        if ($('#txt-producto').val() == '' || $('#txt-producto').val() == undefined || $('#txt-producto').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Producto</strong>.</span><br />';
        }
        if ($('#txt-cajas').val() == '' || $('#txt-cajas').val() == undefined || $('#txt-cajas').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Cajas</strong>.</span><br />';
        }
        if ($('#txt-peso').val() == '' || $('#txt-peso').val() == undefined || $('#txt-peso').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Peso</strong>.</span><br />';
        }        
        if ($('#txt-precio').val() == '' || $('#txt-precio').val() == undefined || $('#txt-precio').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Precio</strong>.</span><br />';
        }        
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg_Datos").css({ display: 'none' });
            cargar_tabla_detalles();
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg_Datos").css({ display: 'none' });
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg_Datos").css({ display: 'Block' });
            cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'Fumigacion/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;                          
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $("#Tbl_Registros_Productos").empty();
    $("#Tbl_Registros_Destajo").empty();
    $EventoID = '';    
}

function asignar_fecha() {
    var f = new Date();
    $("#txt-fecha").val(f.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());
}

function calcular_kilogramos() {
    var cajas = 0;
    var peso = 0;
    var kilogramos = 0;
    var precio = 0;
    var total = 0;

    if ($('#txt-cajas').val() != '' && $('#txt-cajas').val() != undefined && $('#txt-cajas').val() != null)
        cajas = parseFloat($('#txt-cajas').val());
    if ($('#txt-peso').val() != '' && $('#txt-peso').val() != undefined && $('#txt-peso').val() != null)
        peso = parseFloat($('#txt-peso').val());
    if ($('#txt-precio').val() != '' && $('#txt-precio').val() != undefined && $('#txt-precio').val() != null)
        precio = parseFloat($('#txt-precio').val());

    kilogramos = cajas * peso;
    total = precio * kilogramos;
    $('#txt-kg').val(parseFloat(kilogramos).toFixed(2));
    $('#txt-tot').val(parseFloat(total).toFixed(2));
}

function calcular_totales() {
    var tot_cajas= 0;
    var tot_peso = 0;
    var tot_kg = 0;    
    var total = 0;

    oTableDet.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        tot_cajas += parseFloat(data.Cajas);
        tot_peso += parseFloat(data.Peso);
        tot_kg += parseFloat(data.Kilogramos);
        total += parseFloat(data.Total);
    });

    $('#txt-tot_cajas').val(tot_cajas.toFixed(2));
    $('#txt-tot_peso').val(tot_peso.toFixed(2));
    $('#txt-tot_kg').val(tot_kg.toFixed(2));
    $('#txt-total').val(total.toFixed(2));
}

function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}

function Cargar_Destajo_Corte(Fecha) {
    oTableDes = $('#Tbl_Registros_Destajo').DataTable({
        destroy: true,
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        lengthMenu: [10, 25, 50, 75, 100],
        "ajax": {
            "type": "POST",
            "url": "Destajo/GetAgrupadoTDCorte  ",
            "data": function (d) {
                d.Tabla_ID = "NOTA NULL";
                d.Fecha_Inicio = Fecha;
                d.Fecha_Fin = Fecha;
            }
        },
        columns: [
            { title: "Ligar", className: "text-center" },
            { title: "Tipo Destajo ID", className: "text-center", visible: false },
            { title: "Tipo Destajo", className: "text-center" },
            { title: "Cantidad Empleados", className: "text-center" },
            { title: "Cantidad", className: "text-center" },
            { title: "Total_Pago", className: "text-center" }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
                   return "<div class='material-switch'><input type='checkbox' id='chk_" + row[1] + "' name='Menu' value='" + row[1] + "'/><label for='chk_" + row[1] + "' class='badge-success'></label></div>"
               },
               targets: 0
           }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};