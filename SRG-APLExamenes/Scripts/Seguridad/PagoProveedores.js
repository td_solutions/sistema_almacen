﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var $ProveedorID = '';
var oTable;
var oTableFacturas;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Pago Proveedores');
    eventos();
    //cargar_tabla();
    cargar_proveedores();

    $('#Tbl_Facturas').on('click', 'tr', function (event) {
        var data = oTableFacturas.row(this).data();
        if ($(this)[0].cells[0].childNodes[0].children[0].checked == true) {
            $(this)[0].cells[0].childNodes[0].children[0].value = 'SI';
            data[0] = "SI";
            oTableFacturas.rows(this).data(data);
        }
        else {
            $(this)[0].cells[0].childNodes[0].children[0].value = 'NO';
            data[0] = "NO";
            oTableFacturas.rows(this).data(data);
        }
        calcular_totales();
    });

    $('#btn-new').click();
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $("#txt-fecha").datepicker({
        format: 'dd/mm/yyyy'
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');
        $('#btn-new').click();
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
        asignar_fecha();
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();               
        } else {
            $('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });
        }
    });

    /*********Combos**********/
    $('#cmb-proveedor').on("select2:select", function (evt) {
        evt.preventDefault();
        $ProveedorID = evt.params.data.id;
        cargar_tabla_facturas();
    });
    $("#cmb-proveedor").on("select2:unselecting instead", function (e) {
        $ProveedorID = '';
        $("#Tbl_Facturas").empty();
    });
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    var Obj_Capturado_Det = new Object();
    var Array_Capturado_Det = new Array();
    var Contador = 0;

    try {
        Abrir_Ventana_Espera();

        oTableFacturas.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            if (data[0] == "SI") {
                Obj_Capturado_Det = new Object();
                Obj_Capturado_Det.FACTURA_ID = data[8];
                Obj_Capturado_Det.Captura = 'U';
                Array_Capturado_Det[Contador] = Obj_Capturado_Det;
                Contador++;
            }
        });
        Contador = 0;

        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';
        $.ajax({
            url: 'EventMasterMovimientosPagos',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: "{'Fecha':'" + $('#txt-fecha').val() + "', 'Forma_Pago':'" + $('#cmb-forma_pago :selected').val() + "', 'Referencia':'" + $('#txt-referencia').val() + "', 'Concepto':'" + $('#txt-concepto').val() + "', 'Monto':'" + $('#txt-Monto').val() + "', 'Datos':'" + JSON.stringify(Array_Capturado_Det) + "'}",
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                    $('#btn-new').click();
                    Cerrar_Ventana_Espera();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                    Cerrar_Ventana_Espera();
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
        Cerrar_Ventana_Espera();
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Facturas/GetEvents",
        ordering: false,
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [
            { title: "No Factura", className: "text-center" },
            { title: "Proveedor", className: "text-center" },
            { title: "Fecha Recepcion", className: "text-center" },            
            { title: "Fecha Vencimiento", className: "text-center" },
            { title: "Total", className: "text-center" },
            { title: "Estatus", className: "text-right" },
            { title: "Factura ID", visible:false }            
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
                   return '<button type="button" class="btn-primary" title="Modificar" onclick="Cargar_Informacion(' + "'" + row[6] + "'" + ')"><i class="fas fa-edit"></i></button>'
               },
               targets: 7
           }        
        ]
    });
}

function cargar_tabla_facturas() {
    oTableFacturas = $('#Tbl_Facturas').DataTable({
        destroy: true,
        "ajax": {
            "type": "POST",
            "url": "GetEventsFacturasProveedor",
            "data": function (d) {
                d.PROVEEDOR_ID = $ProveedorID;
                d.ESTATUS = "'POR PAGAR','ACTIVA','ABONADA'";
            }
        },
        ordering: false,
        searching: false,
        paging: false,
        info: false,
        lengthMenu: [10, 25, 50, 75, 100],
        ordering: false,
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [
            { title: "Pagar", className: "text-center" },
            { title: "No Factura", className: "text-center" },
            { title: "No Entrada", className: "text-center" },
            { title: "Fecha Recepcion", className: "text-center" },
            { title: "Fecha Vencimiento", className: "text-center" },
            { title: "Total", className: "text-right" },
            { title: "Abono", className: "text-right" },
            { title: "Saldo", className: "text-right" },
            { title: "Factura ID", visible: false }
        ],
        columnDefs: [
           {
               render: function (data, type, row) {                   
                   return "<div class='material-switch'><input type='checkbox' id='chk_" + row[8] + "' name='Menu' value='" + row[0] + "'/><label for='chk_" + row[8] + "' class='badge-success'></label></div>"                   
               },
               targets: 0
           }
        ]
    });
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if (oTableFacturas.data().count() <= 0) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Facturas</strong>.</span><br />';
        }
        if ($('#txt-fecha').val() == '' || $('#txt-fecha').val() == undefined || $('#txt-fecha').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Fecha</strong>.</span><br />';
        }
        if ($('#cmb-forma_pago :selected').val() == '' || $('#cmb-forma_pago :selected').val() == undefined || $('#cmb-forma_pago :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Forma Pago</strong>.</span><br />';
        }    
        if ($('#txt-Monto').val() == '' || $('#txt-Monto').val() == undefined || $('#txt-Monto').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Monto</strong>.</span><br />';
        }
        if ($('#txt-concepto').val() == '' || $('#txt-concepto').val() == undefined || $('#txt-concepto').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Concepto</strong>.</span><br />';
        }        
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });            
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });
            //cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'Facturas/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;                
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {    
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $("#Tbl_Facturas").empty();
    $EventoID = '';
    $ProveedorID = '';    
}

/// <summary>
/// FUNCION PARA CARGAR LOS REGISTROS
/// </summary>
function cargar_proveedores() {
    jQuery('#cmb-proveedor').select2({
        ajax: {
            url: '../Proveedores/GetProveedoresList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $ProveedorID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function formatRepoSelection(repo) {
    return repo.text;
}

function asignar_fecha() {
    var f = new Date();
    $("#txt-fecha").val(f.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());    
}

function calcular_totales() {
    var tot_saldo = 0;
    var tot_pagar = 0;

    oTableFacturas.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();

        if (data[0] == "SI")
            tot_pagar += parseFloat(data[7]);
        
        tot_saldo += parseFloat(data[7]);
    });

    $('#txt-tot_saldo').val(tot_saldo.toFixed(2));
    $('#txt-tot_pagar').val(tot_pagar.toFixed(2));
}

function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}