﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var oTable;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Unidad');
    eventos();
    cargar_tabla();    
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {    
    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();               
        } else {
            $('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });
        }
    });    
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Abrir_Ventana_Espera();

        Obj_Capturado.Unidad_ID = $EventoID;
        Obj_Capturado.Nombre = $('#txt-nombre').val();
        Obj_Capturado.Modelo = $('#txt-modelo').val();
        Obj_Capturado.Anio = $('#txt-anio').val();
        Obj_Capturado.Marca = $('#txt-marca').val();
        Obj_Capturado.Comentarios = $('#txt-comentarios').val();
        Obj_Capturado.Estatus = $('#cmb-stt :selected').val();        
        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';
        $.ajax({
            url: 'Unidad/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_Capturado),
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                    Cerrar_Ventana_Espera();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                    Cerrar_Ventana_Espera();
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
        Cerrar_Ventana_Espera();
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Unidad/GetEvents",
        ordering: false,
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [
            { title: "Nombre", className: "text-left" },
            { title: "Modelo", className: "text-center" },
            { title: "Año", className: "text-center" },
            { title: "Marca", className: "text-center" },
            { title: "Estatus", className: "text-center" },            
            { title: "BancoMateriales_ID", visible:false }            
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
                   return '<button type="button" class="btn-primary" title="Modificar" onclick="Cargar_Informacion(' + "'" + row[5] + "'" + ')"><i class="fas fa-edit"></i></button>'
               },
               targets: 6
           }        
        ]
    });
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-nombre').val() == '' || $('#txt-nombre').val() == undefined || $('#txt-nombre').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> NOMBRE</strong>.</span><br />';
        }
        if ($('#txt-modelo').val() == '' || $('#txt-modelo').val() == undefined || $('#txt-modelo').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Modelo</strong>.</span><br />';
        }
        if ($('#txt-anio').val() == '' || $('#txt-anio').val() == undefined || $('#txt-anio').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Modelo</strong>.</span><br />';
        }
        if ($('#txt-marca').val() == '' || $('#txt-marca').val() == undefined || $('#txt-marca').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Modelo</strong>.</span><br />';
        }
        if ($('#cmb-stt :selected').val() == '' || $('#cmb-stt :selected').val() == undefined || $('#cmb-stt :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> ESTATUS</strong>.</span><br />';
        }        
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":            
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });            
            break;
        case "Edit":            
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        default:            
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });
            cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'Unidad/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;                
                $('#txt-nombre').val(row[0].Nombre);
                $('#txt-modelo').val(row[0].Modelo);
                $('#txt-anio').val(row[0].Anio);
                $('#txt-marca').val(row[0].Marca);
                $('#cmb-stt').val(row[0].Estatus);
                $('#txt-comentarios').val(row[0].Comentarios);
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $EventoID = '';
    $Estatus = '';
    $Usuario = '';
    $Login = '';
}

function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}