﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var oTable;

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Tipo Insumo');
    eventos();
    cargar_tabla();
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();
        } else {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Algunos datos no se completaron!',
                footer: '<a href>' + output.Mensaje + '</a>'
            })

            /*$('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });*/
        }
    });
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Abrir_Ventana_Espera();

        Obj_Capturado.Proveedor_ID = $EventoID;
        Obj_Capturado.Nombre = $('#txt-nombre').val();
        Obj_Capturado.Razon_Social = $('#txt-razon-social').val();
        Obj_Capturado.Rfc = $('#txt-rfc').val();
        Obj_Capturado.Dias_Credito = $('#txt-dias-credito').val();
        Obj_Capturado.Estatus = $('#cmb-stt :selected').val();

        Obj_Capturado.Direccion = $('#txt-direccion').val();
        Obj_Capturado.Colonia = $('#txt-colonia').val();
        Obj_Capturado.Ciudad = $('#txt-ciudad').val();
        Obj_Capturado.Estado = $('#txt-estado').val();
        Obj_Capturado.Cp = $('#txt-cp').val();

        Obj_Capturado.Telefono = $('#txt-tel').val();
        Obj_Capturado.Contacto = $('#txt-contacto').val();
        Obj_Capturado.Email = $('#txt-email').val();

        Obj_Capturado.Descuento = $('#txt-descuento').val();
        Obj_Capturado.Limite_Credito = $('#txt-limite_credito').val();
        Obj_Capturado.Dias_Credito = $('#txt-dias-credito').val();

        Obj_Capturado.Observaciones = $('#txt-observaciones').val();

        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';
        $.ajax({
            url: 'Proveedores/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_Capturado),
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                    Cerrar_Ventana_Espera();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                    Cerrar_Ventana_Espera();
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
        Cerrar_Ventana_Espera();
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Proveedores/Consulta_Catalogo",
        ordering: false,
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [
            { title: "Nombre", className: "text-left" },
            { title: "Razon_Social", className: "text-center" },
            { title: "Estatus", className: "text-center" },
            { title: "Proveedor_ID", visible: false }
        ],
        columnDefs: [
            {
                render: function (data, type, row) {
                    return '<button type="button" class="btn-primary" title="Modificar" onclick="Cargar_Informacion(' + "'" + row[3] + "'" + ')"><i class="fas fa-edit"></i></button>'
                },
                targets: 4
            }
        ]
    });
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-nombre').val() == '' || $('#txt-nombre').val() == undefined || $('#txt-nombre').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> NOMBRE</strong>.</span><br />';
        }
        if ($('#txt-razon-social').val() == '' || $('#txt-razon-social').val() == undefined || $('#txt-razon-social').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> RAZ&Oacute;N SOCIAL</strong>.</span><br />';
        }
        if ($('#txt-rfc').val() == '' || $('#txt-rfc').val() == undefined || $('#txt-rfc').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> RFC</strong>.</span><br />';
        }
        if ($('#txt-dias-credito').val() == '' || $('#txt-dias-credito').val() == undefined || $('#txt-dias-credito').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> DIAS CR&Eacute;DITO</strong>.</span><br />';
        }
        if ($('#cmb-stt :selected').val() == '' || $('#cmb-stt :selected').val() == undefined || $('#cmb-stt :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> ESTATUS</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });
            cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'Proveedores/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;
                $('#txt-nombre').val(row[0].Nombre);
                $('#txt-razon-social').val(row[0].Razon_Social);
                $('#txt-rfc').val(row[0].Rfc);
                $('#txt-dias-credito').val(row[0].Dias_Credito);
                $('#cmb-stt').val(row[0].Estatus);

                $('#txt-direccion').val(row[0].Direccion);
                $('#txt-colonia').val(row[0].Colonia);
                $('#txt-ciudad').val(row[0].Ciudad);
                $('#txt-estado').val(row[0].Estado);
                $('#txt-cp').val(row[0].Cp);

                $('#txt-tel').val(row[0].Telefono);
                $('#txt-contacto').val(row[0].Contacto);
                $('#txt-email').val(row[0].Email);

                $('#txt-descuento').val(row[0].Descuento);
                $('#txt-limite_credito').val(row[0].Limite_Credito);
                $('#txt-dias-credito').val(row[0].Dias_Credito);

                $('#txt-observaciones').val(row[0].Observaciones);
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('textarea').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $EventoID = '';
    $Estatus = '';
    $Usuario = '';
    $Login = '';
}

function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}