﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var oTable;
var $TipoInsumoID = '';
var $UnidadID = '';
var $UnidadConsumoID = '';
var $ProveedorID = '';
var $AlmacenID = '';
var tasa_iva = 0;
var tasa_ieps = 0;
$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Insumos');
    eventos();
    cargar_tabla();
    cargar_tipo_insumo();
    cargar_unidades();
    cargar_unidades_consumo();
    cargar_proveedor();
    cargar_impuestos();
    //cargar_almacen();
});

function cargar_impuestos()
{
    $.ajax({
        url: 'Parametros/Consultar',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (Resultado) {
            if (Resultado.items.Estatus) {

                row = JSON.parse(Resultado.items.Data);
                tasa_iva = row[0].Tasa_IVA;
                tasa_ieps = row[0].Tasa_IEPS;
            }
            else {
                tasa_iva = 0;
                tasa_ieps = 0;
                mostrar_mensaje("Advertencia", Resultado.Mensaje);
            }
        },
        error: function (msj) {
            tasa_ieps = 0;
            tasa_iva = 0;
        }
    });
    
}

function cargar_almacen() {
    jQuery('#cmb-almacen').select2({
        ajax: {
            url: 'Almacen/GetAlmacenList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $AlmacenID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
function cargar_proveedor() {
    jQuery('#cmb-proveedor').select2({
        ajax: {
            url: 'Proveedores/GetProveedoresList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $ProveedorID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
function cargar_unidades() {
    jQuery('#cmb-unidad').select2({
        ajax: {
            url: 'Unidades/GetUnidadesList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $UnidadID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
function cargar_unidades_consumo() {
    jQuery('#cmb-unidad-consumo').select2({
        ajax: {
            url: 'Unidades/GetUnidadesList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $UnidadConsumoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
function cargar_tipo_insumo() {
    jQuery('#cmb-ti').select2({
        ajax: {
            url: 'TipoInsumo/GetTipoIsnumoList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $TipoInsumoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles_insumos();
        habilitar_controles('');
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles_insumos();
        habilitar_controles('new');
    });

    $('#txt-equivalencia').change(function (e) {
        e.preventDefault();
        var bandera_para_saber_si_mostrar_equivalencia = true;
        if ($('#cmb-unidad :selected').val() == '' || $('#cmb-unidad :selected').val() == undefined || $('#cmb-unidad :selected').val() == null) {
            bandera_para_saber_si_mostrar_equivalencia = false;
        }
        if ($('#cmb-unidad-consumo :selected').val() == '' || $('#cmb-unidad-consumo :selected').val() == undefined || $('#cmb-unidad-consumo :selected').val() == null) {
            bandera_para_saber_si_mostrar_equivalencia = false;
        }
        if ($(this).val() == '' || $(this).val() == undefined || $(this).val() == null) {
            bandera_para_saber_si_mostrar_equivalencia = false;
        }
        if (bandera_para_saber_si_mostrar_equivalencia) {
            $('#txt_equivalencia_texto').html('1 ' + $('#cmb-unidad :selected').text() + ' equivale a ' + $(this).val() + ' ' + $('#cmb-unidad-consumo :selected').text());
        }

        
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();
        } else {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Algunos datos no se completaron!',
                footer: '<a href>' + output.Mensaje+'</a>'
            })

            /*$('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });*/
        }
    });
}
/*********Combos**********/
$('#cmb-tipo').on("select2:select", function (evt) {
    $TipoInsumoID = evt.params.data.id;
});
$("#cmb-tipo").on("select2:unselecting instead", function (e) {
    $TipoInsumoID = '';
});
$('#txt-porcentajeIVA').change(function (e) {
    calcular_impuestos();
});
$('#txt-porcentajeIEPS').change(function (e) {
    calcular_impuestos();
});
$('#txt-costo').change(function (e) {
    calcular_impuestos();
});

function calcular_impuestos() {
    var porcentaje_iva =  0;
    var porcentaje_ieps = 0;
    var iva = 0;
    var ieps = 0;
    var total = 0;
    var costo = 0;

    costo = $("#txt-costo").val();
    porcentaje_ieps = $('#txt-porcentajeIEPS').val();
    porcentaje_iva = $('#txt-porcentajeIVA').val();
    
    $("#txt-iva").val(iva);
    $("#txt-ieps").val(ieps);

    if (porcentaje_iva > 0 ) {
        iva = costo * (porcentaje_iva / 100);
        $("#txt-iva").val( Math.round(iva) );
    }
    if (porcentaje_ieps > 0 ) {
        ieps = costo * (porcentaje_ieps / 100);
        $("#txt-ieps").val( Math.round(ieps) );
    }
    
    total = parseFloat(costo) + parseFloat(iva) + parseFloat(ieps );
    $("#txt-total").val(total);
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Obj_Capturado.Insumo_ID = $EventoID;
        Obj_Capturado.Tipo_Insumo_ID = $('#cmb-stt :selected').val();
        Obj_Capturado.Nombre = $('#txt-nombre').val();
        Obj_Capturado.Descripcion = $('#txt-descripcion').val();
        Obj_Capturado.Estatus = $('#cmb-stt :selected').val();
        Obj_Capturado.Tipo_Insumo_ID = $('#cmb-ti :selected').val();
        Obj_Capturado.Maximo = $('#txt-max').val();
        Obj_Capturado.Minimo = $('#txt-min').val();
        Obj_Capturado.Punto_Reorden = $('#txt-reo').val();
        Obj_Capturado.Costo = $('#txt-costo').val();
        Obj_Capturado.Iva = $('#txt-iva').val();
        Obj_Capturado.Ieps = $('#txt-ieps').val();
        Obj_Capturado.Total = $('#txt-total').val();
        Obj_Capturado.Existencias = $('#txt-existencias').val();
        //Obj_Capturado.Almacen_ID = $('#cmb-almacen :selected').val();
        Obj_Capturado.Unidad_ID = $('#cmb-unidad :selected').val();
        Obj_Capturado.Unidad_Consumo_ID = $('#cmb-unidad-consumo :selected').val();
        Obj_Capturado.Equivalencia = $('#txt-equivalencia').val();
        Obj_Capturado.Proveedor_ID = $('#cmb-proveedor :selected').val();
        Obj_Capturado.Ubicacion = $('#txt-ubicacion').val();


        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';
        $.ajax({
            url: 'Insumos/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_Capturado),
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles_insumos();
                    habilitar_controles();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Insumos/Consulta_Catalogo",
        ordering: false,
        lengthMenu: [10, 25, 10, 30],
        columns: [
            { title: "Nombre", className: "text-left" },
            { title: "Estatus", className: "text-center" },
            { title: "Tipo Insumo", className: "text-center" },
            { title: "Costo", className: "text-right" },
            { title: "Proveedor", className: "text-center" },
            { title: "Insumo_ID", visible: false }
        ],
        columnDefs: [
            {
                render: function (data, type, row) {
                    return '<button type="button" class="btn-primary" title="Modificar" onclick="Cargar_Informacion(' + "'" + row[5] + "'" + ')"><i class="fas fa-edit"></i></button>'
                },
                targets: 6
            }
        ]
    });
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-nombre').val() == '' || $('#txt-nombre').val() == undefined || $('#txt-nombre').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> NOMBRE</strong>.</span><br />';
        }
        if($('#txt-existencias').val() == '' || $('#txt-existencias').val() == undefined || $('#txt-existencias').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> EXISTENCIAS</strong>.</span><br />';
        }
        if ($('#txt-descripcion').val() == '' || $('#txt-descripcion').val() == undefined || $('#txt-descripcion').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> DESCRIPCION</strong>.</span><br />';
        }
        if ($('#txt-equivalencia').val() == '' || $('#txt-equivalencia').val() == undefined || $('#txt-equivalencia').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> EQUIVALENCIA</strong>.</span><br />';
        }
        if ($('#cmb-stt :selected').val() == '' || $('#cmb-stt :selected').val() == undefined || $('#cmb-stt :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> ESTATUS</strong>.</span><br />';
        }
        /*if ($('#cmb-almacen :selected').val() == '' || $('#cmb-almacen :selected').val() == undefined || $('#cmb-almacen :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> ALMACEN</strong>.</span><br />';
        }*/
        if ($('#cmb-ti :selected').val() == '' || $('#cmb-ti :selected').val() == undefined || $('#cmb-ti :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> TIPO DE INSUMO</strong>.</span><br />';
        }
        if ($('#cmb-unidad :selected').val() == '' || $('#cmb-unidad :selected').val() == undefined || $('#cmb-unidad :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> UNIDAD</strong>.</span><br />';
        }
        if ($('#cmb-unidad-consumo :selected').val() == '' || $('#cmb-unidad-consumo :selected').val() == undefined || $('#cmb-unidad-consumo :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> UNIDAD DE CONSUMO</strong>.</span><br />';
        }
        if ($('#cmb-proveedor :selected').val() == '' || $('#cmb-proveedor :selected').val() == undefined || $('#cmb-proveedor :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> PROVEEDOR</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });
            cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'Insumos/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;
                $('#txt-nombre').val(row[0].Nombre);
                $('#txt-descripcion').val(row[0].Descripcion);
                $('#cmb-stt').val(row[0].Estatus);

                if (row[0].Tipo_Insumo_ID != undefined) {
                    var $newOption = $("<option selected='selected'></option>").val(row[0].Tipo_Insumo_ID).text(row[0].Tipo_Insumo);
                    $("#cmb-ti").append($newOption).trigger('change');
                    /*$("#cmb-ti").select2({
                        theme: "classic",
                        data: [{ id: parseInt(row[0].Tipo_Insumo_ID), text: row[0].Tipo_Insumo }]
                    });*/
                    //cargar_tipo_insumo();
                }
                
                if (row[0].Unidad_ID != undefined) {

                    var $newOption = $("<option selected='selected'></option>").val(row[0].Unidad_ID).text(row[0].Unidad);
                    $("#cmb-unidad").append($newOption).trigger('change');

                    //$("#cmb-unidad").text(row[0].Unidad);
                    
                    //$("#cmb-unidad").select2("val", row[0].Unidad_ID);
                    /*$("#cmb-unidad").select2({
                        theme: "classic",
                        data: [{ id: parseInt(row[0].Unidad_ID), text: row[0].Unidad }]
                    });*/
                    //cargar_unidades();
                }
                if (row[0].Proveedor_ID != undefined) {
                    var $newOption = $("<option selected='selected'></option>").val(row[0].Proveedor_ID).text(row[0].Proveedor);
                    $("#cmb-proveedor").append($newOption).trigger('change');
                    /*$("#cmb-proveedor").select2({
                        theme: "classic",
                        data: [{ id: parseInt(row[0].Proveedor_ID), text: row[0].Proveedor }]
                    });*/
                    //cargar_proveedor();
                }
                /*if (row[0].Almacen_ID != undefined) {
                    $("#cmb-almacen").select2({
                        theme: "classic",
                        data: [{ id: parseInt(row[0].Almacen_ID), text: row[0].Almacen }]
                    });
                    cargar_almacen();
                }*/
                if (row[0].Unidad_Consumo_ID != undefined) {
                    var $newOption = $("<option selected='selected'></option>").val(row[0].Unidad_Consumo_ID).text(row[0].Unidad_Consumo);
                    $("#cmb-unidad-consumo").append($newOption).trigger('change');
                }
                $('#txt-equivalencia').val(row[0].Equivalencia);
                $('#txt_equivalencia_texto').html('1 ' + row[0].Unidad + ' equivale a ' + row[0].Equivalencia + ' ' + row[0].Unidad_Consumo);
                $('#txt-max').val(row[0].Maximo);
                $('#txt-min').val(row[0].Minimo);
                $('#txt-reo').val(row[0].Punto_Reorden);
                $('#txt-costo').val(row[0].Costo);
                $('#txt-total').val(row[0].Total);
                $('#txt-iva').val(row[0].IVA);
                $('#txt-ieps').val(row[0].IEPS);
                $('#txt-existencias').val(row[0].Existencias);
                $('#txt-ubicacion').val(row[0].Ubicacion);

                $('#txt-porcentajeIVA').val(Math.round( (100 * row[0].IVA) / row[0].Costo ) );
                $('#txt-porcentajeIEPS').val(Math.round( (100 * row[0].IEPS) / row[0].Costo ) );
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles_insumos() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[tipo=numeros]').each(function () { $(this).val('0'); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $('#cmb-iva').val('NO');
    $('#cmb-ieps').val('NO');
    $TipoInsumoID = '';
    $EventoID = '';
    $Estatus = '';
    $Usuario = '';
    $Login = '';
}