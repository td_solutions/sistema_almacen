﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var $ProveedorID = '';
var oTable;
var oTable_Det;
var $Lista_Partidas = [];

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Facturas Proveedor');
    eventos();
    cargar_tabla();
    cargar_proveedores();    
});

/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $("#txt-fecha_recepcion").datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#txt-fecha_vencimiento").datepicker({
        format: 'dd/mm/yyyy'
    });

    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');        
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
        asignar_fecha();
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();               
        } else {
            $('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });
        }
    });

    /*********Combos**********/
    $('#cmb-proveedor').on("select2:select", function (evt) {
        $ProveedorID = evt.params.data.id;        
    });
    $("#cmb-proveedor").on("select2:unselecting instead", function (e) {
        $ProveedorID = '';
    });

    /*********Cajas Texto**********/
    $("#txt-importe").blur(function () {
        calcular_totales();
    });
    $("#txt-descuento").blur(function () {
        calcular_totales();
    });
    $("#txt-iva").blur(function () {
        calcular_totales();
    });
    $("#txt-ieps").blur(function () {
        calcular_totales();
    });
}
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Abrir_Ventana_Espera();

        Obj_Capturado.FACTURA_ID = $EventoID;
        Obj_Capturado.NO_FACTURA = $('#txt-no_factura').val();
        Obj_Capturado.PROVEEDOR_ID = $('#cmb-proveedor :selected').val();
        Obj_Capturado.CONCEPTO = $('#txt-concepto').val();
        Obj_Capturado.TIPO = "NORMAL";
        Obj_Capturado.FECHA_RECEPCION = $('#txt-fecha_recepcion').val();
        Obj_Capturado.FECHA_VENCIMIENTO = $('#txt-fecha_vencimiento').val();
        Obj_Capturado.MONEDA = "PESOS";
        Obj_Capturado.IMPORTE = $('#txt-importe').val();
        Obj_Capturado.IEPS = $('#txt-ieps').val();
        Obj_Capturado.IVA = $('#txt-iva').val();
        Obj_Capturado.DESCUENTO = $('#txt-descuento').val();
        Obj_Capturado.TOTAL = $('#txt-total').val();
        Obj_Capturado.ABONO = "0.00";
        Obj_Capturado.SALDO = $('#txt-total').val();
        Obj_Capturado.PAGADA ="NO";
        Obj_Capturado.CANCELADA = "NO";
        Obj_Capturado.COMENTARIOS = $('#txt-comentarios').val();
        Obj_Capturado.TIPO_CAMBIO = "1.00";
        Obj_Capturado.ESTATUS = 'POR PAGAR';

        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';
        $.ajax({
            url: 'Facturas/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_Capturado),
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                    Cerrar_Ventana_Espera();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                    Cerrar_Ventana_Espera();
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
        Cerrar_Ventana_Espera();
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Facturas/GetEvents",
        ordering: false,
        lengthMenu: [10, 25, 50, 75, 100],
        columns: [
            { title: "No Factura", className: "text-center" },
            { title: "No Entrada", className: "text-center" },
            { title: "Proveedor", className: "text-center" },
            { title: "Fecha Recepcion", className: "text-center" },            
            { title: "Fecha Vencimiento", className: "text-center" },
            { title: "Total", className: "text-center" },
            { title: "Estatus", className: "text-right" },
            { title: "Factura ID", visible: false }                       
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
                   return '<button type="button" class="btn-primary" title="Consultar" onclick="Cargar_Informacion(' + "'" + row[7] + "'" + ')"><i class="fas fa-search"></i></button><button type="button" class="btn-primary" title="Cancelar" onclick="Cancelar_Factura(' + "'" + row[7] + "'" + ', ' + "'" + row[6] + "'" + ')"><i class="fas fa-window-close"></i></button>'
               },
               targets: 8
           }        
        ],        
    });
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-no_factura').val() == '' || $('#txt-no_factura').val() == undefined || $('#txt-no_factura').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> No Factura</strong>.</span><br />';
        }
        if ($('#txt-fecha_recepcion').val() == '' || $('#txt-fecha_recepcion').val() == undefined || $('#txt-fecha_recepcion').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Fecha Recepcion</strong>.</span><br />';
        }
        if ($('#txt-fecha_vencimiento').val() == '' || $('#txt-fecha_vencimiento').val() == undefined || $('#txt-fecha_vencimiento').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Fecha Vencimiento</strong>.</span><br />';
        }
        if ($('#cmb-proveedor :selected').val() == '' || $('#cmb-proveedor :selected').val() == undefined || $('#cmb-proveedor :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Proveedor</strong>.</span><br />';
        }        
        if ($('#txt-concepto').val() == '' || $('#txt-concepto').val() == undefined || $('#txt-concepto').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Concepto</strong>.</span><br />';
        }
        if ($('#txt-total').val() == '' || $('#txt-total').val() == undefined || $('#txt-total').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> Total</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            $("#Reg-Datos_Det").css({ display: 'none' });
            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });
            cargar_tabla();
            break;
    }
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        $.ajax({
            url: 'Facturas/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                //if (row[0].ESTATUS == "POR PAGAR" || row[0].ESTATUS == 'ACTIVA') {
                $("#Reg-Datos_Det").css({ display: 'Block' });
                    $EventoID = Eve_ID;
                    $ProveedorID = row[0].PROVEEDOR_ID;
                    $('#txt-no_factura').val(row[0].NO_FACTURA);
                    $('#txt-concepto').val(row[0].CONCEPTO);
                    $('#txt-fecha_recepcion').val(row[0].FECHA_RECEPCION);
                    $('#txt-fecha_vencimiento').val(row[0].FECHA_VENCIMIENTO);
                    $('#txt-importe').val(row[0].IMPORTE);
                    $('#txt-iva').val(row[0].IVA);
                    $('#txt-descuento').val(row[0].DESCUENTO);
                    $('#txt-comentarios').val(row[0].COMENTARIOS);
                    $('#cmb-stt :selected').val(row[0].ESTATUS);
                    if (row[0].PROVEEDOR_ID != "") {
                        $("#cmb-proveedor").select2({
                            theme: "classic",
                            data: [{ id: parseInt(row[0].PROVEEDOR_ID), text: row[0].Proveedor }]
                        })
                        cargar_proveedores();
                    }
                    calcular_totales();
                    $Lista_Partidas = [];

                    $.ajax({
                        url: 'Entradas/GetDetalles',
                        data: "{'No_Entrada':'" + row[0].No_Entrada + "'}",
                        method: 'POST',
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        cache: false,
                        success: function (Resultado) {

                            for (x = 0; x < Resultado.aaData.length; x++) {
                                var Obj_Partidas = new Object();
                                Obj_Partidas.Cantidad = Resultado.aaData[x][0];
                                Obj_Partidas.Unidad = Resultado.aaData[x][1];
                                Obj_Partidas.Insumo = Resultado.aaData[x][2];
                                Obj_Partidas.Precio = Resultado.aaData[x][3];
                                Obj_Partidas.Importe = Resultado.aaData[x][4];
                                Obj_Partidas.Total = Resultado.aaData[x][5];
                                Obj_Partidas.Iva = Resultado.aaData[x][5];
                                Obj_Partidas.Ieps = Resultado.aaData[x][6];                                
                                $Lista_Partidas.push(Obj_Partidas);
                            }
                            recargarTablaPartidas();
                        }
                    });
                    //FIN AJAX DETALLES
                //} else {
                //    mostrar_mensaje('Informe Técnico', 'La factura no puede ser modificada ya fue: ' + row[0].ESTATUS);
                //    limpiar_controles();
                //    habilitar_controles('');
                //}
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
}

function Cancelar_Factura(Eve_ID, Estatus) {
    var Obj_Capturado = new Object();

    try {
        if (Estatus == 'POR PAGAR' || Estatus == 'ACTIVA') {
            var mensaje = confirm("¿Desea Cancelar la Factura?");            
            if (mensaje) {
                Obj_Capturado.FACTURA_ID = Eve_ID;
                Obj_Capturado.PAGADA = "NO";
                Obj_Capturado.CANCELADA = "SI";
                Obj_Capturado.ESTATUS = 'CANCELADA';
                Obj_Capturado.Captura = 'U';

                $.ajax({
                    url: 'Facturas/EventMaster',
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(Obj_Capturado),
                    cache: false,
                    success: function (Resultado) {
                        if (Resultado.Estatus) {
                            mostrar_mensaje("", Resultado.Mensaje);
                            limpiar_controles();
                            habilitar_controles('');
                        }
                        else {
                            mostrar_mensaje("Advertencia", Resultado.Mensaje);
                        }
                    }
                });
            }
        }
        else {
            mostrar_mensaje('Informe Técnico', 'La factura no puede ser cancelada ya fue: ' + Estatus);
            limpiar_controles();
            habilitar_controles('');
        }
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {    
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $EventoID = '';
    $ProveedorID = '';    
}

/// <summary>
/// FUNCION PARA CARGAR LOS REGISTROS
/// </summary>
function cargar_proveedores() {
    jQuery('#cmb-proveedor').select2({
        ajax: {
            url: 'Proveedores/GetProveedoresList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $ProveedorID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
}

function formatRepoSelection(repo) {
    return repo.text;
}

function asignar_fecha() {
    var f = new Date();
    $("#txt-fecha_recepcion").val(f.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());
    $("#txt-fecha_vencimiento").val(f.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear());    
}

function calcular_totales() {
    var importe = 0;
    var descuento = 0;
    var subtotal = 0;
    var iva = 0;
    var ieps = 0;
    var total = 0;

    if ($('#txt-importe').val() != '' && $('#txt-importe').val() != undefined && $('#txt-importe').val() != null)
        importe = parseFloat($('#txt-importe').val());
    if ($('#txt-descuento').val() != '' && $('#txt-descuento').val() != undefined && $('#txt-descuento').val() != null)
        descuento = parseFloat($('#txt-descuento').val());
    if ($('#txt-iva').val() != '' && $('#txt-iva').val() != undefined && $('#txt-iva').val() != null)
        iva = parseFloat($('#txt-iva').val());
    if ($('#txt-ieps').val() != '' && $('#txt-ieps').val() != undefined && $('#txt-ieps').val() != null)
        ieps = parseFloat($('#txt-ieps').val());

    subtotal = importe - descuento;
    total = subtotal + iva + ieps;

    $('#txt-importe').val(parseFloat(importe).toFixed(2));
    $('#txt-descuento').val(parseFloat(descuento).toFixed(2));
    $('#txt-subtotal').val(parseFloat(subtotal).toFixed(2));
    $('#txt-iva').val(parseFloat(iva).toFixed(2));
    $('#txt-ieps').val(parseFloat(ieps).toFixed(2));
    $('#txt-total').val(parseFloat(total).toFixed(2));
}

function cargar_entradas_detalles(no_entrada) {
    oTable_Det = $('#Tbl_Registros_Entradas').DataTable({
        destroy: true,
        "ajax": {
            "type": "POST",
            "url": "Entradas/GetDetalles  ",
            "data": function (d) {
                d.No_Entrada = no_entrada;
            }
        },
        columns: [
            { title: "Cantidad", className: "text-center" },
            { title: "Unidad", className: "text-center" },
            { title: "Insumo", className: "text-center" },
            { title: "Precio", className: "text-center" },
            { title: "Importe", className: "text-center" },
            { title: "Total", className: "text-center" },
            { title: "Iva", className: "text-center" },
            { title: "Ieps", className: "text-center" },
        ],
        columnDefs: [
           {
               render: function (data, type, row) {
               },
           }
        ],
        rowCallback: function (row, data) {
        }
    });
};

function recargarTablaPartidas() {
    var dataSet = [];
    $Lista_Partidas.forEach(function (element) {
        var dato = [];
        dato.push(element.Cantidad);
        dato.push(element.Unidad);
        dato.push(element.Insumo);
        dato.push(element.Precio);
        dato.push(element.Importe);
        dato.push(element.Iva);
        dato.push(element.Ieps);
        dato.push(element.Total);        
        dataSet.push(dato);
    });
    cargar_tabla_partidas(dataSet);
}

function cargar_tabla_partidas(data) {

    oTable_Det = $('#Tbl_Registros_Entradas').DataTable({
        destroy: true,
        ordering: false,
        data: data,
        lengthMenu: [5, 10, 20, 10, 10, 10, 10, 10, 2],
        columns: [
            { title: "Cantidad", className: "text-left" },
            { title: "Unidad", className: "text-center" },
            { title: "Insumo", className: "text-center" },
            { title: "Precio", className: "text-center" },
            { title: "Importe", className: "text-center" },
            { title: "Iva", className: "text-center" },
            { title: "Ieps", className: "text-center" },
            { title: "Total", className: "text-center" } 
        ],
        columnDefs: [
            { "width": "5%", "targets": 0 },
            { "width": "10%", "targets": 1 },
            { "width": "20%", "targets": 2 },
            { "width": "10%", "targets": 3 },
            { "width": "10%", "targets": 4 },
            { "width": "10%", "targets": 5 },
            { "width": "10%", "targets": 6 },
            { "width": "10%", "targets": 7 },
        ]
    });
}

function Abrir_Ventana_Espera() {
    $('#Ventana_Espera').show();
}

function Cerrar_Ventana_Espera() {
    $('#Ventana_Espera').hide();
}
