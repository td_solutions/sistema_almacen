﻿/*====================================== VARIABLES =====================================*/
var oTable;
var $InsumoID = '';
var $AlmacenID = '';
var $ProveedorID = '';

$.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Today",
    clear: "Clear",
    format: "dd/mm/yyyy",
    titleFormat: "dd/mm/yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
};
$("#txt-fecha-inicio").datepicker({
    format: 'dd/mm/yyyy',
    language: 'es',
    autoclose: true
});
$("#txt-fecha-fin").datepicker({
    format: 'dd/mm/yyyy',
    language: 'es',
    autoclose: true
});
$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});


/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Reporte Inventario');
    eventos();
    cargar_insumos();
    cargar_almacen();
    cargar_proveedor();
    asignar_fecha();
    //Crear_Tabla();    
});
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {
    $('#btn-cns').click(function (e) {
        e.preventDefault();
        Crear_Tabla();
    });

    $('#btn-xcl').click(function (e) {
        e.preventDefault();
        exportar_excel();
    });

}
function cargar_proveedor() {
    jQuery('#cmb-proveedor').select2({
        ajax: {
            url: '../Proveedores/GetProveedoresList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $ProveedorID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
function cargar_insumos() {
    jQuery('#cmb-insumos').select2({
        ajax: {
            url: '../Insumos/GetInsumosList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $InsumoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}


function cargar_almacen() {
    jQuery('#cmb-almacen').select2({
        ajax: {
            url: '../Almacen/GetAlmacenList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $AlmacenID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}

/*====================================== OPERACIONES ===================================*/
function Crear_Tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": {
            "type": "POST",
            "url": "GetReporteSalidas",
            "data": function (d) {
                d.Almacen_ID = $('#cmb-almacen :selected').val();
                d.Insumo_ID = $('#cmb-insumos :selected').val();
                d.Proveedor_ID = $('#cmb-proveedor :selected').val();
                d.Str_Fecha_Inicio = $('#txt-fecha-inicio').val();
                d.Str_Fecha_Fin = $('#txt-fecha-fin').val();
            }
        },
        columns: [
            { title: "No Salida", className: "text-center" },
            { title: "Fecha", className: "text-center" },
            { title: "Estatus", className: "text-center" },
            { title: "Almacen", className: "text-center" },
            { title: "Tipo Salida", className: "text-center" }, 
            { title: "IVA", className: "text-center" }, 
            { title: "IEPS", className: "text-center" }, 
            { title: "Total", className: "text-right" }
        ],
        columnDefs: [
            { "width": "5%", "targets": 0 },
            { "width": "10%", "targets": 1 },
            { "width": "20%", "targets": 2 },
            { "width": "20%", "targets": 3 },
            { "width": "10%", "targets": 4 },
            { "width": "10%", "targets": 5 },
            { "width": "10%", "targets": 6 },
            {
                render: function (data, type, row) {

                },
                //targets: 4
            }
        ],
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable                      
        }
    });
};

function exportar_excel() {
    var $Str_Fecha_Inicio;
    var $Str_Fecha_Fin;

    $AlmacenID= $('#cmb-almacen :selected').val();
    $InsumoID = $('#cmb-insumos :selected').val();
    $Str_Fecha_Inicio = $('#txt-fecha-inicio').val();
    $Str_Fecha_Fin = $('#txt-fecha-fin').val();
    window.location = "Download_Reporte_Salidas?Almacen_Origen_ID=" + $AlmacenID + "&Insumo_ID=" + $InsumoID + "&Str_Fecha_Inicio=" + $Str_Fecha_Inicio + "&Str_Fecha_Fin=" + $Str_Fecha_Fin;
    //window.location = "Download_Reporte_Corte?No_Tarjeta=" + $('#txt-no_empleado').val() + "&Fecha_Inicio=" + $('#txt-fecha_inicio').val() + "&Fecha_Fin=" + $('#txt-fecha_inicio').val(); //$('#txt-fecha_fin').val();
}

/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if (($('#txt-fecha_inicio').val() == '' || $('#txt-fecha_inicio').val() == undefined || $('#txt-fecha_inicio').val() == null)
            && ($('#txt-fecha_fin').val() == '' || $('#txt-fecha_fin').val() == undefined || $('#txt-fecha_fin').val() == null)
            && ($('#txt-no_empleado').val() == '' || $('#txt-no_empleado').val() == undefined || $('#txt-no_empleado').val() == null)) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> SELECCIONE ALGUN FILTRO</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}

function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}

function asignar_fecha() {
    var f = new Date();
    $("#txt-fecha-inicio").datepicker("setDate", f);
    $("#txt-fecha-fin").datepicker("setDate", f);
}