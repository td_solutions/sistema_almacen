﻿$(function () {
    //valida las caja de textos multiline
    $('textarea').keypress(function (event) {
        var no = $.trim($(this).val()).length;
        var propiedad = parseInt($(this).attr('max'));
        if (no >= propiedad) { event.preventDefault(); }
    });
    $('.validacion').keyup(function () { // inicio evento keyup
        var campo = $(this);
        var tipo = $.trim(campo.attr('tipo'));
        var valores = $.trim(campo.attr('valores'));
        //validacion de fecha
        if (tipo == "fecha") {  //inicio
            if (esFecha(campo.val()) == false) {
                campo.val('');
            }
        } //fin
        //validacion de caracteres 
        if (tipo == "caracteres") { //inicio
            var respuesta;
            respuesta = obtenerCadenaPermitida(campo.val(), valores);
            campo.val(respuesta);
        } // fin
        //validacion de caracteres 
        //validacion numeros
        if (tipo == "numeros") {
            var respuesta;
            respuesta = obtenerCadenaNumeros($.trim(campo.val()), valores);
            campo.val(respuesta);
        }
        //eventos de foco
        $('.validacion').focus(function () { // inicio 
            var campo = $(this);
            $(campo).css('border-style', 'groove');
            $(campo).css('border-color', '');
        }); // fin del evento focus
        $('.easyui-numberbox').focus(function () { //inicio
            var campo = $(this);
            $(campo).css('border-style', 'groove');
            $(campo).css('border-color', '');
        }); // fin
    }); // fin
});


// metodos para validar--------------------------------------------------------------------------------

function esFecha(cadena) {
    var fecha = new String(cadena);
    var ano = new String(fecha.substring(fecha.lastIndexOf("/") + 1, fecha.length));
    var mes = new String(fecha.substring(fecha.indexOf("/") + 1, fecha.lastIndexOf("/")));
    var dia = new String(fecha.substring(0, fecha.indexOf("/")));

    if (mes.length == 0 || dia.length == 0) {
        return false;
    }
    // valida si el año
    if (isNaN(ano) || ano.length < 4 || parseFloat(ano) < 1900) {
        return false;
    }
    if (isNaN(mes) || parseFloat(mes) < 1 || parseFloat(mes) > 12) {
        return false;
    }
    // valida el dia
    if (isNaN(dia) || parseInt(dia, 10) < 1 || parseInt(dia, 10) > 31) {
        return false;
    }
    if (parseInt(mes) == 4 || parseInt(mes) == 6 || parseInt(mes) == 9 || parseInt(mes) == 11) {
        if (parseInt(dia) > 30) {
            return false;
        }
    }
    // para el año biciesto
    if (parseInt(ano) % 4 == 0) {
        // si es biciesto
        if (parseInt(dia) > 29 && parseInt(mes) == 2) {
            return false;
        }
    }
    else {
        // nos biciesto
        if (parseInt(dia) > 28 && parseInt(mes) == 2) {
            return false;
        }
    }
    return true;
}

// metodos para validar--------------------------------------------------------------------------------


function obtenerCadenaPermitida(cadena, cadendaExtra) {
    var respuesta = "";
    var letra = "";
    var enter = "\n";
    var caracteres, i;
    caracteres = "abcdefghijklmnopqrstuvwxyz1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ @._-$ áéíóúÁÉÍÓÚÑ ,;´¿?!¡)(%#*+{}[]<>" + enter + cadendaExtra;
    for (i = 0; i < cadena.length; i++) {
        letra = cadena.substring(i, i + 1);
        if (caracteres.indexOf(letra) != -1) {
            respuesta = respuesta + letra
        }
    }
    return respuesta;
}



// metodos para validar numeros

function obtenerCadenaNumeros(cadena, cadendaExtra) {
    var respuesta = "";
    var letra = "";
    var enter = "\n";
    var caracteres, i;
    caracteres = "1234567890" + enter + cadendaExtra;
    for (i = 0; i < cadena.length; i++) {
        letra = cadena.substring(i, i + 1);
        if (caracteres.indexOf(letra) != -1) {
            respuesta = respuesta + letra
        }
    }
    return respuesta;
}

//-------------------------------------------------------------------------------------
function validarFechas(objFechaInicio, objFechaFinal) {
    fecha_inicio = objFechaInicio;
    fecha_final = objFechaFinal;
    fecha_inicio_valor = $.trim(fecha_inicio.val());
    fecha_final_valor = $.trim(fecha_final.val());
    if (fecha_inicio_valor.length == 0) {
        $.messager.alert('Búquedas y Reportes', 'Escribe una fecha de Inicio', '', function () { $(fecha_inicio).focus(); });
        return true;
    }
    if (fecha_final_valor.length == 0) {
        $.messager.alert('Búsqueda y reportes', 'Escribe una Fecha final', '', function () { $(fecha_final).focus(); });
        return true;
    }
    if (mayor(fecha_inicio_valor, fecha_final_valor)) {
        $.messager.alert('Búsqueda ', 'la fecha Inicial es mayor que la fecha final', '', function () { $(fecha_final).focus(); });
        return true;
    }
}

//-------------------------------------------------------------------------------------
//  http://micodigobeta.com.ar/?p=189
function mayor(fecha, fecha2) { //incio
    var xMes = fecha.substring(3, 5);
    var xDia = fecha.substring(0, 2);
    var xAnio = fecha.substring(6, 10);
    var yMes = fecha2.substring(3, 5);
    var yDia = fecha2.substring(0, 2);
    var yAnio = fecha2.substring(6, 10);
    if (xAnio > yAnio) {
        return (true);
    } else {
        if (xAnio == yAnio) {
            if (xMes > yMes) {
                return (true);
            }
            if (xMes == yMes) {
                if (xDia > yDia) {
                    return (true);
                } else {
                    return (false);
                }
            } else {
                return (false);
            }
        } else {
            return (false);
        }
    }
}

/* It accepts one, two or three parameters:
*
*     $.fn.dataTable.render.moment( to );
*     $.fn.dataTable.render.moment( from, to );
*     $.fn.dataTable.render.moment( from, to, locale );
*
* Where:
*
* * `to` - the format that will be displayed to the end user
* * `from` - the format that is supplied in the data (the default is ISO8601 -
*   `YYYY-MM-DD`)
* * `locale` - the locale which MomentJS should use - the default is `en`
*   (English).*/

// UMD
(function (factory) {
    "use strict";

    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery'], function ($) {
            return factory($, window, document);
        });
    }
    else if (typeof exports === 'object') {
        // CommonJS
        module.exports = function (root, $) {
            if (!root) {
                root = window;
            }

            if (!$) {
                $ = typeof window !== 'undefined' ?
					require('jquery') :
					require('jquery')(root);
            }

            return factory($, root, root.document);
        };
    }
    else {
        // Browser
        factory(jQuery, window, document);
    }
}
(function ($, window, document) {


    $.fn.dataTable.render.moment = function (from, to, locale) {
        // Argument shifting
        if (arguments.length === 1) {
            locale = 'en';
            to = from;
            from = 'YYYY-MM-DD';
        }
        else if (arguments.length === 2) {
            locale = 'en';
        }

        return function (d, type, row) {
            var m = window.moment(d, from, locale, true);

            // Order and type get a number value from Moment, everything else
            // sees the rendered value
            return m.format(type === 'sort' || type === 'type' ? 'x' : to);
        };
    };


}));
