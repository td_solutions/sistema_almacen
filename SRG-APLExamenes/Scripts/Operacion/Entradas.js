﻿/*====================================== VARIABLES =====================================*/
var $EventoID = '';
var oTable;
var $InsumoID = '';
var $AlmacenID = '';
var $ProveedorID = '';
var $Lista_Partidas = [];
var tasa_iva = 0;
var tasa_ieps = 0;

var $porcentaje_iva = 0;
var $porcentaje_ieps = 0;

var $obj_insumo = null;

$.fn.datepicker.dates['es'] = {
    autoclose: true,
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Today",
    clear: "Clear",
    format: "dd/mm/yyyy",
    titleFormat: "dd/mm/yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
};


$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "sEmptyTable": "No hay datos en la tabla",
        "lengthMenu": "Mostando _MENU_ Registro(s) por página",
        "zeroRecords": "Nada Encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)",
        "sLoadingRecords": "Cargando ...",
        "sProcessing": "Por favor espere...",
        "oPaginate": {
            "sFirst": "Primero",
            "sPrevious": "Atras",
            "sNext": "Siguiente",
            "sLast": "Ultimo"
        }
    }
});
/*====================================== INICIO-CARGA ==================================*/
jQuery(document).ready(function () {
    $("#St-Map").text('Site Map: /Insumos');
    eventos();
    cargar_tabla();
    //cargar_tabla_partidas();
    cargar_insumos();
    cargar_almacen();
    cargar_proveedor();
    //cargar_unidades();
    asignar_fecha();
    cargar_impuestos();
    
    //$("#cmb-stt option[value='ACTIVO']").attr('selected', 'selected');
});
function cargar_insumos() {
    jQuery('#cmb-insumos').select2({
        ajax: {
            url: 'Insumos/ObtenerListaInsumos',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $InsumoID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}

function cargar_unidades() {
    jQuery('#cmb-unidad').select2({
        ajax: {
            url: 'Unidades/GetUnidadesList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $AlmacenID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}

function cargar_almacen() {
    jQuery('#cmb-almacen').select2({
        ajax: {
            url: 'Almacen/GetAlmacenList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $AlmacenID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
function cargar_impuestos() {
    $.ajax({
        url: 'Parametros/Consultar',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        cache: false,
        success: function (Resultado) {
            if (Resultado.items.Estatus) {

                row = JSON.parse(Resultado.items.Data);
                tasa_iva = row[0].Tasa_IVA;
                tasa_ieps = row[0].Tasa_IEPS;
            }
            else {
                tasa_iva = 0;
                tasa_ieps = 0;
                mostrar_mensaje("Advertencia", Resultado.Mensaje);
            }
        },
        error: function (msj) {
            tasa_ieps = 0;
            tasa_iva = 0;
        }
    });

}
function cargar_proveedor() {
    jQuery('#cmb-proveedor').select2({
        ajax: {
            url: 'Proveedores/GetProveedoresList',
            dataType: 'json',
            type: "POST",
            delay: 250,
            cache: false,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page,
                    Area: $ProveedorID
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            }
        },
        theme: "classic",
        placeholder: 'Seleccione',
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    function formatRepo(repo) {
        if (repo.loading) {
            return repo.text;
        }
        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'><i class='fa fa-tag' style='color:#000;'></i> " + repo.text + "</div>" +
            "</div></div>";
        return markup;
    }
    function formatRepoSelection(repo) {
        return repo.text;
    }
}
/// <summary>
/// FUNCION QUE INICIALIZA LOS MANEJADORES DE EVENTOS.
/// </summary>
function eventos() {

    $("#txt-fecha-entrada").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });

    $("#txt-fecha-factura").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });

    $("#txt-fecha-recepcion").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });

    $("#txt-fecha-pago").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });
    
    $('#btn-cancel').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('');
    });

    $('#btn-new').click(function (e) {
        e.preventDefault();
        limpiar_controles();
        habilitar_controles('new');
        asignar_fecha();
    });
    $('.btn-agregar-partida').click(function (e) {
        e.preventDefault();

        var output = Validar_Partida();
        if (output.Estatus) {
            Agregar_Partidas();
        }
        else
        {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Algunos datos no se completaron!',
                footer: '<a href>' + output.Mensaje + '</a>'
            });
        }
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        var output = validar_datos();
        if (output.Estatus) {
            OperationMaster();
        } else {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Algunos datos no se completaron!',
                footer: '<a href>' + output.Mensaje + '</a>'
            });

            /*$('#btn-save').popModal({
                html: "<h6> Datos requeridos </h6> <hr /> " + output.Mensaje + "<div class='popModal_footer'><button type='button' class='btn btn-primary btn-block' data-popmodal-but='ok'>ok</button></div>",
            });*/
        }
    });

    $("#txt-fecha-recepcion").change(function (e) {
        calcular_fecha_pago();
    });

    $("#txt-precio").change(function (e) {
        calcular_costo_con_impuestos();
    });
    $("#txt-cantidad").change(function (e) {
        calcular_costo_con_impuestos();
    });
}
function calcular_costo_con_impuestos() {
    $iva = 0;
    $ieps = 0;
    $lleva_iva = 0;
    $lleva_ieps = 0;
    $total = 0;
    $importe = 0;
    $cantidad = 1;

    if ($('#txt-precio').val().length > 0) {
        if (parseFloat($('#txt-precio').val()) <= 0)
            return;
    }
    if ($('#txt-cantidad').val().length > 0) {
        if (parseFloat($('#txt-cantidad').val()) <= 0)
            return;
    }
        $cantidad = parseFloat($('#txt-cantidad').val());
        $importe = parseFloat($('#txt-precio').val());
        $importe = $importe * $cantidad;

        $lleva_iva = parseFloat($('#hdn-iva').val() );
        $lleva_ieps = parseFloat($('#hdn-ieps').val());
    if ($lleva_iva > 0)
        $iva = $importe * ($porcentaje_iva / 100 );
    if ($lleva_ieps > 0)
        $ieps = $importe * ($porcentaje_ieps / 100 );

        $total = $iva + $ieps + $importe;

    $("#txt-ieps").val($ieps.toFixed(2));
    $("#txt-iva").val($iva.toFixed(2));
    $("#txt-total").val($total.toFixed(2));
    $("#txt-subtotal").val($importe.toFixed(2));

}
function calcular_totales() {
    $iva = 0;
    $ieps = 0;
    $total = 0;
    $importe = 0;

    $Lista_Partidas.forEach(function (element) {
        $importe += parseFloat(element.Importe);
        $iva += parseFloat(element.Iva);
        $ieps += parseFloat(element.Ieps);
        $total += parseFloat(element.Total);
    });

    $("#hdn-iva-total").val($iva.toFixed(2));
    $("#hdn-ieps-total").val($ieps.toFixed(2));
    $("#hdn-sub-total-general").val($importe.toFixed(2));
    $("#hdn-total-general").val($total.toFixed(2));

}
function calcular_fecha_pago() {
    var fechota;
    var fecha_testo;
    var dias_agregar;
    var dia;
    var mes;
    var anio;
    var fecha_arreglo;
    try {
        if ($("#txt-fecha-recepcion").val().length == 10) {
            fecha_arreglo = $("#txt-fecha-recepcion").val().split("/");
            dia = fecha_arreglo[0];
            mes = parseInt(fecha_arreglo[1]) - 1;
            anio = fecha_arreglo[2];
            fecha_testo = $("#txt-fecha-recepcion").val();
            fechota = new Date(anio, mes, dia);
            dias_agregar = parseInt($('#diasCreditoProv').val());
            var nueva_fechota = fechota;// + dias_agregar;
            const newDate = addDays(fechota, dias_agregar);
            fijar_fecha_pago(newDate);
        }

    }
    catch (error) {
        console.error(error);
        alert(error);
        // expected output: ReferenceError: nonExistentFunction is not defined
        // Note - error messages will vary depending on browser
    }
}
function addDays(date, days) {
    const copy = new Date(Number(date))
    copy.setDate(date.getDate() + days)
    return copy
}
function Agregar_Partidas() {
    var repetido = false;
    var Obj_Partidas = new Object();
    Obj_Partidas.Cantidad = $('#txt-cantidad').val();
    Obj_Partidas.Unidad = $('#cmb-unidad :selected').text();
    Obj_Partidas.Unidad_ID = $('#cmb-unidad :selected').val();
    Obj_Partidas.Insumo_Id = $('#cmb-insumos :selected').val();
    Obj_Partidas.Insumo = $('#cmb-insumos :selected').text();
    Obj_Partidas.Precio = $('#txt-precio').val();
    Obj_Partidas.Importe = parseFloat($('#txt-precio').val()) * parseFloat($('#txt-cantidad').val());
    Obj_Partidas.Ieps = $('#txt-ieps').val();
    Obj_Partidas.Iva = $('#txt-iva').val();
    Obj_Partidas.Total = $('#txt-total').val();

    $Lista_Partidas.forEach(function (element) {
        if (element.Insumo_Id == Obj_Partidas.Insumo_Id && element.Unidad == Obj_Partidas.Unidad) {
            repetido = true;
            Swal.fire({
                title: 'Ya se incluyó ' + element.Cantidad + " " + Obj_Partidas.Unidad +" de " + Obj_Partidas.Insumo,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Agregar de todas formas!'
            }).then((result) => {
                if (result.value) {
                    removeItemFromArr($Lista_Partidas, element);

                    element.Cantidad = parseFloat(element.Cantidad) + parseFloat(Obj_Partidas.Cantidad);
                    element.Importe = parseFloat(Obj_Partidas.Importe) * element.Cantidad;
                    if (parseFloat(element.Iva) > 0)
                        element.Iva = element.Importe * tasa_iva;
                    if (parseFloat(element.Ieps) > 0)
                        element.Iva = element.Importe * tasa_iva;
                    element.Total = parseFloat(element.Importe) + parseFloat(element.Iva) + parseFloat(element.Ieps);

                    $Lista_Partidas.push(element);
                    recargarTablaPartidas();
                }
            });
        }
    });
    if (!repetido) {
        $Lista_Partidas.push(Obj_Partidas);
        //ActualizarCostoInsumo();
        recargarTablaPartidas();
    }
}
function sumarizarFacturas() {
    var table = $('#Tbl_Partidas').DataTable();
    var total_factura = 0;
    var data = table
        .rows()
        .data();
    for (x = 0; x < data.length; x++) {
        total_factura += (parseFloat(data[x][0]) * parseFloat(data[x][3]));
    }
    $('#txt-total-factura').val(total_factura);
}
function recargarTablaPartidas() {
    var dataSet = [];
    $Lista_Partidas.forEach(function (element) {
        var dato = [];
        dato.push(element.Cantidad);
        dato.push(element.Unidad);        
        dato.push(element.Insumo);
        dato.push(element.Precio);
        dato.push(element.Importe);
        dato.push(element.Iva);
        dato.push(element.Ieps);
        dato.push(element.Total);
        dato.push(element.Insumo_Id);        
        dato.push(element.Unidad_ID);
        dataSet.push(dato);
    });
    cargar_tabla_partidas(dataSet);
    calcular_totales();
    //sumarizarFacturas(); 
}
function removeItemFromArr(arr, item) {
    var i = arr.indexOf(item);

    if (i !== -1) {
        arr.splice(i, 1);
    }
}
/*********Combos**********/
$('#cmb-insumos').on("select2:select", function (evt) {
    $obj_insumo = evt;
    $InsumoID = evt.params.data.Insumo_Id;
    $unidadID = evt.params.data.Unidad_ID;
    $Unidad = evt.params.data.Unidad;
    //$Stock = evt.params.data.Existencias;
    $Precio = evt.params.data.Costo;
    $iva_item = evt.params.data.Iva;
    $ieps_item = evt.params.data.Ieps;
    $total_item = evt.params.data.Total;
    //$u_consumo = evt.params.data.Unidad_Consumo;
    //$u_compra = evt.params.data.Unidad;
    //$equivalencia = evt.params.data.Equivalencia;
    //$unidadConsumoID = evt.params.data.Unidad_Consumo_ID;
    //$UnidadConsumo = evt.params.data.Unidad_Consumo;

    $('#cmb-unidad option').remove();
    $("#cmb-unidad").append('<option value=' + $unidadID + '>' + $Unidad+'</option>');
    $("#txt-precio").val($Precio);

    $('#hdn-iva').val($iva_item);
    $('#hdn-ieps').val($ieps_item);

    $('#txt-ieps').val($ieps_item);
    $('#txt-iva').val($iva_item);
    $('#txt-total').val($total_item);

    $porcentaje_iva = Math.round((100 * $iva_item) / $Precio);
    $porcentaje_ieps = Math.round((100 * $ieps_item) / $Precio);

    calcular_costo_con_impuestos();
});
$("#cmb-insumos").on("select2:unselecting instead", function (e) {
    $InsumoID = '';
});
$('#cmb-almacen').on("select2:select", function (evt) {
    $AlmacenID = evt.params.data.id;
});
$("#cmb-almacen").on("select2:unselecting instead", function (e) {
    $AlmacenID = '';
});
$('#cmb-proveedor').on("select2:select", function (evt) {
    $ProveedorID = evt.params.data.id;
    $('#diasCreditoProv').val(evt.params.data.Data2);
    $('#txt-fecha-recepcion').prop("disabled", false);
    calcular_fecha_pago();
});

$("#cmb-proveedor").on("select2:unselecting instead", function (e) {
    $ProveedorID = '';
    $('#diasCreditoProv').val("0");
    $('#txt-fecha-recepcion').prop("disabled", true);
    $('#txt-fecha-pago').val("");
    $('#txt-fecha-recepcion').val("");
});
$('#cmb-tipo-entrada').change(function () {
    var opcion_tipo_entrada = $('#cmb-tipo-entrada :selected').val();
    var style_display = $('#datos_factura').css('display');//$("#datos_factura").attr("style");
    if (opcion_tipo_entrada === "OTROS") {
        if (style_display === "flex") {
            $("#datos_factura").animate({
                opacity: 0.25,
                left: "+=50",
                height: "toggle"
            }, 500, function () {
                $("#datos_factura").attr('style', 'display:none');
            });
            $("#datos_factura_2").animate({
                opacity: 0.25,
                left: "+=50",
                height: "toggle"
            }, 500, function () {
                $("#datos_factura_2").attr('style', 'display:none');
            });
        }
        //$("#datos_factura_2").attr('style','display:none');
        //$("#datos_factura").attr('style','display:none');
    }
    else {
        if (style_display === "none") {
            $("#datos_factura").animate({
                opacity: 1,
                left: "-=50",
                height: "toggle"
            }, 500, function () {
                $("#datos_factura").attr('style', 'display:flex');
            });
            $("#datos_factura_2").animate({
                opacity: 1,
                left: "-=50",
                height: "toggle"
            }, 500, function () {
                $("#datos_factura_2").attr('style', 'display:flex');
            });
            //$("#datos_factura_2").attr('style', 'display:flex');
            //$("#datos_factura").attr('style', 'display:flex');
        }
    }
});
/*====================================== OPERACIONES ===================================*/
/// <summary>
/// Función que ejecuta el alta de los registros
/// </summary>
function OperationMaster() {
    var Obj_Capturado = new Object();
    try {
        Obj_Capturado.No_Entrada = $EventoID;
        Obj_Capturado.Almacen_ID = $('#cmb-almacen :selected').val();
        Obj_Capturado.Proveedor_ID = $('#cmb-proveedor :selected').val();
        Obj_Capturado.Almacen = $('#cmb-almacen :selected').text();
        Obj_Capturado.Proveedor = $('#cmb-proveedor :selected').text();
        Obj_Capturado.Tipo_Factura = $('#cmb-tipo-factura :selected').val();
        Obj_Capturado.No_Factura = $('#txt-no-factura').val();
        Obj_Capturado.Total_Factura = $('#txt-total-factura').val();
        Obj_Capturado.Str_Fecha_Factura = $('#txt-fecha-factura').val();
        Obj_Capturado.Str_Fecha_Recepcion = $('#txt-fecha-recepcion').val();
        Obj_Capturado.Str_Fecha_Pago = $('#txt-fecha-pago').val();
        Obj_Capturado.Str_Fecha_Registro = $('#txt-fecha-entrada').val();

        Obj_Capturado.Iva = $("#hdn-iva-total").val();
        Obj_Capturado.Ieps = $("#hdn-ieps-total").val();
        Obj_Capturado.SubTotal= $("#hdn-sub-total-general").val();
        Obj_Capturado.Total = $("#hdn-total-general").val();

        //Obj_Capturado.Observaciones = $('#txt-observaciones').val();
        Obj_Capturado.Estatus = $('#cmb-stt :selected').val();
        Obj_Capturado.Tipo_Entrada = $('#cmb-tipo-entrada :selected').val();
        Obj_Capturado.Detalles = $Lista_Partidas;
        Obj_Capturado.Captura = $EventoID == "" ? 'I' : 'U';
        $.ajax({
            url: 'Entradas/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_Capturado),
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    window.open(Resultado.Url_PDF, 'XML', 'toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');
                    mostrar_mensaje("", Resultado.Mensaje);
                    limpiar_controles();
                    habilitar_controles();
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
    }
}

function ActualizarCostoInsumo() {
    $obj_insumo;
    var Obj_Capturado = new Object();
    try {
        Obj_Capturado.Insumo_ID = $('#cmb-insumos :selected').val();
        //Valores
        Obj_Capturado.Nombre = $obj_insumo.params.data.Nombre;
        Obj_Capturado.Descripcion = $obj_insumo.params.data.Descripcion;
        Obj_Capturado.Estatus = $obj_insumo.params.data.Estatus;
        Obj_Capturado.Tipo_Insumo_ID = $obj_insumo.params.data.Tipo_Insumo_ID;
        Obj_Capturado.Maximo = $obj_insumo.params.data.Maximo;
        Obj_Capturado.Minimo = $obj_insumo.params.data.Minimo;
        Obj_Capturado.Punto_Reorden = $obj_insumo.params.data.Punto_Reorden;
        Obj_Capturado.Costo = $("#txt-subtotal").val();
        Obj_Capturado.Iva = $("#txt-iva").val();
        Obj_Capturado.Ieps = $("#txt-ieps").val();
        Obj_Capturado.Total = $("#txt-total").val();
        Obj_Capturado.Existencias = $obj_insumo.params.data.Existencias;
        Obj_Capturado.Unidad_ID = $obj_insumo.params.data.Unidad_ID;
        Obj_Capturado.Unidad_Consumo_ID = $obj_insumo.params.data.Unidad_Consumo_ID;
        Obj_Capturado.Equivalencia = $obj_insumo.params.data.Equivalencia;
        Obj_Capturado.Proveedor_ID = $('#cmb-proveedor :selected').val();
        Obj_Capturado.Ubicacion = $obj_insumo.params.data.Ubicacion;

        Obj_Capturado.Captura = 'U';

        $.ajax({
            url: 'Insumos/EventMaster',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Obj_Capturado),
            cache: false,
            success: function (Resultado) {
                if (Resultado.Estatus) {
                    mostrar_mensaje("", Resultado.Mensaje);
                }
                else {
                    mostrar_mensaje("Advertencia", Resultado.Mensaje);
                }
            }
        });
    } catch (e) {
        mostrar_mensaje("Informe Técnico", e);
    }
}
/// <summary>
/// Función para cargar los datos
/// </summary>
function cargar_tabla() {
    oTable = $('#Tbl_Registros').DataTable({
        destroy: true,
        "ajax": "Entradas/Traer_Entrada",
        ordering: false,
        lengthMenu: [10, 25, 10, 30],
        columns: [
            { title: "No_Entrada", className: "text-left" },
            { title: "Fecha_Entrada", className: "text-left" },
            { title: "Proveedor", className: "text-center" },
            { title: "Estatus", className: "text-center" }
        ],
        columnDefs: [
            {
                render: function (data, type, row) {
                    return '<button type="button" class="btn-primary" title="Cancelar" onclick="Cargar_Informacion(' + "'" + row[0] + "'" + ')"><i class="far fa-trash-alt"></i></button>'
                },
                targets: 4
            },
            {
                render: function (data, type, row) {
                    return '<button type="button" class="btn-primary" title="Imprimir" onclick="Imprimir_Formato(' + "'" + row[0] + "'" + ')"><i class="fa fa-print"></i></button>'
                },
                targets: 5
            }
        ]
    });
}

function cargar_tabla_partidas(data) {

    oTable = $('#Tbl_Partidas').DataTable({
        destroy: true,
        ordering: false,
        data:data,
        lengthMenu: [5, 10, 20,10,10,10,10,10,2 ],
        columns: [
            { title: "Cantidad", className: "text-left" },
            { title: "Unidad", className: "text-center" },
            { title: "Insumo", className: "text-center" },
            { title: "Precio", className: "text-center" },
            { title: "Importe", className: "text-center" },
            { title: "Iva", className: "text-center" },
            { title: "Ieps", className: "text-center" },
            { title: "Total", className: "text-center" },
            { title: "Insumo_ID", visible: false }  
        ],
        columnDefs: [
            { "width": "5%", "targets": 0 },
            { "width": "10%", "targets": 1 },
            { "width": "20%", "targets": 2 },
            { "width": "10%", "targets": 3 },
            { "width": "10%", "targets": 4 },
            { "width": "10%", "targets": 5 },
            { "width": "10%", "targets": 6 },
            { "width": "10%", "targets": 7 },
            { "width": "10%", "targets": 8 },
            {
                render: function (data, type, row) {
                    return '<button type="button" class="btn-primary" title="Quitar" onclick="Quitar_Partida(' + "'" + row[0] + "','" + row[1] + "','" + row[2] + "','"+ row[8] +"'" + ')"><i class="far fa-trash-alt"></i></button>';
                },
                targets: 9
            }
        ]
    });
    // modificar precio Insumo
    // Cargar insumos 
}
function Quitar_Partida(cantidad, unidad, insumo, insumo_id) {
    var ediccion = $("#txt-no-entrada").val();
    if (ediccion === '0') {
        $Lista_Partidas.forEach(function (element) {
            if (element.Insumo_Id == insumo_id && element.Unidad == unidad)
                removeItemFromArr($Lista_Partidas, element);
        });

        recargarTablaPartidas();
        
    }
    
}
/*====================================== GENERALES =====================================*/
/// <summary>
/// FUNCION PARA VALIDAR LOS DATOS REQUERIDOS
/// </summary>
function validar_datos() {
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#cmb-tipo-entrada :selected').val() == '' || $('#cmb-tipo-entrada :selected').val() == undefined || $('#cmb-tipo-entrada :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> TIPO DE LA ENTRADA</strong>.</span><br />';
        }
        else {
            if ($('#cmb-tipo-entrada :selected').val() != 'OTROS') {
                if ($('#cmb-tipo-factura :selected').val() == '' || $('#cmb-tipo-factura :selected').val() == undefined || $('#cmb-tipo-factura :selected').val() == null) {
                    output.Estatus = false;
                    output.Mensaje += '<span class="fas fa-angle-right"><strong> TIPO FACTURA</strong>.</span><br />';
                }
                if ($('#txt-fecha-factura').val() == '' || $('#txt-fecha-factura').val() == undefined || $('#txt-fecha-factura').val() == null) {
                    output.Estatus = false;
                    output.Mensaje += '<span class="fas fa-angle-right"><strong> FECHA FACTURA</strong>.</span><br />';
                }
                if ($('#txt-no-factura').val() == '' || $('#txt-no-factura').val() == undefined || $('#txt-no-factura').val() == null) {
                    output.Estatus = false;
                    output.Mensaje += '<span class="fas fa-angle-right"><strong> NO FACTURA/REMISION</strong>.</span><br />';
                }
                if ($('#txt-fecha-recepcion').val() == '' || $('#txt-fecha-recepcion').val() == undefined || $('#txt-fecha-recepcion').val() == null) {
                    output.Estatus = false;
                    output.Mensaje += '<span class="fas fa-angle-right"><strong> FECHA RECEPCI&Oacute;N</strong>.</span><br />';
                }
                if ($('#txt-fecha-pago').val() == '' || $('#txt-fecha-pago').val() == undefined || $('#txt-fecha-pago').val() == null) {
                    output.Estatus = false;
                    output.Mensaje += '<span class="fas fa-angle-right"><strong> FECHA PAGO</strong>.</span><br />';
                }
            }
        }

        if ($('#cmb-stt :selected').val() == '' || $('#cmb-stt :selected').val() == undefined || $('#cmb-stt :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> ESTATUS</strong>.</span><br />';
        }
        
        if ($('#cmb-proveedor :selected').val() == '' || $('#cmb-proveedor :selected').val() == undefined || $('#cmb-proveedor :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> PROVEEDOR</strong>.</span><br />';
        }
        if ($('#cmb-almacen :selected').val() == '' || $('#cmb-almacen :selected').val() == undefined || $('#cmb-almacen :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> ALMACEN</strong>.</span><br />';
        }
        if ($Lista_Partidas.length <= 0) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> PARTIDAS</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
function Validar_Partida() { 
    var output = new Object();
    output.Estatus = true;
    output.Mensaje = '';
    try {
        if ($('#txt-cantidad').val() == '' || $('#txt-cantidad').val() == undefined || $('#txt-cantidad').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> CANTIDAD</strong>.</span><br />';
        }
        if ($('#txt-precio').val() == '' || $('#txt-precio').val() == undefined || $('#txt-precio').val() == null || $('#txt-precio').val() == '0' || $('#txt-precio').val() == '0.0' || $('#txt-precio').val() == '0.' || $('#txt-precio').val() == '.0') {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> PRECIO</strong>.</span><br />';
        }
        else {
            var cant = parseFloat($('#txt-precio').val());
            if (cant <= 0) {
                output.Estatus = false;
                output.Mensaje += '<span class="fas fa-angle-right"><strong> EL PRECIO DEBE SER MAYOR A 0</strong>.</span><br />';
            }
        }
        if ($('#cmb-insumos :selected').val() == '' || $('#cmb-insumos :selected').val() == undefined || $('#cmb-insumos :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> INSUMO</strong>.</span><br />';
        }
        if ($('#cmb-unidad :selected').val() == '' || $('#cmb-unidad :selected').val() == undefined || $('#cmb-unidad :selected').val() == null) {
            output.Estatus = false;
            output.Mensaje += '<span class="fas fa-angle-right"><strong> UNIDAD</strong>.</span><br />';
        }
    } catch (e) {
        output.Mensaje += e;
    } finally {
        return output;
    }
}
/// <summary>
/// FUNCION QUE HABILITA LOS CONTROLES DE LA PAGINA DE ACUERDO A LA OPERACION A REALIZAR.
/// </summary>
function habilitar_controles(opcion) {
    switch (opcion) {
        case "new":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });
            $("#cmb-stt").val("ACTIVO");
            $("#cmb-tipo-entrada").val("");

            $('input[type=text]').each(function () { $(this).prop("disabled", false); });
            $('input[tipo=numeros]').each(function () { $(this).prop("disabled", false); });
            $('select').each(function () { $(this).prop("disabled", false); });
            $('.btn-agregar-partida').prop("disabled", false);

            $('#txt-no-entrada').prop("disabled", true);            

            break;
        case "Edit":
            $('#btn-new').css({ display: 'none' });
            $('#btn-cancel').css({ display: 'Block' });
            $('#btn-save').css({ display: 'Block' });
            $("#wrapper").css({ display: 'Block' });
            $("#Reg-Datos").css({ display: 'none' });

            $('input[type=text]').each(function () { $(this).prop("disabled", true); });
            $('input[tipo=numeros]').each(function () { $(this).prop("disabled", true); });
            $('select').each(function () { $(this).prop("disabled", true); });
            $('#cmb-stt').prop("disabled", false);
            $('.btn-agregar-partida').prop("disabled", true);

            break;
        default:
            $('#btn-new').css({ display: 'Block' });
            $('#btn-cancel').css({ display: 'none' });
            $('#btn-save').css({ display: 'none' });
            $("#wrapper").css({ display: 'none' });
            $("#Reg-Datos").css({ display: 'Block' });

            $('input[type=text]').each(function () { $(this).prop("disabled", false); });
            $('input[tipo=numeros]').each(function () { $(this).prop("disabled", false); });
            $('select').each(function () { $(this).prop("disabled", false); });
            $('.btn-agregar-partida').prop("disabled", false);

            cargar_tabla();
            break;
    }
    $('#txt-ieps').prop("disabled", true);
    $('#txt-iva').prop("disabled", true);
    $('#txt-total').prop("disabled", true);
}
/// <summary>
/// FUNCION PARA CARGAR LA INFORMACION DEL REGISTRO
/// </summary>
function Cargar_Informacion(Eve_ID) {
    try {
        
        $.ajax({
            url: 'Entradas/GetEvent',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                row = JSON.parse(Resultado.items.Data);
                $EventoID = Eve_ID;
                $('#txt-no-entrada').val(row[0].no_entrada);
                //$('#txt-observaciones').val(row[0].observaciones);
                $('#cmb-stt').val(row[0].estatus);
                $('#cmb-tipo-entrada').val(row[0].tipo_entrada);
                $('#cmb-tipo-factura').val(row[0].tipo_factura); 
                $('#txt-no-factura').val(row[0].total_factura);
                $("#cmb-almacen").select2({
                    theme: "classic",
                    data: [{ id: parseInt(row[0].almacen_id), text: row[0].Almacen }]
                });
                cargar_almacen();
                $("#cmb-proveedor").select2({
                    theme: "classic",
                    data: [{ id: parseInt(row[0].proveedor_id), text: row[0].proveedor }]
                });
                cargar_proveedor();
                var dt_entrada = new Date(row[0].fecha_entrada);
                $("#txt-fecha-entrada").datepicker("setDate", dt_entrada);
                var dt_factura = new Date(row[0].fecha_factura);
                $("#txt-fecha-factura").datepicker("setDate", dt_factura);
                $("#txt-total-factura").val(row[0].total_factura);
                var dt_recepcion = new Date(row[0].fecha_recepcion);
                $("#txt-fecha-recepcion").datepicker("setDate", dt_recepcion);
                var dt_pago = new Date(row[0].fecha_pago);
                $("#txt-fecha-pago").datepicker("setDate", dt_pago);
                $('#cmb-stt option[value=INACTIVO]').attr('selected', 'selected');
                //DETALLES
                $.ajax({
                    url: 'Entradas/GetDetalles',
                    data: "{'No_Entrada':'" + row[0].no_entrada + "'}",
                    method: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    cache: false,
                    success: function (Resultado) {

                        for (x = 0; x < Resultado.aaData.length; x++) {
                            var Obj_Partidas = new Object();
                            Obj_Partidas.Cantidad = Resultado.aaData[x][0];
                            Obj_Partidas.Unidad = Resultado.aaData[x][1];
                            Obj_Partidas.Insumo = Resultado.aaData[x][2];
                            Obj_Partidas.Precio = Resultado.aaData[x][3];
                            Obj_Partidas.Importe = Resultado.aaData[x][4];
                            Obj_Partidas.Total = Resultado.aaData[x][5];
                            Obj_Partidas.Iva = Resultado.aaData[x][5];
                            Obj_Partidas.Ieps = Resultado.aaData[x][6];
                            Obj_Partidas.Insumo_Id = Resultado.aaData[x][8];
                            $Lista_Partidas.push(Obj_Partidas);
                        }
                        recargarTablaPartidas();
                        /*oTable = $('#Tbl_Partidas').DataTable({
                            destroy: true,
                            ordering: false,
                            data: Resultado.aaData,
                            lengthMenu: [10, 25, 10, 30],
                            columns: [
                                { title: "Cantidad", className: "text-left" },
                                { title: "Unidad", className: "text-center" },
                                { title: "Insumo", className: "text-center" },
                                { title: "Precio", className: "text-center" },
                                { title: "Insumo_ID", visible: false }
                            ],
                            columnDefs: [
                                { "width": "5%", "targets": 0 },
                                { "width": "10%", "targets": 1 },
                                { "width": "50%", "targets": 2 },
                                { "width": "10%", "targets": 3 },
                                { "width": "10%", "targets": 5 },
                                {
                                    render: function (data, type, row) {
                                        return '<button type="button" class="btn-primary" title="Modificar" onclick="Quitar_Partida(' + "'" + row[0] + "','" + row[1] + "','" + row[2] + "','" + row[3] + "'" + ')"><i class="far fa-trash-alt"></i></button>';
                                    },
                                    targets: 5  
                                }
                            ]
                        });*/

                    }
                });
                //FIN AJAX DETALLES
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }

    habilitar_controles("Edit");
    //$('#cmb-stt option[value=INACTIVO]').attr('selected', 'selected');
}
function Imprimir_Formato(Eve_ID) {
    try {
        $.ajax({
            url: 'Entradas/Imprimir_Formato',
            data: "{'Evento_ID':'" + Eve_ID + "'}",
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            cache: false,
            success: function (Resultado) {
                //window.open("https://69.64.46.231/Administracion_Charca" + Resultado.Url_PDF, 'XML', 'toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');
                window.open(Resultado.Url_PDF, 'XML', 'toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');
            }
        });
    } catch (e) {
        mostrar_mensaje('Informe Técnico', e);
    }
}
/// <summary>
/// CREAR MODAL MENSAJE
/// </summary>
function mostrar_mensaje(titulo, mensaje) {
    $('#content').html(mensaje);
    $('#content').notifyModal({
        placement: 'rightBottomSlide',
        type: "notify",
        overlay: true,
        icon: true,
    });
}
/// <summary>
/// FUNCION PARA LIMPIAR LOS CONTROLES
/// </summary>
function limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[tipo=numeros]').each(function () { $(this).val('0'); });
    $('#txt-cantidad').val('1');
    //$('#txt-observaciones').val('');
    $('input[type=password]').each(function () { $(this).val(''); });
    $('input[type=hidden]').each(function () { $(this).val(''); });
    $('select').each(function () { $(this).val('').trigger("change"); });
    $InsumoID = '';
    $AlmacenID = '';
    $ProveedorID = '';
    $EventoID = '';
    $Estatus = '';
    $Usuario = '';
    $Login = '';
    $Lista_Partidas = [];
}

function asignar_fecha() {
    var f = new Date();
    $("#txt-fecha-entrada").datepicker("setDate", f);
    $("#txt-fecha-factura").datepicker("setDate", f);
    $("#txt-fecha-recepcion").datepicker("setDate", f);
}
function fijar_fecha_pago(f) {
    $("#txt-fecha-pago").datepicker("setDate", f);
}