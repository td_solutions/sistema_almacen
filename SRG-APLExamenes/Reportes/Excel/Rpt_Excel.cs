﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using SRG_APLExamenes.Models.Deal;

namespace SRG_APLExamenes.Reportes.Excel
{
    public class Rpt_Excel
    {        
        FileInfo template;
        String ruta_nueva_archivo;
        int renglon = 11;
        int aux_renglon;
        int renglon_empleados;
        int renglon_tablas;
        DataTable Registros;

        public Rpt_Excel(String ruta_plantilla, String ruta_nueva_archivo, DataTable Registros)
        {
            template = new FileInfo(ruta_plantilla);
            this.ruta_nueva_archivo = ruta_nueva_archivo;
            this.Registros = Registros;            
        }

        public void Elaborar_Reporte_Destajo(Mdl_Destajo Fil_Destajo)
        {
            DataTable Empleados = new DataTable();
            DataTable Empleados_Detalles = new DataTable();
            Double Pago = 0;
            Double Total_Pago = 0;
            int Cantidad = 0;
            int Cantidad_Total = 0;

            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                if (Registros != null && Registros.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Fil_Destajo.No_Tarjeta))
                    {
                        ws.Cells[8, 2].Value = "No Empleado: ";
                        ws.Cells[8, 3].Value = Fil_Destajo.No_Tarjeta;
                    }
                    if (!String.IsNullOrEmpty(Fil_Destajo.Fecha_Inicio))
                    {
                        ws.Cells[8, 6].Value = "Fecha Inicio: ";
                        ws.Cells[8, 7].Value = Fil_Destajo.Fecha_Inicio;
                    }
                    if (!String.IsNullOrEmpty(Fil_Destajo.Fecha_Fin))
                    {
                        ws.Cells[8, 8].Value = "Fecha Fin: ";
                        ws.Cells[8, 9].Value = Fil_Destajo.Fecha_Fin;
                    }

                    //Empleados = Registros.DefaultView.ToTable(true, "No_Tarjeta");
                    foreach (DataRow Dr in Registros.Rows)
                    {
                        ws.Cells[renglon, 2].Value = DateTime.Parse(Dr["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                        ws.Cells[renglon, 3].Value = Dr["No_Tarjeta"].ToString();
                        ws.Cells[renglon, 4].Value = Dr["Empleado"];
                        //ws.Cells[renglon, 5].Value = Dr["Tabla"];
                        ws.Cells[renglon, 5].Value = Dr["Tipo_Destajo"];                        
                        ws.Cells[renglon, 6].Value = Dr["Cantidad"];
                        ws.Cells[renglon, 7].Value = Dr["Pago"];
                        ws.Cells[renglon, 8].Value = Dr["Total_Pago"];
                        Cantidad += int.Parse(Dr["Cantidad"].ToString());
                        Pago += Double.Parse(Dr["Total_Pago"].ToString());
                        renglon++;

                        Cantidad_Total += Cantidad;
                        Total_Pago += Pago;
                        Cantidad = 0;
                        Pago = 0;
                    }
                    ws.Cells[renglon, 6].Value = "CANTIDAD TOTAL";
                    ws.Cells[renglon, 7].Value = Cantidad_Total.ToString();
                    ws.Cells[renglon, 9].Value = Total_Pago.ToString();

                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Corte(Mdl_Destajo Fil_Destajo)
        {
            DataTable Empleados = new DataTable();
            DataTable Empleados_Detalles = new DataTable();
            Double Pago = 0;
            Double Total_Pago = 0;
            int Cantidad = 0;
            int Cantidad_Total = 0;

            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                if (Registros != null && Registros.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Fil_Destajo.No_Tarjeta))
                    {
                        ws.Cells[8, 2].Value = "No Empleado: ";
                        ws.Cells[8, 3].Value = Fil_Destajo.No_Tarjeta;
                    }
                    if (!String.IsNullOrEmpty(Fil_Destajo.Fecha_Inicio))
                    {
                        ws.Cells[8, 4].Value = "Fecha Inicio: ";
                        ws.Cells[8, 5].Value = Fil_Destajo.Fecha_Inicio;
                    }
                    if (!String.IsNullOrEmpty(Fil_Destajo.Fecha_Fin))
                    {
                        ws.Cells[8, 6].Value = "Fecha Fin: ";
                        ws.Cells[8, 7].Value = Fil_Destajo.Fecha_Fin;
                    }

                    //Empleados = Registros.DefaultView.ToTable(true, "No_Tarjeta");
                    foreach (DataRow Dr in Registros.Rows)
                    {
                        ws.Cells[renglon, 2].Value = DateTime.Parse(Dr["Fecha"].ToString()).ToString("dd-MMM-yyyy");                        
                        ws.Cells[renglon, 3].Value = Dr["Tabla"];
                        ws.Cells[renglon, 4].Value = Dr["Tipo_Destajo"];
                        ws.Cells[renglon, 5].Value = Dr["Cantidad_Empleados"];
                        ws.Cells[renglon, 6].Value = Dr["Cantidad"];                        
                        Cantidad += int.Parse(Dr["Cantidad"].ToString());                        
                        renglon++;

                        Cantidad_Total += Cantidad;                        
                    }
                    ws.Cells[renglon, 4].Value = "CANTIDAD TOTAL";
                    ws.Cells[renglon, 6].Value = Cantidad_Total.ToString();

                    ws.Cells[aux_renglon, 2, renglon - 1, 6].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Entradas(Mdl_Entradas RptEnt)
        {
            renglon = 11;
            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;
                //titulo
                //ws.Cells[9, 3].Value = "REPORTE INVENTARIO " + RptInv.Almacen.ToUpper();
                ws.Cells[8, 3].Value = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                if (Registros != null && Registros.Rows.Count > 0)
                {
                    
                    foreach (DataRow Dr in Registros.Rows)
                    {
                        ws.Cells[renglon, 2].Value = Dr["no_entrada"].ToString();
                        ws.Cells[renglon, 3].Value = Dr["fecha_entrada"].ToString();
                        ws.Cells[renglon, 4].Value = Dr["Proveedor"].ToString();
                        ws.Cells[renglon, 5].Value = Dr["Almacen"].ToString();
                        ws.Cells[renglon, 6].Value = Dr["tipo_entrada"].ToString();
                        ws.Cells[renglon, 7].Value = Dr["subtotal"].ToString();
                        ws.Cells[renglon, 8].Value = Dr["IVA"].ToString();
                        ws.Cells[renglon, 9].Value = Dr["IEPS"].ToString();
                        ws.Cells[renglon, 10].Value = Dr["Total"].ToString();
                        ws.Cells[renglon, 11].Value = Dr["Cantidad"].ToString();
                        ws.Cells[renglon, 12].Value = Dr["Codigo_Barras"].ToString();
                        ws.Cells[renglon, 13].Value = Dr["Nombre"].ToString();
                        ws.Cells[renglon, 14].Value = Dr["Unidades"].ToString();
                        ws.Cells[renglon, 15].Value = Dr["Costo"].ToString();
                        ws.Cells[renglon, 16].Value = Dr["Importe"].ToString();
                        ws.Cells[renglon, 17].Value = Dr["IVA_Prod"].ToString();
                        ws.Cells[renglon, 18].Value = Dr["IEPS_Prod"].ToString();
                        ws.Cells[renglon, 19].Value = Dr["Gran_Total"].ToString();

                        renglon++;
                    }
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Salidas(Mdl_Salidas RptSal)
        {
            renglon = 11;
            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;
                //titulo
                //ws.Cells[9, 3].Value = "REPORTE INVENTARIO " + RptInv.Almacen.ToUpper();
                ws.Cells[8, 3].Value = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                if (Registros != null && Registros.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Registros.Rows)
                    {
                        ws.Cells[renglon, 2].Value = Dr["NO_Salida"].ToString();
                        ws.Cells[renglon, 3].Value = Dr["Fecha"];
                        ws.Cells[renglon, 4].Value = Dr["Estatus"];
                        ws.Cells[renglon, 5].Value = Dr["Almacen"];
                        ws.Cells[renglon, 6].Value = Dr["Tipo"];
                        //ws.Cells[renglon, 7].Value = Dr["IVA"];
                        //ws.Cells[renglon, 8].Value = Dr["IEPS"];
                        ws.Cells[renglon, 7].Value = Dr["Costo_Total"];
                        ws.Cells[renglon, 8].Value = Dr["Cantidad"];
                        ws.Cells[renglon, 9].Value = Dr["Codigo_barras"];
                        ws.Cells[renglon, 10].Value = Dr["Insumo"];
                        ws.Cells[renglon, 11].Value = Dr["Unidades"];
                        ws.Cells[renglon, 12].Value = Dr["Precio"];
                        ws.Cells[renglon, 13].Value = Dr["Importe"];
                        ws.Cells[renglon, 14].Value = Dr["IVA_Prod"];
                        ws.Cells[renglon, 15].Value = Dr["IEPS_Prod"];
                        ws.Cells[renglon, 16].Value = Dr["Gran_Total"];

                        renglon++;
                    }
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Kardex(Mld_Rpt_Kardex RptKdex)
        {
            renglon = 13;
            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;
                string nombreInsumo = "";
                string movimiento = "";

                int totalExistencias = 0;
                double totalPrecioVenta = 0;
                double totalcostoVenta = 0;
                double totalGral = 0;
                //Fecha Generacion
                ws.Cells[8, 3].Value = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                //Periodo consulta
                ws.Cells[10, 3].Value = "Del " + RptKdex.Fecha_Inicio.ToString("dd/MMM/yyyy") + " AL " + RptKdex.Fecha_Fin.ToString("dd/MMM/yyyy");
                // Insumo
                if (Registros != null && Registros.Rows.Count > 0)
                    nombreInsumo = Registros.Rows[1]["Nombre"].ToString();
                ws.Cells[11, 3].Value = nombreInsumo;
                if (Registros != null && Registros.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Registros.Rows)
                    {
                        if( String.IsNullOrEmpty(movimiento) )
                        {
                            var celda = ws.Cells[renglon, 3, renglon, 7];
                            celda.Merge = true;
                            celda.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            celda.Style.Font.Bold = true;
                            movimiento = Dr["Movimiento"].ToString();
                            celda.Value = movimiento.ToUpper();
                            renglon++;
                        }
                        else
                        {
                            if ( movimiento != Dr["Movimiento"].ToString() )
                            {
                                var celda = ws.Cells[renglon, 3, renglon, 7];
                                celda.Merge = true;
                                celda.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                celda.Style.Font.Bold = true;
                                movimiento = Dr["Movimiento"].ToString();
                                celda.Value = movimiento.ToUpper();
                                renglon++;
                            }
                        }

                        ws.Cells[renglon, 2].Value = Convert.ToDateTime( Dr["Fecha"] ).ToString("dd/MMM/yyyy");
                        
                        ws.Cells[renglon, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[renglon, 3].Value = Dr["NumMovimiento"].ToString();
                        ws.Cells[renglon, 3].Style.Numberformat.Format = "#,###";

                        ws.Cells[renglon, 4].Value = Dr["TipoMovimiento"].ToString();

                        ws.Cells[renglon, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[renglon, 5].Value = Dr["Unidades"].ToString();
                        ws.Cells[renglon, 5].Style.Numberformat.Format = "#,###.00";

                        ws.Cells[renglon, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[renglon, 6].Value = Dr["PrecioVenta"].ToString();
                        ws.Cells[renglon, 6].Style.Numberformat.Format = "#,###.00";

                        ws.Cells[renglon, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[renglon, 7].Value = Dr["CostoCompra"].ToString();
                        ws.Cells[renglon, 7].Style.Numberformat.Format = "#,###.00";

                        ws.Cells[renglon, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        ws.Cells[renglon, 8].Value = Dr["Total"].ToString();
                        ws.Cells[renglon, 8].Style.Numberformat.Format = "#,###.00";

                        totalExistencias += Convert.ToInt32( Dr["auxUnidad"] );
                        totalPrecioVenta += Convert.ToDouble(Dr["PrecioVenta"]);
                        totalcostoVenta += Convert.ToDouble(Dr["CostoCompra"]);
                        totalGral += Convert.ToDouble(Dr["Total"]);

                        renglon++;
                    }

                    ws.Cells[renglon, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[renglon, 4].Value = "Existencias ";
                    ws.Cells[renglon, 4].Style.Font.Bold = true;

                    ws.Cells[renglon, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    ws.Cells[renglon, 5].Value = totalExistencias.ToString();
                    ws.Cells[renglon, 5].Style.Numberformat.Format = "#,###.00";

                    /*ws.Cells[renglon, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    ws.Cells[renglon, 6].Value = totalPrecioVenta.ToString();
                    ws.Cells[renglon, 6].Style.Font.UnderLineType = ExcelUnderLineType.Double;*/

                    ws.Cells[renglon, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[renglon, 7].Value = "Total ";
                    ws.Cells[renglon, 7].Style.Font.Bold = true;

                    ws.Cells[renglon, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    ws.Cells[renglon, 8].Value = totalGral.ToString();
                    ws.Cells[renglon, 8].Style.Numberformat.Format = "#,###.00";
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }
        public void Elaborar_Reporte_Inventario(Mdl_Rpt_Inventario RptInv)
        {
            DataTable Empleados = new DataTable();
            DataTable Empleados_Detalles = new DataTable();
            Double Pago = 0;
            Double Total_Pago = 0;
            int Cantidad = 0;
            int Cantidad_Total = 0;
            string tipo_insumo = "";
            renglon = 12;
            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;
                //titulo
                ws.Cells[9, 3].Value = "REPORTE INVENTARIO " + RptInv.Almacen.ToUpper();
                ws.Cells[8, 3].Value = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                if (Registros != null && Registros.Rows.Count > 0)
                {
                    //Empleados = Registros.DefaultView.ToTable(true, "No_Tarjeta");
                    //foreach (DataRow Fila in Empleados.Rows)
                    //{
                    //    Registros.DefaultView.RowFilter = "No_Tarjeta = '" + Fila["No_Tarjeta"].ToString().Trim() + "'";
                    //    Empleados_Detalles = Registros.DefaultView.ToTable();

                    //}
                    foreach (DataRow Dr in Registros.Rows)
                    {
                        if (String.IsNullOrEmpty(tipo_insumo))
                        {
                            var celda = ws.Cells[
                                renglon, 2,
                                renglon, 8];
                            celda.Merge = true;
                            celda.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            tipo_insumo = Dr["Tipo_Insumo"].ToString();
                            celda.Value = tipo_insumo.ToUpper();
                            renglon++;
                        }
                        else
                        {
                            if ( tipo_insumo != Dr["Tipo_Insumo"].ToString() )
                            {
                                var celda = ws.Cells[
                                    renglon, 2,
                                    renglon, 8];
                                celda.Merge = true;
                                celda.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                tipo_insumo = Dr["Tipo_Insumo"].ToString();
                                celda.Value = tipo_insumo.ToUpper();
                                renglon++;
                            }
                        }
                        //ws.Cells[renglon, 2].Value = DateTime.Parse(Dr["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                        ws.Cells[renglon, 2].Value = Dr["Codigo_Barras"].ToString();
                        ws.Cells[renglon, 3].Value = Dr["Nombre"];
                        ws.Cells[renglon, 4].Value = Dr["Descripcion"];
                        ws.Cells[renglon, 5].Value = Dr["Existencias"];
                        ws.Cells[renglon, 6].Value = Dr["Unidad"];
                        ws.Cells[renglon, 7].Value = Dr["Costo"];
                        ws.Cells[renglon, 8].Value = Dr["Total_Costo"];
                        renglon++;
                    }
                    //    //ws.Cells[renglon, 6].Value = "TOTAL";                        
                    //    //ws.Cells[renglon, 7].Value = Cantidad.ToString();
                    //    //ws.Cells[renglon, 9].Value = Pago.ToString();
                    //    //using (var range = ws.Cells[renglon, 6, renglon, 9])
                    //    //{
                    //    //    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //    //    range.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                    //    //}

                    //    Cantidad_Total += Cantidad;
                    //    Total_Pago += Pago;
                    //    Cantidad = 0;
                    //    Pago = 0;
                    //    //renglon++;

                    //    //ws.Cells[renglon, 6].Value = "";
                    //    //renglon++;
                    //}
                    //ws.Cells[renglon, 6].Value = "CANTIDAD TOTAL";
                    //ws.Cells[renglon, 7].Value = Cantidad_Total.ToString();
                    //ws.Cells[renglon, 9].Value = Total_Pago.ToString();

                    //ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    //ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    //ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }
        public void Elaborar_Reporte_Inventario_Reorden(Mdl_Rpt_Inventario RptInv)
        {
            DataTable Empleados = new DataTable();
            DataTable Empleados_Detalles = new DataTable();
            Double Pago = 0;
            Double Total_Pago = 0;
            int Cantidad = 0;
            int Cantidad_Total = 0;
            string tipo_insumo = "";
            renglon = 12;
            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;
                //titulo
                ws.Cells[9, 3].Value = "REPORTE INVENTARIO " + RptInv.Almacen.ToUpper();
                ws.Cells[8, 3].Value = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
                if (Registros != null && Registros.Rows.Count > 0)
                {
                    //Empleados = Registros.DefaultView.ToTable(true, "No_Tarjeta");
                    //foreach (DataRow Fila in Empleados.Rows)
                    //{
                    //    Registros.DefaultView.RowFilter = "No_Tarjeta = '" + Fila["No_Tarjeta"].ToString().Trim() + "'";
                    //    Empleados_Detalles = Registros.DefaultView.ToTable();

                    //}
                    foreach (DataRow Dr in Registros.Rows)
                    {                        
                        //ws.Cells[renglon, 2].Value = DateTime.Parse(Dr["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                        ws.Cells[renglon, 2].Value = Dr["Codigo_Barras"].ToString();
                        ws.Cells[renglon, 3].Value = Dr["Nombre"];
                        ws.Cells[renglon, 4].Value = Dr["Descripcion"];
                        ws.Cells[renglon, 5].Value = Dr["Unidad"];
                        ws.Cells[renglon, 6].Value = Dr["Ubicacion"];
                        ws.Cells[renglon, 7].Value = Dr["Existencias"];
                        ws.Cells[renglon, 8].Value = Dr["Sugerido"];
                        ws.Cells[renglon, 9].Value = Dr["Costo"];
                        ws.Cells[renglon, 10].Value = Dr["Total_Costo"];
                        renglon++;
                    }
                    //    //ws.Cells[renglon, 6].Value = "TOTAL";                        
                    //    //ws.Cells[renglon, 7].Value = Cantidad.ToString();
                    //    //ws.Cells[renglon, 9].Value = Pago.ToString();
                    //    //using (var range = ws.Cells[renglon, 6, renglon, 9])
                    //    //{
                    //    //    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //    //    range.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Gray);
                    //    //}

                    //    Cantidad_Total += Cantidad;
                    //    Total_Pago += Pago;
                    //    Cantidad = 0;
                    //    Pago = 0;
                    //    //renglon++;

                    //    //ws.Cells[renglon, 6].Value = "";
                    //    //renglon++;
                    //}
                    //ws.Cells[renglon, 6].Value = "CANTIDAD TOTAL";
                    //ws.Cells[renglon, 7].Value = Cantidad_Total.ToString();
                    //ws.Cells[renglon, 9].Value = Total_Pago.ToString();

                    //ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    //ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    //ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    //ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Fumigacion(Mdl_Fumigacion Fil_Fumigacion)
        {
            Mdl_Fumigacion_Detalles Obj_Empelados = new Mdl_Fumigacion_Detalles();
            Mdl_Fumigacion_Tablas Obj_Tablas = new Mdl_Fumigacion_Tablas();
            Mdl_Salidas Obj_Salidas = new Mdl_Salidas();            
            DataTable Empelados = new DataTable();
            DataTable Tablas = new DataTable();
            DataTable Salidas = new DataTable();
            DataTable Salidas_Detalles = new DataTable();
            int Total_Empleados = 0;
            double Total_Litros = 0;
            double Total_Importe = 0;

            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                if (Registros != null && Registros.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Fil_Fumigacion.Fecha_Inicio))
                    {
                        ws.Cells[8, 2].Value = "Fecha Inicio: ";
                        ws.Cells[8, 3].Value = Fil_Fumigacion.Fecha_Inicio;
                    }
                    if (!String.IsNullOrEmpty(Fil_Fumigacion.Fecha_Fin))
                    {
                        ws.Cells[8, 4].Value = "Fecha Fin: ";
                        ws.Cells[8, 5].Value = Fil_Fumigacion.Fecha_Fin;
                    }

                    foreach (DataRow Fila in Registros.Rows)
                    {
                        renglon_empleados = renglon;
                        renglon_tablas = renglon;

                        ws.Cells[renglon, 2].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                        ws.Cells[renglon, 3].Value = Fila["Avance"].ToString();
                        ws.Cells[renglon, 4].Value = Fila["Pago_Por"].ToString();
                        ws.Cells[renglon, 5].Value = Fila["Total_Empleados"];
                        ws.Cells[renglon, 6].Value = Fila["Litros"];

                        Obj_Empelados.No_Fumigacion = Fila["No_Fumigacion"].ToString().Trim();
                        Empelados = Obj_Empelados.Consult();
                        if (Empelados != null && Empelados.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Empelados.Rows)
                            {
                                ws.Cells[renglon_empleados, 2].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                                ws.Cells[renglon_empleados, 3].Value = Fila["Avance"].ToString();
                                ws.Cells[renglon_empleados, 4].Value = Fila["Pago_Por"].ToString();
                                ws.Cells[renglon_empleados, 5].Value = Fila["Total_Empleados"];
                                ws.Cells[renglon_empleados, 6].Value = Fila["Litros"];
                                ws.Cells[renglon_empleados, 7].Value = Dr["Cantidad"].ToString();
                                ws.Cells[renglon_empleados, 8].Value = Double.Parse(Dr["Pago"].ToString()) / Double.Parse(Dr["Cantidad"].ToString());
                                ws.Cells[renglon_empleados, 9].Value = Dr["Pago"].ToString();
                                ws.Cells[renglon_empleados, 10].Value = Dr["No_Tarjeta"].ToString();
                                ws.Cells[renglon_empleados, 11].Value = Dr["Nombre"];
                                Total_Importe += Double.Parse(Dr["Pago"].ToString());
                                renglon_empleados++;
                            }
                        }
                        
                        Obj_Tablas.No_fumigacion = Fila["No_Fumigacion"].ToString().Trim();
                        Tablas = Obj_Tablas.Consult();
                        if (Tablas != null && Tablas.Rows.Count > 0)
                        {                            
                            foreach (DataRow Dr in Tablas.Rows)
                            {
                                ws.Cells[renglon_tablas, 12].Value = Dr["Tabla"].ToString();
                                ws.Cells[renglon_tablas, 13].Value = Dr["No_Tuneles"].ToString();
                                renglon_tablas++;
                            }
                        }
                        Total_Empleados += int.Parse(Fila["Total_Empleados"].ToString());
                        Total_Litros += double.Parse(Fila["Litros"].ToString());                        
                        if (renglon_tablas >= renglon_empleados)
                            renglon = renglon_tablas;
                        else
                            renglon = renglon_empleados;                        
                    }
                    ws.Cells[renglon, 3].Value = "TOTALES";
                    ws.Cells[renglon, 5].Value = Total_Empleados.ToString();
                    ws.Cells[renglon, 6].Value = Total_Litros.ToString();
                    ws.Cells[renglon, 9].Value = Total_Importe.ToString();
                    renglon++;

                    ws.Cells[aux_renglon, 2, renglon - 1, 13].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 13].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 13].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 13].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    ExcelWorksheet ws_2 = p.Workbook.Worksheets[2];
                    renglon = 3;
                    aux_renglon = renglon;
                    foreach (DataRow Fila in Registros.Rows)
                    {
                        //if (!String.IsNullOrEmpty(Fila["No_Salida"].ToString()))
                        //{
                            Obj_Salidas.No_Fumigacion = Fila["No_Fumigacion"].ToString();
                            Salidas = Obj_Salidas.Consult();
                            if (Salidas != null && Salidas.Rows.Count > 0)
                            {
                                foreach (DataRow Fila_Sal in Salidas.Rows)
                                {                                    
                                    Obj_Salidas.No_Salida = int.Parse(Fila_Sal["No_Salida"].ToString());
                                    Salidas_Detalles = Obj_Salidas.ConsultDet();
                                    if (Salidas_Detalles != null && Salidas_Detalles.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < Salidas_Detalles.Rows.Count; i++)
                                        {
                                            ws_2.Cells[renglon, 1].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                                            ws_2.Cells[renglon, 2].Value = Fila_Sal["No_Salida"].ToString();
                                            ws_2.Cells[renglon, 3].Value = Fila_Sal["Almacen_Origen"].ToString();
                                            ws_2.Cells[renglon, 4].Value = Salidas_Detalles.Rows[i]["Cantidad"].ToString();
                                            ws_2.Cells[renglon, 5].Value = Salidas_Detalles.Rows[i]["Insumo"].ToString();
                                            ws_2.Cells[renglon, 6].Value = Salidas_Detalles.Rows[i]["Unidad"].ToString();
                                            ws_2.Cells[renglon, 7].Value = Salidas_Detalles.Rows[i]["Precio"].ToString();
                                            renglon++;
                                        }
                                        renglon++;
                                    }
                                }
                            }

                            
                        //}
                        //else
                        //    Obj_Salidas.No_Salida = 0;
                    }
                    ws_2.Cells[aux_renglon, 1, renglon - 1, 7].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws_2.Cells[aux_renglon, 1, renglon - 1, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws_2.Cells[aux_renglon, 1, renglon - 1, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws_2.Cells[aux_renglon, 1, renglon - 1, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Riego(Mdl_Riego Fil_Riego)
        {
            Mdl_Riego_Detalles Obj_Empelados = new Mdl_Riego_Detalles();
            Mdl_Riego_Tablas Obj_Tablas = new Mdl_Riego_Tablas();
            Mdl_Salidas Obj_Salidas = new Mdl_Salidas();                        
            DataTable Empelados = new DataTable();
            DataTable Tablas = new DataTable();
            DataTable Salidas = new DataTable();
            DataTable Salidas_Detalles = new DataTable();
            int Total_Empleados = 0;
            double Total_Horas = 0;
            double Total_Importe = 0;

            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                if (Registros != null && Registros.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Fil_Riego.Fecha_Inicio))
                    {
                        ws.Cells[8, 2].Value = "Fecha Inicio: ";
                        ws.Cells[8, 3].Value = Fil_Riego.Fecha_Inicio;
                    }
                    if (!String.IsNullOrEmpty(Fil_Riego.Fecha_Fin))
                    {
                        ws.Cells[8, 4].Value = "Fecha Fin: ";
                        ws.Cells[8, 5].Value = Fil_Riego.Fecha_Fin;
                    }

                    foreach (DataRow Fila in Registros.Rows)
                    {
                        renglon_empleados = renglon;
                        renglon_tablas = renglon;

                        ws.Cells[renglon, 2].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");                        
                        ws.Cells[renglon, 3].Value = Fila["Horas"];
                        ws.Cells[renglon, 4].Value = Fila["Acido"];
                        ws.Cells[renglon, 5].Value = Fila["Total_Empleados"];
                        ws.Cells[renglon, 6].Value = (Double.Parse(Fila["Horas"].ToString())/ Double.Parse(Fila["Total_Empleados"].ToString())).ToString();
                        
                        Obj_Empelados.No_Riego = Fila["No_Riego"].ToString().Trim();
                        Empelados = Obj_Empelados.Consult();
                        if (Empelados != null && Empelados.Rows.Count > 0)
                        {                            
                            foreach (DataRow Dr in Empelados.Rows)
                            {
                                ws.Cells[renglon_empleados, 2].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                                ws.Cells[renglon_empleados, 3].Value = Fila["Horas"];
                                ws.Cells[renglon_empleados, 4].Value = Fila["Acido"];
                                ws.Cells[renglon_empleados, 5].Value = Fila["Total_Empleados"];
                                ws.Cells[renglon_empleados, 6].Value = (Double.Parse(Fila["Horas"].ToString()) / Double.Parse(Fila["Total_Empleados"].ToString())).ToString();
                                ws.Cells[renglon_empleados, 7].Value = Dr["Pago"].ToString();
                                ws.Cells[renglon_empleados, 8].Value = Dr["No_Tarjeta"].ToString();
                                ws.Cells[renglon_empleados, 9].Value = Dr["Nombre"];
                                Total_Importe += Double.Parse(Dr["Pago"].ToString());
                                renglon_empleados++;
                            }
                        }

                        Obj_Tablas.No_Riego = Fila["No_Riego"].ToString().Trim();
                        Tablas = Obj_Tablas.Consult();
                        if (Tablas != null && Tablas.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Tablas.Rows)
                            {
                                ws.Cells[renglon_tablas, 10].Value = Dr["Tabla"].ToString();
                                renglon_tablas++;
                            }
                        }
                        Total_Empleados += int.Parse(Fila["Total_Empleados"].ToString());
                        Total_Horas += double.Parse(Fila["Horas"].ToString());                        
                        if (renglon_tablas >= renglon_empleados)
                            renglon = renglon_tablas;
                        else
                            renglon = renglon_empleados;                        
                    }
                    ws.Cells[renglon, 2].Value = "TOTALES";                    
                    ws.Cells[renglon, 5].Value = Total_Empleados.ToString();
                    ws.Cells[renglon, 6].Value = Total_Horas.ToString();
                    ws.Cells[renglon, 7].Value = Total_Importe.ToString();
                    renglon++;

                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    ExcelWorksheet ws_2 = p.Workbook.Worksheets[2];
                    renglon = 3;
                    aux_renglon = renglon;
                    foreach (DataRow Fila in Registros.Rows)
                    {
                        //if (!String.IsNullOrEmpty(Fila["No_Salida"].ToString()))
                        //{
                            Obj_Salidas.No_Riego = Fila["No_Riego"].ToString();
                            Salidas = Obj_Salidas.Consult();
                            Salidas = Obj_Salidas.Consult();
                            if (Salidas != null && Salidas.Rows.Count > 0)
                            {
                                foreach (DataRow Fila_Sal in Salidas.Rows)
                                {
                                    Obj_Salidas.No_Salida = int.Parse(Fila_Sal["No_Salida"].ToString());
                                    Salidas_Detalles = Obj_Salidas.ConsultDet();
                                    if (Salidas_Detalles != null && Salidas_Detalles.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < Salidas_Detalles.Rows.Count; i++)
                                        {
                                            ws_2.Cells[renglon, 1].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                                            ws_2.Cells[renglon, 2].Value = Fila_Sal["No_Salida"].ToString();
                                            ws_2.Cells[renglon, 3].Value = Fila_Sal["Almacen_Origen"].ToString();
                                            ws_2.Cells[renglon, 4].Value = Salidas_Detalles.Rows[i]["Cantidad"].ToString();
                                            ws_2.Cells[renglon, 5].Value = Salidas_Detalles.Rows[i]["Insumo"].ToString();
                                            ws_2.Cells[renglon, 6].Value = Salidas_Detalles.Rows[i]["Unidad"].ToString();
                                            ws_2.Cells[renglon, 7].Value = Salidas_Detalles.Rows[i]["Precio"].ToString();
                                            renglon++;
                                        }
                                        renglon++;
                                    }
                                }
                            }
                        //}
                        //else
                        //    Obj_Salidas.No_Salida = 0;
                    }
                    ws_2.Cells[aux_renglon, 1, renglon - 1, 7].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws_2.Cells[aux_renglon, 1, renglon - 1, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws_2.Cells[aux_renglon, 1, renglon - 1, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws_2.Cells[aux_renglon, 1, renglon - 1, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Automoviles(Mdl_Automoviles Fil_Automoviles)
        {
            Mdl_Automoviles_Tablas Obj_Tablas = new Mdl_Automoviles_Tablas();
            DataTable Tablas = new DataTable();            
            double Total_Litros = 0;
            double Total_Importe = 0;

            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                if (Registros != null && Registros.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Fil_Automoviles.No_Empleado))
                    {
                        ws.Cells[8, 2].Value = "No Empleado: ";
                        ws.Cells[8, 3].Value = Fil_Automoviles.No_Empleado;
                    }
                    if (!String.IsNullOrEmpty(Fil_Automoviles.Fecha_Inicio))
                    {
                        ws.Cells[8, 4].Value = "Fecha Inicio: ";
                        ws.Cells[8, 5].Value = Fil_Automoviles.Fecha_Inicio;
                    }
                    if (!String.IsNullOrEmpty(Fil_Automoviles.Fecha_Fin))
                    {
                        ws.Cells[8, 6].Value = "Fecha Fin: ";
                        ws.Cells[8, 7].Value = Fil_Automoviles.Fecha_Fin;
                    }

                    foreach (DataRow Fila in Registros.Rows)
                    {
                        renglon_tablas = renglon;

                        ws.Cells[renglon, 2].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                        ws.Cells[renglon, 3].Value = Fila["No_Tarjeta"].ToString();
                        ws.Cells[renglon, 4].Value = Fila["Empleado"].ToString();
                        ws.Cells[renglon, 5].Value = Fila["Unidad_Auto"].ToString();
                        ws.Cells[renglon, 6].Value = Fila["Labor"];
                        ws.Cells[renglon, 7].Value = Fila["Nivel_Gasolina"];
                        ws.Cells[renglon, 8].Value = Fila["Horas_Uso"];
                        ws.Cells[renglon, 9].Value = Fila["LItros"];
                        ws.Cells[renglon, 10].Value = Fila["Precio_Litro"];
                        ws.Cells[renglon, 11].Value = (Double.Parse(Fila["LItros"].ToString()) * Double.Parse(Fila["Precio_Litro"].ToString())).ToString();

                        Obj_Tablas.No_Automovil = Fila["No_Automovil"].ToString().Trim();
                        Tablas = Obj_Tablas.Consult();
                        if (Tablas != null && Tablas.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Tablas.Rows)
                            {
                                ws.Cells[renglon_tablas, 2].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                                ws.Cells[renglon_tablas, 3].Value = Fila["No_Tarjeta"].ToString();
                                ws.Cells[renglon_tablas, 4].Value = Fila["Empleado"].ToString();
                                ws.Cells[renglon_tablas, 5].Value = Fila["Unidad_Auto"].ToString();
                                ws.Cells[renglon_tablas, 6].Value = Fila["Labor"];
                                ws.Cells[renglon_tablas, 7].Value = Fila["Nivel_Gasolina"];
                                ws.Cells[renglon_tablas, 8].Value = Fila["Horas_Uso"];
                                ws.Cells[renglon_tablas, 9].Value = Fila["Litros"];
                                ws.Cells[renglon_tablas, 10].Value = Fila["Precio_Litro"];
                                ws.Cells[renglon_tablas, 11].Value = (Double.Parse(Fila["LItros"].ToString()) * Double.Parse(Fila["Precio_Litro"].ToString())).ToString();
                                ws.Cells[renglon_tablas, 12].Value = Dr["Tabla"].ToString();
                                renglon_tablas++;
                            }
                        }
                        Total_Litros += Double.Parse(Fila["LItros"].ToString());
                        Total_Importe += Double.Parse(Fila["LItros"].ToString()) * Double.Parse(Fila["Precio_Litro"].ToString());
                        if (renglon_tablas >= renglon_empleados)
                            renglon = renglon_tablas;
                        else
                            renglon = renglon_empleados;                             
                    }
                    ws.Cells[renglon, 2].Value = "TOTALES";
                    ws.Cells[renglon, 9].Value = Total_Litros.ToString();
                    ws.Cells[renglon, 11].Value = Total_Importe.ToString();
                    renglon++;

                    ws.Cells[aux_renglon, 2, renglon - 1, 12].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 12].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 12].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 12].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Actividades(Mdl_Actividades Fil_Actividades)
        {
            Mdl_Actividades_Detalles Obj_Empelados = new Mdl_Actividades_Detalles();
            Mdl_Actividades_Tablas Obj_Tablas = new Mdl_Actividades_Tablas();
            DataTable Empelados = new DataTable();
            DataTable Tablas = new DataTable();
            double Total_Importe = 0;

            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                if (Registros != null && Registros.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Fil_Actividades.No_Empleado))
                    {
                        ws.Cells[8, 2].Value = "No Empleado: ";
                        ws.Cells[8, 3].Value = Fil_Actividades.No_Empleado;
                    }
                    if (!String.IsNullOrEmpty(Fil_Actividades.Fecha_Inicio))
                    {
                        ws.Cells[8, 4].Value = "Fecha Inicio: ";
                        ws.Cells[8, 5].Value = Fil_Actividades.Fecha_Inicio;
                    }
                    if (!String.IsNullOrEmpty(Fil_Actividades.Fecha_Fin))
                    {
                        ws.Cells[8, 6].Value = "Fecha Fin: ";
                        ws.Cells[8, 7].Value = Fil_Actividades.Fecha_Fin;
                    }

                    foreach (DataRow Fila in Registros.Rows)
                    {
                        renglon_empleados = renglon;
                        renglon_tablas = renglon;

                        ws.Cells[renglon, 2].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                        ws.Cells[renglon, 3].Value = Fila["Avance"].ToString();
                        ws.Cells[renglon, 8].Value = Fila["Labor"].ToString();

                        Obj_Empelados.No_Actividad = Fila["No_Actividad"].ToString().Trim();
                        Empelados = Obj_Empelados.Consult();
                        if (Empelados != null && Empelados.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Empelados.Rows)
                            {
                                ws.Cells[renglon_empleados, 2].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                                ws.Cells[renglon_empleados, 3].Value = Dr["No_Tarjeta"].ToString();
                                ws.Cells[renglon_empleados, 4].Value = Dr["Avance"].ToString();
                                ws.Cells[renglon_empleados, 5].Value = Dr["Nombre"];
                                ws.Cells[renglon_empleados, 8].Value = Fila["Labor"].ToString();
                                ws.Cells[renglon_empleados, 9].Value = Dr["Monto"].ToString();
                                Total_Importe += Double.Parse(Dr["Monto"].ToString());
                                renglon_empleados++;
                            }
                        }

                        Obj_Tablas.No_Actividad = Fila["No_Actividad"].ToString().Trim();
                        Tablas = Obj_Tablas.Consult();
                        if (Tablas != null && Tablas.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Tablas.Rows)
                            {
                                ws.Cells[renglon_tablas, 6].Value = Dr["Tabla"].ToString();
                                ws.Cells[renglon_tablas, 7].Value = Dr["No_Tuneles"].ToString();
                                renglon_tablas++;
                            }
                        }
                        if (renglon_tablas >= renglon_empleados)
                            renglon = renglon_tablas;
                        else
                            renglon = renglon_empleados;  
                    }
                    ws.Cells[renglon, 3].Value = "TOTALES";                    
                    ws.Cells[renglon, 9].Value = Total_Importe.ToString();
                    renglon++;

                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Estado_Cuenta_Proveedores(Mdl_Facturas Fil_Facturas)
        {
            Double Total = 0;
            Double Abono = 0;
            Double Saldo = 0;

            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                if (Registros != null && Registros.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Registros.Rows)
                    {
                        ws.Cells[renglon, 2].Value = Fila["NO_FACTURA"];
                        ws.Cells[renglon, 3].Value = Fila["PROVEEDOR"];
                        ws.Cells[renglon, 4].Value = DateTime.Parse(Fila["FECHA"].ToString()).ToString("dd-MMM-yyyy");
                        ws.Cells[renglon, 5].Value = DateTime.Parse(Fila["FECHA_VENCIMIENTO"].ToString()).ToString("dd-MMM-yyyy");
                        ws.Cells[renglon, 6].Value = Fila["TOTAL"];
                        ws.Cells[renglon, 7].Value = Fila["ABONO"];
                        ws.Cells[renglon, 8].Value = Fila["SALDO"];
                        ws.Cells[renglon, 9].Value = Fila["ESTATUS"];
                        Total += Double.Parse(Fila["TOTAL"].ToString());
                        Abono += Double.Parse(Fila["ABONO"].ToString());
                        Saldo += Double.Parse(Fila["SALDO"].ToString());
                        renglon++;
                    }
                    ws.Cells[renglon, 2].Value = "TOTALES";
                    ws.Cells[renglon, 6].Value = Total.ToString();
                    ws.Cells[renglon, 7].Value = Abono.ToString();
                    ws.Cells[renglon, 8].Value = Saldo.ToString();
                    renglon++;

                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Saldos_Vencidos_Proveedores(Mdl_Facturas Fil_Facturas)
        {           
            Double Total_30 = 0;
            Double Total_60 = 0;
            Double Total_90 = 0;
            Double Total_90_mas = 0;

            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                if (Registros != null && Registros.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Registros.Rows)
                    {
                        ws.Cells[renglon, 2].Value = Fila["NO_FACTURA"];
                        ws.Cells[renglon, 3].Value = Fila["PROVEEDOR"];
                        ws.Cells[renglon, 4].Value = DateTime.Parse(Fila["FECHA"].ToString()).ToString("dd-MMM-yyyy");
                        ws.Cells[renglon, 5].Value = DateTime.Parse(Fila["FECHA_VENCIMIENTO"].ToString()).ToString("dd-MMM-yyyy");                        

                        //Diferencia fechas para los dias
                        DateTime oldDate;
                        if (!String.IsNullOrEmpty(Fil_Facturas.Fecha_Saldos_Fin))
                            oldDate = DateTime.Parse(Fil_Facturas.Fecha_Saldos_Fin);
                        else
                            oldDate = DateTime.Now;                            
                        DateTime newDate = DateTime.Parse(Fila["FECHA_VENCIMIENTO"].ToString());

                        // Difference in days, hours, and minutes.
                        TimeSpan ts = oldDate - newDate;
                        if (ts.Days > 0 && ts.Days <= 30) 
                        {
                            ws.Cells[renglon, 6].Value = Fila["SALDO"];
                            Total_30 += Double.Parse(Fila["SALDO"].ToString());
                        }
                        if (ts.Days > 30 && ts.Days <= 60)
                        {
                            ws.Cells[renglon, 7].Value = Fila["SALDO"];
                            Total_60 += Double.Parse(Fila["SALDO"].ToString());
                        }
                        if (ts.Days > 60 && ts.Days <= 90)
                        {
                            ws.Cells[renglon, 8].Value = Fila["SALDO"];
                            Total_90 += Double.Parse(Fila["SALDO"].ToString());
                        }
                        if (ts.Days > 90)
                        {
                            ws.Cells[renglon, 9].Value = Fila["SALDO"];
                            Total_90_mas += Double.Parse(Fila["SALDO"].ToString());
                        }
                        ws.Cells[renglon, 10].Value = Fila["ESTATUS"];                                                
                        renglon++;
                    }
                    ws.Cells[renglon, 2].Value = "TOTALES";
                    ws.Cells[renglon, 6].Value = Total_30.ToString();
                    ws.Cells[renglon, 7].Value = Total_60.ToString();
                    ws.Cells[renglon, 8].Value = Total_90.ToString();
                    ws.Cells[renglon, 9].Value = Total_90_mas.ToString();
                    renglon++;

                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }

        public void Elaborar_Reporte_Notas_Salida(Mdl_Notas_Salida Fil_Notas)
        {
            Mdl_Notas_Salida_Detalles Obj_Detalles = new Mdl_Notas_Salida_Detalles();
            DataTable Detalles = new DataTable();
            Double Peso = 0;
            Double Cajas = 0;
            Double Kilogramos = 0;
            Double Total = 0;

            using (ExcelPackage p = new ExcelPackage(template, true))
            {
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                aux_renglon = renglon;

                if (Registros != null && Registros.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Fil_Notas.Fecha_Inicio))
                    {
                        ws.Cells[8, 2].Value = "Fecha Inicio: ";
                        ws.Cells[8, 3].Value = Fil_Notas.Fecha_Inicio;
                    }
                    if (!String.IsNullOrEmpty(Fil_Notas.Fecha_Fin))
                    {
                        ws.Cells[8, 4].Value = "Fecha Fin: ";
                        ws.Cells[8, 5].Value = Fil_Notas.Fecha_Fin;
                    }

                    foreach (DataRow Fila in Registros.Rows)
                    {
                        Obj_Detalles.No_Nota = Fila["No_Nota"].ToString().Trim();
                        Detalles = Obj_Detalles.Consult();

                        if (Detalles != null && Detalles.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Detalles.Rows)
                            {
                                ws.Cells[renglon, 2].Value = DateTime.Parse(Fila["Fecha"].ToString()).ToString("dd-MMM-yyyy");
                                ws.Cells[renglon, 3].Value = Fila["Agricultor"];
                                ws.Cells[renglon, 4].Value = Dr["Clave"];
                                ws.Cells[renglon, 5].Value = Dr["Producto"];
                                ws.Cells[renglon, 6].Value = Dr["Peso"];
                                ws.Cells[renglon, 7].Value = Dr["Cajas"];
                                ws.Cells[renglon, 8].Value = Dr["Kilogramos"];
                                ws.Cells[renglon, 9].Value = Dr["Precio"];
                                ws.Cells[renglon, 10].Value = Dr["Total"];
                                Peso += Double.Parse(Dr["Precio"].ToString());
                                Cajas += Double.Parse(Dr["Cajas"].ToString());
                                Kilogramos += Double.Parse(Dr["Kilogramos"].ToString());
                                Total += Double.Parse(Dr["Total"].ToString());
                                renglon++;
                            }
                        }
                    }
                    ws.Cells[renglon, 2].Value = "TOTALES";
                    ws.Cells[renglon, 6].Value = Peso.ToString();
                    ws.Cells[renglon, 7].Value = Cajas.ToString();
                    ws.Cells[renglon, 8].Value = Kilogramos.ToString();
                    ws.Cells[renglon, 10].Value = Total.ToString();
                    renglon++;

                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[aux_renglon, 2, renglon - 1, 10].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                // guarda los cambios
                Byte[] bin = p.GetAsByteArray();
                String file = ruta_nueva_archivo;
                File.WriteAllBytes(file, bin);
            }
        }
    }
}