﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class MenuController : Controller
    {
        public PartialViewResult ConstruirMenu()
        {
            Mdl_Accesos Controlador = new Mdl_Accesos();
            DataTable Dt_Accesos = new DataTable();
            if (!String.IsNullOrEmpty(SessionSRG.No_Empleado.ToString()))
            {
                Controlador.No_Empleado = SessionSRG.No_Empleado.ToString();
                Dt_Accesos = Controlador.Consult();
            }
            return PartialView(Dt_Accesos);
        }
    }
}
