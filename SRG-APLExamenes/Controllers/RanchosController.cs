﻿using Newtonsoft.Json;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class RanchosController : Controller
    {
        private Mdl_Ranchos ControladorRanchos;
        public RanchosController()
        {
            ControladorRanchos = new Mdl_Ranchos();
        }
        //
        // GET: /Ranchos/
        [SessionExpire]
        public ActionResult Ranchos()
        {
            return View();
        }
        [SessionExpire]
        public JsonResult Consulta_Catalogo()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorRanchos.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                     Fila.Field<String>("Nombre").Trim(),
                                     Fila.Field<String>("Descripcion").ToString(),
                                     Fila.Field<String>("Estatus").Trim(),
                                     Fila.Field<Int32>("Rancho_ID").ToString()
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public ActionResult GetRanchosList(String q)
        {
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorRanchos.Estatus = "ACTIVO";
                Dt_Registros = ControladorRanchos.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<Int32>("Rancho_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorRanchos = new Mdl_Ranchos();
                ControladorRanchos.Rancho_ID = Int32.Parse(Evento_ID);
                Dt_Registros = ControladorRanchos.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public String EventMaster(Mdl_Ranchos Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            try
            {
                Datos.Usuario_Registro = SessionSRG.Nombre;
                Datos.Fecha_Registro = "GETDATE()";
                ControladorRanchos = Datos;
                if (Datos.Captura.Equals("I"))
                {
                    if (ControladorRanchos.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA_IDENTITY))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorRanchos.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }

    }
}
