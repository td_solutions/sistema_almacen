﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class NotasSalidaController : Controller
    {
        private Mdl_Notas_Salida ControladorNotasSalida;
        private Mdl_Notas_Salida_Detalles ControladorNotasSalidaDetalles;
        private Mdl_Destajo ControladorDestajo;
        public NotasSalidaController()
        {
            ControladorNotasSalida = new Mdl_Notas_Salida();
            ControladorNotasSalidaDetalles = new Mdl_Notas_Salida_Detalles();
            ControladorDestajo = new Mdl_Destajo();
        }

        [SessionExpire]
        public ActionResult NotasSalida()
        {
            return View();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvents
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        public JsonResult GetEvents()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorNotasSalida.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {                                      
                                     Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"),
                                     Fila.Field<String>("Agricultor").ToString(),  
                                     Fila.Field<Decimal>("Total_Cajas").ToString(),     
                                     Fila.Field<Decimal>("Total").ToString(),
                                     Fila.Field<int>("No_Nota").ToString()     
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public JsonResult GetNotasSalidaDetalles(Mdl_Notas_Salida_Detalles Datos)
        {
            DataTable Dt_Registros = new DataTable();

            ControladorNotasSalidaDetalles = Datos;
            Dt_Registros = ControladorNotasSalidaDetalles.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                         Fila.Field<String>("Clave").ToString(),
                                         Fila.Field<String>("Producto").ToString(),
                                         Fila.Field<Decimal>("Peso").ToString(),
                                         Fila.Field<Decimal>("Cajas").ToString(),
                                         Fila.Field<Decimal>("Kilogramos").ToString(),
                                         Fila.Field<Decimal>("Precio").ToString(),
                                         Fila.Field<Decimal>("Total").ToString()
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvent
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorNotasSalida = new Mdl_Notas_Salida();
                ControladorNotasSalida.No_Nota = Evento_ID;
                Dt_Registros = ControladorNotasSalida.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Nota Salida [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public String EventMaster(String Datos, String Detalles, String Destajo)
        {
            List<Mdl_Notas_Salida_Detalles> Obj_Detalles = new List<Mdl_Notas_Salida_Detalles>();
            List<Mdl_Destajo> Obj_Destajo = new List<Mdl_Destajo>();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();            
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Destajo = new DataTable();
            String StrDatos = String.Empty;

            try
            {
                ControladorNotasSalida = JsonConvert.DeserializeObject<Mdl_Notas_Salida>(Datos);
                Obj_Detalles = JsonConvert.DeserializeObject<List<Mdl_Notas_Salida_Detalles>>(Detalles);
                Obj_Destajo = JsonConvert.DeserializeObject<List<Mdl_Destajo>>(Destajo);

                ControladorNotasSalida.Usuario_Registro = SessionSRG.Nombre;
                ControladorNotasSalida.Fecha_Registro = "GETDATE()";

                if (ControladorNotasSalida.Captura.Equals("I"))
                {
                    ControladorNotasSalida.No_Nota = ControladorNotasSalida.ConsultMaximoNotaSalida().Rows[0]["No_Nota"].ToString();
                    if (ControladorNotasSalida.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA))
                    {
                        if (Obj_Detalles.Count > 0)
                        {
                            for (int i = 0; i < Obj_Detalles.Count; i++)
                            {
                                ControladorNotasSalidaDetalles.No_Nota = null;
                                ControladorNotasSalidaDetalles.Clave = null;
                                ControladorNotasSalidaDetalles.Producto = null;                                
                                ControladorNotasSalidaDetalles.Cajas = null;
                                ControladorNotasSalidaDetalles.Peso = null;
                                ControladorNotasSalidaDetalles.Kilogramos = null;                                
                                ControladorNotasSalidaDetalles.Precio = null;
                                ControladorNotasSalidaDetalles.Total = null;

                                ControladorNotasSalidaDetalles.No_Nota = ControladorNotasSalida.No_Nota;
                                ControladorNotasSalidaDetalles.Clave = Obj_Detalles[i].Clave;
                                ControladorNotasSalidaDetalles.Producto = Obj_Detalles[i].Producto;                                
                                ControladorNotasSalidaDetalles.Cajas = Obj_Detalles[i].Cajas;
                                ControladorNotasSalidaDetalles.Peso = Obj_Detalles[i].Peso;
                                ControladorNotasSalidaDetalles.Kilogramos = Obj_Detalles[i].Kilogramos;                                
                                ControladorNotasSalidaDetalles.Precio = Obj_Detalles[i].Precio;
                                ControladorNotasSalidaDetalles.Total = Obj_Detalles[i].Total;
                                ControladorNotasSalidaDetalles.Captura = Obj_Detalles[i].Captura;
                                if (ControladorNotasSalidaDetalles.Captura.Equals("I"))
                                {
                                    ControladorNotasSalidaDetalles.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA);
                                }
                                else
                                {
                                    ControladorNotasSalidaDetalles.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);
                                }
                            }
                        }

                        if (Obj_Destajo.Count > 0)
                        {
                            for (int i = 0; i < Obj_Destajo.Count; i++)
                            {
                                ControladorDestajo.No_Destajo = null;
                                ControladorDestajo.Fecha_Inicio = null;
                                ControladorDestajo.Fecha_Fin = null;
                                ControladorDestajo.Tipo_Destajo_ID = null;
                                ControladorDestajo.Tabla_ID = null;
                                ControladorDestajo.Cantidad_Empleados = 0;
                                ControladorDestajo.No_Nota = null;

                                ControladorDestajo.Fecha_Inicio = Obj_Destajo[i].Fecha;
                                ControladorDestajo.Fecha_Fin = Obj_Destajo[i].Fecha;
                                ControladorDestajo.Tipo_Destajo_ID = Obj_Destajo[i].Tipo_Destajo_ID;
                                ControladorDestajo.Tabla_ID = "NOTA NULL";
                                ControladorDestajo.Cantidad_Empleados = 2;
                                Dt_Destajo = ControladorDestajo.Consult();
                                if (Dt_Destajo != null && Dt_Destajo.Rows.Count > 0)
                                {
                                    foreach (DataRow Dr in Dt_Destajo.Rows)
                                    {
                                        ControladorDestajo.No_Destajo = null;
                                        ControladorDestajo.Fecha_Inicio = null;
                                        ControladorDestajo.Fecha_Fin = null;
                                        ControladorDestajo.Tipo_Destajo_ID = null;
                                        ControladorDestajo.Tabla_ID = null;
                                        ControladorDestajo.Cantidad_Empleados = 0;
                                        ControladorDestajo.No_Nota = null;

                                        ControladorDestajo.No_Destajo = Dr["No_Destajo"].ToString();
                                        ControladorDestajo.No_Nota = ControladorNotasSalida.No_Nota;
                                        ControladorDestajo.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);
                                    }
                                }
                            }
                        }
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorNotasSalida.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
