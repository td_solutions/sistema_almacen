﻿using Newtonsoft.Json;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class AlmacenController : Controller
    {
        private Mdl_Almacen ControladorAlmacen;
        public AlmacenController()
        {
            ControladorAlmacen = new Mdl_Almacen();
        }
        //
        // GET: /Almacen/
        [SessionExpire]
        public ActionResult Almacen()
        {
            return View();
        }
        public JsonResult Consulta_Catalogo()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorAlmacen.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                     Fila.Field<String>("Nombre").Trim(),
                                     Fila.Field<String>("Clave").Trim(),
                                     Fila.Field<String>("Estatus").ToString(),
                                     Fila.Field<Int32>("Almacen_ID").ToString()
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorAlmacen = new Mdl_Almacen();
                ControladorAlmacen.Almacen_ID = Int32.Parse(Evento_ID);
                Dt_Registros = ControladorAlmacen.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public ActionResult GetAlmacenList(String q)
        {
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorAlmacen.Estatus = "ACTIVO";
                Dt_Registros = ControladorAlmacen.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<Int32>("Almacen_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public String EventMaster(Mdl_Almacen Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            try
            {
                Datos.Usuario_Registro = SessionSRG.Nombre;
                Datos.Fecha_Registro = "GETDATE()";
                ControladorAlmacen = Datos;
                if (Datos.Captura.Equals("I"))
                {
                    if (ControladorAlmacen.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA_IDENTITY))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorAlmacen.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
