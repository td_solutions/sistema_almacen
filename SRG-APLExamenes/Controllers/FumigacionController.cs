﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class FumigacionController : Controller
    {
        private Mdl_Fumigacion ControladorFumigacion;
        private Mdl_Fumigacion_Detalles ControladorFumigacionDetalles;
        private Mdl_Fumigacion_Tablas ControladorFumigacionTablas;
        private Mdl_Empleados ControladorEmpleados;
        private Mdl_Salidas ControladorSalidas;
        private Mdl_Parametros ControladorParametros;
        public FumigacionController()
        {
            ControladorFumigacion = new Mdl_Fumigacion();
            ControladorFumigacionDetalles = new Mdl_Fumigacion_Detalles();
            ControladorFumigacionTablas = new Mdl_Fumigacion_Tablas();
            ControladorEmpleados = new Mdl_Empleados();
            ControladorParametros = new Mdl_Parametros();
            ControladorSalidas = new Mdl_Salidas();
        }

        [SessionExpire]
        public ActionResult Fumigacion()
        {
            return View();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvents
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        public JsonResult GetEvents()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorFumigacion.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {                                      
                                     Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"),                                     
                                     Fila.Field<Decimal>("Litros").ToString(),                                          
                                     Fila.Field<String>("Pago_Por").ToString(),                                         
                                     Fila.Field<Decimal>("Total_Empleados").ToString(),
                                     Fila.Field<int>("No_Fumigacion").ToString()     
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public JsonResult GetFumigacionTablas(Mdl_Fumigacion_Tablas Datos)
        {
            DataTable Dt_Registros = new DataTable();

            ControladorFumigacionTablas = Datos;
            Dt_Registros = ControladorFumigacionTablas.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                         Fila.Field<String>("Tabla").ToString(),
                                         Fila.Field<int>("No_Tuneles").ToString()                                                                    
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public JsonResult GetFumigacionDetalles(Mdl_Fumigacion_Detalles Datos)
        {
            DataTable Dt_Registros = new DataTable();

            ControladorFumigacionDetalles = Datos;
            Dt_Registros = ControladorFumigacionDetalles.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                         Fila.Field<Decimal>("No_Tarjeta").ToString(),
                                         Fila.Field<String>("Nombre").ToString()                                         
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvent
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorFumigacion = new Mdl_Fumigacion();
                ControladorFumigacion.No_Fumigacion = Evento_ID;
                Dt_Registros = ControladorFumigacion.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public String EventMaster(String Datos, String Detalles, String Tablas, String Salidas)
        {
            List<Mdl_Fumigacion_Detalles> Obj_Empleados = new List<Mdl_Fumigacion_Detalles>();
            List<Mdl_Fumigacion_Tablas> Obj_Tablas = new List<Mdl_Fumigacion_Tablas>();
            List<Mdl_Salidas> Obj_Salidas = new List<Mdl_Salidas>();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Empleados = new DataTable();
            Respuesta ObjMensajeServidor = new Respuesta();
            Double Cantidad_Litros = 0;
            Double Pago_Litro_Fumigacion = 0;
            String StrDatos = String.Empty;

            try
            {
                ControladorFumigacion = JsonConvert.DeserializeObject<Mdl_Fumigacion>(Datos);
                Obj_Empleados = JsonConvert.DeserializeObject<List<Mdl_Fumigacion_Detalles>>(Detalles);
                Obj_Tablas = JsonConvert.DeserializeObject<List<Mdl_Fumigacion_Tablas>>(Tablas);
                Obj_Salidas = JsonConvert.DeserializeObject<List<Mdl_Salidas>>(Salidas);

                ControladorFumigacion.Usuario_Registro = SessionSRG.Nombre;
                ControladorFumigacion.Fecha_Registro = "GETDATE()";

                Pago_Litro_Fumigacion = Double.Parse(ControladorParametros.Consult().Rows[0]["Precio_Litro_Fumigacion"].ToString());
                if (ControladorFumigacion.Pago_Por == "LITROS")
                {
                    Cantidad_Litros = Double.Parse(ControladorFumigacion.Litros) / Double.Parse(ControladorFumigacion.Total_Empleados.ToString());
                    for (int i = 0; i < Obj_Empleados.Count; i++)
                    {
                        Obj_Empleados[i].Cantidad = Decimal.Parse(Cantidad_Litros.ToString());
                        Obj_Empleados[i].Pago = Decimal.Parse(Cantidad_Litros.ToString()) * Decimal.Parse(Pago_Litro_Fumigacion.ToString());
                    }
                }
                else
                {
                    Cantidad_Litros = 1;
                    for (int i = 0; i < Obj_Empleados.Count; i++)
                    {
                        Obj_Empleados[i].Cantidad = Decimal.Parse(Cantidad_Litros.ToString());
                        ControladorEmpleados.No_Empleado = Obj_Empleados[i].No_Empleado;
                        Dt_Empleados = ControladorEmpleados.Consult();
                        if (Dt_Empleados != null && Dt_Empleados.Rows.Count > 0)
                        {
                            Obj_Empleados[i].Pago = Decimal.Parse(Cantidad_Litros.ToString()) * Decimal.Parse(Dt_Empleados.Rows[0]["Salario_Diario_Fumigacion"].ToString());
                        }
                        else
                        {
                            Obj_Empleados[i].Pago = 0;
                        }
                    }
                }

                if (ControladorFumigacion.Captura.Equals("I"))
                {
                    ControladorFumigacion.No_Fumigacion = ControladorFumigacion.ConsultMaximoFumigacion().Rows[0]["No_Fumigacion"].ToString();
                    if (ControladorFumigacion.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA))
                    {
                        if (Obj_Empleados.Count > 0)
                        {
                            for (int i = 0; i < Obj_Empleados.Count; i++)
                            {
                                ControladorFumigacionDetalles.No_Fumigacion = null;
                                ControladorFumigacionDetalles.No_Empleado = null;
                                ControladorFumigacionDetalles.No_Tarjeta = null;
                                ControladorFumigacionDetalles.Nombre = null;
                                ControladorFumigacionDetalles.Cantidad = 0;
                                ControladorFumigacionDetalles.Pago = 0;

                                ControladorFumigacionDetalles.No_Fumigacion = ControladorFumigacion.No_Fumigacion;
                                if (!String.IsNullOrEmpty(Obj_Empleados[i].No_Empleado))
                                    ControladorFumigacionDetalles.No_Empleado =  int.Parse(Obj_Empleados[i].No_Empleado).ToString("00000");
                                ControladorFumigacionDetalles.No_Tarjeta = Obj_Empleados[i].No_Tarjeta;
                                ControladorFumigacionDetalles.Nombre = Obj_Empleados[i].Nombre;
                                ControladorFumigacionDetalles.Cantidad = Obj_Empleados[i].Cantidad;
                                ControladorFumigacionDetalles.Pago = Obj_Empleados[i].Pago;
                                ControladorFumigacionDetalles.Captura = Obj_Empleados[i].Captura;
                                if (ControladorFumigacionDetalles.Captura.Equals("I"))
                                {
                                    ControladorFumigacionDetalles.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA);                                    
                                }
                                else
                                {
                                    ControladorFumigacionDetalles.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);                                    
                                }                               
                            }
                        }

                        if (Obj_Tablas.Count > 0)
                        {
                            for (int i = 0; i < Obj_Tablas.Count; i++)
                            {
                                ControladorFumigacionTablas.No_fumigacion = null;
                                ControladorFumigacionTablas.Tabla_ID = null;
                                ControladorFumigacionTablas.No_Tuneles = 0;                                

                                ControladorFumigacionTablas.No_fumigacion = ControladorFumigacion.No_Fumigacion;
                                ControladorFumigacionTablas.Tabla_ID = Obj_Tablas[i].Tabla_ID;
                                ControladorFumigacionTablas.No_Tuneles = Obj_Tablas[i].No_Tuneles;

                                ControladorFumigacionTablas.Captura = Obj_Tablas[i].Captura;   
                                if (ControladorFumigacionTablas.Captura.Equals("I"))
                                {
                                    ControladorFumigacionTablas.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA);                                    
                                }
                                else
                                {
                                    ControladorFumigacionTablas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);                                    
                                }                               
                            }
                        }

                        if (Obj_Salidas.Count > 0)
                        {
                            for (int i = 0; i < Obj_Salidas.Count; i++)
                            {
                                ControladorSalidas.No_Salida = 0;
                                ControladorSalidas.No_Fumigacion = null;
                                ControladorSalidas.Salio = null;
                                ControladorSalidas.Usuario_Registro = null;
                                ControladorSalidas.Fecha_Registro = DateTime.Now;

                                ControladorSalidas.No_Salida = Obj_Salidas[i].No_Salida;
                                ControladorSalidas.No_Fumigacion = ControladorFumigacion.No_Fumigacion;
                                ControladorSalidas.Salio = "SI";
                                ControladorSalidas.Usuario_Registro = SessionSRG.Nombre;
                                ControladorSalidas.Fecha_Registro = DateTime.Now;
                                ControladorSalidas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);
                            }
                        }

                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorFumigacion.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
