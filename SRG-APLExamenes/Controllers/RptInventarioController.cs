﻿using Newtonsoft.Json;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Reportes.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class RptInventarioController : Controller
    {
         private Mdl_Rpt_Inventario ControladorReporte;
        //
        public RptInventarioController()
        {
            ControladorReporte = new Mdl_Rpt_Inventario();
        }
        // GET: /RptInventario/
        [SessionExpire]
        public ActionResult RptInventario()
        {
            return View();
        }

        [SessionExpire]
        public ActionResult Download_Reporte_Linea(string Almacen_ID, string Almacen, string Insumo_ID)
        {
            DataTable Dt_Registros = new DataTable(); 
            string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Inventario.xlsx";
            string nombre_archivo = "Reporte_Inventario.xlsx";
            string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);
            int int_almacen_id = 0;
            int int_insumo_id = 0;
            try
            {
                Int32.TryParse(Almacen_ID, out int_almacen_id);
                Int32.TryParse(Insumo_ID, out int_insumo_id);
                ControladorReporte.Almacen_ID = int_almacen_id;
                ControladorReporte.Almacen = Almacen;
                ControladorReporte.Insumo_ID = int_insumo_id;
                Dt_Registros = ControladorReporte.Consult();
                ControladorReporte.dt_existencias = Dt_Registros;
                if (String.IsNullOrEmpty(ControladorReporte.Almacen))
                    ControladorReporte.Almacen = "ALMACEN GENERAL";

                Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                Obj_Reporte_Pago.Elaborar_Reporte_Inventario(ControladorReporte);

                this.Response.Clear();
                this.Response.ContentType = "application/vnd.ms-excel";
                this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                this.Response.WriteFile(ruta_almacenamiento);
                this.Response.End();
                //return RedirectToAction("RptInventario", "RptInventario");
            }
            catch (Exception Ex)
            {
                return RedirectToAction("RptInventario", "RptInventario");
            }
           
            return null;
        }
        public ActionResult Download_Reporte_Reorden(string Almacen_ID, string Almacen, string Insumo_ID)
        {
            DataTable Dt_Registros = new DataTable(); 
            string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Inventario_Reorden.xlsx";
            string nombre_archivo = "Reporte_Inventario_Reorden.xlsx";
            string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);
            int int_almacen_id = 0;
            int int_insumo_id = 0;
            try
            {
                Int32.TryParse(Almacen_ID, out int_almacen_id);
                Int32.TryParse(Insumo_ID, out int_insumo_id);
                ControladorReporte.Almacen_ID = int_almacen_id;
                ControladorReporte.Almacen = Almacen;
                ControladorReporte.Insumo_ID = int_insumo_id;
                Dt_Registros = ControladorReporte.Consulta_Reorden();
                ControladorReporte.dt_existencias = Dt_Registros;
                if (String.IsNullOrEmpty(ControladorReporte.Almacen))
                    ControladorReporte.Almacen = "ALMACEN GENERAL";

                Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                Obj_Reporte_Pago.Elaborar_Reporte_Inventario_Reorden(ControladorReporte);

                this.Response.Clear();
                this.Response.ContentType = "application/vnd.ms-excel";
                this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                this.Response.WriteFile(ruta_almacenamiento);
                this.Response.End();
                //return RedirectToAction("RptInventario", "RptInventario");
            }
            catch (Exception Ex)
            {
                return RedirectToAction("RptInventario", "RptInventario");
            }
           
            return null;
        }
        public string obtenerRutaParaGuardar(String sFileName)
        {
            string respuesta = "";

            if (!Path.IsPathRooted(sFileName))
                sFileName = Path.Combine(Path.GetTempPath(), sFileName);

            if (System.IO.File.Exists(sFileName))
            {
                String sDateTime = DateTime.Now.ToString("yyyyMMdd\\_HHmmss");
                String s = Path.GetFileNameWithoutExtension(sFileName) + "_" + sDateTime + Path.GetExtension(sFileName);
                sFileName = Path.Combine(Path.GetDirectoryName(sFileName), s);
            }
            else
                Directory.CreateDirectory(Path.GetDirectoryName(sFileName));

            respuesta = sFileName;
            return respuesta;
        }
        public JsonResult Consulta_Reporte(Mdl_Rpt_Inventario Datos)
        {
            DataTable Dt_Registros = new DataTable();
            ControladorReporte = Datos;
            Dt_Registros = ControladorReporte.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                     Fila.Field<String>("Codigo_Barras").ToString(),
                                     Fila.Field<String>("Nombre").ToString(),
                                     Fila.Field<String>("Unidad").ToString(),
                                     Fila.Field<Decimal>("Existencias").ToString(),
                                     Fila.Field<Decimal>("Sugerido").ToString(),
                                     Fila.Field<Decimal>("Costo").ToString(),
                                     Fila.Field<Decimal>("Total_Costo").ToString()
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Consulta_Reporte_reorden(Mdl_Rpt_Inventario Datos)
        {
            DataTable Dt_Registros = new DataTable();
            ControladorReporte = Datos;
            Dt_Registros = ControladorReporte.Consulta_Reorden();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                     Fila.Field<String>("Codigo_Barras").ToString(),
                                     Fila.Field<String>("Nombre").ToString(),
                                     Fila.Field<String>("Unidad").ToString(),
                                     Fila.Field<Decimal>("Existencias").ToString(),
                                     Fila.Field<Decimal>("Sugerido").ToString(),
                                     Fila.Field<Decimal>("Costo").ToString(),
                                     Fila.Field<Decimal>("Total_Costo").ToString()
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorReporte = new Mdl_Rpt_Inventario();
                ControladorReporte.Almacen_ID = Int32.Parse(Evento_ID);
                Dt_Registros = ControladorReporte.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }
        //[SessionExpire]
        //[HttpPost]
        //public JsonResult GetDetalles(String No_Salida)
        //{
        //    Respuesta ObjMensajeServidor = new Respuesta();
        //    DataTable Dt_Registros = new DataTable();
        //    try
        //    {
        //        ControladorReporte = new Mdl_Rpt_Inventario();
        //        ControladorReporte.Almacen_ID = Int32.Parse(No_Salida);
        //        Dt_Registros = ControladorReporte.ConsultDet();
        //        var result = from Fila in Dt_Registros.AsEnumerable()
        //                     select new[] {
        //                             Fila.Field<Decimal>("Cantidad").ToString(),
        //                             Fila.Field<String>("Unidad").ToString(),
        //                             Fila.Field<String>("Insumo").ToString(),
        //                             Fila.Field<Int32>("Insumo_ID").ToString()
        //                         };
        //        return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception Ex)
        //    {
        //        ObjMensajeServidor.Estatus = false;
        //        ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
        //    }
        //    return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        //}
    }
}
