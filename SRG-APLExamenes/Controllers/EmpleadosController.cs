﻿    using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class EmpleadosController : Controller
    {        
        private Mdl_Empleados ControladorEmpleado;
        private Mdl_Accesos ControladorAcceso;
        public EmpleadosController()
        {            
            ControladorEmpleado = new Mdl_Empleados();
            ControladorAcceso = new Mdl_Accesos();
        }

        [SessionExpire]
        public ActionResult Empleados()
        {
            return View();
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       FRANCISCO JAVIER BECERRA TOLEDO
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        public JsonResult GetEmployee()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorEmpleado.Consult();
            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                     Fila.Field<String>("No_Empleado").Trim(),
                                     Fila.Field<String>("Nombre").Trim(),                                                                          
                                     Fila.Field<String>("Email") == null ? "" : Seguridad.DesEncriptar(Fila.Field<String>("Email").Trim()),
                                     Fila.Field<String>("Password") == null ? "" :Seguridad.DesEncriptar(Fila.Field<String>("Password").Trim()),                                     
                                     Fila.Field<String>("Estatus").Trim(),
                                     Fila.Field<String>("No_Empleado").Trim(),
                                     Fila.Field<Decimal>("Salario_Diario").ToString(),
                                     Fila.Field<Decimal>("Salario_Diario_Fumigacion").ToString(),
                                     Fila.Field<String>("Usuario") == null ? "" : Seguridad.DesEncriptar(Fila.Field<String>("Usuario").Trim())                                     
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       FRANCISCO JAVIER BECERRA TOLEDO
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public String OperationMaster(String Datos, String Accesos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            List<Mdl_Accesos> Obj_Acceso = new List<Mdl_Accesos>();
            String StrDatos = String.Empty;
            try
            {                
                ControladorEmpleado = JsonConvert.DeserializeObject<Mdl_Empleados>(Datos);
                Obj_Acceso = JsonConvert.DeserializeObject<List<Mdl_Accesos>>(Accesos);
                ControladorEmpleado.Usuario_Registro = SessionSRG.Nombre;
                ControladorEmpleado.Fecha_Registro = "GETDATE()";
                if (ControladorEmpleado.Captura.Equals("I"))
                {
                    ControladorEmpleado.No_Empleado = int.Parse(ControladorEmpleado.ConsultMaximoEmpleado().Rows[0]["No_Empleado"].ToString()).ToString("00000");
                    if (ControladorEmpleado.MasterManagement(Obj_Acceso, Core.MODO_DE_CAPTURA.CAPTURA_ALTA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorEmpleado.MasterManagement(Obj_Acceso, Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       FRANCISCO JAVIER BECERRA TOLEDO
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public String Importar()
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            try
            {
                //ControladorEmpleado.ImportData();
                ObjMensajeServidor.Estatus = true;
                ObjMensajeServidor.Mensaje = "Registros importados correctamente.";
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEmployeeList
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       FRANCISCO JAVIER BECERRA TOLEDO
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public ActionResult GetEmployeeList(String q)
        {
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorEmpleado.Estatus = "ACTIVO";
                Dt_Registros = ControladorEmpleado.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<String>("No_Empleado").Trim(),
                        text = row.Field<String>("Nombre").ToString().Trim()
                    }).ToList();
                if (!(string.IsNullOrEmpty(q) || string.IsNullOrWhiteSpace(q)))
                {
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }

            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvent
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEmployee(String No_Empleado)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorEmpleado = new Mdl_Empleados();
                ControladorEmpleado.No_Empleado = int.Parse(No_Empleado).ToString("00000");
                Dt_Registros = ControladorEmpleado.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Empleado [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEmployeeList
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       FRANCISCO JAVIER BECERRA TOLEDO
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public ActionResult GetAccessList(Mdl_Accesos Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorAcceso = Datos;
                Dt_Registros = ControladorAcceso.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }

            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }
    }
}
