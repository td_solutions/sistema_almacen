﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class RiegoController : Controller
    {
        private Mdl_Riego ControladorRiego;
        private Mdl_Riego_Detalles ControladorRiegoDetalles;
        private Mdl_Riego_Tablas ControladorRiegoTablas;
        private Mdl_Salidas ControladorSalidas;
        public RiegoController()
        {
            ControladorRiego = new Mdl_Riego();
            ControladorRiegoDetalles = new Mdl_Riego_Detalles();
            ControladorRiegoTablas = new Mdl_Riego_Tablas();
            ControladorSalidas = new Mdl_Salidas();
        }

        [SessionExpire]
        public ActionResult Riego()
        {
            return View();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvents
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        public JsonResult GetEvents()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorRiego.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {                                      
                                     Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"),                                                                                                                    
                                     //Fila.Field<String>("Tanque").ToString(),    
                                     //Fila.Field<Decimal>("Num_Tanque").ToString(),    
                                     Fila.Field<Decimal>("Horas").ToString(),
                                     Fila.Field<int>("Total_Empleados").ToString(),
                                     Fila.Field<int>("No_Riego").ToString()     
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public JsonResult GetRiegoTablas(Mdl_Riego_Tablas Datos)
        {
            DataTable Dt_Registros = new DataTable();

            ControladorRiegoTablas = Datos;
            Dt_Registros = ControladorRiegoTablas.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                         Fila.Field<String>("Tabla").ToString()                                         
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public JsonResult GetRiegoDetalles(Mdl_Riego_Detalles Datos)
        {
            DataTable Dt_Registros = new DataTable();

            ControladorRiegoDetalles = Datos;
            Dt_Registros = ControladorRiegoDetalles.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                         Fila.Field<Decimal>("No_Tarjeta").ToString(),
                                         Fila.Field<String>("Nombre").ToString(),
                                         Fila.Field<Decimal>("Pago").ToString()
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvent
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorRiego = new Mdl_Riego();
                ControladorRiego.No_Riego = Evento_ID;
                Dt_Registros = ControladorRiego.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar GetEvent [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        [HttpPost]
        public JsonResult GetEventValidar(String Fecha)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorRiego = new Mdl_Riego();
                ControladorRiego.Fecha = Fecha;
                Dt_Registros = ControladorRiego.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar GetEventValidar [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        [HttpPost]
        public JsonResult GetEventAcidoRiego(String Fecha)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {                
                Dt_Registros = ControladorRiego.ConsultAcidoRiego();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar GetEventAcidoRiego [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public String EventMaster(String Datos, String Detalles, String Tablas, String Salidas)
        {
            List<Mdl_Riego_Detalles> Obj_Empleados = new List<Mdl_Riego_Detalles>();
            List<Mdl_Riego_Tablas> Obj_Tablas = new List<Mdl_Riego_Tablas>();
            List<Mdl_Salidas> Obj_Salidas = new List<Mdl_Salidas>();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            try
            {
                ControladorRiego = JsonConvert.DeserializeObject<Mdl_Riego>(Datos);
                Obj_Empleados = JsonConvert.DeserializeObject<List<Mdl_Riego_Detalles>>(Detalles);
                Obj_Tablas = JsonConvert.DeserializeObject<List<Mdl_Riego_Tablas>>(Tablas);
                Obj_Salidas = JsonConvert.DeserializeObject<List<Mdl_Salidas>>(Salidas);

                ControladorRiego.Usuario_Registro = SessionSRG.Nombre;
                ControladorRiego.Fecha_Registro = "GETDATE()";

                if (ControladorRiego.Captura.Equals("I"))
                {
                    ControladorRiego.No_Riego = ControladorRiego.ConsultMaximoRiego().Rows[0]["No_Riego"].ToString();
                    if (ControladorRiego.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA))
                    {
                        if (Obj_Empleados.Count > 0)
                        {
                            for (int i = 0; i < Obj_Empleados.Count; i++)
                            {
                                ControladorRiegoDetalles.No_Riego = null;
                                ControladorRiegoDetalles.No_Empleado = null;
                                ControladorRiegoDetalles.No_Tarjeta = null;
                                ControladorRiegoDetalles.Nombre = null;
                                ControladorRiegoDetalles.Pago = 0;

                                ControladorRiegoDetalles.No_Riego = ControladorRiego.No_Riego;
                                if (!String.IsNullOrEmpty(Obj_Empleados[i].No_Empleado))
                                    ControladorRiegoDetalles.No_Empleado = int.Parse(Obj_Empleados[i].No_Empleado).ToString("00000");
                                ControladorRiegoDetalles.No_Tarjeta = Obj_Empleados[i].No_Tarjeta;
                                ControladorRiegoDetalles.Nombre = Obj_Empleados[i].Nombre;
                                ControladorRiegoDetalles.Pago = Obj_Empleados[i].Pago;
                                ControladorRiegoDetalles.Captura = Obj_Empleados[i].Captura;
                                if (ControladorRiegoDetalles.Captura.Equals("I"))
                                {
                                    ControladorRiegoDetalles.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA);
                                }
                                else
                                {
                                    ControladorRiegoDetalles.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);
                                }
                            }
                        }

                        if (Obj_Tablas.Count > 0)
                        {
                            for (int i = 0; i < Obj_Tablas.Count; i++)
                            {
                                ControladorRiegoTablas.No_Riego= null;
                                ControladorRiegoTablas.Tabla_ID = null;

                                ControladorRiegoTablas.No_Riego = ControladorRiego.No_Riego;
                                ControladorRiegoTablas.Tabla_ID = Obj_Tablas[i].Tabla_ID;
                                ControladorRiegoTablas.Captura = Obj_Tablas[i].Captura;
                                if (ControladorRiegoTablas.Captura.Equals("I"))
                                {
                                    ControladorRiegoTablas.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA);
                                }
                                else
                                {
                                    ControladorRiegoTablas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);
                                }
                            }
                        }

                        if (Obj_Salidas.Count > 0)
                        {
                            for (int i = 0; i < Obj_Salidas.Count; i++)
                            {
                                ControladorSalidas.No_Salida = 0;
                                ControladorSalidas.No_Riego = null;
                                ControladorSalidas.Salio = null;
                                ControladorSalidas.Usuario_Registro = null;
                                ControladorSalidas.Fecha_Registro = DateTime.Now;

                                ControladorSalidas.No_Salida = Obj_Salidas[i].No_Salida;
                                ControladorSalidas.No_Riego = ControladorRiego.No_Riego;
                                ControladorSalidas.Salio = "SI";
                                ControladorSalidas.Usuario_Registro = SessionSRG.Nombre;
                                ControladorSalidas.Fecha_Registro = DateTime.Now;
                                ControladorSalidas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);
                            }
                        }

                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorRiego.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
