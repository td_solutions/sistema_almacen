﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;

namespace SRG_APLExamenes.Controllers
{
    public class AdministracionController : Controller
    {
        private Mdl_Empleados ControladorEmpleado;

        public AdministracionController()
        {            
            ControladorEmpleado = new Mdl_Empleados();
        }

        [HttpGet]
        public ViewResult VerificarUsuario()
        {
            return View();
        }

        [HttpPost]        
        public ActionResult VerificarUsuario(Login Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Mdl_Empleados Obj_Capturado = new Mdl_Empleados();
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dtregistro = new DataTable();
            String StrDatos = String.Empty;
            if (ModelState.IsValid)
            {
                try
                {
                    ControladorEmpleado.Usuario = Datos.Usuario;
                    ControladorEmpleado.Password = Datos.Password;
                    Dtregistro = ControladorEmpleado.Consult();

                    if (Dtregistro != null && Dtregistro.Rows.Count > 0)
                    {                        
                        SessionSRG.Nombre = Dtregistro.Rows[0]["Nombre"].ToString();
                        SessionSRG.No_Empleado = Dtregistro.Rows[0]["No_Empleado"].ToString();
                        FormsAuthentication.SetAuthCookie(Dtregistro.Rows[0]["No_Empleado"].ToString(), false);
                        return RedirectToAction("Inicio", "Administracion");
                    }
                    else
                    {
                        ModelState.AddModelError("error", "Correo o Contraseña incorrecta");
                        return View();
                    }
                }
                catch (Exception ex)
                {
                    return View();
                }
            }
            else
                return View();
        }

        [SessionExpire]
        [Authorize]
        public ViewResult Inicio()
        {
            return View("Inicio");
        }

        public ActionResult Salir()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("VerificarUsuario", "");
        }
    }
}
