﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class ActividadesController : Controller
    {
        private Mdl_Actividades ControladorActividades;
        private Mdl_Actividades_Detalles ControladorActividadesDetalles;
        private Mdl_Actividades_Tablas ControladorActividadesTablas;
        public ActividadesController()
        {
            ControladorActividades = new Mdl_Actividades();
            ControladorActividadesDetalles = new Mdl_Actividades_Detalles();
            ControladorActividadesTablas = new Mdl_Actividades_Tablas();
        }

        [SessionExpire]
        public ActionResult Actividades()
        {
            return View();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvents
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        public JsonResult GetEvents()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorActividades.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {                                      
                                     Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"),                           
                                     //Fila.Field<int>("No_Tarjeta").ToString(),                                      
                                     Fila.Field<String>("Labor").ToString(),                                                                               
                                     //Fila.Field<Decimal>("Monto").ToString(),
                                     Fila.Field<int>("No_Actividad").ToString()     
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public JsonResult GetActividadesTablas(Mdl_Actividades_Tablas Datos)
        {
            DataTable Dt_Registros = new DataTable();

            ControladorActividadesTablas = Datos;
            Dt_Registros = ControladorActividadesTablas.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                         Fila.Field<String>("Tabla").ToString(),
                                         Fila.Field<int>("No_Tuneles").ToString()                                                                    
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public JsonResult GetActividadesDetalles(Mdl_Actividades_Detalles Datos)
        {
            DataTable Dt_Registros = new DataTable();

            ControladorActividadesDetalles = Datos;
            Dt_Registros = ControladorActividadesDetalles.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                         Fila.Field<Decimal>("No_Tarjeta").ToString(),
                                         Fila.Field<String>("Nombre").ToString(),                                                                              
                                         Fila.Field<Decimal>("Monto").ToString()
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvent
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorActividades = new Mdl_Actividades();
                ControladorActividades.No_Actividad = Evento_ID;
                Dt_Registros = ControladorActividades.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public String EventMaster(String Datos, String Detalles, String Tablas)
        {
            List<Mdl_Actividades_Detalles> Obj_Empleados = new List<Mdl_Actividades_Detalles>();
            List<Mdl_Actividades_Tablas> Obj_Tablas = new List<Mdl_Actividades_Tablas>();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;

            try
            {
                ControladorActividades = JsonConvert.DeserializeObject<Mdl_Actividades>(Datos);
                Obj_Empleados = JsonConvert.DeserializeObject<List<Mdl_Actividades_Detalles>>(Detalles);
                Obj_Tablas = JsonConvert.DeserializeObject<List<Mdl_Actividades_Tablas>>(Tablas);

                ControladorActividades.Usuario_Registro = SessionSRG.Nombre;
                ControladorActividades.Fecha_Registro = "GETDATE()";
                if (ControladorActividades.Captura.Equals("I"))
                {
                    ControladorActividades.No_Actividad = ControladorActividades.ConsultMaximoActividad().Rows[0]["No_Actividad"].ToString();
                    if (ControladorActividades.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA))
                    {
                        if (Obj_Empleados.Count > 0)
                        {
                            for (int i = 0; i < Obj_Empleados.Count; i++)
                            {
                                ControladorActividadesDetalles.No_Actividad = null;
                                ControladorActividadesDetalles.No_Empleado = null;
                                ControladorActividadesDetalles.No_Tarjeta = null;
                                ControladorActividadesDetalles.Nombre = null;
                                ControladorActividadesDetalles.Monto = 0;

                                ControladorActividadesDetalles.No_Actividad = ControladorActividades.No_Actividad;
                                if (!String.IsNullOrEmpty(Obj_Empleados[i].No_Empleado))
                                    ControladorActividadesDetalles.No_Empleado = int.Parse(Obj_Empleados[i].No_Empleado).ToString("00000");
                                ControladorActividadesDetalles.No_Tarjeta = Obj_Empleados[i].No_Tarjeta;
                                ControladorActividadesDetalles.Nombre = Obj_Empleados[i].Nombre;
                                ControladorActividadesDetalles.Monto = Obj_Empleados[i].Monto;
                                ControladorActividadesDetalles.Captura = Obj_Empleados[i].Captura;
                                if (ControladorActividadesDetalles.Captura.Equals("I"))
                                {
                                    ControladorActividadesDetalles.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA);
                                }
                                else
                                {
                                    ControladorActividadesDetalles.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);
                                }
                            }
                        }

                        if (Obj_Tablas.Count > 0)
                        {
                            for (int i = 0; i < Obj_Tablas.Count; i++)
                            {
                                ControladorActividadesTablas.No_Actividad = null;
                                ControladorActividadesTablas.Tabla_ID = null;
                                ControladorActividadesTablas.No_Tuneles = 0;

                                ControladorActividadesTablas.No_Actividad = ControladorActividades.No_Actividad;
                                ControladorActividadesTablas.Tabla_ID = Obj_Tablas[i].Tabla_ID;
                                ControladorActividadesTablas.No_Tuneles = Obj_Tablas[i].No_Tuneles;

                                ControladorActividadesTablas.Captura = Obj_Tablas[i].Captura;
                                if (ControladorActividadesTablas.Captura.Equals("I"))
                                {
                                    ControladorActividadesTablas.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA);
                                }
                                else
                                {
                                    ControladorActividadesTablas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);
                                }
                            }
                        }

                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorActividades.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
