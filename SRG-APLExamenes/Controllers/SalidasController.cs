﻿using Newtonsoft.Json;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class SalidasController : Controller
    {
        private Mdl_Salidas ControladorSalida;

        public SalidasController()
        {
            ControladorSalida = new Mdl_Salidas();
        }

        [SessionExpire]
        public ActionResult Salidas()
        {
            return View();
        }

        public JsonResult Traer_Salida()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorSalida.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                     Fila.Field<int>("No_Salida").ToString(),
                                     Fila.Field<DateTime>("Fecha").ToString(),
                                     Fila.Field<String>("Tipo").ToString(),
                                     Fila.Field<String>("Estatus").ToString(),
                                     //Fila.Field<Decimal>("IVA").ToString(),
                                     //Fila.Field<Decimal>("IEPS").ToString(),
                                     Fila.Field<Decimal>("Costo_Total").ToString()
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorSalida = new Mdl_Salidas();
                ControladorSalida.No_Salida = Int32.Parse(Evento_ID);
                Dt_Registros = ControladorSalida.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        [HttpPost]
        public JsonResult GetSalidas(String Tipo, String No_Fumi_Riego)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorSalida = new Mdl_Salidas();
                if (Tipo == "FUMIGACION")
                    ControladorSalida.No_Fumigacion = No_Fumi_Riego;
                else if (Tipo == "RIEGO")
                    ControladorSalida.No_Riego = No_Fumi_Riego;

                Dt_Registros = ControladorSalida.Consult();
                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] {
                                     Fila.Field<int>("No_Salida").ToString(),
                                     Fila.Field<DateTime>("Fecha").ToString(),
                                     Fila.Field<String>("Tipo").ToString(),
                                     Fila.Field<String>("Estatus").ToString(),
                                     //Fila.Field<Decimal>("IVA").ToString(),
                                     //Fila.Field<Decimal>("IEPS").ToString(),
                                     Fila.Field<Decimal>("Costo_Total").ToString()
                                 };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        [HttpPost]
        public JsonResult GetDetalles(String No_Salida)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorSalida = new Mdl_Salidas();
                ControladorSalida.No_Salida = Int32.Parse(No_Salida);
                Dt_Registros = ControladorSalida.ConsultDet();
                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] {
                                     Fila.Field<decimal>("Cantidad").ToString(),
                                     Fila.Field<String>("Unidad").ToString(),
                                     Fila.Field<String>("Insumo").ToString(),
                                     Fila.Field<int>("Insumo_ID").ToString(),
                                     Fila.Field<decimal>("Precio").ToString(),
                                     Fila.Field<decimal>("Iva").ToString(),
                                     Fila.Field<decimal>("Ieps").ToString(),
                                     Fila.Field<decimal>("Total").ToString()
                                 };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public ActionResult GetSalidasList(String q, String Tipo_Actividad)
        {
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorSalida.Salio = "NO";
                ControladorSalida.Tipo_Actividad = Tipo_Actividad;                
                Dt_Registros = ControladorSalida.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<int>("No_Salida").ToString(),
                        text = row.Field<int>("No_Salida").ToString() + " - " + row.Field<DateTime>("Fecha").ToString("dd/MMM/yyyy") + " " + row.Field<String>("Almacen_Origen").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        [HttpPost]
        public String EventMaster(Mdl_Salidas Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            DateTime Fecha = DateTime.MinValue;
            try
            {
                Datos.Usuario_Registro = SessionSRG.Nombre;
                Datos.Fecha_Registro = DateTime.Now;
                try { Fecha = DateTime.ParseExact(Datos.Str_Fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture); } catch { }
                Datos.Fecha = Fecha;
                ControladorSalida = Datos;
                if (Datos.Captura.Equals("I"))
                {
                    if (ControladorSalida.AltaSalida(MODO_DE_CAPTURA.CAPTURA_ALTA_IDENTITY))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorSalida.UpdateSalida(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
