﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class DestajoController : Controller
    {
        private Mdl_Destajo ControladorDestajo;
        private Mdl_Tipo_Destajo ControladorTipoDestajo;
        public DestajoController()
        {
            ControladorDestajo = new Mdl_Destajo();
            ControladorTipoDestajo = new Mdl_Tipo_Destajo();
        }

        [SessionExpire]
        public ActionResult Destajo()
        {
            return View();
        }

        [SessionExpire]
        public ActionResult DestajoCorte()
        {
            return View();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvents
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        public JsonResult GetEvents()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorDestajo.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {                                      
                                     Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"),                                      
                                     Fila.Field<String>("Tipo_Destajo").Trim(),             
                                     Fila.Field<Decimal>("Cantidad").ToString(),                                         
                                     Fila.Field<int>("No_Destajo").ToString() 
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public JsonResult GetAgrupadoTDCorte(Mdl_Destajo Datos)
        {
            DataTable Dt_Registros = new DataTable();

            ControladorDestajo = Datos;
            Dt_Registros = ControladorDestajo.ConsultAgrupadoTDCorte();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                         "NO",
                                         Fila.Field<String>("Tipo_Destajo_ID").ToString(),
                                         Fila.Field<String>("Tipo_Destajo").ToString(),
                                         Fila.Field<Decimal>("Cantidad_Empleados").ToString(),
                                         Fila.Field<Decimal>("Cantidad").ToString(),
                                         Fila.Field<Decimal>("Total_Pago").ToString()
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvent
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorDestajo = new Mdl_Destajo();
                ControladorDestajo.No_Destajo = Evento_ID;
                Dt_Registros = ControladorDestajo.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public String EventMaster(Mdl_Destajo Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Tipo_Destajo = new DataTable();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            try
            {
                Datos.Usuario_Registro = SessionSRG.Nombre;
                Datos.Fecha_Registro = "GETDATE()";                
                ControladorDestajo = Datos;

                if (!String.IsNullOrEmpty(ControladorDestajo.No_Empleado))
                    ControladorDestajo.No_Empleado = int.Parse(ControladorDestajo.No_Empleado).ToString("00000");
                if (!String.IsNullOrEmpty(ControladorDestajo.Tabla_ID))
                    ControladorDestajo.Tabla_ID = int.Parse(ControladorDestajo.Tabla_ID).ToString("00000");
                ControladorDestajo.Tipo_Destajo_ID = int.Parse(ControladorDestajo.Tipo_Destajo_ID).ToString("00000");

                ControladorTipoDestajo.Tipo_Destajo_ID = ControladorDestajo.Tipo_Destajo_ID;
                Dt_Tipo_Destajo = ControladorTipoDestajo.Consult();
                if (Dt_Tipo_Destajo != null && Dt_Tipo_Destajo.Rows.Count > 0 )
                {
                    ControladorDestajo.Pago = decimal.Parse(Dt_Tipo_Destajo.Rows[0]["Pago"].ToString()) * ControladorDestajo.Cantidad_Empleados;
                    ControladorDestajo.Total_Pago = decimal.Parse(Dt_Tipo_Destajo.Rows[0]["Pago"].ToString()) * ControladorDestajo.Cantidad;
                }
                if (Datos.Captura.Equals("I"))
                {
                    ControladorDestajo.No_Destajo = ControladorDestajo.ConsultMaximoDestajo().Rows[0]["No_Destajo"].ToString();
                    if (ControladorDestajo.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorDestajo.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
