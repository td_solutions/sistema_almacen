﻿using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class TipoInsumoController : Controller
    {
        private Mdl_Tipo_Insumo ControladorTipoInsumo;
        public TipoInsumoController()
        {
            ControladorTipoInsumo = new Mdl_Tipo_Insumo();
        }
        //
        // GET: /TipoInsumo/
        [SessionExpire]
        public ActionResult TipoInsumo()
        {
            return View();
        }
        [SessionExpire]
        public JsonResult Consulta_Catalogo()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorTipoInsumo.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                     Fila.Field<String>("Nombre").Trim(),
                                     Fila.Field<String>("Descripcion").ToString(),
                                     Fila.Field<String>("Estatus").Trim(),
                                     Fila.Field<Int32>("Tipo_Insumo_ID").ToString()
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public ActionResult GetTipoIsnumoList(String q)
        {
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorTipoInsumo.Estatus = "ACTIVO";
                Dt_Registros = ControladorTipoInsumo.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<Int32>("Tipo_Insumo_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorTipoInsumo = new Mdl_Tipo_Insumo();
                ControladorTipoInsumo.Tipo_Insumo_ID = Int32.Parse(Evento_ID);
                Dt_Registros = ControladorTipoInsumo.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public String EventMaster(Mdl_Tipo_Insumo Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            try
            {
                Datos.Usuario_Registro = SessionSRG.Nombre;
                Datos.Fecha_Registro = "GETDATE()";
                ControladorTipoInsumo = Datos;
                if (Datos.Captura.Equals("I"))
                {
                    //ControladorTipoInsumo.Tipo_Insumo_ID = int.Parse(ControladorTipoInsumo.ConsultMaximoTipoDestajo().Rows[0]["Tipo_Destajo_ID"].ToString()).ToString("00000");
                    if (ControladorTipoInsumo.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA_IDENTITY))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorTipoInsumo.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
