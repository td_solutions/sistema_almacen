﻿using Newtonsoft.Json;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Deal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class InsumosController : Controller
    {
        private Mdl_Insumos ControladorInsumo;
        public InsumosController()
        {
            ControladorInsumo = new Mdl_Insumos();
        }
        //
        // GET: /Insumos/
        [SessionExpire]
        public ActionResult Insumos()
        {
            return View();
        }
        public JsonResult Consulta_Catalogo()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorInsumo.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                     Fila.Field<String>("Nombre").Trim(),
                                     Fila.Field<String>("Estatus").Trim(),
                                     Fila.Field<String>("Tipo_Insumo").ToString(),
                                     Fila.Field<Decimal>("Total").ToString(),
                                     Fila.Field<String>("Proveedor").ToString(),
                                     Fila.Field<Int32>("Insumo_ID").ToString()
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorInsumo = new Mdl_Insumos();
                ControladorInsumo.Insumo_ID = Int32.Parse(Evento_ID);
                Dt_Registros = ControladorInsumo.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }                                                                                                                                                                                                                                  
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public ActionResult GetInsumosList(String q)
        {
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorInsumo.Estatus = "ACTIVO";
                Dt_Registros = ControladorInsumo.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<Int32>("Insumo_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                        Data2 = row.Field<Int32>("Unidad_ID").ToString().Trim(),
                        Data3 = row.Field<String>("Unidad").ToString().Trim(),
                        Data4 = row.Field<Decimal>("Existencias").ToString().Trim(),
                        Data5 = row.Field<Decimal>("Total").ToString().Trim(),
                        Data6 = row.Field<Int32>("Unidad_Consumo_ID").ToString().Trim(),
                        Data7 = row.Field<String>("Unidad_Consumo").ToString().Trim()
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public ActionResult ObtenerListaInsumos(String q)
        {
            DataTable Dt_Registros = new DataTable();
            List<Select2Insumos> list = new List<Select2Insumos>();
            try
            {
                ControladorInsumo.Estatus = "ACTIVO";
                Dt_Registros = ControladorInsumo.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Insumos
                    {
                        id = row.Field<Int32>("Insumo_ID").ToString(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                        Insumo_ID = row.Field<Int32>("Insumo_ID"),
                        Nombre = row.Field<String>("Nombre").ToString().Trim(),
                        Unidad_ID = row.Field<Int32>("Unidad_ID"),
                        Unidad = row.Field<String>("Unidad").ToString().Trim(),
                        Existencias = row.Field<Decimal>("Existencias"),
                        Equivalencia = row.Field<Decimal>("Equivalencia"),
                        Unidad_Consumo_ID = row.Field<Int32>("Unidad_Consumo_ID"),
                        Unidad_Consumo = row.Field<String>("Unidad_Consumo").ToString().Trim(),
                        Costo = row.Field<Decimal>("Costo"),
                        Total = row.Field<Decimal>("Total"),
                        Iva = row.Field<Decimal>("IVA"),
                        Ieps = row.Field<Decimal>("IEPS"),
                        Descripcion = row.Field<String>("Descripcion").ToString().Trim(),
                        Estatus = row.Field<String>("Estatus").ToString().Trim(),
                        Tipo_Insumo_ID = row.Field<Int32>("Tipo_Insumo_ID"),
                        Maximo = row.Field<Int32>("Maximo"),
                        Minimo = row.Field<Int32>("Minimo"),
                        Punto_Reorden = row.Field<Int32>("Punto_Reorden")
                        //Ubicacion = row.Field<String>("Ubicacion").ToString().Trim()
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.Nombre.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public String EventMaster(Mdl_Insumos Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            try
            {
                Datos.Usuario_Registro = SessionSRG.Nombre;
                Datos.Fecha_Registro = "GETDATE()";
                ControladorInsumo = Datos;
                if (Datos.Captura.Equals("I"))
                {
                    //ControladorTipoInsumo.Tipo_Insumo_ID = int.Parse(ControladorTipoInsumo.ConsultMaximoTipoDestajo().Rows[0]["Tipo_Destajo_ID"].ToString()).ToString("00000");
                    if (ControladorInsumo.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA_IDENTITY))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorInsumo.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
