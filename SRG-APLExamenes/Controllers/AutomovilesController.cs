﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class AutomovilesController : Controller
    {
        private Mdl_Automoviles ControladorAutomoviles;
        private Mdl_Automoviles_Tablas ControladorAutomovilesTablas;
        public AutomovilesController()
        {
            ControladorAutomoviles = new Mdl_Automoviles();
            ControladorAutomovilesTablas = new Mdl_Automoviles_Tablas();
        }

        [SessionExpire]
        public ActionResult Automoviles()
        {
            return View();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvents
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        public JsonResult GetEvents()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorAutomoviles.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                     Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"), 
                                     Fila.Field<String>("Unidad_Auto").Trim(),                                                  
                                     Fila.Field<String>("Empleado").Trim(),                                                                               
                                     Fila.Field<String>("Nivel_Gasolina").Trim(),     
                                     Fila.Field<Decimal>("Horas_Uso").ToString(), 
                                     Fila.Field<String>("Labor").ToString(),    
                                     Fila.Field<int>("No_Automovil").ToString() 
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public JsonResult GetAutomovilesTablas(Mdl_Automoviles_Tablas Datos)
        {
            DataTable Dt_Registros = new DataTable();

            ControladorAutomovilesTablas = Datos;
            Dt_Registros = ControladorAutomovilesTablas.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                         Fila.Field<String>("Tabla").ToString()
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvent
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorAutomoviles = new Mdl_Automoviles();
                ControladorAutomoviles.No_Automovil = Evento_ID;
                Dt_Registros = ControladorAutomoviles.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public String EventMaster(String Datos, String Tablas)
        {
            List<Mdl_Automoviles_Tablas> Obj_Tablas = new List<Mdl_Automoviles_Tablas>();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;

            try
            {
                ControladorAutomoviles = JsonConvert.DeserializeObject<Mdl_Automoviles>(Datos);
                Obj_Tablas = JsonConvert.DeserializeObject<List<Mdl_Automoviles_Tablas>>(Tablas);

                ControladorAutomoviles.Usuario_Registro = SessionSRG.Nombre;
                ControladorAutomoviles.Fecha_Registro = "GETDATE()";

                if (!String.IsNullOrEmpty(ControladorAutomoviles.No_Empleado))
                    ControladorAutomoviles.No_Empleado = int.Parse(ControladorAutomoviles.No_Empleado).ToString("00000");

                if (ControladorAutomoviles.Captura.Equals("I"))
                {
                    ControladorAutomoviles.No_Automovil = ControladorAutomoviles.ConsultMaximoAutomovil().Rows[0]["No_Automovil"].ToString();
                    if (ControladorAutomoviles.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA))
                    {
                        if (Obj_Tablas.Count > 0)
                        {
                            for (int i = 0; i < Obj_Tablas.Count; i++)
                            {
                                ControladorAutomovilesTablas.No_Automovil = null;
                                ControladorAutomovilesTablas.Tabla_ID = null;

                                ControladorAutomovilesTablas.No_Automovil = ControladorAutomoviles.No_Automovil;
                                ControladorAutomovilesTablas.Tabla_ID = Obj_Tablas[i].Tabla_ID;
                                ControladorAutomovilesTablas.Captura = Obj_Tablas[i].Captura;
                                if (ControladorAutomovilesTablas.Captura.Equals("I"))
                                {
                                    ControladorAutomovilesTablas.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA);
                                }
                                else
                                {
                                    ControladorAutomovilesTablas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA);
                                }
                            }
                        }
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorAutomoviles.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
