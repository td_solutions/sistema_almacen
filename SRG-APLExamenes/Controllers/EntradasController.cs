﻿using Newtonsoft.Json;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Ayudante;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Models.Negocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class EntradasController : Controller
    {
        private Mdl_Entradas ControladorEntrada;
        public EntradasController()
        {
            ControladorEntrada = new Mdl_Entradas();
        }
        //
        // GET: /Entradas/
        [SessionExpire]
        public ActionResult Entradas()
        {
            return View();
        }
        public JsonResult Traer_Entrada()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorEntrada.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                     Fila.Field<Int32>("No_Entrada").ToString(),
                                     Fila.Field<DateTime>("Fecha_Entrada").ToString(),
                                     Fila.Field<String>("proveedor").ToString(),
                                     Fila.Field<String>("Estatus").ToString()
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorEntrada = new Mdl_Entradas();
                ControladorEntrada.No_Entrada = Int32.Parse(Evento_ID);
                Dt_Registros = ControladorEntrada.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public JsonResult GetDetalles(String No_Entrada)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorEntrada = new Mdl_Entradas();
                ControladorEntrada.No_Entrada = Int32.Parse(No_Entrada);
                Dt_Registros = ControladorEntrada.ConsultDet();
                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] {
                                     Fila.Field<Decimal>("Cantidad").ToString(),
                                     Fila.Field<String>("Unidad").ToString(),
                                     Fila.Field<String>("Insumo").ToString(),
                                     Fila.Field<Decimal>("Precio").ToString(),
                                     Fila.Field<Decimal>("Importe").ToString(),
                                     Fila.Field<Decimal>("Total").ToString(),
                                     Fila.Field<Decimal>("Iva").ToString(),
                                     Fila.Field<Decimal>("Ieps").ToString(),
                                     Fila.Field<Int32>("Insumo_ID").ToString()
                                 };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }
        [SessionExpire]
        [HttpPost]
        public String Imprimir_Formato(String Evento_ID)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            string jsonEntrada;
            JsonResult standByMe;
            Respuesta MensajeServidor = new Respuesta();
            List<Mdl_Entradas> entradas;
            List<Mdl_Entradas_Detalles> entradasDetalles;
            Mdl_Entradas entrada;
            DataTable Dt_Registros = new DataTable();
            String StringJsonDetalles;
            string json;
            try
            {
                standByMe = GetEvent(Evento_ID);
                json = new JavaScriptSerializer().Serialize(standByMe.Data);
                jsonEntrada = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(json).items.ToString();
                MensajeServidor = JsonConvert.DeserializeObject<Respuesta>(jsonEntrada);
                entradas = JsonConvert.DeserializeObject<List<Mdl_Entradas>>(MensajeServidor.Data);

                Dt_Registros = ControladorEntrada.ConsultDet();
                StringJsonDetalles = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                                
                entradasDetalles = JsonConvert.DeserializeObject<List<Mdl_Entradas_Detalles>>(StringJsonDetalles);

                entrada = entradas[0];
                entrada.Detalles = entradasDetalles;


                String ruta_pdf = Cls_Rpt_Formato_Entradas.Obtener_Pdf(entrada);
                ObjMensajeServidor.Url_PDF = ruta_pdf;
                ObjMensajeServidor.Estatus = true;
                ObjMensajeServidor.Mensaje = "Registro exitoso.";
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
        [SessionExpire]
        [HttpPost]
        public String EventMaster(Mdl_Entradas Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            int Entrada_Registro = 0;

            DateTime Fecha_Registro = DateTime.MinValue;
            DateTime Fecha_Factura = DateTime.MinValue; 
            DateTime Fecha_Recepcion = DateTime.MinValue;
            DateTime Fecha_Pago = DateTime.MinValue;

            try
            {
                Datos.Usuario_Registro = SessionSRG.Nombre;
                try { Fecha_Registro = DateTime.ParseExact(Datos.Str_Fecha_Registro, "dd/MM/yyyy", CultureInfo.InvariantCulture); } catch { }
                try { Fecha_Factura = DateTime.ParseExact(Datos.Str_Fecha_Factura, "dd/MM/yyyy", CultureInfo.InvariantCulture); } catch { }
                try { Fecha_Recepcion = DateTime.ParseExact(Datos.Str_Fecha_Recepcion, "dd/MM/yyyy", CultureInfo.InvariantCulture); } catch { }
                try { Fecha_Pago = DateTime.ParseExact(Datos.Str_Fecha_Pago, "dd/MM/yyyy", CultureInfo.InvariantCulture); } catch { }

                Datos.Fecha_Registro = Fecha_Registro;
                Datos.Fecha_Factura = Fecha_Factura;
                Datos.Fecha_Recepcion = Fecha_Recepcion;
                Datos.Fecha_Pago = Fecha_Pago;

                ControladorEntrada = Datos;
                if (Datos.Captura.Equals("I"))
                {
                    Entrada_Registro = ControladorEntrada.AltaEntrada(MODO_DE_CAPTURA.CAPTURA_ALTA_IDENTITY);
                    if (Entrada_Registro>0)
                    {
                        ControladorEntrada.No_Entrada = Entrada_Registro;
                        String ruta_pdf = Cls_Rpt_Formato_Entradas.Obtener_Pdf(ControladorEntrada);
                        ObjMensajeServidor.Url_PDF = ruta_pdf;
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorEntrada.UpdateEntrada(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }

    }
}
