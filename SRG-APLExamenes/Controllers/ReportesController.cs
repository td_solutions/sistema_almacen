﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;
using SRG_APLExamenes.Reportes.Excel;
using System.Globalization;

namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class ReportesController : Controller
    {
        private Mdl_Destajo ControladorDestajo;
        private Mdl_Entradas ControladorEntradas;
        private Mdl_Salidas ControladorSalidas;
        private Mld_Rpt_Kardex ControladorKardex;
        private Mdl_Fumigacion ControladorFumigacion;
        private Mdl_Riego ControladorRiego;
        private Mdl_Automoviles ControladorAutomoviles;
        private Mdl_Actividades ControladorActividades;
        private Mdl_Facturas ControladorFacturas;
        private Mdl_Notas_Salida ControladorNotasSalidas;
        public ReportesController()
        {
            ControladorDestajo = new Mdl_Destajo();
            ControladorFumigacion = new Mdl_Fumigacion();
            ControladorRiego = new Mdl_Riego();
            ControladorActividades = new Mdl_Actividades();
            ControladorFacturas = new Mdl_Facturas();
            ControladorNotasSalidas = new Mdl_Notas_Salida();
        }

        #region REPORTE ENTRADAS
        [SessionExpire]
        public ActionResult RptEntradas()
        {
            return View();
        }

        [SessionExpire]
        public JsonResult GetReporteEntradas(Mdl_Entradas Datos)
        {
            DataTable Dt_Conciliacion = new DataTable();
            DataTable Dt_Registros = new DataTable();
            DateTime Fecha_Inicio = DateTime.MinValue;
            DateTime Fecha_Fin = DateTime.MinValue;
            try { Fecha_Inicio = DateTime.ParseExact(Datos.Str_Fecha_Inicio, "dd/MM/yyyy", CultureInfo.InvariantCulture); } catch { }
            try { Fecha_Fin = DateTime.ParseExact(Datos.Str_Fecha_Fin, "dd/MM/yyyy", CultureInfo.InvariantCulture); } catch { }
            Datos.Fecha_Inicio = Fecha_Inicio;
            Datos.Fecha_Fin = Fecha_Fin;
            ControladorEntradas = Datos;
            Dt_Registros = ControladorEntradas.ConsultarReporte();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                         Fila.Field<int>("no_entrada").ToString(),
                                         Fila.Field<DateTime>("fecha_entrada").ToString("dd-MMM-yyyy"),
                                         Fila.Field<String>("proveedor") == null ? "" : Fila.Field<String>("proveedor").Trim(),
                                         Fila.Field<String>("almacen") == null ? "" : Fila.Field<String>("almacen").Trim(),
                                         Fila.Field<String>("tipo_entrada").Trim(),
                                         Fila.Field<decimal>("subtotal").ToString(),
                                         Fila.Field<decimal>("iva").ToString(),
                                         Fila.Field<decimal>("ieps").ToString(),
                                         Fila.Field<decimal>("total").ToString()
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public ActionResult Download_Reporte_Entradas(Mdl_Entradas Datos)
        {
            DataTable Dt_Registros = new DataTable();
            string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Entradas.xlsx";
            string nombre_archivo = "Rpt_Entradas.xlsx";
            string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

            try
            {
                ControladorEntradas = Datos;
                Dt_Registros = ControladorEntradas.ConsultaDetalleReporte();

                Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                Obj_Reporte_Pago.Elaborar_Reporte_Entradas(ControladorEntradas);
                this.Response.Clear();
                this.Response.ContentType = "application/vnd.ms-excel";
                this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                this.Response.WriteFile(ruta_almacenamiento);
                this.Response.End();
            }
            catch 
            {
                return RedirectToAction("RptEntradas", "Reportes");
            }
            return null;
        }
        #endregion
        
        #region REPORTE SALIDAS
        [SessionExpire]
        public ActionResult RptSalidas()
        {
            return View();
        }

        [SessionExpire]
        public JsonResult GetReporteSalidas(Mdl_Salidas Datos)
        {
            DataTable Dt_Conciliacion = new DataTable();
            DataTable Dt_Registros = new DataTable();
            DateTime Fecha_Inicio = DateTime.MinValue;
            DateTime Fecha_Fin = DateTime.MinValue;
            try { Fecha_Inicio = DateTime.ParseExact(Datos.Str_Fecha_Inicio, "dd/MM/yyyy", CultureInfo.InvariantCulture); } catch { }
            try { Fecha_Fin = DateTime.ParseExact(Datos.Str_Fecha_Fin, "dd/MM/yyyy", CultureInfo.InvariantCulture); } catch { }
            Datos.Fecha_Inicio = Fecha_Inicio;
            Datos.Fecha_Fin = Fecha_Fin;
            ControladorSalidas = Datos;
            Dt_Registros = ControladorSalidas.ConsultarReporte();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                         Fila.Field<int>("No_Salida").ToString(),
                                         Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"),
                                         Fila.Field<String>("Estatus").ToString(),
                                         Fila.Field<String>("Almacen") == null ? "" : Fila.Field<String>("almacen").Trim(),
                                         Fila.Field<String>("Tipo").Trim(),
                                         Fila.Field<decimal>("IVA").ToString(),
                                         Fila.Field<decimal>("IEPS").ToString(),
                                         Fila.Field<decimal>("Costo_Total").ToString()
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public ActionResult Download_Reporte_Salidas(Mdl_Salidas Datos)
        {
            DataTable Dt_Registros = new DataTable();
            string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Salidas.xlsx";
            string nombre_archivo = "Rpt_Salidas.xlsx";
            string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

            try
            {
                ControladorSalidas = Datos;
                Dt_Registros = ControladorSalidas.ConsultarReporteDetalles();

                Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                Obj_Reporte_Pago.Elaborar_Reporte_Salidas(ControladorSalidas);
                this.Response.Clear();
                this.Response.ContentType = "application/vnd.ms-excel";
                this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                this.Response.WriteFile(ruta_almacenamiento);
                this.Response.End();
            }
            catch
            {
                return RedirectToAction("RptSalidas", "Reportes");
            }
            return null;
        }
        #endregion

        #region REPORTE KARDEX
        [SessionExpire]
        public ActionResult RptKardexInsumo()
        {
            return View();
        }

        [SessionExpire]
        public JsonResult GetReporteKardex(Mld_Rpt_Kardex Datos)
        {
            DataTable Dt_Registros = new DataTable();
            ControladorKardex = Datos;
            
            Dt_Registros = ControladorKardex.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] {
                                         Fila.Field<Object>("Fecha") == null ? "" : Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"),
                                         Fila.Field<Object>("NumMovimiento") == null ? "" : Fila.Field<int>("NumMovimiento").ToString(),
                                         Fila.Field<Object>("Movimiento") == null ? "" : Fila.Field<String>("Movimiento").Trim(),
                                         Fila.Field<Object>("TipoMovimiento") == null ? "" : Fila.Field<String>("TipoMovimiento").Trim(),
                                         Fila.Field<Object>("Unidades") == null ? "" : Fila.Field<decimal>("Unidades").ToString(),
                                         Fila.Field<Object>("PrecioVenta") == null ? "" : Fila.Field<decimal>("PrecioVenta").ToString(),
                                         Fila.Field<Object>("CostoCompra") == null ? "" : Fila.Field<decimal>("CostoCompra").ToString(),
                                         Fila.Field<Object>("Total") == null ? "" : Fila.Field<decimal>("Total").ToString()
                                     };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        public ActionResult Download_Reporte_Kardex(string Insumo_ID, string Fecha_Inicio, string Fecha_Fin)
        {
            string Mensaje = "";

            Mld_Rpt_Kardex Datos = new Mld_Rpt_Kardex( DateTime.Parse(Fecha_Inicio),DateTime.Parse( Fecha_Fin), int.Parse(Insumo_ID));
            DataTable Dt_Registros = new DataTable();
            string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Kardex.xlsx";
            string nombre_archivo = "Rpt_Kardex.xlsx";
            string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

            try
            {
                ControladorKardex = Datos;
                Dt_Registros = ControladorKardex.ConsultarReporteDetalles();

                Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                Obj_Reporte_Pago.Elaborar_Reporte_Kardex(ControladorKardex);
                this.Response.Clear();
                this.Response.ContentType = "application/vnd.ms-excel";
                this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                this.Response.WriteFile(ruta_almacenamiento);
                this.Response.End();
            }
            catch(Exception Ex)
            {
                Mensaje = "Consultar Evento [" + Ex.Message + "]";
                return RedirectToAction("RptKardexInsumo", "Reportes");
            }
            return null;
        }
        #endregion

        #region REPORTE DESTAJO
            [SessionExpire]
            public ActionResult RptDestajo()
            {
                return View();
            }

            [SessionExpire]
            public JsonResult GetReporteDestajo(Mdl_Destajo Datos)
            {
                DataTable Dt_Conciliacion = new DataTable();
                DataTable Dt_Registros = new DataTable();

                Datos.Cantidad_Empleados = 1;
                ControladorDestajo = Datos;
                Dt_Registros = ControladorDestajo.Consult();

                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] { 
                                         Fila.Field<int>("No_Destajo").ToString(),
                                         Fila.Field<DateTime>("Fecha").ToString("dd/MM/yyyy"), //.ToShortDateString(),//Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"),                                   
                                         Fila.Field<object>("No_Tarjeta") == null ? "" : Fila.Field<decimal>("No_Tarjeta").ToString(),
                                         Fila.Field<String>("Empleado") == null ? "" : Fila.Field<String>("Empleado").Trim(),     
                                         Fila.Field<String>("Tipo_Destajo").Trim(),                                                                                                                        
                                         Fila.Field<decimal>("Cantidad").ToString(),
                                         Fila.Field<decimal>("Pago").ToString(),
                                         Fila.Field<decimal>("Total_Pago").ToString()
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }

            [SessionExpire]
            public ActionResult Download_Reporte_Destajo(Mdl_Destajo Datos)
            {
                DataTable Dt_Registros = new DataTable();
                string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Destajo.xlsx";
                string nombre_archivo = "Rpt_Destajo.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                try
                {
                    Datos.Cantidad_Empleados = 1;
                    ControladorDestajo = Datos;
                    Dt_Registros = ControladorDestajo.Consult();

                    Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                    Obj_Reporte_Pago.Elaborar_Reporte_Destajo(ControladorDestajo);
                    this.Response.Clear();
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                    this.Response.WriteFile(ruta_almacenamiento);
                    this.Response.End();
                }
                catch
                {
                    return RedirectToAction("Reportes", "RptDestajo");
                }
                return null;
            }
        #endregion

        #region REPORTE CORTE
            [SessionExpire]
            public ActionResult RptCorte()
            {
                return View();
            }

            [SessionExpire]
            public JsonResult GetReporteCorte(Mdl_Destajo Datos)
            {
                DataTable Dt_Conciliacion = new DataTable();
                DataTable Dt_Registros = new DataTable();

                Datos.Cantidad_Empleados = 2;
                ControladorDestajo = Datos;
                Dt_Registros = ControladorDestajo.Consult();

                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] { 
                                         Fila.Field<int>("No_Destajo").ToString(),
                                         Fila.Field<DateTime>("Fecha").ToString("dd/MM/yyyy"), //.ToShortDateString(),//Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"),                                   
                                         Fila.Field<String>("Tabla") == null ? "" : Fila.Field<String>("Tabla").Trim(),
                                         Fila.Field<String>("Tipo_Destajo").Trim(),                                                                                                                        
                                         Fila.Field<object>("Cantidad_Empleados") == null ? "" : Fila.Field<decimal>("Cantidad_Empleados").ToString(),
                                         Fila.Field<decimal>("Cantidad").ToString()                                         
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }

            [SessionExpire]
            public ActionResult Download_Reporte_Corte(Mdl_Destajo Datos)
            {
                DataTable Dt_Registros = new DataTable();
                string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Corte.xlsx";
                string nombre_archivo = "Rpt_Corte.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                try
                {
                    Datos.Cantidad_Empleados = 2;
                    ControladorDestajo = Datos;
                    Dt_Registros = ControladorDestajo.Consult();

                    Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                    Obj_Reporte_Pago.Elaborar_Reporte_Corte(ControladorDestajo);
                    this.Response.Clear();
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                    this.Response.WriteFile(ruta_almacenamiento);
                    this.Response.End();
                }
                catch
                {
                    return RedirectToAction("Reportes", "RptDestajo");
                }
                return null;
            }
        #endregion

        #region REPORTE FUMIGACION
            [SessionExpire]
            public ActionResult RptFumigacion()
            {
                return View();
            }

            [SessionExpire]
            public JsonResult GetReporteFumigacion(Mdl_Fumigacion Datos)
            {
                Mdl_Fumigacion_Detalles Obj_Detalles = new Mdl_Fumigacion_Detalles();
                Mdl_Fumigacion_Tablas Obj_Tablas = new Mdl_Fumigacion_Tablas();
                DataTable Dt_Registros = new DataTable();
                DataTable Dt_Registros_Detalles = new DataTable();
                DataTable Dt_Registros_Tablas = new DataTable();
                DataTable Dt_Datos = new DataTable();
                DataRow Dr_Registro;
                int No_Registros;
                Dt_Datos.Columns.Add("ID");
                Dt_Datos.Columns.Add("Fecha");
                Dt_Datos.Columns.Add("Avance");
                Dt_Datos.Columns.Add("Pago_Por");
                Dt_Datos.Columns.Add("Total_Empleados");
                Dt_Datos.Columns.Add("Litros");
                //Dt_Datos.Columns.Add("Cantidad");
                //Dt_Datos.Columns.Add("Total");
                //Dt_Datos.Columns.Add("Pago");
                //Dt_Datos.Columns.Add("No_Tarjeta");
                //Dt_Datos.Columns.Add("Nombre");
                //Dt_Datos.Columns.Add("Tabla");
                //Dt_Datos.Columns.Add("No_Tuneles");

                ControladorFumigacion = Datos;
                Dt_Registros = ControladorFumigacion.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Registros.Rows)
                    {
                        //Obj_Tablas.No_fumigacion = Dr["No_Fumigacion"].ToString();
                        //Dt_Registros_Tablas = Obj_Tablas.Consult();
                        //Obj_Detalles.No_Fumigacion = Dr["No_Fumigacion"].ToString();
                        //Dt_Registros_Detalles = Obj_Detalles.Consult();
                        //if ((Dt_Registros_Tablas != null && Dt_Registros_Tablas.Rows.Count > 0) || (Dt_Registros_Detalles != null && Dt_Registros_Detalles.Rows.Count > 0))
                        //{
                        //    if (Dt_Registros_Tablas.Rows.Count > Dt_Registros_Detalles.Rows.Count)
                        //        No_Registros = Dt_Registros_Tablas.Rows.Count;
                        //    else
                        //        No_Registros = Dt_Registros_Detalles.Rows.Count;

                        //    for (int i = 0; i < No_Registros; i++)
                        //    {
                                Dr_Registro = Dt_Datos.NewRow();
                                Dr_Registro["ID"] = Dr["No_Fumigacion"].ToString();
                                Dr_Registro["Fecha"] = DateTime.Parse(Dr["Fecha"].ToString()).ToString("dd/MM/yyyy"); //.ToString("dd-MMM-yyyy");
                                Dr_Registro["Avance"] = Decimal.Parse(Dr["Avance"].ToString()).ToString("N2");
                                Dr_Registro["Pago_Por"] = Dr["Pago_Por"].ToString();
                                Dr_Registro["Total_Empleados"] = Dr["Total_Empleados"].ToString();
                                Dr_Registro["Litros"] = Dr["Litros"].ToString();                                
                                //if ((i + 1) <= Dt_Registros_Detalles.Rows.Count)
                                //{
                                //    Dr_Registro["Cantidad"] = Dt_Registros_Detalles.Rows[i]["Cantidad"];
                                //    Dr_Registro["Total"] = (Double.Parse(Dt_Registros_Detalles.Rows[i]["Pago"].ToString()) / Double.Parse(Dt_Registros_Detalles.Rows[i]["Cantidad"].ToString())).ToString("N2");
                                //    Dr_Registro["Pago"] = Dt_Registros_Detalles.Rows[i]["Pago"];
                                //    Dr_Registro["No_Tarjeta"] = Dt_Registros_Detalles.Rows[i]["No_Tarjeta"];
                                //    Dr_Registro["Nombre"] = Dt_Registros_Detalles.Rows[i]["Nombre"];
                                //}
                                //else
                                //{
                                //    Dr_Registro["Cantidad"] = "";
                                //    Dr_Registro["Total"] = "";
                                //    Dr_Registro["Pago"] = "";
                                //    Dr_Registro["No_Tarjeta"] = "";
                                //    Dr_Registro["Nombre"] = "";
                                //}
                                //if ((i + 1) <= Dt_Registros_Tablas.Rows.Count)
                                //{
                                //    Dr_Registro["Tabla"] = Dt_Registros_Tablas.Rows[i]["Tabla"];
                                //    Dr_Registro["No_Tuneles"] = Dt_Registros_Tablas.Rows[i]["No_Tuneles"];
                                //}
                                //else
                                //{
                                //    Dr_Registro["Tabla"] = "";
                                //    Dr_Registro["No_Tuneles"] = "";
                                //}
                                Dt_Datos.Rows.Add(Dr_Registro);
                                Dt_Datos.AcceptChanges();
                            //}
                        //}
                    }
                }

                var result = from Fila in Dt_Datos.AsEnumerable()
                             select new[] { 
                                         Fila.Field<String>("ID").ToString(),
                                         Fila.Field<String>("Fecha").ToString(),   
                                         Fila.Field<String>("Avance").ToString(),   
                                         Fila.Field<String>("Pago_Por").ToString(),                                                                                                                                                 
                                         Fila.Field<String>("Total_Empleados").ToString(),
                                         Fila.Field<String>("Litros").ToString()
                                         //Fila.Field<String>("Cantidad").ToString(),
                                         //Fila.Field<String>("Total").ToString(),                                     
                                         //Fila.Field<String>("Pago").ToString(),                                                                                                                                                 
                                         //Fila.Field<String>("No_Tarjeta").ToString(),
                                         //Fila.Field<String>("Nombre").ToString(),
                                         //Fila.Field<String>("Tabla").ToString(),
                                         //Fila.Field<String>("No_Tuneles").ToString()
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }

            [SessionExpire]
            public ActionResult Download_Reporte_Fumigacion(Mdl_Fumigacion Datos)
            {
                DataTable Dt_Registros = new DataTable();
                string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Fumigacion.xlsx";
                string nombre_archivo = "Rpt_Fumigacion.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                try
                {
                    ControladorFumigacion = Datos;
                    Dt_Registros = ControladorFumigacion.Consult();

                    Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                    Obj_Reporte_Pago.Elaborar_Reporte_Fumigacion(ControladorFumigacion);
                    this.Response.Clear();
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                    this.Response.WriteFile(ruta_almacenamiento);
                    this.Response.End();
                }
                catch
                {
                    return RedirectToAction("Reportes", "RptFumigacion");
                }
                return null;
            }
        #endregion

        #region REPORTE RIEGO
            [SessionExpire]
            public ActionResult RptRiego()
            {
                return View();
            }

            [SessionExpire]
            public JsonResult GetReporteRiego(Mdl_Riego Datos)
            {
                Mdl_Riego_Detalles Obj_Detalles = new Mdl_Riego_Detalles();
                Mdl_Riego_Tablas Obj_Tablas = new Mdl_Riego_Tablas();
                DataTable Dt_Registros = new DataTable();
                DataTable Dt_Registros_Detalles = new DataTable();
                DataTable Dt_Registros_Tablas = new DataTable();
                DataTable Dt_Datos = new DataTable();
                DataRow Dr_Registro;
                //int No_Registros;
                Dt_Datos.Columns.Add("ID");
                Dt_Datos.Columns.Add("Fecha");
                Dt_Datos.Columns.Add("Horas");
                Dt_Datos.Columns.Add("Acido");
                Dt_Datos.Columns.Add("Total_Empleados");
                Dt_Datos.Columns.Add("Total");
                //Dt_Datos.Columns.Add("Pago");
                //Dt_Datos.Columns.Add("No_Tarjeta");
                //Dt_Datos.Columns.Add("Nombre");
                //Dt_Datos.Columns.Add("Tabla");                

                ControladorRiego = Datos;
                Dt_Registros = ControladorRiego.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Registros.Rows)
                    {
                        //Obj_Tablas.No_Riego = Dr["No_Riego"].ToString();
                        //Dt_Registros_Tablas = Obj_Tablas.Consult();
                        //Obj_Detalles.No_Riego = Dr["No_Riego"].ToString();
                        //Dt_Registros_Detalles = Obj_Detalles.Consult();
                        //if ((Dt_Registros_Tablas != null && Dt_Registros_Tablas.Rows.Count > 0) || (Dt_Registros_Detalles != null && Dt_Registros_Detalles.Rows.Count > 0))
                        //{
                            //if (Dt_Registros_Tablas.Rows.Count > Dt_Registros_Detalles.Rows.Count)
                            //    No_Registros = Dt_Registros_Tablas.Rows.Count;
                            //else
                            //    No_Registros = Dt_Registros_Detalles.Rows.Count;

                            //for (int i = 0; i < No_Registros; i++)
                            //{
                            //for (int i = 0; i < Dt_Registros.Rows.Count; i++)
                            //{
                                Dr_Registro = Dt_Datos.NewRow();
                                Dr_Registro["ID"] = Dr["No_Riego"].ToString();
                                Dr_Registro["Fecha"] = DateTime.Parse(Dr["Fecha"].ToString()).ToString("dd/MM/yyyy"); //.ToString("dd-MMM-yyyy");
                                Dr_Registro["Horas"] = Dr["Horas"].ToString();
                                Dr_Registro["Acido"] = Dr["Acido"].ToString();
                                Dr_Registro["Total_Empleados"] = Dr["Total_Empleados"].ToString();
                                Dr_Registro["Total"] = (Double.Parse(Dr["Horas"].ToString()) / Double.Parse(Dr["Total_Empleados"].ToString())).ToString("N2");
                                //if ((i+1) <= Dt_Registros_Detalles.Rows.Count)
                                //{
                                //    Dr_Registro["Pago"] = Dt_Registros_Detalles.Rows[i]["Pago"];
                                //    Dr_Registro["No_Tarjeta"] = Dt_Registros_Detalles.Rows[i]["No_Tarjeta"];
                                //    Dr_Registro["Nombre"] = Dt_Registros_Detalles.Rows[i]["Nombre"];
                                //}                                    
                                //else
                                //{
                                //    Dr_Registro["Pago"] = "";
                                //    Dr_Registro["No_Tarjeta"] = "";
                                //    Dr_Registro["Nombre"] = "";
                                //}
                                //if ((i + 1) <= Dt_Registros_Tablas.Rows.Count)
                                //    Dr_Registro["Tabla"] = Dt_Registros_Tablas.Rows[i]["Tabla"];
                                //else
                                //    Dr_Registro["Tabla"] = "";
                                Dt_Datos.Rows.Add(Dr_Registro);
                                Dt_Datos.AcceptChanges();
                        //    }
                        //}
                    }
                }
                var result = from Fila in Dt_Datos.AsEnumerable()
                             select new[] { 
                                         Fila.Field<String>("ID").ToString(),
                                         Fila.Field<String>("Fecha").ToString(),                                                                              
                                         Fila.Field<String>("Horas").ToString(),                                                                                              
                                         Fila.Field<String>("Acido").ToString(),
                                         Fila.Field<String>("Total_Empleados").ToString(),
                                         Fila.Field<String>("Total").ToString(),
                                         //Fila.Field<String>("Pago").ToString(),
                                         //Fila.Field<String>("No_Tarjeta").ToString(),
                                         //Fila.Field<String>("Nombre").ToString(),
                                         //Fila.Field<String>("Tabla").ToString()
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }

            [SessionExpire]
            public ActionResult Download_Reporte_Riego(Mdl_Riego Datos)
            {
                DataTable Dt_Registros = new DataTable();
                string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Riego.xlsx";
                string nombre_archivo = "Rpt_Riego.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                try
                {
                    ControladorRiego = Datos;
                    Dt_Registros = ControladorRiego.Consult();

                    Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                    Obj_Reporte_Pago.Elaborar_Reporte_Riego(ControladorRiego);
                    this.Response.Clear();
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                    this.Response.WriteFile(ruta_almacenamiento);
                    this.Response.End();
                }
                catch
                {
                    return RedirectToAction("Reportes", "RptRiego");
                }
                return null;
            }
        #endregion

        #region REPORTE AUTOMOVILES
            [SessionExpire]
            public ActionResult RptAutomoviles()
            {
                return View();
            }

            [SessionExpire]
            public JsonResult GetReporteAutomoviles(Mdl_Automoviles Datos)
            {
                Mdl_Automoviles_Tablas Obj_Tablas = new Mdl_Automoviles_Tablas();
                DataTable Dt_Registros = new DataTable();
                DataTable Dt_Registros_Detalles = new DataTable();
                DataTable Dt_Datos = new DataTable();          
                DataRow Dr_Registro;
                Dt_Datos.Columns.Add("ID");
                Dt_Datos.Columns.Add("Fecha");
                Dt_Datos.Columns.Add("No_Tarjeta");
                Dt_Datos.Columns.Add("Empleado");
                Dt_Datos.Columns.Add("Unidad_Auto");
                Dt_Datos.Columns.Add("Labor");
                Dt_Datos.Columns.Add("Nivel_Gasolina");
                Dt_Datos.Columns.Add("Horas_Uso");
                Dt_Datos.Columns.Add("Litros");
                Dt_Datos.Columns.Add("Precio_Litro");
                Dt_Datos.Columns.Add("Total");
                Dt_Datos.Columns.Add("Tabla");

                ControladorAutomoviles = Datos;
                Dt_Registros = ControladorAutomoviles.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Registros.Rows)
                    {
                        Obj_Tablas.No_Automovil = Dr["No_Automovil"].ToString();
                        Dt_Registros_Detalles = Obj_Tablas.Consult();
                        if (Dt_Registros_Detalles != null && Dt_Registros_Detalles.Rows.Count > 0)
                        {
                            foreach (DataRow Dr_Det in Dt_Registros_Detalles.Rows)
                            {
                                Dr_Registro = Dt_Datos.NewRow();
                                Dr_Registro["ID"] = Dr["No_Automovil"].ToString();
                                Dr_Registro["Fecha"] = DateTime.Parse(Dr["Fecha"].ToString()).ToString("dd/MM/yyyy"); //.ToString("dd-MMM-yyyy");
                                Dr_Registro["No_Tarjeta"] = Dr["No_Tarjeta"].ToString();
                                Dr_Registro["Empleado"] = Dr["Empleado"].ToString();
                                Dr_Registro["Unidad_Auto"] = Dr["Unidad_Auto"].ToString();
                                Dr_Registro["Labor"] = Dr["Labor"];
                                Dr_Registro["Nivel_Gasolina"] = Dr["Nivel_Gasolina"];
                                Dr_Registro["Horas_Uso"] = Dr["Horas_Uso"];
                                Dr_Registro["Litros"] = Dr["Litros"];
                                Dr_Registro["Precio_Litro"] = Dr["Precio_Litro"];
                                Dr_Registro["Total"] = (Double.Parse(Dr["LItros"].ToString()) * Double.Parse(Dr["Precio_Litro"].ToString())).ToString();
                                Dr_Registro["Tabla"] = Dr_Det["Tabla"].ToString();
                                Dt_Datos.Rows.Add(Dr_Registro);
                                Dt_Datos.AcceptChanges();
                            }
                        }
                    }
                }
                var result = from Fila in Dt_Datos.AsEnumerable()
                             select new[] { 
                                         Fila.Field<String>("ID").ToString(),
                                         Fila.Field<String>("Fecha").ToString(),                                     
                                         Fila.Field<String>("No_Tarjeta") == null ? "" : Fila.Field<String>("No_Tarjeta").ToString(),  
                                         Fila.Field<String>("Empleado") == null ? "" : Fila.Field<String>("Empleado").Trim(),  
                                         Fila.Field<String>("Unidad_Auto").Trim(),
                                         Fila.Field<String>("Labor").Trim(),
                                         Fila.Field<String>("Nivel_Gasolina").Trim(),                                                                                                                        
                                         Fila.Field<String>("Horas_Uso").ToString(),
                                         Fila.Field<String>("Litros").ToString(),
                                         Fila.Field<String>("Precio_Litro").ToString(),
                                         Fila.Field<String>("Total").ToString(),
                                         Fila.Field<String>("Tabla").Trim()
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }

            [SessionExpire]
            public ActionResult Download_Reporte_Automoviles(Mdl_Automoviles Datos)
            {
                DataTable Dt_Registros = new DataTable();
                string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Automoviles.xlsx";
                string nombre_archivo = "Rpt_Automoviles.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                try
                {
                    ControladorAutomoviles = Datos;
                    Dt_Registros = ControladorAutomoviles.Consult();

                    Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                    Obj_Reporte_Pago.Elaborar_Reporte_Automoviles(ControladorAutomoviles);
                    this.Response.Clear();
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                    this.Response.WriteFile(ruta_almacenamiento);
                    this.Response.End();
                }
                catch
                {
                    return RedirectToAction("Reportes", "RptDestajo");
                }
                return null;
            }
        #endregion

        #region REPORTE ACTIVIDADES
            [SessionExpire]
            public ActionResult RptActividades()
            {
                return View();
            }

            [SessionExpire]
            public JsonResult GetReporteActividades(Mdl_Actividades Datos)
            {
                DataTable Dt_Conciliacion = new DataTable();
                DataTable Dt_Registros = new DataTable();

                ControladorActividades = Datos;
                Dt_Registros = ControladorActividades.Consult();

                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] { 
                                         Fila.Field<int>("No_Actividad").ToString(),
                                         Fila.Field<DateTime>("Fecha").ToString("dd/MM/yyyy"), //.ToShortDateString(),//Fila.Field<DateTime>("Fecha").ToString("dd-MMM-yyyy"), 
                                         Fila.Field<object>("Avance") == null ? "" : Fila.Field<int>("Avance").ToString("N2"),                                         
                                         //Fila.Field<decimal>("No_Tarjeta").ToString().Trim(),
                                         //Fila.Field<String>("Empleado") == null ? "" : Fila.Field<String>("Empleado").Trim(),                                          
                                         //Fila.Field<String>("Tabla") == null ? "" : Fila.Field<String>("Tabla").Trim(),
                                         //Fila.Field<object>("No_Tuneles") == null ? "" : Fila.Field<int>("No_Tuneles").ToString(),
                                         Fila.Field<String>("Labor").Trim(),
                                         //Fila.Field<decimal>("Monto").ToString().Trim()
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }

            [SessionExpire]
            public ActionResult Download_Reporte_Actividades(Mdl_Actividades Datos)
            {
                DataTable Dt_Registros = new DataTable();
                string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Actividades.xlsx";
                string nombre_archivo = "Rpt_Actividades.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                try
                {
                    ControladorActividades = Datos;
                    Dt_Registros = ControladorActividades.Consult();

                    Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                    Obj_Reporte_Pago.Elaborar_Reporte_Actividades(ControladorActividades);
                    this.Response.Clear();
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                    this.Response.WriteFile(ruta_almacenamiento);
                    this.Response.End();
                }
                catch
                {
                    return RedirectToAction("Reportes", "RptDestajo");
                }
                return null;
            }
        #endregion

        #region REPORTE ESTADO CUENTA PROVEEDORES
            [SessionExpire]
            public ActionResult RptEstadoCuentaProveedores()
            {
                return View();
            }

            [SessionExpire]
            public JsonResult GetReporteEstadoCuentaProveedores(Mdl_Facturas Datos)
            {
                DataTable Dt_Conciliacion = new DataTable();
                DataTable Dt_Registros = new DataTable();

                ControladorFacturas = Datos;
                Dt_Registros = ControladorFacturas.Consult();

                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] { 
                                         Fila.Field<String>("NO_FACTURA").ToString(),
                                         Fila.Field<String>("PROVEEDOR").ToString(),
                                         Fila.Field<DateTime>("FECHA").ToString("dd-MMM-yyyy"),                                     
                                         Fila.Field<DateTime>("FECHA_VENCIMIENTO").ToString("dd-MMM-yyyy"),                                     
                                         Fila.Field<Decimal>("TOTAL").ToString().Trim(),
                                         Fila.Field<Decimal>("ABONO").ToString().Trim(),
                                         Fila.Field<Decimal>("SALDO").ToString().Trim(),
                                         Fila.Field<String>("ESTATUS").Trim()                                         
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }

            [SessionExpire]
            public ActionResult Download_Reporte_Estado_Cuenta_Proveedores(Mdl_Facturas Datos)
            {
                DataTable Dt_Registros = new DataTable();
                string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Estado_Cuenta_Proveedores.xlsx";
                string nombre_archivo = "Rpt_Rpt_Estado_Cuenta_Proveedores.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                try
                {
                    ControladorFacturas = Datos;
                    Dt_Registros = ControladorFacturas.Consult();

                    Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                    Obj_Reporte_Pago.Elaborar_Reporte_Estado_Cuenta_Proveedores(ControladorFacturas);
                    this.Response.Clear();
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                    this.Response.WriteFile(ruta_almacenamiento);
                    this.Response.End();
                }
                catch
                {
                    return RedirectToAction("Reportes", "RptEstadoCuentaProveedores");
                }
                return null;
            }
        #endregion

        #region REPORTE SALDOS VENCIDOS PROVEEDORES
            [SessionExpire]
            public ActionResult RptSaldosVencidosProveedores()
            {
                return View();
            }

            [SessionExpire]
            public JsonResult GetReporteSaldosVencidosProveedores(Mdl_Facturas Datos)
            {
                DataTable Dt_Conciliacion = new DataTable();
                DataTable Dt_Registros = new DataTable();

                ControladorFacturas = Datos;
                Dt_Registros = ControladorFacturas.Consult();

                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] { 
                                         Fila.Field<String>("NO_FACTURA").ToString(),
                                         Fila.Field<String>("PROVEEDOR").ToString(),
                                         Fila.Field<DateTime>("FECHA").ToString("dd-MMM-yyyy"),                                     
                                         Fila.Field<DateTime>("FECHA_VENCIMIENTO").ToString("dd-MMM-yyyy"),                                                                              
                                         Fila.Field<Decimal>("SALDO").ToString().Trim(),
                                         Fila.Field<String>("ESTATUS").Trim()                                         
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }

            [SessionExpire]
            public ActionResult Download_Reporte_Saldos_Vencidos_Proveedores(Mdl_Facturas Datos)
            {
                DataTable Dt_Registros = new DataTable();
                string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Saldos_Vencidos_Proveedores.xlsx";
                string nombre_archivo = "Rpt_Saldos_Vencidos_Proveedores.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                try
                {
                    ControladorFacturas = Datos;
                    Dt_Registros = ControladorFacturas.Consult();

                    Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                    Obj_Reporte_Pago.Elaborar_Reporte_Saldos_Vencidos_Proveedores(ControladorFacturas);
                    this.Response.Clear();
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                    this.Response.WriteFile(ruta_almacenamiento);
                    this.Response.End();
                }
                catch
                {
                    return RedirectToAction("Reportes", "RptEstadoCuentaProveedores");
                }
                return null;
            }
        #endregion

        #region REPORTE NOTAS SALIDAS
            [SessionExpire]
            public ActionResult RptNotasSalidas()
            {
                return View();
            }

            [SessionExpire]
            public JsonResult GetReporteNotasSalidas(Mdl_Notas_Salida Datos)
            {
                Mdl_Notas_Salida_Detalles Obj_Detalles = new Mdl_Notas_Salida_Detalles();
                DataTable Dt_Registros = new DataTable();
                DataTable Dt_Registros_Detalles = new DataTable();
                DataTable Dt_Datos = new DataTable();
                DataRow Dr_Registro;
                Dt_Datos.Columns.Add("ID");
                Dt_Datos.Columns.Add("Fecha");
                Dt_Datos.Columns.Add("Agricultor");
                Dt_Datos.Columns.Add("Clave");
                Dt_Datos.Columns.Add("Producto");
                Dt_Datos.Columns.Add("Peso");
                Dt_Datos.Columns.Add("Cajas");
                Dt_Datos.Columns.Add("Kilogramos");
                Dt_Datos.Columns.Add("Precio");
                Dt_Datos.Columns.Add("Total");

                ControladorNotasSalidas = Datos;
                Dt_Registros = ControladorNotasSalidas.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Registros.Rows)
                    {
                        Obj_Detalles.No_Nota = Dr["No_Nota"].ToString();
                        Dt_Registros_Detalles = Obj_Detalles.Consult();
                        if (Dt_Registros_Detalles != null && Dt_Registros_Detalles.Rows.Count > 0)
                        {
                            for (int i = 0; i < Dt_Registros_Detalles.Rows.Count; i++)
                            {
                                Dr_Registro = Dt_Datos.NewRow();
                                Dr_Registro["ID"] = Dr["No_Nota"].ToString();
                                Dr_Registro["Fecha"] = DateTime.Parse(Dr["Fecha"].ToString()).ToString("dd/MM/yyyy"); //.ToString("dd-MMM-yyyy");
                                Dr_Registro["Agricultor"] = Dr["Agricultor"].ToString();
                                Dr_Registro["Clave"] = Dt_Registros_Detalles.Rows[i]["Clave"];
                                Dr_Registro["Producto"] = Dt_Registros_Detalles.Rows[i]["Producto"];
                                Dr_Registro["Peso"] = Dt_Registros_Detalles.Rows[i]["Peso"];
                                Dr_Registro["Cajas"] = Dt_Registros_Detalles.Rows[i]["Cajas"];
                                Dr_Registro["Kilogramos"] = Dt_Registros_Detalles.Rows[i]["Kilogramos"];
                                Dr_Registro["Precio"] = Dt_Registros_Detalles.Rows[i]["Precio"];
                                Dr_Registro["Total"] = Dt_Registros_Detalles.Rows[i]["Total"];
                                Dt_Datos.Rows.Add(Dr_Registro);
                                Dt_Datos.AcceptChanges();
                            }
                        }
                    }
                }

                var result = from Fila in Dt_Datos.AsEnumerable()
                             select new[] { 
                                         Fila.Field<String>("ID").ToString(),
                                         Fila.Field<String>("Fecha").ToString(),                                     
                                         Fila.Field<String>("Agricultor").ToString(),                                                                                                                                                 
                                         Fila.Field<String>("Clave").ToString(),
                                         Fila.Field<String>("Producto").ToString(),
                                         Fila.Field<String>("Peso").ToString(),
                                         Fila.Field<String>("Cajas").ToString(),
                                         Fila.Field<String>("Kilogramos").ToString(),
                                         Fila.Field<String>("Precio").ToString(),
                                         Fila.Field<String>("Total").ToString()
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }

            [SessionExpire]
            public ActionResult Download_Reporte_Notas_Salida(Mdl_Notas_Salida Datos)
            {
                DataTable Dt_Registros = new DataTable();
                string ruta_plantilla = System.AppDomain.CurrentDomain.BaseDirectory + "Plantillas\\Rpt_Notas_Salida.xlsx";
                string nombre_archivo = "Rpt_Notas_Salida.xlsx";
                string ruta_almacenamiento = obtenerRutaParaGuardar(nombre_archivo);

                try
                {
                    ControladorNotasSalidas = Datos;
                    Dt_Registros = ControladorNotasSalidas.Consult();

                    Rpt_Excel Obj_Reporte_Pago = new Rpt_Excel(ruta_plantilla, ruta_almacenamiento, Dt_Registros);
                    Obj_Reporte_Pago.Elaborar_Reporte_Notas_Salida(ControladorNotasSalidas);
                    this.Response.Clear();
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=" + nombre_archivo);
                    this.Response.WriteFile(ruta_almacenamiento);
                    this.Response.End();
                }
                catch
                {
                    return RedirectToAction("Reportes", "RptDestajo");
                }
                return null;
            }
        #endregion

        public string obtenerRutaParaGuardar(String sFileName)
        {
            string respuesta = "";

            if (!Path.IsPathRooted(sFileName))
                sFileName = Path.Combine(Path.GetTempPath(), sFileName);

            if (System.IO.File.Exists(sFileName))
            {
                String sDateTime = DateTime.Now.ToString("yyyyMMdd\\_HHmmss");
                String s = Path.GetFileNameWithoutExtension(sFileName) + "_" + sDateTime + Path.GetExtension(sFileName);
                sFileName = Path.Combine(Path.GetDirectoryName(sFileName), s);
            }
            else
                Directory.CreateDirectory(Path.GetDirectoryName(sFileName));

            respuesta = sFileName;
            return respuesta;
        }
    }
}
