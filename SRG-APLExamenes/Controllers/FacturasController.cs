﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;


namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class FacturasController : Controller
    {
        private Mdl_Facturas ControladorFacturas;
        private Mdl_Movimientos_Pagos ControladorMovimientos;   
        public FacturasController()
        {
            ControladorFacturas = new Mdl_Facturas();
            ControladorMovimientos = new Mdl_Movimientos_Pagos();       
        }

        #region FACTURAS PROVEEDORES
            [SessionExpire]
            public ActionResult Facturas()
            {
                return View();
            }

            ///******************************************************************************* 
            ///NOMBRE DE LA FUNCIÓN: GetEvents
            ///DESCRIPCIÓN:
            ///PARAMETROS:  
            ///CREO:       DAVID HERRERA RINCON
            ///FECHA_CREO:  05/04/2018
            ///MODIFICO: 
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            [SessionExpire]
            public JsonResult GetEvents()
            {
                DataTable Dt_Registros = new DataTable();
                Dt_Registros = ControladorFacturas.Consult();

                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] { 
                                         Fila.Field<String>("NO_FACTURA").ToString(),     
                                         Fila.Field<int>("NO_ENTRADA") == 0 ? "" : Fila.Field<int>("NO_ENTRADA").ToString().Trim(),
                                         Fila.Field<String>("PROVEEDOR").Trim(),     
                                         Fila.Field<DateTime>("FECHA_RECEPCION").ToString("dd-MMM-yyyy"),
                                         Fila.Field<DateTime>("FECHA_VENCIMIENTO").ToString("dd-MMM-yyyy"),
                                         Fila.Field<Decimal>("TOTAL").ToString().Trim(),     
                                         Fila.Field<String>("ESTATUS").Trim(),                                                  
                                         Fila.Field<int>("FACTURA_ID").ToString(),
                                         Fila.Field<int>("FACTURA_ID").ToString() 
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }
        #endregion

        #region PAGO PROVEEDORES
            [SessionExpire]
            public ActionResult PagoProveedores()
            {
                return View();
            }

            ///******************************************************************************* 
            ///NOMBRE DE LA FUNCIÓN: GetEvents
            ///DESCRIPCIÓN:
            ///PARAMETROS:  
            ///CREO:       DAVID HERRERA RINCON
            ///FECHA_CREO:  05/04/2018
            ///MODIFICO: 
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            [SessionExpire]
            public JsonResult GetEventsFacturasProveedor(Mdl_Facturas Datos)
            {
                DataTable Dt_Registros = new DataTable();
                ControladorFacturas = Datos;
                Dt_Registros = ControladorFacturas.Consult();

                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] { 
                                         "NO",   
                                         Fila.Field<String>("NO_FACTURA").ToString(),                                              
                                         Fila.Field<int>("NO_ENTRADA") == 0 ? "" : Fila.Field<int>("NO_ENTRADA").ToString().Trim(),
                                         Fila.Field<DateTime>("FECHA_RECEPCION").ToString("dd-MMM-yyyy"),
                                         Fila.Field<DateTime>("FECHA_VENCIMIENTO").ToString("dd-MMM-yyyy"),
                                         Fila.Field<Decimal>("TOTAL").ToString().Trim(), 
                                         Fila.Field<Decimal>("ABONO").ToString().Trim(), 
                                         Fila.Field<Decimal>("SALDO").ToString().Trim(),                                                                                       
                                         Fila.Field<int>("FACTURA_ID").ToString() 
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }
        #endregion

        #region CANCELACION PAGO PROVEEDORES
            [SessionExpire]
            public ActionResult CancelacionPagoProveedores()
            {
                return View();
            }

            ///******************************************************************************* 
            ///NOMBRE DE LA FUNCIÓN: GetEvents
            ///DESCRIPCIÓN:
            ///PARAMETROS:  
            ///CREO:       DAVID HERRERA RINCON
            ///FECHA_CREO:  05/04/2018
            ///MODIFICO: 
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            [SessionExpire]
            public JsonResult GetEventsMovimientosFacturas(Mdl_Movimientos_Pagos Datos)
            {
                DataTable Dt_Registros = new DataTable();                
                ControladorMovimientos = Datos;
                Dt_Registros = ControladorMovimientos.Consult();

                var result = from Fila in Dt_Registros.AsEnumerable()
                             select new[] { 
                                         Fila.Field<DateTime>("FECHA").ToString("dd-MMM-yyyy"),
                                         Fila.Field<String>("NO_FACTURA_PROVEEDOR").ToString(),
                                         Fila.Field<String>("PROVEEDOR").ToString(),
                                         Fila.Field<String>("CONCEPTO").ToString(),                                              
                                         Fila.Field<Decimal>("CANTIDAD").ToString().Trim(),                                                                                       
                                         Fila.Field<int>("NO_MOVIMIENTO").ToString() 
                                     };
                return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
            }
        #endregion

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvent
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorFacturas = new Mdl_Facturas();
                ControladorFacturas.FACTURA_ID = Evento_ID;
                Dt_Registros = ControladorFacturas.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Factura [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        [SessionExpire]
        [HttpPost]
        public JsonResult GetEventMovimiento(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorMovimientos = new Mdl_Movimientos_Pagos();
                ControladorMovimientos.NO_MOVIMIENTO = Evento_ID;
                Dt_Registros = ControladorMovimientos.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Movimiento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public String EventMaster(Mdl_Facturas Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Tipo_Destajo = new DataTable();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            try
            {
                Datos.Usuario_Registro = SessionSRG.Nombre;
                Datos.FECHA = "GETDATE()";
                Datos.Fecha_Registro = "GETDATE()";
                ControladorFacturas = Datos;
                                
                if (Datos.Captura.Equals("I"))
                {
                    ControladorFacturas.FACTURA_ID = ControladorFacturas.ConsultMaximoFactura().Rows[0]["FACTURA_ID"].ToString();
                    if (ControladorFacturas.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorFacturas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }

        [SessionExpire]
        [HttpPost]
        public String EventMasterMovimientosPagos(String Fecha, String Forma_Pago, String Referencia, String Concepto, String Monto, String Datos)
        {
            List<Mdl_Facturas> Obj_Facturas = new List<Mdl_Facturas>();
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Facturas = new DataTable();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            String Estatus = "";
            Double Cantidad = 0;
            Double Saldo = 0;
            Double Abono = 0;
            Double Importe = 0;

            try
            {
                Obj_Facturas = JsonConvert.DeserializeObject<List<Mdl_Facturas>>(Datos);

                if (Obj_Facturas.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Monto))
                    {
                        Importe = Double.Parse(Monto);
                        if (Importe > 0)
                        {
                            for (int i = 0; i < Obj_Facturas.Count; i++)
                            {
                                ControladorFacturas.FACTURA_ID = null;
                                ControladorFacturas.ESTATUS = null;
                                ControladorFacturas.ABONO = null;
                                ControladorFacturas.SALDO = null;
                                ControladorFacturas.FECHA_PAGO = null;
                                ControladorFacturas.FORMA_PAGO = null;
                                ControladorFacturas.PAGADA = null;
                                ControladorMovimientos.NO_MOVIMIENTO = null;
                                ControladorMovimientos.PROVEEDOR_ID = null;
                                ControladorMovimientos.TIPO = null;
                                ControladorMovimientos.ESTATUS = null;
                                ControladorMovimientos.FECHA = null;
                                ControladorMovimientos.REFERENCIA = null;
                                ControladorMovimientos.NO_FACTURA_PROVEEDOR = null;
                                ControladorMovimientos.FORMA_PAGO = null;
                                ControladorMovimientos.CANTIDAD = null;
                                ControladorMovimientos.MONEDA = null;
                                ControladorMovimientos.TIPO_CAMBIO = null;
                                ControladorMovimientos.CONCEPTO = null;
                                ControladorMovimientos.SALDO = null;
                                ControladorMovimientos.BENEFICIARIO = null;
                                ControladorMovimientos.Fecha_Registro = null;
                                ControladorMovimientos.Usuario_Registro = null;


                                ControladorFacturas.FACTURA_ID = Obj_Facturas[i].FACTURA_ID;
                                Dt_Facturas = ControladorFacturas.Consult();
                                if (Importe > 0)
                                {
                                    Abono = Double.Parse(Dt_Facturas.Rows[0]["ABONO"].ToString());
                                    Saldo = Double.Parse(Dt_Facturas.Rows[0]["SALDO"].ToString());
                                    if (Saldo < Importe)
                                    {
                                        Estatus = "PAGADA";
                                        Cantidad = Saldo;
                                        Abono = Abono + Cantidad;
                                        Saldo = Saldo - Cantidad;
                                    }
                                    else
                                    {
                                        Estatus = "ABONADA";
                                        Cantidad = Importe;
                                        Abono = Abono + Cantidad;
                                        Saldo = Saldo - Cantidad;
                                    }

                                    ControladorFacturas.ESTATUS = Estatus;
                                    ControladorFacturas.ABONO = Abono.ToString();
                                    ControladorFacturas.SALDO = Saldo.ToString();
                                    if (Estatus == "PAGADA")
                                    {
                                        ControladorFacturas.FECHA_PAGO = Fecha;
                                        ControladorFacturas.FORMA_PAGO = Forma_Pago;
                                        ControladorFacturas.PAGADA = "SI";
                                    }

                                    ControladorMovimientos.NO_MOVIMIENTO = ControladorMovimientos.ConsultMaximoMovimiento().Rows[0]["NO_MOVIMIENTO"].ToString();
                                    ControladorMovimientos.PROVEEDOR_ID = Dt_Facturas.Rows[0]["PROVEEDOR_ID"].ToString();
                                    ControladorMovimientos.TIPO = "EGRESO";
                                    ControladorMovimientos.ESTATUS = "ACTIVO";
                                    ControladorMovimientos.FECHA = Fecha;
                                    if (!String.IsNullOrEmpty(Referencia))
                                        ControladorMovimientos.REFERENCIA = Referencia;
                                    ControladorMovimientos.NO_FACTURA_PROVEEDOR = Dt_Facturas.Rows[0]["NO_FACTURA"].ToString();
                                    ControladorMovimientos.FORMA_PAGO = Forma_Pago;
                                    ControladorMovimientos.CANTIDAD = Cantidad.ToString();
                                    ControladorMovimientos.MONEDA = "PESOS";
                                    ControladorMovimientos.TIPO_CAMBIO = "1";
                                    ControladorMovimientos.CONCEPTO = Concepto;
                                    ControladorMovimientos.SALDO = "0";
                                    ControladorMovimientos.BENEFICIARIO = Dt_Facturas.Rows[0]["PROVEEDOR"].ToString();
                                    ControladorMovimientos.Fecha_Registro = "GETDATE()";
                                    ControladorMovimientos.Usuario_Registro = SessionSRG.Nombre;

                                    if (ControladorMovimientos.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA))
                                    {
                                        if (ControladorFacturas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                                        {
                                            Importe = Importe - Cantidad;
                                            ObjMensajeServidor.Estatus = true;
                                            ObjMensajeServidor.Mensaje = "Registro exitoso.";
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            ObjMensajeServidor.Estatus = true;
                            ObjMensajeServidor.Mensaje = "El importe es menor de cero.";
                        }
                    }
                    else
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Sin monto a pagar.";
                    }
                }
                else
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Mensaje = "Sin datos que ingresar.";
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }

        [SessionExpire]
        [HttpPost]
        public String EventMasterCancelarPagoProveedor(Mdl_Movimientos_Pagos Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            DataTable Dt_Movimiento = new DataTable();
            DataTable Dt_Factura = new DataTable();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            String Factura_ID = "";
            String Estatus = "";
            Double Cantidad = 0;            
            Double Abono = 0;
            Double Saldo = 0;
            try
            {
                ControladorMovimientos.NO_MOVIMIENTO = Datos.NO_MOVIMIENTO;
                Dt_Movimiento = ControladorMovimientos.Consult();
                if (Dt_Movimiento != null && Dt_Movimiento.Rows.Count > 0)
                {
                    ControladorFacturas.NO_FACTURA = Dt_Movimiento.Rows[0]["NO_FACTURA_PROVEEDOR"].ToString();
                    ControladorFacturas.PROVEEDOR_ID = Dt_Movimiento.Rows[0]["PROVEEDOR_ID"].ToString();
                    Cantidad = Double.Parse(Dt_Movimiento.Rows[0]["CANTIDAD"].ToString());
                    Dt_Factura = ControladorFacturas.Consult();
                    if (Dt_Factura != null && Dt_Factura.Rows.Count > 0)
                    {
                        Factura_ID = Dt_Factura.Rows[0]["FACTURA_ID"].ToString();                        
                        Abono = Double.Parse(Dt_Factura.Rows[0]["ABONO"].ToString());
                        Saldo = Double.Parse(Dt_Factura.Rows[0]["SALDO"].ToString());

                        Abono = Abono - Cantidad;
                        Saldo = Saldo + Cantidad;
                        if (Abono == 0)
                            Estatus = "POR PAGAR";
                        else
                            Estatus = "ABONADA";
                    }
                }

                Datos.Usuario_Registro = SessionSRG.Nombre;
                Datos.FECHA_CANCELACION = "GETDATE()";
                Datos.Fecha_Registro = "GETDATE()";
                ControladorMovimientos.NO_MOVIMIENTO = null;
                ControladorMovimientos = Datos;
                if (ControladorMovimientos.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                {
                    ControladorFacturas.NO_FACTURA = null;
                    ControladorFacturas.PROVEEDOR_ID = null;
                    ControladorFacturas.FACTURA_ID = Factura_ID;
                    ControladorFacturas.ABONO = Abono.ToString();
                    ControladorFacturas.SALDO = Saldo.ToString();
                    ControladorFacturas.PAGADA = "NO";
                    ControladorFacturas.ESTATUS = Estatus;
                    if (ControladorFacturas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
