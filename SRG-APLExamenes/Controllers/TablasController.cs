﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Script.Serialization;
using SRG_APLExamenes.Models.Assistant;
using SRG_APLExamenes.Models.Data;
using SRG_APLExamenes.Models.Deal;
using SRG_APLExamenes.Core;


namespace SRG_APLExamenes.Controllers
{
    [SessionExpire]
    [Authorize]
    public class TablasController : Controller
    {
        private Mdl_Tablas ControladorTablas;
        public TablasController()
        {
            ControladorTablas = new Mdl_Tablas();
        }

        [SessionExpire]
        public ActionResult Tablas()
        {
            return View();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvents
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        public JsonResult GetEvents()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = ControladorTablas.Consult();

            var result = from Fila in Dt_Registros.AsEnumerable()
                         select new[] { 
                                     Fila.Field<String>("Clave").Trim(),
                                     Fila.Field<String>("Nombre").Trim(),
                                     Fila.Field<String>("Rancho").ToString(),
                                     Fila.Field<String>("Medida_HAS").ToString(),
                                     Fila.Field<int>("No_Tuneles").ToString(),                                     
                                     Fila.Field<String>("Estatus").Trim(),                                     
                                     Fila.Field<String>("Tabla_ID").ToString()                                     
                                 };
            return Json(new { aaData = result }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: GetEvent
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public JsonResult GetEvent(String Evento_ID)
        {
            Respuesta ObjMensajeServidor = new Respuesta();
            DataTable Dt_Registros = new DataTable();
            try
            {
                ControladorTablas = new Mdl_Tablas();
                ControladorTablas.Tabla_ID = Evento_ID;
                Dt_Registros = ControladorTablas.Consult();
                if (Dt_Registros != null && Dt_Registros.Rows.Count > 0)
                {
                    ObjMensajeServidor.Estatus = true;
                    ObjMensajeServidor.Data = JsonConvert.SerializeObject(Dt_Registros, Formatting.None);
                }
            }
            catch (Exception Ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "Consultar Evento [" + Ex.Message + "]";
            }
            return Json(new { items = ObjMensajeServidor }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public ActionResult GetTablaList(String q)
        {
            DataTable Dt_Registros = new DataTable();
            List<Select2Model> list = new List<Select2Model>();
            try
            {
                ControladorTablas.Estatus = "ACTIVO";
                Dt_Registros = ControladorTablas.Consult();
                list = Dt_Registros.AsEnumerable()
                    .Select(row => new Select2Model
                    {
                        id = row.Field<String>("Tabla_ID").ToString().Trim(),
                        text = row.Field<String>("Nombre").ToString().Trim(),
                    }).ToList();
                if (!(String.IsNullOrEmpty(q) || String.IsNullOrWhiteSpace(q)))
                    list = list.Where(x => x.text.ToLower().StartsWith(q.ToLower())).ToList();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.StackTrace);
            }
            return Json(new { items = list }, JsonRequestBehavior.AllowGet);
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: OperationInsert
        ///DESCRIPCIÓN:
        ///PARAMETROS:  
        ///CREO:       DAVID HERRERA RINCON
        ///FECHA_CREO:  05/04/2018
        ///MODIFICO: 
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        [SessionExpire]
        [HttpPost]
        public String EventMaster(Mdl_Tablas Datos)
        {
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Respuesta ObjMensajeServidor = new Respuesta();
            String StrDatos = String.Empty;
            try
            {
                Datos.Usuario_Registro = SessionSRG.Nombre;
                Datos.Fecha_Registro = "GETDATE()";
                ControladorTablas = Datos;
                if (Datos.Captura.Equals("I"))
                {
                    ControladorTablas.Tabla_ID = int.Parse(ControladorTablas.ConsultMaximoTablas().Rows[0]["Tabla_ID"].ToString()).ToString("00000");
                    if (ControladorTablas.MasterManagement(MODO_DE_CAPTURA.CAPTURA_ALTA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro exitoso.";
                    }
                }
                else
                {
                    if (ControladorTablas.MasterManagement(Core.MODO_DE_CAPTURA.CAPTURA_ACTUALIZA))
                    {
                        ObjMensajeServidor.Estatus = true;
                        ObjMensajeServidor.Mensaje = "Registro actualizado correctamente.";
                    }
                }
            }
            catch (Exception ex)
            {
                ObjMensajeServidor.Estatus = false;
                ObjMensajeServidor.Mensaje = "[" + ex.Message + "]";
            }
            finally
            {
                StrDatos = Serializer.Serialize(ObjMensajeServidor);
            }
            return StrDatos;
        }
    }
}
