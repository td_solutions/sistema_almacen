﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SRG_APLExamenes
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               "Administracion",
               "Administracion/{action}/{id}",
               defaults: new { controller = "Administracion", action = "Inicio", id = UrlParameter.Optional }
           );
            
            routes.MapRoute(
                "Empleados",
                "Empleados/{action}/{id}",
                defaults: new { controller = "Empleados", action = "Empleados", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Parametros",
                "Parametros/{action}/{id}",
                defaults: new { controller = "Parametros", action = "Parametros", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "TipoDestajo",
                "TipoDestajo/{action}/{id}",
                defaults: new { controller = "TipoDestajo", action = "TipoDestajo", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Unidad",
                "Unidad/{action}/{id}",
                defaults: new { controller = "Unidad", action = "Unidad", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Labores",
                "Labores/{action}/{id}",
                defaults: new { controller = "Labores", action = "Labores", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Ranchos",
                "Ranchos/{action}/{id}",
                defaults: new { controller = "Ranchos", action = "Ranchos", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    "Reporte",
            //    "RptDestajo/{action}/{id}",
            //    defaults: new { controller = "Reportes", action = "RptDestajo", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                "Inventarios",
                "RptInventario/{action}/{id}",
                defaults: new { controller = "RptInventario", action = "RptInventario", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Proveedores",
                "Proveedores/{action}/{id}",
                defaults: new { controller = "Proveedores", action = "Proveedores", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Entradas",
                "Entradas/{action}/{id}",
                defaults: new { controller = "Entradas", action = "Entradas", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Salidas",
                "Salidas/{action}/{id}",
                defaults: new { controller = "Salidas", action = "Salidas", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "TipoInsumo",
                "TipoInsumo/{action}/{id}",
                defaults: new { controller = "TipoInsumo", action = "TipoInsumo", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Unidades",
                "Unidades/{action}/{id}",
                defaults: new { controller = "Unidades", action = "Unidades", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Insumos",
                "Insumos/{action}/{id}",
                defaults: new { controller = "Insumos", action = "Insumos", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Almacen",
                "Almacen/{action}/{id}",
                defaults: new { controller = "Almacen", action = "Almacen", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Tablas",
                "Tablas/{action}/{id}",
                defaults: new { controller = "Tablas", action = "Tablas", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Actividades",
                "Actividades/{action}/{id}",
                defaults: new { controller = "Actividades", action = "Actividades", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Destajo",
                "Destajo/{action}/{id}",
                defaults: new { controller = "Destajo", action = "Destajo", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Fumigacion",
                "Fumigacion/{action}/{id}",
                defaults: new { controller = "Fumigacion", action = "Fumigacion", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Riego",
                "Riego/{action}/{id}",
                defaults: new { controller = "Riego", action = "Riego", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Automoviles",
                "Automoviles/{action}/{id}",
                defaults: new { controller = "Automoviles", action = "Automoviles", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "Facturas",
                "Facturas/{action}/{id}",
                defaults: new { controller = "Facturas", action = "Facturas", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "NotasSalida",
                "NotasSalida/{action}/{id}",
                defaults: new { controller = "NotasSalida", action = "NotasSalida", id = UrlParameter.Optional }
            );

           

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Administracion", action = "VerificarUsuario", id = UrlParameter.Optional },
                namespaces: new[] { "Administracion.Controllers" }
            );
        }
    }
}